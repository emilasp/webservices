<?php

namespace FileBundle\Twig;

use FileBundle\Entity\File;
use FileBundle\Service\FileService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class FileExtension
 *
 * @package FileBundle\Twig
 */
class FileExtension extends AbstractExtension
{
    private $fileService;

    /**
     * FileExtension constructor.
     *
     * @param FileService $fileService
     */
    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('get_image_url', [$this, 'getImageUrl']),
        ];
    }

    /* public function getFunctions(): array
     {
         return [
             new TwigFunction('function_name', [$this, 'doSomething']),
         ];
     }*/

    /**
     * Получаем Url картинки
     *
     * @param File $file
     * @param string $format
     * @param string $defaultImage
     *
     * @return string
     */
    public function getImageUrl(
        File $file = null,
        string $format = FileService::IMAGE_FORMAT_MEDIUM,
        string $defaultImage = '/images/noAvatar.png'
    ): string {
        if (!$file) {
            return $defaultImage;
        }
        return $this->fileService->getImageUrl($file, $format);
    }
}
