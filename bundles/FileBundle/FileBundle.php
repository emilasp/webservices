<?php
namespace FileBundle;

use FileBundle\DependencyInjection\FileExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class FileBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new FileExtension();
    }
}