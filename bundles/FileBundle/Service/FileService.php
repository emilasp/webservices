<?php


namespace FileBundle\Service;


use App\Core\Interfaces\IEntityCore;
use Cocur\Slugify\Slugify;
use Cocur\Slugify\SlugifyInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FileBundle\Entity\File;
use Impulze\Bundle\InterventionImageBundle\ImageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Сервис отвечает за взаимодействие между Entity проекта и File entity бандла
 *
 * Class FileRelationService
 *
 * @package FileBundle\Service
 */
class FileService
{
    public const IMAGE_FORMAT_XS      = 'xs';
    public const IMAGE_FORMAT_ICON    = 'icon';
    public const IMAGE_FORMAT_SQUARE  = 'square';
    public const IMAGE_FORMAT_SMALL   = 'small';
    public const IMAGE_FORMAT_MEDIUM  = 'medium';
    public const IMAGE_FORMAT_BIG     = 'big';

    public const IMAGE_FORMAT_PREVIEW = 'preview';

    /** @var ContainerInterface */
    protected $container;
    /** @var EntityManagerInterface */
    protected $em;
    /** @var Filesystem */
    protected $fileSystem;
    /** @var ImageManager */
    protected $imageManager;
    /** @var Slugify */
    protected $slugify;

    /**
     * FileService constructor.
     *
     * @param ContainerInterface     $container
     * @param EntityManagerInterface $em
     * @param Filesystem             $fileSystem
     * @param ImageManager           $imageManager
     * @param SlugifyInterface       $slugify
     */
    public function __construct(
        ContainerInterface $container,
        EntityManagerInterface $em,
        Filesystem $fileSystem,
        ImageManager $imageManager,
        SlugifyInterface $slugify
    ) {
        $this->container    = $container;
        $this->fileSystem   = $fileSystem;
        $this->em           = $em;
        $this->imageManager = $imageManager;
        $this->slugify      = $slugify;
    }

    /**
     * Сохраняем загруженное изобраджение как обычный файл
     *
     * @param UploadedFile $uploadedFile
     * @param array        $context
     *
     * @return array
     */
    public function saveSimpleImageFromUploadedFile(UploadedFile $uploadedFile, array $context = ['uploaded']): array
    {
        $fileName    = $uploadedFile->getClientOriginalName();
        $fileNewName = $this->slugify->slugify($fileName) . '.' . $uploadedFile->getClientOriginalExtension();

        $publicDir = $this->container->getParameter('file.web_folder');
        $basePath  = $this->container->get('kernel')->getProjectDir();
        $webPath   = implode(DIRECTORY_SEPARATOR, $context) . DIRECTORY_SEPARATOR;

        $path = $basePath . DIRECTORY_SEPARATOR . $publicDir . DIRECTORY_SEPARATOR . $webPath;

        if (!is_dir($path)) {
            $this->fileSystem->mkdir($path);
        }

        $fileTemp = $uploadedFile->getPathname();
        $fileDist = $path . $fileNewName;

        $this->fileSystem->copy($fileTemp, $fileDist);

        return ['url' => '/' . $webPath . $fileNewName, 'path' => $fileDist, 'fileName' => $fileNewName];
    }


    /**
     * Создаем сущность файл
     * копируем оригинальный файл в соотвествующую папку
     * привязываем сущность файл к entity(опционально)
     *
     * @param UploadedFile $uploadedFile
     * @param IEntityCore  $entity
     * @param string       $context
     *
     * @return File
     */
    public function createFile(UploadedFile $uploadedFile, IEntityCore $entity, string $context = 'default'): File
    {
        $fileName = $uploadedFile->getClientOriginalName();

        $file          = $this->getFile($entity, $fileName, $context);
        $fileDirectory = $this->getFileDirectoryPath($file, true, true);

        $fileTemp = $uploadedFile->getPathname();

        $fileDist = $fileDirectory . $fileName;
        $this->fileSystem->copy($fileTemp, $fileDist);

        return $file;
    }


    /**
     * Получаем путь до файла
     *
     * @param File $fileEntity
     * @param bool $absolute
     * @param bool $createDir
     * @param bool $cache
     * @param bool $withPublic
     *
     * @return string
     */
    public function getFileDirectoryPath(
        File $fileEntity,
        bool $absolute = true,
        bool $createDir = false,
        bool $cache = false,
        bool $withPublic = true
    ): string {
        $publicDir = $this->container->getParameter('file.web_folder');
        $basePath  = $absolute ? $this->container->get('kernel')->getProjectDir() : '';
        $webPath   = $basePath . ($withPublic ? DIRECTORY_SEPARATOR . $publicDir : '');

        $class = (substr($fileEntity->getEntityClass(), strrpos($fileEntity->getEntityClass(), '\\') + 1));

        if ($cache) {
            $prefixDir = $this->container->getParameter('file.cache_folder');
        } else {
            $prefixDir = $this->container->getParameter('file.original_folder');
        }

        $path = $webPath
            . DIRECTORY_SEPARATOR . $prefixDir
            . DIRECTORY_SEPARATOR . $class
            . DIRECTORY_SEPARATOR . $fileEntity->getEntityId()
            . DIRECTORY_SEPARATOR . $fileEntity->getId()
            . DIRECTORY_SEPARATOR;

        if ($createDir && !is_dir($path)) {
            $this->fileSystem->mkdir($path);
        }
        return $path;
    }

    /**
     * Получаем путь до изображения нужного формата
     *
     * @param File   $fileEntity
     * @param string $format
     *
     * @return string
     */
    public function getPathToFormatImage(File $fileEntity, string $format): string
    {
        $directoryCache = $this->getFileDirectoryPath($fileEntity, true, true, true);
        $formatFileName = $this->getFileNameFormat($fileEntity->getName(), $format);
        $formatFile     = $directoryCache . $formatFileName;

        if (!is_file($formatFile)) {
            $directory    = $this->getFileDirectoryPath($fileEntity, true);
            $originalFile = $directory . $fileEntity->getName();

            if (is_file($originalFile)) {
                $this->processImage($originalFile, $formatFile, $format);
            }
        }

        $relativeFormatDir = $this->getFileDirectoryPath($fileEntity, false, false, true, false);

        return $relativeFormatDir . $formatFileName;
    }

    /**
     * Отдаем из кеша ии генерируем новое изображение
     *
     * @param string $original
     * @param string $destination
     * @param string $format
     */
    private function processImage(string $original, string $destination, string $format): void
    {
        $params = $this->container->getParameter('file.formats')[$format];

        $image = $this->imageManager->make($original);

        if ($params['resize_type'] === 'auto') {
            /*if ($image->getWidth() ) {

            }*/
        }

        switch ($params['resize_type']) {
            case 'fit':
                $image->fit($params['width'], $params['height'], function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                break;
            case 'widen':
                $image->widen($params['width']);
                break;
            case 'heighten':
                $image->heighten($params['height']);
                break;
            case 'auto':
                $image->fit($params['width'], $params['height']);
                break;
            case 'crop':
                $image->crop($params['width'], $params['height']);
                break;
        }

        $image->save($destination, $params['quality'] ?? 90);
    }


    /**
     * Получаем Url на файл
     *
     * @param string $format
     * @param bool   $withScheme
     *
     * @return string
     */
    public function getImageUrl(
        File $file,
        string $format = FileService::IMAGE_FORMAT_MEDIUM,
        bool $withScheme = false
    ): string {
        $url = $this->getPathToFormatImage($file, $format);

        if ($withScheme) {
            $httpDomain = $this->getHttpDomain();
            $url        = $httpDomain . DIRECTORY_SEPARATOR . $url;
        }

        return $url;
    }

    /**
     * Получаем Url на файл в разных форматах
     *
     * @param array $formats
     * @param bool  $withScheme
     *
     * @return array
     */
    public function getImageFormatsUrls(File $file, array $formats, bool $withScheme = false): array
    {
        $urls = [];
        foreach ($formats as $format) {
            $urls[$format] = $this->getImageUrl($file, $format, $withScheme);
        }
        return $urls;
    }


    /**
     * Получаем Url на файл в разных форматах
     *
     * @param Collection  $collection
     * @param string      $relationImageName
     * @param array       $formats
     * @param string|null $relationName
     * @param bool        $withScheme
     *
     * @return array
     */
    /*public function getImageFormatsUrlsFromCollection(
        Collection $collection,
        string $relationImageName,
        array $formats,
        string $relationName = null,
        bool $withScheme = false
    ): array {
        $images = [];
        foreach ($collection as $link) {
            $relationImageName = 'get' . ucfirst($relationImageName);
            if ($relationName) {
                $relationName = 'get' . ucfirst($relationName);
                $file = $link->{$relationName}()->{$relationImageName}();
            } else {
                $file = $link->{$relationImageName}();
            }

            if ($file) {
                $images[$file->getId()] = $this->getImageFormatsUrls($file, $formats, $withScheme);
            }
        }
        return $images;
    }*/


    /**
     * Поулчаем имя файла с учетом формата
     *
     * @param string $fileNameBase
     * @param string $format
     *
     * @return string
     */
    public function getFileNameFormat(string $fileNameBase, string $format): string
    {
        $formatFileName = $this->slugify->slugify($fileNameBase) . '.jpg';

        return $format . '_' . $formatFileName;
    }


    /**
     * Получаем домен со схемой
     *
     * @return string
     */
    public function getHttpDomain(): string
    {
        return $this->container->getParameter('http_domain');
    }

    /**
     * Получаем FileEntity
     *
     * @param IEntityCore   $entity
     * @param string        $fileName
     * @param , string $context
     * @param EntityManager $em
     *
     * @return File
     */
    private function getFile(IEntityCore $entity, string $fileName, string $context = 'default'): File
    {
        $class = get_class($entity);

        if (!$fileEntity = $this->em->getRepository(File::class)->findOneBy([
            'entityClass' => $class,
            'entityId'    => $entity->getId(),
            'name'        => $fileName
        ])) {
            $fileEntity = new File();
            $fileEntity->setEntityClass($class);
            $fileEntity->setContext($context);
            $fileEntity->setEntityId($entity->getId());

            $this->em->persist($fileEntity);
        }

        $fileEntity->setName($fileName);

        $this->em->flush($fileEntity);

        return $fileEntity;
    }
}