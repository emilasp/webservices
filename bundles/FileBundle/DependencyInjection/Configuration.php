<?php

namespace FileBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('FileConfig');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->scalarNode('web_folder')->end()
                ->scalarNode('cache_folder')->end()
                ->scalarNode('original_folder')->end()
                ->arrayNode('formats')
                    ->arrayPrototype()
                        ->children()
                        ->integerNode('quality')->defaultValue(90)->end()
                        ->integerNode('width')->defaultFalse()->end()
                        ->integerNode('height')->defaultFalse()->end()
                        ->enumNode('resize_type')->values(['fit', 'widen', 'heighten', 'auto', 'crop'])->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}