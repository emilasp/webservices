<?php

namespace FileBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class FileExtension
 *
 * @package FileBundle\DependencyInjection
 */
class FileExtension extends Extension
{
    /**
     * @param array            $config
     * @param ContainerBuilder $container
     */
    public function load(array $config, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $this->processConfiguration($configuration, $config);

        $this->updateContainerParameters($container, $config);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');
    }

    /**
     * Update parameters using configuratoin values.
     *
     * @param ContainerBuilder $container
     * @param array $config
     */
    protected function updateContainerParameters(ContainerBuilder $container, array $config)
    {
        $container->setParameter('file.web_folder', $config[0]['web_folder']);
        $container->setParameter('file.cache_folder', $config[0]['cache_folder']);
        $container->setParameter('file.original_folder', $config[0]['original_folder']);
        $container->setParameter('file.formats', $config[0]['formats']);
    }
}