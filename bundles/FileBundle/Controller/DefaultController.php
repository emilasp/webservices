<?php

namespace FileBundle\Controller;

use App\Core\Abstracts\AController;
use CoursesBundle\Entity\User;
use App\Queue\Message\EmailSenderMessage;
use Doctrine\ORM\EntityManagerInterface;
use FileBundle\Entity\File;
use FileBundle\Service\FileService;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class DefaultController
 *
 * @package App\Controller
 */
class DefaultController extends AController
{
    /**
     * @Route("/file", name="file" , options={"expose"=true})
     *
     *
     * @Template(template="/Default/test.html.twig")
     * @return Response
     */
    public function testAction(EntityManagerInterface $entityManager, MessageBusInterface $bus)
    {
        //phpinfo();


        $admin = $this->isGranted(User::ROLE_ADMIN);
        $user = $this->isGranted(User::ROLE_USER);




        //$bus->dispatch(new EmailSenderMessage(rand(1, 10000)));





        $this->addFlash('danger', 'Your changes were saved!');
        $this->addFlash('success', 'Your changes were saved! GOOD');

        $this->addBreadcrumbs(['Home' => 'index', "Some text without link"]);

        // Example with parameter injected into translation "user.profile"


        //$fileEntity = $entityManager->getRepository(File::class)->find(1);
        $fileEntity = new File();
        $fileEntity->setName('Testwssdassseewrasd');
        $fileEntity->setContext('test');
        $fileEntity->setEntityClass('Test');
        $fileEntity->setEntityId(1);

        $entityManager->persist($fileEntity);
        $entityManager->flush($fileEntity);

        //


        dump([342]);

        return [
            'vars' => '342'
        ];
    }

    /**
     * @Route("/upload-test", name="upload")
     *
     * @param Request $request
     *
     * @Template(template="/Default/upload.html.twig")
     * @return Response
     */
    public function uploadFileAction(Request $request, FileService $fileService, EntityManagerInterface $entityManager)
    {
        $form = $this->createFormBuilder()
            ->add('brochure', FileType::class, ['label' => 'Brochure (PDF file)'])
            ->add('name',  CKEditorType::Class, ['label' => 'Brochure (PDF file)'])
            ->getForm();

        $form->handleRequest($request);

        $file = $entityManager->getRepository(File::class)->find(39);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($files = $request->files->get('form')) {
                $fileEntity = $entityManager->getRepository(File::class)->find(1);

                $file = $fileService->attachFile($files['brochure'], $fileEntity);
            }
        }

        return $this->render('/templates/Default/upload.html.twig', [
            'form' => $form->createView(),
            'file' => $file,
        ]);
    }

    /**
     * @Route("/list-test", name="list")
     *
     * @param Request $request
     *
     * @Template(template="/templates/Default/list.html.twig")
     * @return Response
     */
    public function listAction(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $repository = $em->getRepository(File::class);
        $queryBuilder = $repository->createQueryBuilder('base');
        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        // parameters to template
        return ['pagination' => $pagination];
    }


    /**
     * Загрузка изображений из CKE editor
     *
     * @param Request     $request
     * @param FileService $fileService
     *
     * @return JsonResponse
     */
    public function upload(Request $request, FileService $fileService)
    {
        /*$referer = $request->headers->get('referer');
        $referer = str_replace($request->getSchemeAndHttpHost(), '', $referer);
        $router = $this->get('router');
        $dataReferrer = $router->match($referer);*/

        $file = $request->files->get('upload');

        $data = $fileService->saveSimpleImageFromUploadedFile($file, ['uploaded', date('Y-m-d')]);

        return  new JsonResponse(['fileName' => $data['fileName'], 'uploaded' => 1, 'url' => $data['url']]);
    }
}
