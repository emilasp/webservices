<?php

namespace FileBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\BlameableEntityTrait;
use App\Core\Traits\TimestampableEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="FileBundle\Repository\FileRepository")
 */
class File implements IEntityCore
{
    use TimestampableEntityTrait;
    use BlameableEntityTrait;


    public const TYPE_IMAGE = 1;

    /**
     * @Groups({"appDashboard"})
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $entityClass;

    /**
     * @ORM\Column(type="integer", options={"default":null})
     */
    private $entityId;
    /**
     * @ORM\Column(type="smallint", options={"default":1})
     */
    private $type = self::TYPE_IMAGE;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $context;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $isMain = false;

    public function __clone() {
        if ($this->id) {
            $this->id = null;
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEntityClass(): ?string
    {
        return $this->entityClass;
    }

    public function setEntityClass(string $entityClass): self
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    public function getEntityId(): ?int
    {
        return $this->entityId;
    }

    public function setEntityId(?int $entityId): self
    {
        $this->entityId = $entityId;

        return $this;
    }

    public function getContext(): ?string
    {
        return $this->context;
    }

    public function setContext(string $context): self
    {
        $this->context = $context;

        return $this;
    }

    public function getIsMain()
    {
        return $this->isMain;
    }

    public function setIsMain($isMain): self
    {
        $this->isMain = $isMain;

        return $this;
    }
}
