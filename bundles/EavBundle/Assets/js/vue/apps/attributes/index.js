'use strict';

import Vue from 'vue';
import Vuelidate from "vuelidate";
import App from "./App";

Vue.use(Vuelidate);

Vue.config.productionTip = false;

import './css/app.scss';

new Vue(App);
