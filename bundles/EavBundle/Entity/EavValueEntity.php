<?php

namespace EavBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="eav_value_entity",
 *     indexes={@ORM\Index(name="idx_eav_value_entity_class", columns={"entity_class"})},
 * )
 * @ORM\Entity(repositoryClass="EavBundle\Repository\EavValueEntityRepository")
 */
class EavValueEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $entityClass;

    /**
     * @ORM\ManyToOne(targetEntity="EavBundle\Entity\EavValue", inversedBy="valueEntityLinks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $value;

    /**
     * @ORM\Column(type="integer")
     */
    private $entityId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntityClass(): ?string
    {
        return $this->entityClass;
    }

    public function setEntityClass(string $entityClass): self
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    public function getValue(): ?EavValue
    {
        return $this->value;
    }

    public function setValue(?EavValue $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getEntityId(): ?int
    {
        return $this->entityId;
    }

    public function setEntityId(int $entityId): self
    {
        $this->entityId = $entityId;

        return $this;
    }
}
