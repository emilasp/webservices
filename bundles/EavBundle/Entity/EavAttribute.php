<?php

namespace EavBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="EavBundle\Repository\EavAttributeRepository")
 */
class EavAttribute
{
    public const TYPE_NORMAL = 1;

    public const TYPES = [
        self::TYPE_NORMAL => 'Обычный'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"appEav"})
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     * @Groups({"appEav"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"appEav"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"appEav"})
     */
    private $grouped;

    /**
     * @ORM\OneToMany(targetEntity="EavBundle\Entity\EavValue", mappedBy="attribute")
     * @Groups({"appEav"})
     */
    private $values;

    /**
     * @ORM\OneToMany(targetEntity="EavBundle\Entity\EavAttributeEntity", mappedBy="attribute")
     */
    private $attributeEntityLinks;


    public function __construct()
    {
        $this->values            = new ArrayCollection();
        $this->attributeEntityLinks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getGrouped(): ?string
    {
        return $this->grouped;
    }

    public function setGrouped(string $grouped): self
    {
        $this->grouped = $grouped;

        return $this;
    }

    /**
     * @return Collection|EavValue[]
     */
    public function getValues(): Collection
    {
        return $this->values;
    }

    public function addValue(EavValue $value): self
    {
        if (!$this->values->contains($value)) {
            $this->values[] = $value;
            $value->setAttribute($this);
        }

        return $this;
    }

    public function removeValue(EavValue $value): self
    {
        if ($this->values->contains($value)) {
            $this->values->removeElement($value);
            // set the owning side to null (unless already changed)
            if ($value->getAttribute() === $this) {
                $value->setAttribute(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EavAttributeEntity[]
     */
    public function getAttributeEntityLinks(): Collection
    {
        return $this->attributeEntityLinks;
    }

    public function addAttributeEntityLink(EavAttributeEntity $attributeEntityLink): self
    {
        if (!$this->attributeEntityLinks->contains($attributeEntityLink)) {
            $this->attributeEntityLinks[] = $attributeEntityLink;
            $attributeEntityLink->setAttribute($this);
        }

        return $this;
    }

    public function removeAttributeEntityLink(EavAttributeEntity $attributeEntityLink): self
    {
        if ($this->attributeEntityLinks->contains($attributeEntityLink)) {
            $this->attributeEntityLinks->removeElement($attributeEntityLink);
            // set the owning side to null (unless already changed)
            if ($attributeEntityLink->getAttribute() === $this) {
                $attributeEntityLink->setAttribute(null);
            }
        }

        return $this;
    }
}
