<?php

namespace EavBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="EavBundle\Repository\EavValueRepository")
 */
class EavValue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"appEav"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="EavBundle\Entity\EavAttribute", inversedBy="values")
     * @ORM\JoinColumn(nullable=false)
     */
    private $attribute;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"appEav"})
     */
    private $value;

    /**
     * @ORM\OneToMany(targetEntity="EavBundle\Entity\EavValueEntity", mappedBy="value")
     */
    private $valueEntityLinks;

    public function __construct()
    {
        $this->valueEntityLinks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAttribute(): ?EavAttribute
    {
        return $this->attribute;
    }

    public function setAttribute(?EavAttribute $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Collection|EavValueEntity[]
     */
    public function getValueEntityLinks(): Collection
    {
        return $this->valueEntityLinks;
    }

    public function addValueEntityLink(EavValueEntity $valueEntityLink): self
    {
        if (!$this->valueEntityLinks->contains($valueEntityLink)) {
            $this->valueEntityLinks[] = $valueEntityLink;
            $valueEntityLink->setValue($this);
        }

        return $this;
    }

    public function removeValueEntityLink(EavValueEntity $valueEntityLink): self
    {
        if ($this->valueEntityLinks->contains($valueEntityLink)) {
            $this->valueEntityLinks->removeElement($valueEntityLink);
            // set the owning side to null (unless already changed)
            if ($valueEntityLink->getValue() === $this) {
                $valueEntityLink->setValue(null);
            }
        }

        return $this;
    }
}
