<?php

namespace EavBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(
 *     name="eav_attribute_entity",
 *     indexes={@ORM\Index(name="idx_eav_attribute_entity_class", columns={"entity_class"})},
 * )
 *
 * @ORM\Entity(repositoryClass="EavBundle\Repository\EavAttributeEntityRepository")
 */
class EavAttributeEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"appEav"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="EavBundle\Entity\EavAttribute", inversedBy="attributeEntityLinks")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"appEav"})
     */
    private $attribute;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"appEav"})
     */
    private $entityClass;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"appEav"})
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAttribute(): ?EavAttribute
    {
        return $this->attribute;
    }

    public function setAttribute(?EavAttribute $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function getEntityClass(): ?string
    {
        return $this->entityClass;
    }

    public function setEntityClass(string $entityClass): self
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
