<?php

namespace EavBundle\Service;

use App\Core\Helpers\EntityHelper;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use EavBundle\Entity\EavAttribute;
use EavBundle\Entity\EavValue;

/**
 * Сервис отвечает за взаимодействие между Entity проекта и File entity бандла
 *
 * Class FileRelationService
 *
 * @package EavBundle\Service
 */
class EavService
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * CourseUserService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Добавляем значения атрибутам
     *
     * @param EavAttribute $attribute
     * @param array        $values
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function addAttributeValues(EavAttribute $attribute, array $values)
    {
        $attributeValues = EntityHelper::setFieldAsKey($attribute->getValues()->getValues(), 'value');

        foreach ($values as $value) {
            if ($value->value && !isset($attributeValues[$value->id])) {
                $eavValue = new EavValue();
                $eavValue->setValue($value->value);
                $this->em->persist($eavValue);
                $attribute->addValue($eavValue);
            }
        }
    }

    /**
     * Удаляем значения атрибутам
     *
     * @param EavAttribute $attribute
     * @param array        $values
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function removeAttributeValues(EavAttribute $attribute, array $values)
    {
        $attributeValues = EntityHelper::setFieldAsKey($attribute->getValues()->getValues(), 'value');

        foreach ($values as $value) {
            if (isset($attributeValues[$value->id])) {
                unset($attributeValues[$value->id]);
            }
        }

        /** @var EavValue $valueToRemove */
        foreach ($attributeValues as $valueToRemove) {
            $this->em->remove($valueToRemove);
        }
    }
}