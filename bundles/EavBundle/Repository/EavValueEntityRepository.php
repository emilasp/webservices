<?php

namespace EavBundle\Repository;

use EavBundle\Entity\EavValueEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EavValueEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method EavValueEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method EavValueEntity[]    findAll()
 * @method EavValueEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EavValueEntityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EavValueEntity::class);
    }

    // /**
    //  * @return EavValueEntity[] Returns an array of EavValueEntity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EavValueEntity
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
