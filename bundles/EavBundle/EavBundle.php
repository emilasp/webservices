<?php
namespace EavBundle;

use EavBundle\DependencyInjection\EavExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class EavBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new EavExtension();
    }
}