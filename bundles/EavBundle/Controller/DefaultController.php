<?php

namespace EavBundle\Controller;

use App\Core\Abstracts\AController;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class DefaultController
 *
 * @Route("/eav")
 * @package EavBundle\Controller
 *
 * @Security("is_granted('ROLE_USER')")
 */
class DefaultController extends AController
{
    /**
     * @Route("/attributes", name="eav_attributes" , options={"expose"=true})
     *
     * @Template(template="/Default/attributes.html.twig")
     * @return Response
     */
    public function attributesAction(EntityManagerInterface $entityManager, MessageBusInterface $bus)
    {
        return [];
    }
    /**
     * @Route("/values", name="eav_values" , options={"expose"=true})
     *
     * @Template(template="/Default/values.html.twig")
     * @return Response
     */
    public function valuesAction(EntityManagerInterface $entityManager, MessageBusInterface $bus)
    {
        return [];
    }
    /**
     * @Route("/entity-attributes", name="eav_attribute_entity_index" , options={"expose"=true})
     *
     * @Template(template="/Default/attributes-entity.html.twig")
     * @return Response
     */
    public function entityAttributesAction(EntityManagerInterface $entityManager, MessageBusInterface $bus)
    {
        return [];
    }
}
