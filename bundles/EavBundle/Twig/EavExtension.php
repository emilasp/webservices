<?php

namespace EavBundle\Twig;

use EavBundle\Entity\File;
use EavBundle\Service\EavService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class FileExtension
 *
 * @package EavBundle\Twig
 */
class EavExtension extends AbstractExtension
{
    private $fileService;

    /**
     * FileExtension constructor.
     *
     * @param EavService $fileService
     */
    public function __construct(EavService $fileService)
    {
        $this->fileService = $fileService;
    }
}
