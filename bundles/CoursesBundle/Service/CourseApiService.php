<?php


namespace CoursesBundle\Service;

use App\Core\Abstracts\AInnerApiService;
use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseLesson;
use CoursesBundle\Entity\CourseLessonUserStudent;
use CoursesBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use FileBundle\Service\FileService;

/**
 * Class CourseApiService
 *
 * @package CoursesBundle\Service
 */
class CourseApiService extends AInnerApiService
{
    private const IMAGE_TYPES = [FileService::IMAGE_FORMAT_XS, FileService::IMAGE_FORMAT_SMALL, FileService::IMAGE_FORMAT_SQUARE, FileService::IMAGE_FORMAT_MEDIUM];

    /** @var CourseUserService */
    private $courseUserService;

    /**
     * LessonService constructor.
     *
     * @param CourseUserService $courseUserService
     */
    public function __construct(CourseUserService $courseUserService, EntityManagerInterface $em, FileService $fileService)
    {
        parent::__construct($em, $fileService);

        $this->courseUserService = $courseUserService;
    }

    /**
     * Получаем Данные по студенту для приложения Dashboard
     *
     * @param CourseLesson $lesson
     *
     * @return array|\DateTime[]
     * @throws \Exception
     */
    public function getDataForDashboard(User $user): array
    {
        return [
            'courses'      => $this->getCoursesFullDataForDashboardApp($user),
            'achievements' => $this->getAchievementsForDashboardApp($user),
        ];
    }

    /**
     * Получаем информацию по ачивкам юзера
     *
     * @param User $user
     *
     * @return array
     */
    private function getAchievementsForDashboardApp(User $user): array
    {
        $data = [];
        foreach ($user->getAchievementsLink() as $achievementUser) {
            $achievement                 = $achievementUser->getAchievement();
            $data[$achievement->getId()] = [
                'achievement' => $achievement,
                'images'      => $this->getEntityImages($achievement, 'image', self::IMAGE_TYPES)
            ];
        }
        return $data;
    }

    /**
     * Получаем всю информацию по курсам пользователя
     *
     * @param User $user
     *
     * @return array
     * @throws \Exception
     */
    private function getCoursesFullDataForDashboardApp(User $user): array
    {
        $data = ['programms' => [], 'singleton' => []];
        foreach ($user->getCoursesLink() as $courseLink) {
            if (!$courseLink->getCourse()->isProgramm() && $courseLink->getStatus() === CourseLessonUserStudent::STATUS_CURRENT) {
                $courseData = $this->courseUserService->getCourseData($courseLink);
                $courseData['images' ] = [
                    'image'    => $this->getEntityImages($courseLink->getCourse(), 'image', self::IMAGE_TYPES),
                    'imageBig' => $this->getEntityImages($courseLink->getCourse(), 'imageBig', self::IMAGE_TYPES),
                ];

                // Separate Programm and singleton
                if ($programm = $courseLink->getCourse()->getProgramm()) {
                    $data['programms'][$programm->getId()]['programm']  = $programm;
                    $data['programms'][$programm->getId()]['courses'][] = $courseData;
                } else {
                    $data['singleton'][] = $courseData;
                }
            }
        }

        // Statistics for programms
        foreach ($data['programms'] as $programmId => $dataProgramm) {
            $all   = 0;
            $ended = 0;
            foreach ($dataProgramm['courses'] as $course) {
                $all   += $course['stat']['all'];
                $ended += ($course['stat']['overdue'] + $course['stat']['finish']);
            }
            $data['programms'][$programmId]['stat']['all']     = $all;
            $data['programms'][$programmId]['stat']['ended']   = $ended;
            $data['programms'][$programmId]['stat']['percent'] = ceil($ended / $all * 100);


            $data['programms'][$programmId]['images'] = [
                'image'    => $this->getEntityImages($dataProgramm['programm'], 'image', self::IMAGE_TYPES),
                'imageBig' => $this->getEntityImages($dataProgramm['programm'], 'imageBig', self::IMAGE_TYPES)
            ];

            $data['programms'][$programmId]['gestalt'] = $user->getCourseLink($dataProgramm['programm'])->getGestaltHistory();
        }

        return $data;
    }
}
