<?php


namespace CoursesBundle\Service;

use App\Core\Helpers\DateHelper;
use CoursesBundle\Entity\CourseLesson;
use CoursesBundle\Entity\CourseLessonUserStudent;
use CoursesBundle\Entity\CourseUserStudent;
use CoursesBundle\Entity\LevelExperience;
use CoursesBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserService
 *
 * @package CoursesBundle\Service
 */
class UserService
{
    private const DEFAULT_STATS = [
        'duration'       => 0,
        'experience'     => ['current' => 0, 'next' => 0],
        'courses'        => ['current' => 0, 'finished' => 0, 'overdue' => 0],
        'lessons'        => ['current' => 0, 'finished' => 0, 'overdue' => 0],
        'level'          => 0,
        'achievementIds' => [],
    ];

    /** @var EntityManagerInterface */
    private $em;
    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /**
     * LessonService constructor.
     *
     * @param EntityManagerInterface   $em
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $eventDispatcher)
    {
        $this->em              = $em;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * Перегенерируем статистику пользователя
     *
     * @param User $user
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function regenerateStats(User $user): void
    {
        $data = self::DEFAULT_STATS;

        $experienceFull = 0;
        // Regenerate stats for courses
        foreach ($user->getCoursesLink() as $link) {
            $course = $link->getCourse();

            $lessonsLink = $user->getLessonsLink($course);

            $duration    = 0;
            $difficultly = 0;
            foreach ($lessonsLink as $lessonLink) {
                switch ($lessonLink->getStatus()) {
                    case  CourseLessonUserStudent::STATUS_CURRENT:
                        $data['lessons']['current']++;
                        break;
                    case  CourseLessonUserStudent::STATUS_FINISH:
                        $data['lessons']['finished']++;

                        $difficultly += $lessonLink->getLesson()->getDifficulty();
                        $duration    += $lessonLink->getLesson()->getDuration();
                        break;
                    case  CourseLessonUserStudent::STATUS_OVERDUE:
                        $data['lessons']['overdue']++;
                        break;
                }
            }

            $link->setDifficulty($difficultly);
            $link->setDuration($duration);

            // Set stats
            $data['duration'] += $duration;
            $experienceFull   += $difficultly;

            switch ($link->getStatus()) {
                case  CourseUserStudent::STATUS_CURRENT:
                    $data['courses']['current']++;
                    break;
                case  CourseUserStudent::STATUS_FINISH:
                    $data['courses']['finished']++;
                    break;
                case  CourseUserStudent::STATUS_OVERDUE:
                    $data['courses']['overdue']++;
                    break;
            }
        }


        // Regenerate stats for achievements
        foreach ($user->getAchievementsLink() as $linkAchievement) {
            $experienceFull           += $linkAchievement->getAchievement()->getDifficulty();
            $data['achievementIds'][] = $linkAchievement->getAchievement()->getId();
        }

        $data['experience'] = $this->getExperienceData($experienceFull);

        $user->setJsonProperty(User::JF_COURSES_STATS, $data);

        $this->em->flush();
    }

    /**
     * Получаем сатистику по опыту и уровням
     *
     * @param int  $experienceFull
     * @param null $level
     *
     * @return array
     */
    private function getExperienceData(int $experienceFull, $level = null): array
    {
        $data = ['current' => $experienceFull,'level' => 0, 'nextExp' => 0];
        $queryBilder = $this->em->getRepository(LevelExperience::class)->createQueryBuilder('level')
            ->andWhere("level.difficulty > :userDifficultly")->setParameter('userDifficultly', $experienceFull)
            ->orderBy('level.level', 'ASC')
            ->setMaxResults(1);

        $expNulled1 = $experienceFull;
        $levelId = 0;
        if ($level) {
            $data['level'] = $level->getLevel();
            $levelId = $level->getId();

            $expNulled1 -=  $level->getDifficulty();
        }

        $expNulled2 = $expNulled1;
        $queryBilder->andWhere('level.id != :currentLevel')->setParameter('currentLevel', $levelId);
        /** @var LevelExperience $levelNext */
        if ($levelNext = $queryBilder->getQuery()->getResult()) {
            $data['nextExp'] = $levelNext[0]->getDifficulty();
            $expNulled2 = $levelNext[0]->getDifficulty();
        }

        $data['percent'] = ceil($expNulled1/$expNulled2 * 100);

        return $data;
    }

    /**
     * Получаем текущую статстику пользователя
     *
     * @param User $user
     *
     * @return array
     */
    public function getStats(User $user): array
    {
        $currentData = $user->getJsonProperty(User::JF_COURSES_STATS) ?: [];

        $resolver = new OptionsResolver();
        $resolver->setDefaults(self::DEFAULT_STATS);

        return $resolver->resolve($currentData);
    }

    /**
     * Получаем все поздравления по достижениям - окончание урока, курса, новый уровень и новая ачивка
     *
     * @param User  $user
     * @param array $prevStats
     *
     * @return array
     */
    public function getCongratulationData(User $user, array $prevStats): array
    {
        $newStats = $this->getStats($user);

        $new = [
            'course'       => false,
            'lesson'       => false,
            'level'        => false,
            'achievements' => [],
        ];

        if ($newStats['lessons']['finished'] > $prevStats['lessons']['finished']) {
            $new['lesson'] = true;
        }
        if ($newStats['courses']['finished'] > $prevStats['courses']['finished']) {
            $new['course'] = true;
        }
        if ($newStats['level'] > $prevStats['level']) {
            $new['level'] = true;
        }

        if ($ids = array_diff($newStats['achievementIds'], $prevStats['achievementIds'])) {
            foreach ($user->getAchievementsLink() as $link) {
                if (in_array($link->getAchievement()->getId(), $ids)) {
                    $new['achievements'][] = $link->getAchievement();
                }
            }
        }

        return [
            'statistics' => $newStats,
            'newData'    => $new,
        ];
    }
}