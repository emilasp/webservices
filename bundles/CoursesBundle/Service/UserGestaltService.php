<?php

namespace CoursesBundle\Service;

use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseLessonUserStudent;
use CoursesBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UserProgrammGestaltService
 *
 * @package CoursesBundle\Service
 */
class UserGestaltService
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * UserGestaltService constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * Считаем и сохраняем гельштальт статистику по курсу
     *
     * @param User   $user
     * @param Course $programm
     *
     * @throws \Exception
     */
    public function regenerateGestaltStats(User $user, Course $programm = null)
    {
        if ($programm) {
            $maxDifficultly = $programm->getJsonProperty(Course::JF_GESTALT_MAX_DIFFICULTLY);
            $negativeRatio  = $programm->getJsonProperty(Course::JF_GESTALT_NEGATIVE_RATIO);

            $data = [
                'difficultly' => 0,
                'percent'     => 0,
                'courses'     => [],
            ];
            if ($maxDifficultly && $negativeRatio) {
                $coursesLinks = [];
                foreach ($user->getCoursesLink() as $courseLink) {
                    $course = $courseLink->getCourse();
                    if ($course->getProgramm() === $programm) {
                        $coursesLinks[] = $courseLink;
                        $courseStat     = $this->getCourseHistory($user, $course, $maxDifficultly, $negativeRatio);

                        $data['courses'][$course->getId()] = $courseStat;
                        $data['difficultly']               += $courseStat['difficultly'];
                        $data['percent']                   = ceil($data['difficultly'] / $maxDifficultly * 100);
                        $data['percent']                   = $data['percent'] > 100 ? 100 : $data['percent'];
                    }
                }
            }

            $courseStudentLink = $user->getCourseLink($programm);
            $courseStudentLink->setGestaltHistory($data);

            $this->em->flush($courseStudentLink);
        }
    }

    /**
     * Считаем гештальт статистику для курса
     *
     * @param User   $user
     * @param Course $course
     * @param int    $maxDifficultly
     * @param int    $negativeRatio
     *
     * @return array
     * @throws \Exception
     */
    private function getCourseHistory(User $user, Course $course, int $maxDifficultly, int $negativeRatio): array
    {
        $all         = ['finish' => 0, 'overdue' => 0, 'current' => 0];
        $period      = ['finish' => 0, 'overdue' => 0, 'current' => 0];
        $percent     = 0;
        $difficultly = 0;
        foreach ($user->getLessonsLink($course) as $lessonLink) {
            if ($lessonLink->getStartAt() < new \DateTime()) {
                $lessonDifficultly = $lessonLink->getLesson()->getDifficulty();
                switch ($lessonLink->getStatus()) {
                    case CourseLessonUserStudent::STATUS_CURRENT:
                        $all['current']++;
                        $period['current']++;
                        $difficultly -= $lessonDifficultly;
                        break;
                    case CourseLessonUserStudent::STATUS_FINISH:
                        $all['finish']++;
                        $period['finish']++;
                        $difficultly += $lessonDifficultly;
                        break;
                    case CourseLessonUserStudent::STATUS_OVERDUE:
                        $all['overdue']++;
                        $period['overdue']++;
                        $difficultly -= $lessonDifficultly * $negativeRatio;
                        break;
                }

                $difficultly = $difficultly < 0 ? 0 : $difficultly;

                $percent = ceil($difficultly / $maxDifficultly * 100);
            }
        }

        return ['all' => $all, 'period' => $period, 'difficultly' => $difficultly, 'percent' => $percent];
    }
}