<?php


namespace CoursesBundle\Service;

use App\Core\Helpers\DateHelper;
use CoursesBundle\Entity\CourseLesson;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class LessonService
 *
 * @package CoursesBundle\Service
 */
class LessonService
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /**
     * LessonService constructor.
     *
     * @param EntityManagerInterface   $em
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $eventDispatcher)
    {
        $this->em              = $em;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * Клонируем урок
     *
     * @param CourseLesson $lesson
     * @param int          $count
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function cloneLesson(CourseLesson $lesson, int $count)
    {
        $parts = clone $lesson->getParts();

        for ($i = 1; $i <= $count; $i++) {
            $newLesson = clone $lesson;
            $newLesson->setPosition($lesson->getPosition() + $i);
            $this->em->persist($newLesson);

            foreach ($parts as $part) {
                $newPart = clone $part;

                $this->em->persist($newPart);

                $newLesson->addPart($newPart);
            }
        }

        $this->em->flush();
    }


    /**
     * Получаем даты для уроков по полю periodic
     *
     * @param CourseLesson $lesson
     *
     * @return array|\DateTime[]
     * @throws \Exception
     */
    public function getDatesByPeriodic(CourseLesson $lesson): array
    {
        $dates = [];

        if ($periodic = $lesson->getPeriodic()) {
            // For selected days
            foreach ($periodic['days'] as $dayData) {
                if ($dayData['active']) {
                    $dates[] = new \DateTime($dayData['date']);
                }
            }

            // For selected week days
            $weekDays = [];
            foreach ($periodic['daysOfWeek'] as $deyWeek => $dataOfWeekDay) {
                if ($dataOfWeekDay['active']) {
                    $weekDays[] = $deyWeek;
                }
            }

            $datesFromRange = DateHelper::getDatesFromRange(
                new \DateTime($periodic['from']),
                new \DateTime($periodic['to'])
            );
            foreach ($datesFromRange as $dateTime) {
                if (in_array($dateTime->format('N'), $weekDays)) {
                    $dates[] = $dateTime;
                }
            }
        }

        return $dates;
    }
}