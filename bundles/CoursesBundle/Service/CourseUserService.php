<?php


namespace CoursesBundle\Service;

use CoursesBundle\Entity\CourseLessonUserStudentArchive;
use CoursesBundle\Entity\User;
use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseLesson;
use CoursesBundle\Entity\CourseLessonUserStudent;
use CoursesBundle\Entity\CourseUserStudent;
use CoursesBundle\Event\CourseUserEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/**
 * Class CourseUserService
 *
 * @package CoursesBundle\Service
 */
class CourseUserService
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var EventDispatcherInterface */
    private $eventDispatcher;
    /** @var AchievementUserService */
    private $achievementUserService;
    /** @var LessonService */
    private $lessonService;
    /** @var UserService */
    private $userService;

    /**
     * CourseUserService constructor.
     *
     * @param EntityManagerInterface   $em
     * @param LessonService            $lessonService
     * @param UserService              $userService
     * @param EventDispatcherInterface $eventDispatcher
     * @param AchievementUserService   $achievementUserService
     */
    public function __construct(
        EntityManagerInterface $em,
        LessonService $lessonService,
        UserService $userService,
        EventDispatcherInterface $eventDispatcher,
        AchievementUserService $achievementUserService
    ) {
        $this->em                     = $em;
        $this->lessonService          = $lessonService;
        $this->userService            = $userService;
        $this->eventDispatcher        = $eventDispatcher;
        $this->achievementUserService = $achievementUserService;
    }

    /**
     * Подписан ли пользователь на курс
     *
     * @param User   $user
     * @param Course $course
     *
     * @return bool
     */
    public function hasUserCourse(User $user, Course $course): bool
    {
        foreach ($user->getCoursesLink() as $item) {
            if ($item->getCourse() === $course && $item->getStatus() === CourseUserStudent::STATUS_CURRENT) {
                return true;
            }
        }
        return false;
    }


    /**
     * Подписан ли пользователь на урок
     *
     * @param User           $user
     * @param CourseLesson   $lesson
     * @param \DateTime|null $date
     *
     * @return bool
     */
    public function hasUserLesson(User $user, CourseLesson $lesson, \DateTime $date = null): bool
    {
        foreach ($user->getLessonsLink($lesson->getCourse()) as $item) {
            if ($item->getLesson() === $lesson) {
                if ($date) {
                    return $item->getStartAt() === $date;
                } else {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * Проверяем статус урока для пользователя
     *
     * @param User         $user
     * @param CourseLesson $lesson
     *
     * @return int
     * @throws \Exception
     */
    public function getUserLessonStatus(User $user, CourseLesson $lesson): int
    {
        return $this->getUserCurrentCourseLessonLink($user, $lesson->getCourse())->getStatus();
    }

    /**
     * Получаем текущий урок курса для пользователя
     * Устанавливаем слудующему линку дату начала.
     *
     * @param User   $user
     * @param Course $course
     *
     * @return CourseLessonUserStudent
     * @throws \Exception
     */
    public function getUserCurrentCourseLessonLink(User $user, Course $course): ?CourseLessonUserStudent
    {
        $currentLink = null;
        $nextLessons = [];

        foreach ($course->getSections() as $section) {
            foreach ($section->getLessons() as $lesson) {
                // Собираем все последующие уроки
                if ($currentLink) {
                    $nextLessons[] = $lesson;
                }
                //Устанавливаем текущий линк
                foreach ($user->getLessonLinks($lesson) as $link) {
                    if (!$currentLink) {
                        if ($link->getStatus() === CourseLessonUserStudent::STATUS_CURRENT) {
                            $currentLink = $link;
                        } elseif ($link->getStatus() === CourseLessonUserStudent::STATUS_NEW) {
                            $currentLink = $link;
                            $currentLink->setStatus(CourseLessonUserStudent::STATUS_CURRENT);
                            $currentLink->setStartAt($this->getCurrentLinkStartAt($currentLink));

                            $this->em->flush($currentLink);
                        }
                    }
                }
            }
        }

        // Устанавливаем все следующим не переодичным урокам даты старта(более чем текущий)
        if (!$course->isPeriodical()) {
            foreach ($nextLessons as $nextLesson) {
                $nextLesson->setStartAt(null);
            }
        }

        return $currentLink;
    }

    /**
     * Устанавливаем текущему курсу дату старта в зависимости от типа урока
     *
     * @param CourseLessonUserStudent $currentLink
     *
     * @return \DateTimeInterface|null
     * @throws \Exception
     */
    private function getCurrentLinkStartAt(CourseLessonUserStudent $currentLink): ?\DateTimeInterface
    {
        $startAt = $currentLink->getStartAt();
        $lesson  = $currentLink->getLesson();

        if (!$lesson->getCourse()->isPeriodical()) {
            switch ($lesson->getTypeAdmission()) {
                case CourseLesson::ADMISSION_TYPE_AFTER_PREV_LESSON:
                    $startAt = new \DateTime();
                    break;
                case CourseLesson::ADMISSION_TYPE_AFTER_PREV_LESSON_NEXT_DAY:
                    $startAt = \DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d 00:00:00', strtotime('+1 days')));
                    break;
                case CourseLesson::ADMISSION_TYPE_ON_DATE_START:
                    $startAt = $lesson->getStartAt();
                    break;
            }
        }
        return $startAt;
    }

    /**
     * Получаем сегодняшний урок курса для пользователя
     *
     * @param User   $user
     * @param Course $course
     *
     * @return CourseLessonUserStudent|null
     * @throws \Exception
     */
    public function getUserTodayCourseLessonLink(User $user, Course $course): ?CourseLessonUserStudent
    {
        if (!$current = $this->getUserCurrentCourseLessonLink($user, $course)) {
            return null;
        }

        if ($current->getStartAt()->format('Y-m-d') <= (new \DateTime())->format('Y-m-d')) {
            return $current;
        }

        $finishedLink = null;
        $currentLink  = null;

        $dateNow = (new \DateTime())->format('Y-m-d');
        foreach ($user->getLessonsLink($course) as $lessonLink) {
            if ($lessonLink->getStatus() === CourseLessonUserStudent::STATUS_FINISH) {
                if ($lessonLink->getFinishAt()->format('Y-m-d') === $dateNow) {
                    $finishedLink = $lessonLink;
                }
            }
            if ($lessonLink->getStatus() === CourseLessonUserStudent::STATUS_CURRENT) {
                if ($lessonLink->getStartAt()->format('Y-m-d') <= $dateNow) {
                    $currentLink = $lessonLink;
                }
            }
        }

        return $currentLink ?: $finishedLink ?: null;
    }

    /**
     * Получаем следующий текущий урок курса для пользователя
     *
     * @param CourseLessonUserStudent $currentLink
     *
     * @return CourseLessonUserStudent|null
     * @throws \Exception
     */
    public function getNextLessonLink(CourseLessonUserStudent $currentLink = null): ?CourseLessonUserStudent
    {
        if ($currentLink) {
            $currentWill = false;
            foreach ($currentLink->getLesson()->getCourse()->getSections() as $section) {
                foreach ($section->getLessons() as $lesson) {
                    foreach ($currentLink->getUser()->getLessonLinks($lesson) as $link) {
                        if ($currentWill) {
                            return $link;
                        }

                        if ($link === $currentLink) {
                            $currentWill = true;
                        }
                    }
                }
            }
        }
        return null;
    }


    /**
     * Получаем предыдущий урок курса для пользователя
     *
     * @param CourseLessonUserStudent $currentLink
     *
     * @return CourseLessonUserStudent|null
     * @throws \Exception
     */
    public function getPrevLessonLink(CourseLessonUserStudent $currentLink = null): ?CourseLessonUserStudent
    {
        if ($currentLink) {
            $prevLink = null;
            foreach ($currentLink->getLesson()->getCourse()->getSections() as $section) {
                foreach ($section->getLessons() as $lesson) {
                    foreach ($currentLink->getUser()->getLessonLinks($lesson) as $link) {
                        if ($link === $currentLink) {
                            return $prevLink;
                        }
                        $prevLink = $link;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Устанавливаем прохождение курса
     *
     * @param User   $user
     * @param Course $course
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setFinishCourse(User $user, Course $course)
    {
        $link = $user->getCourseLink($course);
        if ($link && $link->getStatus() === CourseUserStudent::STATUS_CURRENT) {
            $lessonNoFinish = $user->getLessonsLink($course)->filter(function (CourseLessonUserStudent $link) {
                return $link->getStatus() !== CourseLessonUserStudent::STATUS_FINISH;
            });

            if (!$lessonNoFinish->count()) {
                $link->setStatus(CourseUserStudent::STATUS_FINISH);

                $this->em->flush($link);

                $this->eventDispatcher->dispatch(
                    CourseUserEvent::NAME_COURSE_USER_FINISH,
                    new CourseUserEvent($course, $user)
                );
            }
        }
    }


    /**
     * Формируем массив с данными по курсу
     *
     * @param CourseUserStudent $courseLink
     *
     * @return array
     * @throws \Exception
     */
    public function getCourseData(CourseUserStudent $courseLink): array
    {
        $user   = $courseLink->getUser();
        $course = $courseLink->getCourse();

        $courseData = [
            'courseLink'  => $courseLink,
            'course'      => $course,
            'lessons'     => $course->getLessons(),
            'timeline'    => $this->getTimeLineData($user, $course),
            'userlessons' => [
                CourseLessonUserStudent::STATUS_NEW     => [],
                CourseLessonUserStudent::STATUS_CURRENT => [],
                CourseLessonUserStudent::STATUS_OVERDUE => [],
                CourseLessonUserStudent::STATUS_FINISH  => [],
            ]
        ];

        // Set leesonLinks By type
        foreach ($courseData['lessons'] as $lesson) {
            foreach ($user->getLessonLinks($lesson) as $lessonUserStudent) {
                $courseData['userlessons'][$lessonUserStudent->getStatus()][] = $lessonUserStudent;
                $courseData['userlessons']['all'][]                           = $lessonUserStudent;
            }
        }

        // Set statistics
        $stat            = [];
        $stat['all']     = count($courseData['userlessons']['all']);
        $stat['new']     = count($courseData['userlessons'][CourseLessonUserStudent::STATUS_NEW]);
        $stat['new']     += count($courseData['userlessons'][CourseLessonUserStudent::STATUS_CURRENT]);
        $stat['overdue'] = count($courseData['userlessons'][CourseLessonUserStudent::STATUS_OVERDUE]);
        $stat['finish']  = count($courseData['userlessons'][CourseLessonUserStudent::STATUS_FINISH]);
        $stat['percent'] = ceil(($stat['overdue'] + $stat['finish']) / $stat['all'] * 100);
        $stat['older']   = false; //@TODO Добавить рассчет просроченного тек. урока для курса

        $courseData['stat'] = $stat;

        // Other
        $courseData['userlessons']['current'] = $this->getUserCurrentCourseLessonLink($user, $course);
        $courseData['userlessons']['today']   = $this->getUserTodayCourseLessonLink($user, $course) ?: $courseData['userlessons']['current'];
        $courseData['userlessons']['next']    = $this->getNextLessonLink($courseData['userlessons']['today']);


        return $courseData;
    }

    /**
     * Получаем историю для timeline
     *
     * @param User   $user
     * @param Course $course
     *
     * @return array
     * @throws \Exception
     */
    private function getTimeLineData(User $user, Course $course): array
    {
        $timeLineData = [];
        foreach ($user->getLessonsLink($course) as $lessonLink) {
            $timeLineData[$lessonLink->getStartAt()->format('Y-m-d H:i:s')] = [
                'status'   => $lessonLink->getStatus(),
                'finishAt' => $lessonLink->getFinishAt() ? $lessonLink->getFinishAt()->format('Y-m-d H:i:s') : null,
                'lessonId' => $lessonLink->getLesson()->getId(),
            ];
        }
        return $timeLineData;
    }

    /**
     * Подписываем пользователя на курс
     * Создаем линки на курсы для программы и уроки для курса
     *
     * @param User   $user
     * @param Course $course
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function subscribeUserOnCourse(User $user, Course $course)
    {
        if ($courseLink = $user->getCourseLink($course)) {
            if ($courseLink->getStatus() === CourseUserStudent::STATUS_CANCEL) {
                $courseLink->setStatus(CourseUserStudent::STATUS_CURRENT);
            }
        } else {
            if (!$this->hasUserCourse($user, $course)) {
                $courseUserLink = new CourseUserStudent();
                $courseUserLink->setUser($user);
                $courseUserLink->setCourse($course);
                $courseUserLink->setStatus(CourseUserStudent::STATUS_CURRENT);
                //$courseUserLink->setDifficulty(CourseUserStudent::DIFFICULTLY_DEFAULT);

                $this->em->persist($courseUserLink);
            }

            foreach ($course->getLessons() as $lesson) {
                if ($lesson->getStatus() === CourseLesson::STATUS_ACTIVE) {
                    if ($course->isPeriodical()) {
                        $this->createUserLessonsLinkByPeriodicLesson($user, $lesson);
                    } else {
                        $this->createUserLessonsLink($user, $lesson);
                    }
                }
            }
        }

        $this->em->flush();

        if ($course->getType() === Course::TYPE_PROGRAMM) {
            foreach ($course->getCourses() as $childCourse) {
                $this->subscribeUserOnCourse($user, $childCourse);
            }
        } else {
            $this->getUserCurrentCourseLessonLink($user, $course);
        }
    }

    /**
     * Отписываем пользователя от курса
     *
     * @param User   $user
     * @param Course $course
     *
     * @throws \Exception
     */
    public function unSubscribeUserOnCourse(User $user, Course $course)
    {
        $this->archiveCourse($user, $course);
    }

    /**
     * Архивируем ликнки по курсу пользователя
     *
     * @param User   $user
     * @param Course $course
     *
     * @throws \Exception
     */
    private function archiveCourse(User $user, Course $course)
    {
        if ($course->getType() === Course::TYPE_PROGRAMM) {
            foreach ($course->getCourses() as $childCourse) {
                $this->archiveCourse($user, $childCourse);
            }
        }

        if ($this->hasUserCourse($user, $course)) {
            $dateArchive = new \DateTime();

            $courseLink = $user->getCourseLink($course);
            $courseLink->setStatus(CourseUserStudent::STATUS_CANCEL);
            $courseLink->setIsArchive(true);
            $courseLink->setArchiveAt($dateArchive);

            foreach ($user->getLessonsLink($course) as $link) {
                $link->setIsArchive(true);
                $link->setArchiveAt($dateArchive);
            }
        }

        $this->em->flush();

        $this->userService->regenerateStats($user);
    }


    /**
     * Создаем по одной ссылке на урок
     *
     * @param User         $user
     * @param CourseLesson $lesson
     * @param int          $index
     *
     * @throws \Exception
     */
    private function createUserLessonsLink(User $user, CourseLesson $lesson)
    {
        if (!$this->hasUserLesson($user, $lesson)) {
            $startAt = $lesson->getTypeAdmission() === CourseLesson::ADMISSION_TYPE_ON_DATE_START ? $lesson->getStartAt() : null;

            $lessonUserLink = new CourseLessonUserStudent();
            $lessonUserLink->setUser($user);
            $lessonUserLink->setLesson($lesson);
            $lessonUserLink->setStatus(CourseLessonUserStudent::STATUS_NEW);
            $lessonUserLink->setStartAt($startAt);

            $user->addLessonsLink($lessonUserLink);
        }
    }

    /**
     * Создаем ссылки массово по периоду урока
     *
     * @param User         $user
     * @param CourseLesson $lesson
     *
     * @throws \Exception
     */
    private function createUserLessonsLinkByPeriodicLesson(User $user, CourseLesson $lesson)
    {
        $dates = $this->lessonService->getDatesByPeriodic($lesson);

        foreach ($dates as $date) {
            if (!$this->hasUserLesson($user, $lesson, $date)) {
                $lessonUserLink = new CourseLessonUserStudent();
                $lessonUserLink->setUser($user);
                $lessonUserLink->setLesson($lesson);
                $lessonUserLink->setStatus(CourseLessonUserStudent::STATUS_NEW);
                $lessonUserLink->setStartAt($date);

                $user->addLessonsLink($lessonUserLink);
            }
        }
    }
}