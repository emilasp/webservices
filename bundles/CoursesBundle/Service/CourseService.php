<?php


namespace CoursesBundle\Service;

use CoursesBundle\Entity\CourseLessonPart;
use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class CourseUserService
 *
 * @package CoursesBundle\Service
 */
class CourseService
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /**
     * CourseUserService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $eventDispatcher)
    {
        $this->em              = $em;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * Получаем части уроков по типу
     *
     * @param Course   $course
     * @param int|null $type
     *
     * @return ArrayCollection|CourseLessonPart[]
     */
    public function getCourseLessonParts(Course $course, int $type = null): ?ArrayCollection
    {
        $parts = ['all' => []];
        foreach ($course->getLessons() as $lesson) {
            foreach ($lesson->getParts() as $part) {
                $parts[$part->getType()][] = $part;
                $parts['all'][] = $part;
            }
        }

        if (!$type) {
            return new ArrayCollection($parts['all']);
        } elseif(isset($parts[$type])) {
            return new ArrayCollection($parts[$type]);
        }

        return new ArrayCollection();
    }
}