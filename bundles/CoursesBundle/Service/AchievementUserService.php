<?php


namespace CoursesBundle\Service;

use CoursesBundle\Entity\User;
use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseLesson;
use CoursesBundle\Entity\CourseLessonUserStudent;
use CoursesBundle\Entity\CourseUserStudent;
use CoursesBundle\Event\LessonUserEvent;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/**
 * Class AchievementUserService
 *
 * @package CoursesBundle\Service
 */
class AchievementUserService
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /**
     * CourseUserService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $eventDispatcher)
    {
        $this->em              = $em;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * Поулчаем ссылку: урок - курс
     *
     * @param User $user
     *
     * @return ArrayCollection|CourseUserStudent[]
     */
    public function getNewAchievements(User $user): ?ArrayCollection
    {
        $achievements = [];

        // NEW Achiev!
        //$this->eventDispatcher->dispatch(LessonUserEvent::NAME_USER_LESSON_START, new LessonUserEvent($lesson, $user));

        return new ArrayCollection($achievements);
    }
}