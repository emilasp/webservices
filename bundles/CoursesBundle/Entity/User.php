<?php

namespace CoursesBundle\Entity;


use AffirmationsBundle\Entity\AffirmAffirmationUser;
use AffirmationsBundle\Entity\AffirmGoalUser;
use App\Core\Traits\BlameableEntityTrait;
use App\Core\Traits\JsonPropertiesTrait;
use App\Entity\UserBase;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FileBundle\Entity\File;

/**
 * @ORM\Entity(repositoryClass="CoursesBundle\Repository\UserRepository")
 * @ORM\Table(name="users")
 */
class User extends UserBase
{
    use BlameableEntityTrait;
    use JsonPropertiesTrait;

    public const ROLE_COURSE_ADMIN     = 'ROLE_COURSE_ADMIN';
    public const ROLE_COURSE_MODERATOR = 'ROLE_COURSE_MODERATOR';

    public const ROLES = [
        self::ROLE_ADMIN            => 'Админ',
        self::ROLE_COURSE_ADMIN     => 'Курс: администратор',
        self::ROLE_COURSE_MODERATOR => 'Курс: модератор',
    ];

    // Статистика по курсам
    public const JF_COURSES_STATS = '[courses][stats]';

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="FileBundle\Entity\File", cascade={"persist", "remove"})
     */
    protected $avatar;

    /**
     * @ORM\OneToMany(targetEntity="CoursesBundle\Entity\CourseLessonUserStudent", mappedBy="user", cascade={"persist"})
     * @ORM\OrderBy({"startAt" = "ASC"})
     */
    private $lessonsLink;

    /**
     * @ORM\OneToMany(targetEntity="CoursesBundle\Entity\CourseUserStudent", mappedBy="user", cascade={"persist"})
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $coursesLink;

    /**
     * @ORM\OneToMany(targetEntity="CoursesBundle\Entity\CourseAchievementUser", mappedBy="user", cascade={"persist"})
     */
    private $achievementsLink;

    /**
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\LevelExperience", inversedBy="users")
     */
    private $levelExperience;


    public function __construct()
    {
        parent::__construct();

        $this->lessonsLink      = new ArrayCollection();
        $this->coursesLink      = new ArrayCollection();
        $this->achievementsLink = new ArrayCollection();

        //Affirmations
        $this->affirmationUserLinks = new ArrayCollection();
        $this->goalsUserLinks = new ArrayCollection();
        $this->moderateCourses = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAvatar(): ?File
    {
        return $this->avatar;
    }

    public function setAvatar(?File $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }


    /**
     *
     *
     * Sections
     *
     *
     */

    /**
     * Поулчаем текущую секцию
     *
     * @return CourseSection|null
     */
    public function getCurrentSection(Course $course): ?CourseSection
    {
        $currentLessonLink = $this->getCourseCurrentLessonLink($course);
        $currentLesson     = $currentLessonLink ? $currentLessonLink->getLesson() : null;

        return $currentLesson ? $currentLesson->getSection() : null;
    }

    /**
     * Поулчаем текущую секцию
     *
     * @return CourseSection $section
     */
    public function getSectionStatistics(CourseSection $section): array
    {
        $lessonsLinks = $this->getLessonsLink($section->getCourse());
        $sectionsData = [
            CourseLessonUserStudent::STATUS_CURRENT => 0,
            CourseLessonUserStudent::STATUS_CANCEL  => 0,
            CourseLessonUserStudent::STATUS_FINISH  => 0,
            CourseLessonUserStudent::STATUS_NEW     => 0,
            CourseLessonUserStudent::STATUS_OVERDUE => 0,
            'all'                                   => 0,
        ];
        foreach ($lessonsLinks as $lessonLink) {
            if ($lessonLink->getLesson()->getSection() === $section) {
                $sectionsData[$lessonLink->getStatus()]++;
                $sectionsData['all']++;
            }
        }
        return $sectionsData;
    }


    /**
     * Проверяем, что секция завершена
     *
     * @param CourseSection $section
     *
     * @return bool
     */
    public function isSectionFinished(CourseSection $section): bool
    {
        $linksActive = $this->getLessonLinksWithoutArchive();

        $sectionLinks = $linksActive->filter(function (CourseLessonUserStudent $link) use ($section) {
            return $link->getLesson()->getSection() === $section && $link->getStatus() !== CourseLessonUserStudent::STATUS_FINISH;
        });

        return !(bool)$sectionLinks;
    }

    /**
     *
     *
     * Lesson links
     *
     *
     */
    /**
     * Получаем текущий урок для курса
     *
     * @param Course $course
     *
     * @return CourseLessonUserStudent|null
     */
    public function getCourseCurrentLessonLink(Course $course): ?CourseLessonUserStudent
    {
        $linksActive = $this->getLessonLinksWithoutArchive();

        $links = $linksActive->filter(function (CourseLessonUserStudent $link) use ($course) {
            return $link->getLesson()->getCourse() === $course && $link->getStatus() === CourseLessonUserStudent::STATUS_CURRENT;
        });

        return $links->first() ?: null;
    }


    /**
     * Возвращаем свзязь урок - пользователь
     *
     * @param CourseLesson $lesson
     * @param int|null     $status
     *
     * @return CourseLessonUserStudent|null
     * @throws \Exception
     */
    public function getLessonLink(CourseLesson $lesson, int $status = null): ?CourseLessonUserStudent
    {
        $linksActive = $this->getLessonLinksWithoutArchive();

        $links = $linksActive->filter(function (CourseLessonUserStudent $link) use ($lesson, $status) {
            if ($link->getLesson() === $lesson && (!$status || $status === $link->getStatus())) {
                return true;
            }
            return false;
        });
        return $links->current() ?: null;
    }

    /**
     * Возвращаем если есть все свзязи урок - пользователь
     *
     * @param CourseLesson $lesson
     *
     * @return ArrayCollection|CourseLessonUserStudent[]
     * @throws \Exception
     */
    public function getLessonLinks(CourseLesson $lesson): ArrayCollection
    {
        $linksActive = $this->getLessonLinksWithoutArchive();

        $links = $linksActive->filter(function (CourseLessonUserStudent $link) use ($lesson) {
            if ($link->getLesson() === $lesson) {
                return true;
            }
            return false;
        });
        return $links;
    }


    /**
     * Поулучаем ссылки на уроки студента
     *
     * @param int|null $status
     *
     * @return Collection|CourseLessonUserStudent[]
     */
    public function getLessonsLinks(int $status = null): Collection
    {
        $linksActive = $this->getLessonLinksWithoutArchive();

        if (!$status) {
            return $linksActive;
        }

        return $linksActive->filter(function (CourseLessonUserStudent $link) use ($status) {
            return $link->getStatus() === $status;
        });
    }

    public function addLessonsLink(CourseLessonUserStudent $lessonsLink): self
    {
        if (!$this->lessonsLink->contains($lessonsLink)) {
            $this->lessonsLink[] = $lessonsLink;
            $lessonsLink->setUser($this);
        }

        return $this;
    }

    public function removeLessonsLink(CourseLessonUserStudent $lessonsLink): self
    {
        if ($this->lessonsLink->contains($lessonsLink)) {
            $this->lessonsLink->removeElement($lessonsLink);
            // set the owning side to null (unless already changed)
            if ($lessonsLink->getUser() === $this) {
                $lessonsLink->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @param Course|null $course
     *
     * @return Collection|CourseLessonUserStudent[]
     */
    public function getLessonsLink(Course $course = null): Collection
    {
        $linksActive = $this->getLessonLinksWithoutArchive();

        if (!$course) {
            return $linksActive;
        }

        return $linksActive->filter(function (CourseLessonUserStudent $link) use ($course) {
            return !$link->getIsArchive() && $link->getLesson()->getCourse() === $course;
        });
    }

    /**
     * Получаем не архивные линки
     *
     * @return ArrayCollection
     */
    private function getLessonLinksWithoutArchive()
    {
        return $this->lessonsLink->filter(function (CourseLessonUserStudent $link) {
            return !$link->getIsArchive();
        });
    }

    /**
     * @return Collection|CourseUserStudent[]
     */
    public function getCoursesLink(): Collection
    {
        return $courseLinksActive = $this->getCourseLinksWithoutArchive();
    }


    /**
     * @param Course $course
     *
     * @return CourseUserStudent
     */
    public function getCourseLink(Course $course): ?CourseUserStudent
    {
        $courseLinksActive = $this->getCourseLinksWithoutArchive();
        $courseLinks       = $courseLinksActive->filter(function (CourseUserStudent $link) use ($course) {
            return $link->getCourse() === $course;
        });

        return $courseLinks->count() ? $courseLinks->first() : null;
    }

    /**
     * Получаем не архивные линки
     *
     * @return ArrayCollection
     */
    private function getCourseLinksWithoutArchive()
    {
        return $this->coursesLink->filter(function (CourseUserStudent $link) {
            return !$link->getIsArchive();
        });
    }


    public function addCoursesLink(CourseUserStudent $coursesLink): self
    {
        if (!$this->coursesLink->contains($coursesLink)) {
            $this->coursesLink[] = $coursesLink;
            $coursesLink->setUser($this);
        }

        return $this;
    }

    public function removeCoursesLink(CourseUserStudent $coursesLink): self
    {
        if ($this->coursesLink->contains($coursesLink)) {
            $this->coursesLink->removeElement($coursesLink);
            // set the owning side to null (unless already changed)
            if ($coursesLink->getUser() === $this) {
                $coursesLink->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CourseAchievementUser[]
     */
    public function getAchievementsLink(): Collection
    {
        return $this->achievementsLink;
    }

    public function addAchievementsLink(CourseAchievementUser $achievementsLink): self
    {
        if (!$this->achievementsLink->contains($achievementsLink)) {
            $this->achievementsLink[] = $achievementsLink;
            $achievementsLink->setUser($this);
        }

        return $this;
    }

    public function removeAchievementsLink(CourseAchievementUser $achievementsLink): self
    {
        if ($this->achievementsLink->contains($achievementsLink)) {
            $this->achievementsLink->removeElement($achievementsLink);
            // set the owning side to null (unless already changed)
            if ($achievementsLink->getUser() === $this) {
                $achievementsLink->setUser(null);
            }
        }

        return $this;
    }

    public function getLevelExperience(): ?LevelExperience
    {
        return $this->levelExperience;
    }

    public function setLevelExperience(?LevelExperience $levelExperience): self
    {
        $this->levelExperience = $levelExperience;

        return $this;
    }



    /// Affirmations
    ///
    /**
     * @ORM\OneToMany(targetEntity="AffirmationsBundle\Entity\AffirmAffirmationUser", mappedBy="user")
     */
    private $affirmationUserLinks;

    /**
     * @ORM\OneToMany(targetEntity="AffirmationsBundle\Entity\AffirmGoalUser", mappedBy="user")
     */
    private $goalsUserLinks;

    /**
     * @ORM\ManyToMany(targetEntity="CoursesBundle\Entity\Course", mappedBy="moderators")
     */
    private $moderateCourses;


    /**
     * @return Collection|AffirmAffirmationUser[]
     */
    public function getAffirmationUserLinks(): Collection
    {
        return $this->affirmationUserLinks;
    }

    /**
     * @return Collection|AffirmGoalUser[]
     */
    public function getGoalUserLInks(): Collection
    {
        return $this->goalsUserLinks;
    }

    /**
     * @return Collection|Course[]
     */
    public function getModerateCourses(): Collection
    {
        return $this->moderateCourses;
    }

    public function addModerateCourse(Course $moderateCourse): self
    {
        if (!$this->moderateCourses->contains($moderateCourse)) {
            $this->moderateCourses[] = $moderateCourse;
            $moderateCourse->addModerator($this);
        }

        return $this;
    }

    public function removeModerateCourse(Course $moderateCourse): self
    {
        if ($this->moderateCourses->contains($moderateCourse)) {
            $this->moderateCourses->removeElement($moderateCourse);
            $moderateCourse->removeModerator($this);
        }

        return $this;
    }

    public function addAffirmationUserLink(AffirmAffirmationUser $affirmationUserLink): self
    {
        if (!$this->affirmationUserLinks->contains($affirmationUserLink)) {
            $this->affirmationUserLinks[] = $affirmationUserLink;
            $affirmationUserLink->setUser($this);
        }

        return $this;
    }

    public function removeAffirmationUserLink(AffirmAffirmationUser $affirmationUserLink): self
    {
        if ($this->affirmationUserLinks->contains($affirmationUserLink)) {
            $this->affirmationUserLinks->removeElement($affirmationUserLink);
            // set the owning side to null (unless already changed)
            if ($affirmationUserLink->getUser() === $this) {
                $affirmationUserLink->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AffirmGoalUser[]
     */
    public function getGoalsUserLinks(): Collection
    {
        return $this->goalsUserLinks;
    }

    public function addGoalsUserLink(AffirmGoalUser $goalsUserLink): self
    {
        if (!$this->goalsUserLinks->contains($goalsUserLink)) {
            $this->goalsUserLinks[] = $goalsUserLink;
            $goalsUserLink->setUser($this);
        }

        return $this;
    }

    public function removeGoalsUserLink(AffirmGoalUser $goalsUserLink): self
    {
        if ($this->goalsUserLinks->contains($goalsUserLink)) {
            $this->goalsUserLinks->removeElement($goalsUserLink);
            // set the owning side to null (unless already changed)
            if ($goalsUserLink->getUser() === $this) {
                $goalsUserLink->setUser(null);
            }
        }

        return $this;
    }
    ///
    /// Affirmations END
}
