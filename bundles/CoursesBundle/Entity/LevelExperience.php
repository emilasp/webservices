<?php

namespace CoursesBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FileBundle\Entity\File;

/**
 * @ORM\Entity(repositoryClass="CoursesBundle\Repository\LevelExperienceRepository")
 * @ORM\Table("user_level_experience")
 */
class LevelExperience
{
    public const JEWEL_NORMAL    = 1;
    public const JEWEL_RARE      = 2;
    public const JEWEL_LEGENDARY = 3;
    public const JEWEL_MYTHICAL  = 4;

    public const JEWELS = [
        self::JEWEL_NORMAL    => 'обычный',
        self::JEWEL_RARE      => 'редкий',
        self::JEWEL_LEGENDARY => 'легендарный',
        self::JEWEL_MYTHICAL  => 'мифический',
    ];


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $jewej;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity="FileBundle\Entity\File", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="CoursesBundle\Entity\User", mappedBy="levelExperience")
     */
    private $users;

    /**
     * @ORM\Column(type="integer")
     */
    private $difficulty;

    /**
     * @ORM\Column(type="smallint")
     */
    private $level;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJewej(): ?int
    {
        return $this->jewej;
    }

    public function setJewej(int $jewej): self
    {
        $this->jewej = $jewej;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(?File $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setLevelExperience($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getLevelExperience() === $this) {
                $user->setLevelExperience(null);
            }
        }

        return $this;
    }

    public function getDifficulty(): ?int
    {
        return $this->difficulty;
    }

    public function setDifficulty(int $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    /**
     * Получаем стандартную ценность
     *
     * @param int $level
     *
     * @return int
     */
    public static function getJewelByLevel(int $level): int
    {
        if ($level % 100 === 0) {
            return self::JEWEL_MYTHICAL;
        }
        if ($level % 10 === 0) {
            return self::JEWEL_LEGENDARY;
        }
        if ($level % 5 === 0) {
            return self::JEWEL_RARE;
        }
        return self::JEWEL_NORMAL;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }
}
