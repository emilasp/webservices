<?php

namespace CoursesBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\TimestampableEntityTrait;
use CoursesBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="CoursesBundle\Repository\CourseAchievementUserRepository")
 */
class CourseAchievementUser implements IEntityCore
{
    use TimestampableEntityTrait;

    /**
     * @Groups({"appDashboard"})
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"appDashboard"})
     *
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\CourseAchievement", inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $achievement;

    /**
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\User", inversedBy="achievementsLink")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAchievement(): ?CourseAchievement
    {
        return $this->achievement;
    }

    public function setAchievement(?CourseAchievement $achievement): self
    {
        $this->achievement = $achievement;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
