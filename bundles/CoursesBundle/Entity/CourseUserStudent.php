<?php

namespace CoursesBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\JsonPropertiesTrait;
use App\Core\Traits\TimestampableEntityTrait;
use CoursesBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Студент курса
 *
 * @ORM\Entity(repositoryClass="CoursesBundle\Repository\CourseUserStudentRepository")
 */
class CourseUserStudent implements IEntityCore
{
    use TimestampableEntityTrait;

    public const STATUS_CURRENT  = 1;
    public const STATUS_FINISH   = 2;
    public const STATUS_NO_START = 5;
    public const STATUS_OVERDUE  = 7;
    public const STATUS_CANCEL   = 9;


    public const STATUSES = [
        self::STATUS_CURRENT  => 'текущий',
        self::STATUS_FINISH   => 'закончен',
        self::STATUS_NO_START => 'не начат',
        self::STATUS_OVERDUE  => 'просрочен',
        self::STATUS_CANCEL   => 'отписан',
    ];

    public const DIFFICULTLY_DEFAULT = 3;

    //Гельштальт история: процент на день, всего пройдено, всего пропущено, пройдено за период, пропущено за период
    public const JF_GESTALT_HISTORY = 'gestaltHistory';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"appDashboard"})
     *
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\Course", inversedBy="students")
     * @ORM\JoinColumn(nullable=false)
     */
    private $course;

    /**
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\User", inversedBy="coursesLink")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * Опыт набранный за курс
     *
     * @Groups({"appDashboard"})
     *
     * @ORM\Column(type="smallint", options={"default": 1})
     */
    private $difficulty = 1;


    /**
     * Количество времени затраченное на обучение в минутах(наш стандарт)
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $duration = 0;

    /**
     * Курс оплачен
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isPaid = false;

    /**
     * Статус прохождения курса:
     *
     * @Groups({"appDashboard"})
     *
     * 0 - не начат, 1 - в процессе, 2 - завершен, 3 - отменен
     * @ORM\Column(type="smallint", options={"default": 1})
     */
    private $status = 1;


    /**
     * Это архивный линк
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isArchive = false;

    /**
     * Дата архивавции
     * @ORM\Column(type="datetime", nullable=true, options={"default": null})
     */
    protected $archiveAt;

    /**
     * Гельштальт история
     * @ORM\Column(type="json", options={"default"="{}"})
     */
    protected $gestaltHistory = [];

    /**
     * @return mixed
     */
    public function getGestaltHistory(): array
    {
        return $this->gestaltHistory;
    }

    /**
     * @param mixed $gestaltHistory
     */
    public function setGestaltHistory(array $gestaltHistory): void
    {
        $this->gestaltHistory = $gestaltHistory;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDifficulty(): ?int
    {
        return $this->difficulty;
    }

    public function setDifficulty(int $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    public function getIsPaid(): ?bool
    {
        return $this->isPaid;
    }

    public function setIsPaid(bool $isPaid): self
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getArchiveAt(): ?\DateTimeInterface
    {
        return $this->archiveAt;
    }

    public function setArchiveAt(?\DateTimeInterface $archiveAt): self
    {
        $this->archiveAt = $archiveAt;

        return $this;
    }
}
