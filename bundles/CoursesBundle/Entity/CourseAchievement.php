<?php

namespace CoursesBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\BlameableEntityTrait;
use App\Core\Traits\TimestampableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FileBundle\Entity\File;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Достижения пользователей
 * - базовые в зависимости от правил(10 уроков/первый курс и тп)
 * - относящиеся к конкретному курсу/тематические(повелитель воображения)
 *
 * @ORM\Entity(repositoryClass="CoursesBundle\Repository\CourseAchievementRepository")
 */
class CourseAchievement implements IEntityCore
{
    use TimestampableEntityTrait;
    use BlameableEntityTrait;

    public const TYPE_SYSTEM    = 1;
    public const TYPE_COURSE    = 2;
    public const TYPE_MODERATOR = 3;

    public const TYPES = [
        self::TYPE_SYSTEM    => 'системный',
        self::TYPE_COURSE    => 'курсовой',
        self::TYPE_MODERATOR => 'модераторский',
    ];

    public const JEWEL_NORMAL    = 1;
    public const JEWEL_RARE      = 2;
    public const JEWEL_LEGENDARY = 3;
    public const JEWEL_MYTHICAL  = 4;

    public const JEWELS = [
        self::JEWEL_NORMAL    => 'обычный',
        self::JEWEL_RARE      => 'редкий',
        self::JEWEL_LEGENDARY => 'легендарный',
        self::JEWEL_MYTHICAL  => 'мифический',
    ];

    /**
     * @Groups({"appDashboard"})
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\Course", inversedBy="achievements")
     */
    private $course;

    /**
     * Правила - пройдено 10 уроков/завершен урок "Воображение"
     * @ORM\Column(type="json", nullable=true)
     */
    private $rules = [];

    /**
     * Тип ачивки
     * 1 - системный: опыт, долгожительность, количество уроков, количество курсов, количество целей, количество челенджей
     * 2 - курсовой: прохождение урока на курсе, прохождение курса
     * 3 - модераторский: за гениальность, за быть вовремя, за активность и т.п.
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="smallint")
     */
    private $type = 1;

    /**
     * Драгоценность/ценность/редкость ачивки
     * 1 - обычный, 2 - редкий, 3 - легендарный, 4 - мифический
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="smallint", options={"default":1})
     */
    private $jewel = 1;


    /**
     * Категория для модераторских
     * @ORM\Column(type="string", length=255)
     */
    private $category;

    /**
     * Наименование/звание/достижение
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * Опыт за достижение(для модераторских и курсовых максимум 5)
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="smallint")
     */
    private $difficulty = 3;

    /**
     * Картинка
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\OneToOne(targetEntity="FileBundle\Entity\File", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="CoursesBundle\Entity\CourseAchievementUser", mappedBy="achievement")
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getRules(): ?array
    {
        return $this->rules;
    }

    public function setRules(?array $rules): self
    {
        $this->rules = $rules;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(File $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getJewel(): ?int
    {
        return $this->jewel;
    }

    public function setJewel(int $jewel): self
    {
        $this->jewel = $jewel;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDifficulty(): ?int
    {
        return $this->difficulty;
    }

    public function setDifficulty(int $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    /**
     * @return Collection|CourseAchievementUser[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(CourseAchievementUser $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setAchievement($this);
        }

        return $this;
    }

    public function removeUser(CourseAchievementUser $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getAchievement() === $this) {
                $user->setAchievement(null);
            }
        }

        return $this;
    }
}
