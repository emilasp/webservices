<?php

namespace CoursesBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\BlameableEntityTrait;
use App\Core\Traits\TimestampableEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Части урока: Текст/Видео/Аудио/Тест/Приложение
 *
 * @ORM\Entity(repositoryClass="CoursesBundle\Repository\CourseLessonPartRepository")
 */
class CourseLessonPart implements IEntityCore
{
    use TimestampableEntityTrait;
    use BlameableEntityTrait;

    public const TYPE_TEXT          = 1;
    public const TYPE_VIDEO_YOUTUBE = 2;
    public const TYPE_AUDIO         = 3;
    public const TYPE_TEST          = 5;
    public const TYPE_CHECKLIST     = 6;

    public const TYPES = [
        self::TYPE_TEXT          => 'Текст',
        self::TYPE_VIDEO_YOUTUBE => 'Видео',
        self::TYPE_AUDIO         => 'Аудио',
        self::TYPE_CHECKLIST     => 'Чеклист',
        self::TYPE_TEST          => 'Тест',
    ];

    /**
     * @Groups({"appLesson"})
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Урок
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\CourseLesson", inversedBy="parts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lesson;

    /**
     * Тип - текст/виде(ссылка)/аудио(ссылка)/чеклист
     *
     * @Groups({"appLesson"})
     *
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * Подзагаловок для части урока
     *
     * @Groups({"appLesson"})
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @Groups({"appLesson"})
     *
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * Ссылка на файл или идентификаторы для теста или приложения
     *
     * @Groups({"appLesson"})
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    public function __clone() {
        if ($this->id) {
            $this->id = null;
        }
    }

    /**
     * @Groups({"appLesson"})
     *
     * @Gedmo\Mapping\Annotation\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLesson(): ?CourseLesson
    {
        return $this->lesson;
    }

    public function setLesson(?CourseLesson $lesson): self
    {
        $this->lesson = $lesson;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }
}
