<?php

namespace CoursesBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\BlameableEntityTrait;
use App\Core\Traits\EntityTrait;
use App\Core\Traits\JsonPropertiesTrait;
use App\Core\Traits\TimestampableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use FileBundle\Entity\File;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Обучающий курс
 *
 * @ORM\Entity(repositoryClass="CoursesBundle\Repository\CourseRepository")
 */
class Course implements IEntityCore
{
    use TimestampableEntityTrait;
    use BlameableEntityTrait;
    use EntityTrait;
    use JsonPropertiesTrait;

    public const TYPE_COURSE        = 1;
    public const TYPE_COURSE_STREAM = 2;
    public const TYPE_CHALLENGE     = 3;
    public const TYPE_INCULCATION   = 4;
    public const TYPE_GOAL          = 5;
    public const TYPE_PROGRAMM      = 9;

    public const TYPES = [
        self::TYPE_COURSE        => 'Курс',
        self::TYPE_COURSE_STREAM => 'Курс(поток)',
        self::TYPE_CHALLENGE     => 'Челендж',
        self::TYPE_PROGRAMM      => 'Программа',
        self::TYPE_INCULCATION   => 'Привычка',
        self::TYPE_GOAL          => 'Цель',
    ];

    public const TYPES_PERIODICAL = [
        self::TYPE_CHALLENGE,
        self::TYPE_INCULCATION,
    ];

    public const STATUS_ACTIVE    = 1;
    public const STATUS_NO_ACTIVE = 2;
    public const STATUS_PRIVATE   = 3;
    public const STATUS_ARCHIVE   = 4;
    public const STATUS_DELETED   = 0;

    public const STATUSES = [
        self::STATUS_ACTIVE    => 'активный',
        self::STATUS_NO_ACTIVE => 'не активный',
        self::STATUS_PRIVATE   => 'приватный',
        self::STATUS_ARCHIVE   => 'архивный',
        self::STATUS_DELETED   => 'удаленный',
    ];

    /** @var array Статусы при которыхх на курсе возможно обучение */
    public const LEAR_ALLOW_STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_PRIVATE,
    ];

    // Гельштальт настройки: maxDifficultly - максимальное значение для 100%, negativeRatio - коэффициент для пропусков
    // gelstalt:{maxDifficultly: 100, negativeRatio:2},
    public const JF_GESTALT                 = '[gestalt]';
    public const JF_GESTALT_MAX_DIFFICULTLY = '[gestalt][maxDifficultly]';
    public const JF_GESTALT_NEGATIVE_RATIO  = '[gestalt][negativeRatio]';

    //use SeoEntityTrait;

    /**
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * Краткое описание - для вывода в списке и рекламе
     * @ORM\Column(type="text", nullable=true)
     */
    private $annotation;

    /**
     * Полное описание - на страницу с информацией о курсе
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * Большое изображение на страницу с информауией о курсе и в промо список
     * @ORM\OneToOne(targetEntity="FileBundle\Entity\File", cascade={"persist", "remove"})
     */
    private $imageBig;

    /**
     * Небольшое изображение в список курсов
     *
     * @Groups({"appDashboard"})
     *
     * @ORM\OneToOne(targetEntity="FileBundle\Entity\File", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * Тип курса: 1 - курс, 2 - челендж, 3 - привычка, 4 - цель
     *
     * @Groups({"appDashboard"})
     *
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * Статус: 1 - активен, 2 - недоступен/закрыт(выводится в списке), 3 - удален(не видим нигде)
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * Блоки/секции: 1 неделя или 1 ступень
     * @ORM\OneToMany(targetEntity="CoursesBundle\Entity\CourseSection", mappedBy="course", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     *
     */
    private $courseSections;

    /**
     * Уроки
     *
     * @Groups({"appDashboard"})
     *
     * @ORM\OneToMany(targetEntity="CoursesBundle\Entity\CourseLesson", mappedBy="course")
     * @ORM\OrderBy({"startAt" = "ASC", "position" = "ASC"})
     */
    private $lessons;


    /**
     * @ORM\OneToMany(targetEntity="CoursesBundle\Entity\CourseUserStudent", mappedBy="course", cascade={"persist"})
     */
    private $students;

    /**
     * @ORM\OneToMany(targetEntity="CoursesBundle\Entity\CourseAchievement", mappedBy="course")
     */
    private $achievements;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $commercial = 0;


    /**
     * @Groups({"appDashboard"})
     *
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\Course", inversedBy="courses")
     */
    private $programm;

    /**
     *
     * @ORM\OneToMany(targetEntity="CoursesBundle\Entity\Course", mappedBy="programm")
     */
    private $courses;

    /**
     * @ORM\ManyToMany(targetEntity="CoursesBundle\Entity\User", inversedBy="moderateCourses")
     */
    private $moderators;

    public function __construct()
    {
        $this->courseSections = new ArrayCollection();
        $this->lessons        = new ArrayCollection();
        $this->achievements   = new ArrayCollection();
        $this->students       = new ArrayCollection();
        $this->courses        = new ArrayCollection();
        $this->moderators = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAnnotation(): ?string
    {
        return $this->annotation;
    }

    public function setAnnotation(?string $annotation): self
    {
        $this->annotation = $annotation;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function getImageBig(): ?File
    {
        return $this->imageBig;
    }

    public function setImageBig(?File $image): self
    {
        $this->imageBig = $image;

        return $this;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(?File $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }


    public function getMaxDifficultly(): ?int
    {
        return $this->getJsonProperty(self::JF_GESTALT_MAX_DIFFICULTLY);
    }

    public function setMaxDifficultly(int $value = null)
    {
        $this->setJsonProperty(self::JF_GESTALT_MAX_DIFFICULTLY, $value);
    }
    public function getNegativeRatio(): ?int
    {
        return $this->getJsonProperty(self::JF_GESTALT_NEGATIVE_RATIO);
    }

    public function setNegativeRatio(int $value = null)
    {
        $this->setJsonProperty(self::JF_GESTALT_NEGATIVE_RATIO, $value);
    }
    /**
     * JSON properties END
     */


    /**
     * @return Collection|CourseSection[]
     */
    public function getSections(): Collection
    {
        $criteria = new Criteria(null, ['position' => 'ASC']);
        return $this->courseSections->matching($criteria);
    }

    public function addSection(CourseSection $courseSection): self
    {
        if (!$this->courseSections->contains($courseSection)) {
            $this->courseSections[] = $courseSection;
            $courseSection->setCourse($this);
        }

        return $this;
    }

    public function removeSection(CourseSection $courseSection): self
    {
        if ($this->courseSections->contains($courseSection)) {
            $this->courseSections->removeElement($courseSection);
            // set the owning side to null (unless already changed)
            if ($courseSection->getCourse() === $this) {
                $courseSection->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CourseLesson[]
     */
    public function getLessons(): Collection
    {
        return $this->lessons;
    }

    public function addLesson(CourseLesson $lesson): self
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons[] = $lesson;
            $lesson->setCourse($this);
        }

        return $this;
    }

    public function removeLesson(CourseLesson $lesson): self
    {
        if ($this->lessons->contains($lesson)) {
            $this->lessons->removeElement($lesson);
            // set the owning side to null (unless already changed)
            if ($lesson->getCourse() === $this) {
                $lesson->setCourse(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|CourseAchievement[]
     */
    public function getAchievements(): Collection
    {
        return $this->achievements;
    }

    public function addAchievement(CourseAchievement $achievement): self
    {
        if (!$this->achievements->contains($achievement)) {
            $this->achievements[] = $achievement;
            $achievement->setCourse($this);
        }

        return $this;
    }

    public function removeAchievement(CourseAchievement $achievement): self
    {
        if ($this->achievements->contains($achievement)) {
            $this->achievements->removeElement($achievement);
            // set the owning side to null (unless already changed)
            if ($achievement->getCourse() === $this) {
                $achievement->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CourseSection[]
     */
    public function getCourseSections(): Collection
    {
        return $this->courseSections;
    }

    public function addCourseSection(CourseSection $courseSection): self
    {
        if (!$this->courseSections->contains($courseSection)) {
            $this->courseSections[] = $courseSection;
            $courseSection->setCourse($this);
        }

        return $this;
    }

    public function removeCourseSection(CourseSection $courseSection): self
    {
        if ($this->courseSections->contains($courseSection)) {
            $this->courseSections->removeElement($courseSection);
            // set the owning side to null (unless already changed)
            if ($courseSection->getCourse() === $this) {
                $courseSection->setCourse(null);
            }
        }

        return $this;
    }

    public function getCommercial(): ?bool
    {
        return $this->commercial;
    }

    public function setCommercial(bool $commercial): self
    {
        $this->commercial = $commercial;

        return $this;
    }


    /**
     * @return Collection|CourseUserStudent[]
     */
    public function getStudents(): Collection
    {
        $collection = new ArrayCollection();

        /** @var CourseUserStudent $student */
        foreach ($this->students as $student) {
            $collection->add($student->getUser());
        }

        return $collection;
    }

    public function addStudent(User $student): self
    {
        if (!$this->getStudents()->contains($student)) {
            $courseUser = new CourseUserStudent();
            $courseUser->setCourse($this);
            $courseUser->setUser($student);
            $this->students->add($courseUser);
        }

        return $this;
    }

    public function removeStudent(User $student): self
    {
        if ($this->getStudents()->contains($student)) {
            /** @var CourseUserStudent $student */
            foreach ($this->students as $courseUser) {
                if ($courseUser->getUser() === $student) {
                    $this->students->removeElement($courseUser);
                    $courseUser->setUser(null);
                }
            }
        }

        return $this;
    }

    public function getProgramm(): ?self
    {
        return $this->programm;
    }

    public function setProgramm(?self $programm): self
    {
        $this->programm = $programm;

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): ?Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setProgramm($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
            // set the owning side to null (unless already changed)
            if ($course->getProgramm() === $this) {
                $course->setProgramm(null);
            }
        }

        return $this;
    }

    /**
     * Чекаем что курс periodical
     *
     * @return bool
     */
    public function isPeriodical(): bool
    {
        return in_array($this->getType(), self::TYPES_PERIODICAL);
    }

    public function isProgramm()
    {
        return $this->type === self::TYPE_PROGRAMM;
    }

    /**
     * @return Collection|User[]
     */
    public function getModerators(): Collection
    {
        return $this->moderators;
    }

    public function addModerator(User $moderator): self
    {
        if (!$this->moderators->contains($moderator)) {
            $this->moderators[] = $moderator;
        }

        return $this;
    }

    public function removeModerator(User $moderator): self
    {
        if ($this->moderators->contains($moderator)) {
            $this->moderators->removeElement($moderator);
        }

        return $this;
    }
}
