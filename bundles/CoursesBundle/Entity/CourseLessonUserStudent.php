<?php

namespace CoursesBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\TimestampableEntityTrait;
use CoursesBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Прохождение урока студентом
 *
 * @ORM\Entity(repositoryClass="CoursesBundle\Repository\CourseLessonUserStudentRepository")
 */
class CourseLessonUserStudent implements IEntityCore
{
    use TimestampableEntityTrait;

    public const STATUS_OVERDUE = 0;
    public const STATUS_CURRENT = 1;
    public const STATUS_FINISH  = 2;
    public const STATUS_NEW     = 3;
    public const STATUS_CANCEL  = 9;


    public const STATUSES = [
        self::STATUS_CURRENT => 'текущий',
        self::STATUS_FINISH  => 'закончен',
        self::STATUS_NEW     => 'не начат',
        self::STATUS_OVERDUE => 'просрочен',
        self::STATUS_CANCEL  => 'отписан',
    ];


    /**
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\CourseLesson", inversedBy="students")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $lesson;

    /**
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\User", inversedBy="lessonsLink")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;

    /**
     * Ответ на урок
     *
     * @Groups({"appLesson"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $answer;

    /**
     * Результат прохождения урока
     * @ORM\Column(type="json")
     */
    protected $result = [];

    /**
     * Статус прохождения урока
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * 1 - пройден, 2 - в процессе
     * @ORM\Column(type="smallint")
     */
    protected $status;

    /**
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="datetime", nullable=true, options={"default": null})
     */
    protected $startAt;

    /**
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="datetime", nullable=true, options={"default": null})
     */
    protected $finishAt;


    /**
     * Это архивный линк
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isArchive = false;

    /**
     * Дата архивавции
     * @ORM\Column(type="datetime", nullable=true, options={"default": null})
     */
    protected $archiveAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLesson(): ?CourseLesson
    {
        return $this->lesson;
    }

    public function setLesson(?CourseLesson $lesson)
    {
        $this->lesson = $lesson;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user)
    {
        $this->user = $user;
    }

    public function getResult(): ?array
    {
        return $this->result;
    }

    public function setResult(array $result): self
    {
        $this->result = $result;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getFinishAt(): ?\DateTimeInterface
    {
        return $this->finishAt;
    }

    public function setFinishAt(?\DateTimeInterface $finishAt): self
    {
        $this->finishAt = $finishAt;

        return $this;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(?string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function getStartAt(): ?\DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(?\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getArchiveAt(): ?\DateTimeInterface
    {
        return $this->archiveAt;
    }

    public function setArchiveAt(?\DateTimeInterface $archiveAt): self
    {
        $this->archiveAt = $archiveAt;

        return $this;
    }
}
