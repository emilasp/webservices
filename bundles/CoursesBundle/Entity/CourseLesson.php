<?php

namespace CoursesBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\BlameableEntityTrait;
use App\Core\Traits\SeoEntityTrait;
use App\Core\Traits\TimestampableEntityTrait;
use CoursesBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FileBundle\Entity\File;
use SocialBundle\Entity\Comment;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Урок курса
 *
 * @ORM\Entity(repositoryClass="CoursesBundle\Repository\CourseLessonRepository")
 */
class CourseLesson implements IEntityCore
{
    use TimestampableEntityTrait;
    use BlameableEntityTrait;
    //use SeoEntityTrait;

    public const STATUS_ACTIVE    = 1;
    public const STATUS_NO_ACTIVE = 2;
    public const STATUS_DELETED   = 3;

    public const STATUSES = [
        self::STATUS_ACTIVE    => 'активный',
        self::STATUS_NO_ACTIVE => 'не активный',
        self::STATUS_DELETED   => 'удаленный',
    ];


    public const ADMISSION_TYPE_AFTER_PREV_LESSON          = 1;
    public const ADMISSION_TYPE_AFTER_PREV_LESSON_NEXT_DAY = 2;
    public const ADMISSION_TYPE_ON_DATE_START              = 3;

    public const ADMISSION_TYPES = [
        self::ADMISSION_TYPE_AFTER_PREV_LESSON          => 'Сразу же после предыдущего урока',
        self::ADMISSION_TYPE_AFTER_PREV_LESSON_NEXT_DAY => 'На следующий день после предыдущего урока',
        self::ADMISSION_TYPE_ON_DATE_START              => 'По дате',
    ];

    public const ANSWER_TYPE_TEXT = 1;
    public const ANSWER_TYPE_FILE = 2;
    public const ANSWER_TYPE_ALL  = 3;
    public const ANSWER_TYPE_NONE = 6;

    public const ANSWER_TYPES = [
        self::ANSWER_TYPE_TEXT => 'Текст',
        self::ANSWER_TYPE_FILE => 'Файл',
        self::ANSWER_TYPE_ALL  => 'Все',
        self::ANSWER_TYPE_NONE => 'нет',
    ];

    public const FINISH_TYPE_FAST         = 1;
    public const FINISH_TYPE_FAST_COMMENT = 2;
    public const FINISH_TYPE_FULL         = 3;

    public const FINISH_TYPES = [
        self::FINISH_TYPE_FAST         => 'Сразу',
        self::FINISH_TYPE_FAST_COMMENT => 'Сразу + комментарий',
        self::FINISH_TYPE_FULL         => 'Через урок',
    ];

    /**
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Курс
     *
     * @Groups({"appLesson"})
     *
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\Course", inversedBy="lessons")
     * @ORM\JoinColumn(nullable=false)
     */
    private $course;

    /**
     * Секция
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\CourseSection", inversedBy="lessons")
     */
    private $section;

    /**
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="FileBundle\Entity\File", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * Цель урока
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="text")
     */
    private $purpose;

    /**
     * Что в студенте станет лучше после прохождения урока
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bested;

    /**
     * Вводная часть
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $entry;

    /**
     * Заключение/выводы
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $conclusion;

    /**
     * Дата урока/действия
     *
     * @Groups({"appDashboard"})
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startAt;

    /**
     * Время действия урока
     * @ORM\Column(type="smallint")
     */
    private $endPeriod = 1;

    /**
     * Статус урока: 1 - активный, 2 - не активный, 3 - удален
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * Верные ответы на вопросы в конце урока
     *
     * @Groups({"appLesson"})
     *
     * @ORM\Column(type="json")
     */
    private $answers = [];

    /**
     * Сложность урока - она же опыт. От 1 до 10
     *
     * @Groups({"appLesson"})
     *
     * @ORM\Column(type="smallint", options={"default": 3})
     */
    private $difficulty = 3;

    /**
     * Тип доступа к уроку после прохождения предыдущего: 1 - after prev|2 - after prev nex dey|3 - по дате
     *
     * @ORM\Column(type="smallint")
     */
    private $typeAdmission;

    /**
     * Тип ответа: 1 - text| 2 - file| 3 - all| 0 - none
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="smallint")
     */
    private $typeAnswer;

    /**
     * Тип завершения: 1 - сразу| 2 - сразу(с комментарием)| 3 - через просмотр урока
     *
     * @Groups({"appLesson", "appDashboard"})
     *
     * @ORM\Column(type="smallint")
     */
    private $typeFinish = 1;


    /**
     * Чеклист
     *
     * @Groups({"appLesson"})
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $checklist = [];

    /**
     * Период для рассчета дат исполнения
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $periodic = [];

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $commercial = 0;

    /**
     * @Groups({"appLesson"})
     *
     * @ORM\Column(type="smallint")
     */
    private $duration;

    /**
     *
     * @Groups({"appDashboard"})
     *
     * @Gedmo\Mapping\Annotation\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;


    /**
     * Части урока
     *
     * @Groups({"appLesson"})
     *
     * @ORM\OneToMany(targetEntity="CoursesBundle\Entity\CourseLessonPart", mappedBy="lesson",cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $parts;

    /**
     * @ORM\OneToMany(targetEntity="CoursesBundle\Entity\CourseLessonUserStudent", mappedBy="lesson",
     *                                                                             cascade={"persist"})
     */
    private $students;

    public function __construct()
    {
        $this->parts    = new ArrayCollection();
        $this->students = new ArrayCollection();
        $this->startAt  = new \DateTime();
    }

    public function __clone()
    {
        if ($this->id) {
            $this->id    = null;
            $this->image = null;
            $this->parts = clone $this->parts;
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getSection(): ?CourseSection
    {
        return $this->section;
    }

    public function setSection(?CourseSection $section): self
    {
        $this->section = $section;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(?File $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getPurpose(): ?string
    {
        return $this->purpose;
    }

    public function setPurpose(string $purpose): self
    {
        $this->purpose = $purpose;

        return $this;
    }

    public function getBested(): ?string
    {
        return $this->bested;
    }

    public function setBested(?string $bested): self
    {
        $this->bested = $bested;

        return $this;
    }

    public function getEntry(): ?string
    {
        return $this->entry;
    }

    public function setEntry(?string $entry): self
    {
        $this->entry = $entry;

        return $this;
    }

    public function getConclusion(): ?string
    {
        return $this->conclusion;
    }

    public function setConclusion(?string $conclusion): self
    {
        $this->conclusion = $conclusion;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAnswers()
    {
        return $this->answers;
    }

    public function setAnswers($answers): self
    {
        $this->answers = $answers;

        return $this;
    }

    public function getChecklist(): ?array
    {
        return $this->checklist;
    }

    public function setChecklist(?array $checklist): self
    {
        $this->checklist = $checklist;

        return $this;
    }

    public function getDifficulty(): ?int
    {
        return $this->difficulty;
    }

    public function setDifficulty(int $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    public function getTypeAdmission(): ?int
    {
        return $this->typeAdmission;
    }

    public function setTypeAdmission(int $typeAdmission): self
    {
        $this->typeAdmission = $typeAdmission;

        return $this;
    }

    public function getTypeAnswer(): ?int
    {
        return $this->typeAnswer;
    }

    public function setTypeAnswer(int $typeAnswer): self
    {
        $this->typeAnswer = $typeAnswer;

        return $this;
    }

    public function getCommercial(): ?bool
    {
        return $this->commercial;
    }

    public function setCommercial(bool $commercial): self
    {
        $this->commercial = $commercial;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTypeFinish(): int
    {
        return $this->typeFinish;
    }

    /**
     * @param mixed $typeFinish
     */
    public function setTypeFinish(int $typeFinish): void
    {
        $this->typeFinish = $typeFinish;
    }


    /**
     * @return Collection|CourseLessonPart[]
     */
    public function getParts(): Collection
    {
        return $this->parts;
    }

    public function addPart(CourseLessonPart $part): self
    {
        if (!$this->parts->contains($part)) {
            $this->parts[] = $part;
            $part->setLesson($this);
        }

        return $this;
    }

    public function removePart(CourseLessonPart $part): self
    {
        if ($this->parts->contains($part)) {
            $this->parts->removeElement($part);
            // set the owning side to null (unless already changed)
            if ($part->getLesson() === $this) {
                $part->setLesson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CourseUserStudent[]
     */
    public function getStudents(): Collection
    {
        $collection = new ArrayCollection();

        /** @var CourseUserStudent $student */
        foreach ($this->students as $student) {
            $collection->add($student->getUser());
        }

        return $collection;
    }

    public function addStudent(User $student): self
    {
        if (!$this->getStudents()->contains($student)) {
            $lessonUser = new CourseLessonUserStudent();
            $lessonUser->setLesson($this);
            $lessonUser->setUser($student);
            $this->students->add($lessonUser);
        }

        return $this;
    }

    public function removeStudent(User $student): self
    {
        if ($this->getStudents()->contains($student)) {
            /** @var CourseUserStudent $student */
            foreach ($this->students as $lessonUser) {
                if ($lessonUser->getUser() === $student) {
                    $this->students->removeElement($lessonUser);
                    $lessonUser->setUser(null);
                }
            }
        }

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getStartAt(): ?\DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeInterface $startAt = null): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndPeriod(): ?int
    {
        return $this->endPeriod;
    }

    public function setEndPeriod(int $endPeriod): self
    {
        $this->endPeriod = $endPeriod;

        return $this;
    }

    public function getPeriodic(): ?array
    {
        return $this->periodic;
    }

    public function setPeriodic(?array $periodic): self
    {
        $this->periodic = $periodic;

        return $this;
    }
}
