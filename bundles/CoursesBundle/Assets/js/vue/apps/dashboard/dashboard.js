'use strict';

import Vue from 'vue';
import DashboardApp from "./DashboardApp.vue";
import Vuelidate from "vuelidate";
import VModal from 'vue-js-modal'

Vue.use(VModal, { componentName: "modal" });
Vue.use(Vuelidate);

Vue.config.productionTip = false;

require('./css/app.scss');

new Vue(DashboardApp);
