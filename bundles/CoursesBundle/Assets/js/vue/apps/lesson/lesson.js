'use strict';

import Vue from 'vue';
import VueYoutube from 'vue-youtube'
import Vuelidate from 'vuelidate'
import AppLesson from "./LessonApp.vue";
import VModal from 'vue-js-modal'

Vue.config.productionTip = false;

Vue.use(VueYoutube);
Vue.use(Vuelidate);
Vue.use(VModal, { componentName: "modal" });

require('./css/lesson.scss');

new Vue(AppLesson);

