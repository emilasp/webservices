<?php

namespace CoursesBundle\Controller\auditorium;

use App\Core\Abstracts\AController;
use CoursesBundle\Entity\CourseLesson;
use CoursesBundle\Entity\CourseLessonUserStudent;
use CoursesBundle\Form\UserLessonLearnType;
use CoursesBundle\Security\LessonAuditoriumVoter;
use CoursesBundle\Service\CourseUserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\LogicException;

/**
 * Class LessonController
 *
 * @Route("/auditorium/lessons")
 *
 * @Security("is_granted('ROLE_USER')")
 *
 * @package App\Controller
 */
class LessonController extends AController
{
    /**
     * Учим урок курса
     *
     * @Route("/learn/{lessonLink}", name="course_auditorium_lesson_learn")
     *
     * @Template(template="/Lesson/auditorium/learn.html.twig")
     *
     * @param CourseLessonUserStudent $lessonLink
     * @param CourseUserService       $courseUserService
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function learn(CourseLessonUserStudent $lessonLink, CourseUserService $courseUserService)
    {
        $lesson = $lessonLink->getLesson();

        $this->denyAccessUnlessGranted(LessonAuditoriumVoter::VIEW, $lesson);

        /** @var $lessonLink CourseLessonUserStudent */
        if (!$lessonLink && !$lessonLink = $this->getUser()->getLessonLink($lesson, CourseLessonUserStudent::STATUS_CURRENT)) {
            throw new LogicException('Данный урок пока недоступен');
        }

        $course = $lesson->getCourse();

        if (!in_array($lessonLink->getStatus(), [CourseLessonUserStudent::STATUS_CURRENT, CourseLessonUserStudent::STATUS_FINISH])) {
            return $this->redirectToRoute('course_auditorium_learn', ['course' => $course->getId()]);
        }
        if ($lessonLink->getStartAt()->format('Y-m-d') > (new \DateTime())->format('Y-m-d')) {
            return $this->redirectToRoute('course_auditorium_learn', ['course' => $course->getId()]);
        }

        $this->addBreadcrumbs([
            'Главная'          => 'index',
            'Курсы'            => 'courses',
            $course->getName() => ['course_auditorium_learn', ['course' => $course->getId()]],
            $lesson->getName()
        ]);


        $user           = $this->getUser();
        $userCourseLink = $user->getCourseLink($lesson->getCourse());
        $courseData     = $courseUserService->getCourseData($userCourseLink);
        $todayLink      = $courseUserService->getUserTodayCourseLessonLink($this->getUser(), $lesson->getCourse());
        $nextLink       = $courseUserService->getNextLessonLink($todayLink);
        $prevLink       = $courseUserService->getPrevLessonLink($todayLink);

        return [
            'lesson'     => $lesson,
            'lessonLink' => $lessonLink,
            'courseData' => $courseData,
            'nextLink'   => $nextLink,
            'prevLink'   => $prevLink,
        ];
    }

    /**
     * Текущий урок курса
     *
     * @Route("/view/{lessonLink}", name="course_auditorium_lesson_view")
     *
     * @param CourseLessonUserStudent $lessonLink
     * @param CourseUserService       $courseUserService
     *
     * @Template(template="/Lesson/auditorium/view.html.twig")
     *
     * @param CourseLessonUserStudent $lessonLink
     * @param CourseUserService       $courseUserService
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function view(CourseLessonUserStudent $lessonLink, CourseUserService $courseUserService)
    {
        $this->denyAccessUnlessGranted(LessonAuditoriumVoter::VIEW, $lessonLink->getLesson());

        $lesson = $lessonLink->getLesson();
        $course = $lesson->getCourse();

        $currentLink = $courseUserService->getUserCurrentCourseLessonLink($this->getUser(), $course);
        if ($currentLink && $currentLink->getLesson() === $lesson) {
            return $this->redirectToRoute('course_auditorium_lesson_learn', ['lessonLink' => $lessonLink->getId()]);
        }

        $this->addBreadcrumbs([
            'Главная'          => 'index',
            'Курсы'            => 'courses',
            $course->getName() => ['course_auditorium_learn', ['course' => $course->getId()]],
            $lesson->getName()
        ]);

        return [
            'lesson' => $lesson,
        ];
    }
}
