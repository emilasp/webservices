<?php

namespace CoursesBundle\Controller\auditorium;

use App\Core\Abstracts\AController;
use CoursesBundle\Entity\CourseLesson;
use CoursesBundle\Entity\CourseLessonUserStudent;
use CoursesBundle\Entity\User;
use CoursesBundle\Event\LessonUserEvent;
use CoursesBundle\Repository\CourseLessonRepository;
use CoursesBundle\Service\AchievementUserService;
use CoursesBundle\Service\CourseApiService;
use CoursesBundle\Service\CourseUserService;
use CoursesBundle\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LessonAppController
 *
 * @Route("/auditorium/lessons")
 *
 * @Security("is_granted('ROLE_USER')")
 *
 * @package App\Controller
 */
class ApiController extends AController
{
    /**
     * Учим урок курса
     *
     * @Route("/auditorium/lessons/api/dashboard", name="course_auditorium_dashboard_api")
     *
     * @param CourseApiService $courseApiService
     *
     * @return Response
     * @throws \Exception
     */
    public function dashboardApi(CourseApiService $courseApiService): Response
    {
        $data = $this->get('serializer')->serialize(
            $courseApiService->getDataForDashboard($this->getUser()),
            'json',
            ['groups' => ['appDashboard']]
        );
        return new Response($data);
    }

    /**
     * Учим урок курса
     *
     * @Route("/api/learn/{lessonLink}", name="course_auditorium_lesson_learn_api")
     *
     * @param CourseLessonUserStudent $lessonLink
     *
     * @return Response
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function learnApi(CourseLessonUserStudent $lessonLink): Response
    {
        $data = $this->get('serializer')->serialize([
            'lesson'     => $lessonLink->getLesson(),
            'lessonLink' => $lessonLink
        ], 'json', ['groups' => ['appLesson']]);
        return new Response($data);
    }


    /**
     * Ответ по уроку курса
     *
     * @Route("/api/learn/lesson/answer", name="course_auditorium_lesson_learn_api_answer")
     *
     * @param Request                  $request
     * @param CourseLessonRepository   $lessonRepository
     * @param CourseUserService        $courseUserService ,
     * @param EventDispatcherInterface $eventDispatcher
     *
     * @return Response
     * @throws \Exception
     */
    public function learnAnswerApi(
        Request $request,
        CourseLessonRepository $lessonRepository,
        CourseUserService $courseUserService,
        UserService $userService,
        EventDispatcherInterface $eventDispatcher
    ): Response {
        $data = json_decode($request->getContent());

        $lesson = $lessonRepository->find($data->id);
        /** @var User $user */
        $user          = $this->getUser();
        $userPrevStats = $userService->getStats($user);


        $link = $courseUserService->getUserCurrentCourseLessonLink($user, $lesson->getCourse());
        $link->setStatus(CourseLessonUserStudent::STATUS_FINISH);
        $link->setAnswer($data->answer);
        $link->setFinishAt(new \DateTime());

        $this->getEm()->flush($link);

        $eventDispatcher->dispatch(LessonUserEvent::NAME_USER_LESSON_FINISH, new LessonUserEvent($lesson, $user));

        $congratulationData           = $userService->getCongratulationData($user, $userPrevStats);
        $congratulationData['lesson'] = $lesson;

        $data = $this->get('serializer')->serialize(['congratulation' => $congratulationData], 'json', ['groups' => ['appLesson']]);

        return new Response($data);
    }
}
