<?php

namespace CoursesBundle\Controller\auditorium;

use App\Core\Abstracts\AController;
use App\Service\EntityService;
use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseLesson;
use CoursesBundle\Event\CourseEvent;
use CoursesBundle\Event\CourseUserEvent;
use CoursesBundle\Form\CourseType;
use CoursesBundle\Form\UserLessonLearnType;
use CoursesBundle\Repository\CourseRepository;
use CoursesBundle\Security\CourseVoter;
use CoursesBundle\Security\LessonAuditoriumVoter;
use CoursesBundle\Service\CourseUserService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManagerInterface;
use FileBundle\Service\FileService;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class CourseController
 *
 * @Route("/courses/auditorium")
 *
 * @Security("is_granted('ROLE_USER')")
 *
 * @package App\Controller
 */
class CourseController extends AController
{
    /**
     * Выводим все активные курсы
     *
     * @Route("/dashboard", name="courses_auditorium_dashboard")
     *
     * @param Request $request
     *
     * @Template(template="/Course/auditorium/dashboard.html.twig")
     * @return array
     */
    public function dashboard(
        Request $request,
        PaginatorInterface $paginator,
        CourseRepository $courseRepository,
        CourseUserService $courseService
    ) {
        $this->addBreadcrumbs(['Главная' => 'index', 'Courses', "Мой университет"]);

        $user      = $this->getUser();
        $myCourses = $user->getCoursesLink();
        $courses   = array_map(function ($entity) {
            return $entity->getCourse();
        }, $myCourses->getValues());


        $queryBuilder = $courseRepository->createQueryBuilder('base');
        $queryBuilder->andWhere('base.status = :status')->setParameter('status', Course::STATUS_ACTIVE);
        $queryBuilder->andWhere('base IN (:courses)')->setParameter('courses', $courses, Type::SIMPLE_ARRAY);

        $pagination = $paginator->paginate($queryBuilder, $request->query->getInt('page', 1), 10);

        return [
            'courseService' => $courseService,
            'pagination'    => $pagination
        ];
    }


    /**
     * Стартуем курс
     *
     * @Route("/start/{course}", name="course_auditorium_start")
     *
     * @param Course                   $course
     * @param CourseUserService        $courseUserService
     * @param EventDispatcherInterface $eventDispatcher
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function start(Course $course, CourseUserService $courseUserService, EventDispatcherInterface $eventDispatcher)
    {
        $user = $this->getUser();

        /** Подписываем пользователя на курс */
        $courseUserService->subscribeUserOnCourse($user, $course);

        $eventDispatcher->dispatch(CourseUserEvent::NAME_COURSE_USER_START, new CourseUserEvent($course, $user));

        $this->addFlash('success', 'Поздравляем с успешным поступлением на наш волшебный курс!');

        return $this->redirectToRoute('course_auditorium_learn', ['course' => $course->getId()]);
    }


    /**
     * Стартуем курс
     *
     * @Route("/exit/{course}", name="course_auditorium_exit")
     *
     * @param Course                   $course
     * @param CourseUserService        $courseUserService
     * @param EventDispatcherInterface $eventDispatcher
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function exit(Course $course, CourseUserService $courseUserService, EventDispatcherInterface $eventDispatcher)
    {
        $user = $this->getUser();

        /** Отписываем пользователя от курса */
        $courseUserService->unSubscribeUserOnCourse($user, $course);

        $eventDispatcher->dispatch(CourseUserEvent::NAME_COURSE_USER_CANCEL, new CourseUserEvent($course, $user));

        $this->addFlash('success', 'Вы отчислились от курса.');

        return $this->redirectToRoute('courses');
    }


    /**
     * Страница курса для ученика
     *
     * @Route("/learn/{course}", name="course_auditorium_learn")
     *
     * @IsGranted("ROLE_USER")
     *
     * @Template(template="/Course/auditorium/learn.html.twig")
     *
     * @param CourseUserService $courseService
     * @param Course            $course
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function learnView(CourseUserService $courseService, Course $course)
    {
        $this->addBreadcrumbs(['Главная' => 'index', 'Courses' => 'courses', "Обучение на курсе"]);

        $user = $this->getUser();
        if ($user && !$courseService->hasUserCourse($user, $course)) {
            $this->addFlash('danger', 'Вы не являетесь слушателем этого курса!');

            return $this->redirectToRoute('course_view', ['course' => $course->getId()]);
        }

        if (!$currentLink = $courseService->getUserCurrentCourseLessonLink($user, $course)) {
            $this->addFlash('danger', 'Вы не являетесь слушателем этого курса!');

            return $this->redirectToRoute('course_view', ['course' => $course->getId()]);
        }

        return [
            'course'        => $course,
            'currentLesson' => $currentLink ? $currentLink->getLesson() : null
        ];
    }


    /**
     * Переходим на текущий урок курса
     *
     * @Route("/learn/lesson/{course}", name="course_auditorium_learn_lesson")
     *
     * @IsGranted("ROLE_USER")
     *
     * @param CourseUserService $courseService
     * @param Course            $course
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function learnLesson(CourseUserService $courseService, Course $course)
    {
        $this->addBreadcrumbs(['Главная' => 'index', 'Courses' => 'courses', "Список"]);

        $user = $this->getUser();
        if ($user && !$courseService->hasUserCourse($user, $course)) {
            $this->addFlash('danger', "Запишитесь на курс, что бы начать обучение");

            return $this->redirectToRoute('course_view', ['course' => $course]);
        }

        if (!$currentLink = $courseService->getUserCurrentCourseLessonLink($user, $course)) {
            $this->addFlash('danger', "На этом курсе отвутствует текущий урок");

            return $this->redirectToRoute('course_view', ['course' => $course->getId()]);
        }

        return $this->redirectToRoute('course_auditorium_lesson_learn', ['lessonLink' => $currentLink->getId()]);
    }

    /**
     * Строим дерево уроков
     *
     * @Route("/tree/{course}", name="course_auditorium_tree")
     *
     * @IsGranted("ROLE_USER")
     *
     * @Template(template="/Course/auditorium/part.tree.html.twig")
     * @param CourseUserService $courseService
     * @param Course            $course
     *
     * @return array
     */
    public function getTreePartAction(Course $course, CourseUserService $courseService): array
    {
        if (!$courseService->hasUserCourse($this->getUser(), $course)) {
            throw new AccessDeniedException('Вы не являетесь учеником данного курса');
        }

        return [
            'course' => $course
        ];
    }

}
