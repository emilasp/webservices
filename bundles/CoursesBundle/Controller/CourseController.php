<?php

namespace CoursesBundle\Controller;

use App\Core\Abstracts\AController;
use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseLessonPart;
use CoursesBundle\Repository\CourseRepository;
use CoursesBundle\Service\CourseService;
use CoursesBundle\Service\CourseUserService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 *
 * @Route("/courses")
 *
 * @package App\Controller
 */
class CourseController extends AController
{
    /**
     * Выводим все активные курсы для гостя|не подписанного пользователя
     *
     * @Route("/", name="courses")
     *
     * @param Request $request
     *
     * @Template(template="/Course/index.html.twig")
     *
     * @param Request            $request
     * @param PaginatorInterface $paginator
     * @param CourseRepository   $courseRepository
     * @param CourseUserService  $courseService
     * @param int|null           $type
     *
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function indexAction(
        Request $request,
        PaginatorInterface $paginator,
        CourseRepository $courseRepository,
        CourseUserService $courseService,
        int $type = null
    ) {
        $this->addBreadcrumbs(['Главная' => 'index', 'Courses', "Список"]);

        $dataTabs = [];
        foreach (array_keys(Course::TYPES) as $courseType) {
            $queryBuilder   = clone $courseRepository->getAllowCoursesQueryBilder([$courseType], $this->getUser());

            if (!$type && $queryBuilder->getQuery()->getResult()) {
                $type = $courseType;
            }

            $paginatorNew   = clone $paginator;
            $paginatorAlias = 'page_' . $courseType;
            $paginatorNew->setDefaultPaginatorOptions(array(
                'pageParameterName' => $paginatorAlias,
            ));

            $dataTabs[$courseType] = [
                'pagination' => $paginatorNew->paginate(
                    $queryBuilder,
                    $request->query->getInt($paginatorAlias, 1),
                    10,
                    ['alias' => $paginatorAlias]
                )
                ,
            ];
        }

        return [
            'type'          => $type,
            'courseService' => $courseService,
            'dataTabs'      => $dataTabs
        ];
    }


    /**
     * Просмотр курса гостем
     *
     * @Route("/view/{course}", name="course_view")
     *
     * @param Course        $course
     * @param CourseService $courseService
     *
     * @Template(template="/Course/view.html.twig")
     * @return array
     */
    public function view(Course $course, CourseService $courseService)
    {
        $this->addBreadcrumbs(['Главная' => 'index', 'Courses' => 'courses', $course->getName()]);


        return [
            'course' => $course,
            'videos' => $courseService->getCourseLessonParts($course, CourseLessonPart::TYPE_VIDEO_YOUTUBE),
            'tests'  => $courseService->getCourseLessonParts($course, CourseLessonPart::TYPE_TEST),
            'audios' => $courseService->getCourseLessonParts($course, CourseLessonPart::TYPE_AUDIO),
            'checks' => $courseService->getCourseLessonParts($course, CourseLessonPart::TYPE_CHECKLIST),
        ];
    }
}
