<?php

namespace CoursesBundle\Controller\manage;

use App\Core\Abstracts\AController;
use App\Service\EntityService;
use CoursesBundle\Entity\CourseAchievement;
use CoursesBundle\Form\CourseAchievementType;
use CoursesBundle\Repository\CourseAchievementRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * CourseAchievementController *
 * @Route("/course/achievement")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class CourseAchievementController extends AController
{
    /**
     * Список
     *
     * @Route("/", name="course_achievement_index", methods={"GET"})
     */
    public function indexAction(
        CourseAchievementRepository $courseAchievementRepository,
        Request $request,
        PaginatorInterface $paginator
    ): Response {
        $this->addBreadcrumbs(['Главная' => 'index', 'CourseAchievement', "Список"]);

        $queryBuilder = $courseAchievementRepository->createQueryBuilder('base');
        $pagination   = $paginator->paginate($queryBuilder, $request->query->getInt('page', 1), 10);

        return $this->render('CourseAchievement/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }


    /**
     * @Route("/show/{id}", name="course_achievement_show", methods={"GET"})
     */
    public function showAction(CourseAchievement $courseAchievement): Response
    {
        $this->addBreadcrumbs([
            'Главная'           => 'index',
            'CourseAchievement' => 'course_achievement_index',
            'Просмотр: ' . $courseAchievement->getId()
        ]);

        return $this->render('CourseAchievement/show.html.twig', [
            'course_achievement' => $courseAchievement,
        ]);
    }

    /**
     * @Route("/create", name="course_achievement_new", methods={"GET","POST"})
     */
    public function newAction(Request $request, EntityManagerInterface $em, EntityService $es): Response
    {
        $this->addBreadcrumbs([
            'Главная'           => 'index',
            'CourseAchievement' => 'course_achievement_index',
            'Создание'
        ]);

        $courseAchievement = new CourseAchievement();
        $form              = $this->createForm(CourseAchievementType::class, $courseAchievement,
            ['user' => $this->getUser()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($courseAchievement);
            $em->flush();

            $es->attachUploadedImage($request, $courseAchievement, 'courseAchievement', 'image');

            $em->flush();

            return $this->redirectToRoute('course_achievement_index');
        }

        return $this->render('CourseAchievement/new.html.twig', [
            'course_achievement' => $courseAchievement,
            'form'               => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="course_achievement_edit", methods={"GET","POST"})
     */
    public function editAction(
        Request $request,
        CourseAchievement $courseAchievement,
        EntityManagerInterface $em,
        EntityService $es
    ): Response {
        $this->addBreadcrumbs([
            'Главная'                                        => 'index',
            'CourseAchievement'                              => 'course_achievement_index',
            'Редактирование: ' . $courseAchievement->getId() => [
                'course_achievement_show',
                ['id' => $courseAchievement->getId()]
            ]
        ]);

        $form = $this->createForm(CourseAchievementType::class, $courseAchievement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $es->attachUploadedImage($request, $courseAchievement, 'courseAchievement', 'image');

            $em->flush();

            return $this->redirectToRoute('course_achievement_index', [
                'id' => $courseAchievement->getId(),
            ]);
        }

        return $this->render('CourseAchievement/edit.html.twig', [
            'course_achievement' => $courseAchievement,
            'form'               => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="course_achievement_delete", methods={"DELETE"})
     */
    public function deleteAction(
        Request $request,
        CourseAchievement $courseAchievement,
        EntityManagerInterface $em
    ): Response {
        if ($this->isCsrfTokenValid('delete' . $courseAchievement->getId(), $request->request->get('_token'))) {
            $em->remove($courseAchievement);
            $em->flush();
        }

        return $this->redirectToRoute('course_achievement_index');
    }


    /**
     * @Route("/search/rules/", name="achievements_rules_ajax_search")
     */
    public function searchRulesAction(Request $request): Response
    {
        $data = [
            'results' => [
                ['id' => 1, 'text' => 'Текст'],
                ['id' => 1, 'text' => 'Текст']
            ]
        ];

        return new JsonResponse($data);
    }
}
