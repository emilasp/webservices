<?php

namespace CoursesBundle\Controller\manage;

use App\Core\Abstracts\AController;
use App\Service\EntityService;
use CoursesBundle\Entity\Course;
use CoursesBundle\Form\CourseType;
use CoursesBundle\Repository\CourseRepository;
use CoursesBundle\Repository\UserRepository;
use CoursesBundle\Security\CourseVoter;
use CoursesBundle\Service\CourseUserService;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class CourseController
 *
 * @Route("/courses/manage")
 *
 * @Security("is_granted('ROLE_USER')")
 *
 * @package App\Controller
 */
class CourseController extends AController
{
    /**
     * Выводим все активные курсы
     *
     * @Route("/my", name="course_my_courses")
     *
     * @IsGranted("ROLE_USER")
     *
     * @Template(template="/Course/manage/index.html.twig")
     * @param Request            $request
     * @param PaginatorInterface $paginator
     * @param CourseRepository   $courseRepository
     * @param CourseUserService  $courseService
     * @param int                $type
     *
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function indexMy(
        Request $request,
        PaginatorInterface $paginator,
        CourseRepository $courseRepository,
        CourseUserService $courseService,
        int $type = null
    ) {
        $this->addBreadcrumbs(['Главная' => 'index', 'Courses' => 'courses', "Список"]);

        $dataTabs = [];
        foreach (array_keys(Course::TYPES) as $courseType) {
            $queryBuilder = clone $courseRepository->getMyCoursesQueryBilder([$courseType], $this->getUser());

            if (!$type && $queryBuilder->getQuery()->getResult()) {
                $type = $courseType;
            }

            $paginatorNew   = clone $paginator;
            $paginatorAlias = 'page_' . $courseType;

            $paginatorNew->setDefaultPaginatorOptions(array(
                'pageParameterName' => $paginatorAlias,
            ));

            $dataTabs[$courseType] = [
                'pagination' => $paginatorNew->paginate(
                    $queryBuilder,
                    $request->query->getInt($paginatorAlias, 1),
                    10,
                    ['alias' => $paginatorAlias]
                )
                ,
            ];
        }

        return [
            'type'          => $type,
            'courseService' => $courseService,
            'dataTabs'      => $dataTabs
        ];
    }

    /**
     *
     * @Route("/create", name="course_create")
     * @Route("/create/{programm}", name="course_create_programm")
     *
     * @Template(template="/Course/manage/update.html.twig")
     *
     */
    public function create(Request $request, EntityManagerInterface $em, EntityService $es, Course $programm = null)
    {
        $this->denyAccessUnlessGranted(CourseVoter::CREATE);

        $this->addBreadcrumbs([
            'Главная'   => 'index',
            'Мои курсы' => 'course_my_courses',
            'Создание нового курса'
        ]);

        $course = new Course();
        $em->persist($course);

        $course->setType(Course::TYPE_COURSE);
        $course->setStatus(Course::STATUS_ACTIVE);

        if ($programm) {
            $course->setProgramm($programm);
        }

        $originalSections = $es->getOriginalRelationCollection($course, 'sections');

        $editForm = $this->createForm(CourseType::class, $course, ['user' => $this->getUser()]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $es->setRelationOneToMany($course, 'course', 'sections', $originalSections);

            $em->flush();

            $es->attachUploadedImage($request, $course, 'course', 'imageBig');
            $es->attachUploadedImage($request, $course, 'course', 'image');
            $es->attachUploadedRelationImages($request, $course, 'course', 'sections', 'image');

            $em->flush();

            return $this->redirectToRoute('course_update', ['course' => $course->getId()]);
        }

        return [
            'form'   => $editForm->createView(),
            'course' => $course,
        ];
    }

    /**
     *
     * @Route("/update/{course}", name="course_update")
     *
     * @Template(template="/Course/manage/update.html.twig")
     *
     */
    public function update(Request $request, EntityManagerInterface $em, EntityService $es, Course $course = null)
    {
        $this->denyAccessUnlessGranted(CourseVoter::EDIT, $course);

        if ($course->isProgramm()) {
            return $this->redirectToRoute('course_programm_update', ['course' => $course->getId()]);
        }

        $this->addBreadcrumbs([
            'Главная'          => 'index',
            'Мои курсы'        => 'course_my_courses',
            $course->getName() => ['course_update', ['course' => $course->getId()]],
            'Редактирование курса',
        ]);

        $originalSections = $es->getOriginalRelationCollection($course, 'sections');

        $editForm = $this->createForm(CourseType::class, $course, ['user' => $this->getUser()]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $es->setRelationOneToMany($course, 'course', 'sections', $originalSections);

            $em->flush();

            $es->attachUploadedImage($request, $course, 'course', 'imageBig');
            $es->attachUploadedImage($request, $course, 'course', 'image');
            $es->attachUploadedRelationImages($request, $course, 'course', 'sections', 'image');

            $em->flush();

            return $this->redirectToRoute('course_update', ['course' => $course->getId()]);
        }

        return [
            'form'   => $editForm->createView(),
            'course' => $course,
        ];
    }

    /**
     * Подписываем польхователя на курс
     *
     * @Route("/subscribe/user/{course}", name="course_subscribe_user")
     *
     * @Template(template="/Course/manage/subscribe.html.twig")
     *
     * @param Request           $request
     * @param Course            $course
     * @param UserRepository    $userRepository
     * @param CourseUserService $courseUserService
     *
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function subscribeUser(Request $request, Course $course, UserRepository $userRepository, CourseUserService $courseUserService)
    {
        if ($course->getCreatedBy() !== $this->getUser()) {
            throw new AccessDeniedException('Вы не можете подписывать на данный курс - вы не являетесь автором курса');
        }
        if (!in_array($course->getStatus(), Course::LEAR_ALLOW_STATUSES)) {
            throw new AccessDeniedException('Вы не можете подписывать на данный курс - курс закрыт для обучения');
        }

        $form = $this->createFormBuilder()
            ->add('email', EmailType::class, ['label' => 'Email пользователя'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $userRepository->findOneBy(['email' => $form->get('email')->getData()]);

            if ($user) {
                $courseUserService->subscribeUserOnCourse($user, $course);

                $this->addFlash('success', 'Пользователь успешно подписан на курс');
            } else {
                $form->get('email')->addError(new FormError('Пользователь с таким email не найден'));
            }
        }

        return [
            'course' => $course,
            'form'   => $form->createView(),
        ];
    }
}
