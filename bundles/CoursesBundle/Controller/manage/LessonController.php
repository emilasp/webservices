<?php

namespace CoursesBundle\Controller\manage;

use App\Core\Abstracts\AController;
use App\Service\EntityService;
use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseLesson;
use CoursesBundle\Form\LessonType;
use CoursesBundle\Security\LessonManageVoter;
use CoursesBundle\Service\LessonService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FileBundle\Service\FileService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LessonController
 *
 * @Route("/lessons")
 *
 * @Security("is_granted('ROLE_USER')")
 *
 * @package App\Controller
 */
class LessonController extends AController
{
    /**
     *
     * @Route("/create/{course}", name="lesson_create")
     *
     * @Template(template="/Lesson/manage/update.html.twig")
     *
     */
    public function create(Request $request, EntityManagerInterface $em, EntityService $es, Course $course)
    {
        $this->denyAccessUnlessGranted(LessonManageVoter::CREATE);

        $this->addBreadcrumbs([
            'Главная' => 'index',
            'Курсы' => 'courses',
            $course->getName() => ['course_update', ['course' => $course->getId()]],
            'Новый урок'
        ]);

        $lesson = new CourseLesson();

        $lesson->setCourse($course);
        $lesson->setStatus(Course::STATUS_NO_ACTIVE);
        $lesson->setPosition(0);

        $em->persist($lesson);

        $originalParts = $es->getOriginalRelationCollection($lesson, 'parts');

        $editForm = $this->createForm(LessonType::class, $lesson, ['user' => $this->getUser(), 'course' => $course]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $es->setRelationOneToMany($lesson, 'lesson', 'parts', $originalParts);

            $em->flush();

            $es->attachUploadedImage($request, $lesson, 'lesson', 'image');

            $em->flush();

            return $this->redirectToRoute('course_update', ['course' => $course->getId()]);
        }

        return [
            'form'   => $editForm->createView(),
            'lesson' => $lesson,
        ];
    }


    /**
     *
     * @Route("/update/{lesson}", name="lesson_update")
     *
     * @Template(template="/Lesson/manage/update.html.twig")
     *
     */
    public function update(Request $request, EntityManagerInterface $em, EntityService $es, LessonService $ls, CourseLesson $lesson)
    {
        $this->denyAccessUnlessGranted(LessonManageVoter::EDIT, $lesson);

        $course = $lesson->getCourse();

        $this->addBreadcrumbs([
            'Главная' => 'index',
            'Курсы' => 'courses',
            $course->getName() => ['course_update', ['course' => $course->getId()]],
            $lesson->getName()
        ]);

        $originalParts = $es->getOriginalRelationCollection($lesson, 'parts');

        $editForm = $this->createForm(LessonType::class, $lesson, ['user' => $this->getUser(), 'course' => $course]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $es->setRelationOneToMany($lesson, 'lesson', 'parts', $originalParts);

            $em->flush();

            $es->attachUploadedImage($request, $lesson, 'lesson', 'image');

            $em->flush();

            if ($cloneCount = $editForm->get('clone')->getData()) {
                $ls->cloneLesson($lesson, $cloneCount);
            }

            return $this->redirectToRoute('course_update', ['course' => $course->getId()]);
        }

        return [
            'form'   => $editForm->createView(),
            'lesson' => $lesson,
        ];
    }
}
