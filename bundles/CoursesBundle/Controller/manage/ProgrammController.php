<?php

namespace CoursesBundle\Controller\manage;

use App\Core\Abstracts\AController;
use App\Service\EntityService;
use CoursesBundle\Entity\Course;
use CoursesBundle\Form\CourseType;
use CoursesBundle\Form\ProgrammType;
use CoursesBundle\Repository\CourseRepository;
use CoursesBundle\Security\CourseVoter;
use CoursesBundle\Security\ProgrammVoter;
use CoursesBundle\Service\CourseUserService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FileBundle\Service\FileService;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProgrammController
 *
 * @Route("/programm/manage")
 *
 * @Security("is_granted('ROLE_USER')")
 *
 * @package App\Controller
 */
class ProgrammController extends AController
{
    /**
     * Выводим все активные курсы
     *
     * @Route("/my", name="course_my_programms")
     *
     * @param Request $request
     *
     * @IsGranted("ROLE_USER")
     *
     * @Template(template="/Programm/manage/index.html.twig")
     * @return array
     */
    public function indexMy(
        Request $request,
        PaginatorInterface $paginator,
        CourseRepository $courseRepository,
        CourseUserService $courseService
    ) {
        $this->addBreadcrumbs(['Главная' => 'index', 'Courses' => 'courses', "Список"]);

        $queryBuilder = $courseRepository->createQueryBuilder('base');
        $queryBuilder->andWhere('base.createdBy = :user')->setParameter('user', $this->getUser());
        $queryBuilder->andWhere('base.type = :type')->setParameter('type', Course::TYPE_PROGRAMM);

        $pagination = $paginator->paginate($queryBuilder, $request->query->getInt('page', 1), 10);

        return [
            'courseService' => $courseService,
            'pagination'    => $pagination
        ];
    }

    /**
     *
     * @Route("/create", name="course_programm_create")
     *
     * @Template(template="/Programm/manage/update.html.twig")
     *
     */
    public function create(Request $request, EntityManagerInterface $em, EntityService $es)
    {
        $this->denyAccessUnlessGranted(ProgrammVoter::CREATE);

        $this->addBreadcrumbs([
            'Главная' => 'index',
            'Мои курсы' => 'course_my_courses',
            'Создание новоой программы'
        ]);


        $course = new Course();
        $em->persist($course);

        $course->setType(Course::TYPE_PROGRAMM);
        $course->setStatus(Course::STATUS_PRIVATE);

        $editForm = $this->createForm(ProgrammType::class, $course);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $em->flush();

            $es->attachUploadedImage($request, $course, 'course', 'imageBig');
            $es->attachUploadedImage($request, $course, 'course', 'image');

            $em->flush();

            return $this->redirectToRoute('course_programm_update', ['course' => $course->getId()]);
        }

        return [
            'form'   => $editForm->createView(),
            'course' => $course,
        ];
    }

    /**
     *
     * @Route("/update/{course}", name="course_programm_update")
     *
     * @Template(template="/Programm/manage/update.html.twig")
     *
     */
    public function update(Request $request, EntityManagerInterface $em, EntityService $es, Course $course)
    {
        $this->denyAccessUnlessGranted(ProgrammVoter::EDIT, $course);

        $this->addBreadcrumbs([
            'Главная' => 'index',
            'Мои курсы' => 'course_my_courses',
            $course->getName() => ['course_update', ['course' => $course->getId()]],
            'Редактирование программы',
        ]);


        $editForm = $this->createForm(ProgrammType::class, $course);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $em->flush();

            $es->attachUploadedImage($request, $course, 'programm', 'imageBig');
            $es->attachUploadedImage($request, $course, 'programm', 'image');

            $em->flush();

            return $this->redirectToRoute('course_programm_update', ['course' => $course->getId()]);
        }

        return [
            'form'   => $editForm->createView(),
            'course' => $course,
        ];
    }
}
