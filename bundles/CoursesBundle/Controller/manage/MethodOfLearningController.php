<?php

namespace CoursesBundle\Controller\manage;

use CoursesBundle\Entity\MethodOfLearning;
use CoursesBundle\Form\MethodOfLearningType;
use CoursesBundle\Repository\TechnicRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/method/of/learning")
 */
class MethodOfLearningController extends AbstractController
{
    /**
     * @Route("/", name="method_of_learning_index", methods={"GET"})
     */
    public function index(TechnicRepository $technicRepository): Response
    {
        return $this->render('Method/index.html.twig', [
            'method_of_learnings' => $technicRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="method_of_learning_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $methodOfLearning = new MethodOfLearning();
        $methodOfLearning->setType(MethodOfLearning::TYPE_DEFAULT);
        $methodOfLearning->setStatus(MethodOfLearning::STATUS_ACTIVE);

        $form = $this->createForm(MethodOfLearningType::class, $methodOfLearning);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($methodOfLearning);
            $entityManager->flush();

            return $this->redirectToRoute('method_of_learning_index');
        }

        return $this->render('Method/new.html.twig', [
            'method_of_learning' => $methodOfLearning,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="method_of_learning_show", methods={"GET"})
     */
    public function show(MethodOfLearning $methodOfLearning): Response
    {
        return $this->render('Method/show.html.twig', [
            'method_of_learning' => $methodOfLearning,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="method_of_learning_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, MethodOfLearning $methodOfLearning): Response
    {
        $form = $this->createForm(MethodOfLearningType::class, $methodOfLearning);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('method_of_learning_index', [
                'id' => $methodOfLearning->getId(),
            ]);
        }

        return $this->render('Method/edit.html.twig', [
            'method_of_learning' => $methodOfLearning,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="method_of_learning_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MethodOfLearning $methodOfLearning): Response
    {
        if ($this->isCsrfTokenValid('delete'.$methodOfLearning->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($methodOfLearning);
            $entityManager->flush();
        }

        return $this->redirectToRoute('method_of_learning_index');
    }
}
