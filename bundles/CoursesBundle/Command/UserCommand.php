<?php

namespace CoursesBundle\Command;

use App\Core\Abstracts\ACommandActions;
use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseLessonUserStudent;
use CoursesBundle\Event\LessonUserEvent;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Обновляем статусы у артикулов
 *
 * Class ArticleInventoryCommand
 *
 * @package BackofficeBundle\Command
 */
final class UserCommand extends ACommandActions
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $this->setName('services:cron:user');
        $this->setDescription("Работаем с пользоватлем");
    }

    /**
     * Устанавливаем текущие уроки(periodical) для полоьзователей
     */
    public function setPeriodicalCurrentLessonsAction(InputInterface $input)
    {
       $this->display('Start set current periodical lesson links');

        /*$qb = $this->em->createQueryBuilder();
        $lessonLinks = $qb ->select('link')
            ->from('CoursesBundle:CourseLessonUserStudent', 'link')
            ->innerJoin('link.lesson', 'lesson')
            ->innerJoin('lesson.course', 'course')
            ->where('course.type IN (:types)')->setParameter("types", Course::TYPES_PERIODICAL)
            ->andWhere('link.status = :status')->setParameter("status", CourseLessonUserStudent::STATUS_NEW)
            ->andWhere('link.startAt=:startAt')->setParameter("startAt", date('Y-m-d 00:00:00'))
            ->getQuery()
            ->getResult();

        foreach ($lessonLinks as $link) {
            $link->setStatus(CourseLessonUserStudent::STATUS_CURRENT);

            $this->eventDispatcher->dispatch(
                LessonUserEvent::NAME_SYSTEM_LESSON_SET_CURRENT,
                new LessonUserEvent($link->getLesson(), $link->getUser())
            );

            $this->display("Set current: {$link->getId()}");
        }

        $this->em->flush();*/

        // Более не актуально, теперь у курса всегда есть текущий урок.
    }

    /**
     * Устанавливаем статус просрочен(periodical) для невыполненных уроков
     */
    public function setPeriodicalOverdueLessonsAction(InputInterface $input)
    {
        $this->display('Start set current periodical lesson links');

        // Periodical
        $qb = $this->em->createQueryBuilder();
        $lessonLinks = $qb ->select('link')
            ->from('CoursesBundle:CourseLessonUserStudent', 'link')
            ->innerJoin('link.lesson', 'lesson')
            ->innerJoin('lesson.course', 'course')
            ->where('course.type IN (:types)')->setParameter("types", Course::TYPES_PERIODICAL)
            ->andWhere('link.status IN (:status)')
            ->setParameter("status", [CourseLessonUserStudent::STATUS_NEW, CourseLessonUserStudent::STATUS_CURRENT])
            ->andWhere('link.startAt<:startAt')
            ->setParameter("startAt", date('Y-m-d 00:00:00'))
            ->getQuery()
            ->getResult();

        /** @var CourseLessonUserStudent $link */
        foreach ($lessonLinks as $link) {
            $link->setStatus(CourseLessonUserStudent::STATUS_OVERDUE);
            $link->setFinishAt(new \DateTime());

            $this->eventDispatcher->dispatch(
                LessonUserEvent::NAME_SYSTEM_LESSON_SET_OVERDUE,
                new LessonUserEvent($link->getLesson(), $link->getUser())
            );

            $this->display("Set overdue: {$link->getStartAt()->format('Y-m-d')}");
        }

        $this->em->flush();
    }
}
