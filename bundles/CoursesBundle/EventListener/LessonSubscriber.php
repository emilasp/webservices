<?php

namespace CoursesBundle\EventListener;

use App\Service\MailSenderQueue;
use CoursesBundle\Event\CourseUserEvent;
use CoursesBundle\Event\LessonUserEvent;
use CoursesBundle\Service\CourseUserService;
use CoursesBundle\Service\UserGestaltService;
use CoursesBundle\Service\UserService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class LessonSubscriber
 *
 * @package App\EventListener
 */
class LessonSubscriber implements EventSubscriberInterface
{
    /**
     * @var MailSenderQueue
     */
    private $mailer;

    /** @var ContainerInterface */
    private $container;
    /** @var CourseUserService */
    private $courseUserService;
    /** @var UserService */
    private $userService;
    /** @var UserGestaltService */
    private $gestaltService;

    /**
     * LessonSubscriber constructor.
     *
     * @param MailSenderQueue           $mailer
     * @param ContainerInterface        $container
     * @param CourseUserService         $courseUserService
     * @param UserService               $userService
     * @param UserGestaltService $gestaltService
     */
    public function __construct(
        MailSenderQueue $mailer,
        ContainerInterface $container,
        CourseUserService $courseUserService,
        UserService $userService,
        UserGestaltService $gestaltService
    ) {
        $this->mailer            = $mailer;
        $this->container         = $container;
        $this->courseUserService = $courseUserService;
        $this->gestaltService    = $gestaltService;
        $this->userService       = $userService;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            LessonUserEvent::NAME_USER_LESSON_START         => 'onUserLessonStart',
            LessonUserEvent::NAME_USER_LESSON_FINISH        => 'onUserLessonFinish',
            LessonUserEvent::NAME_SYSTEM_LESSON_SET_CURRENT => 'onUserLessonSetSystemCurrent',
            LessonUserEvent::NAME_SYSTEM_LESSON_SET_OVERDUE => 'onUserLessonSetSystemOverdue',
        ];
    }

    /**
     * @param LessonUserEvent $lessonEvent
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function onUserLessonStart(LessonUserEvent $lessonEvent)
    {
        $user   = $lessonEvent->getUser();
        $lesson = $lessonEvent->getLesson();

        /*if ($lesson->getCourse()->isPeriodical()) {
            //
        }*/


        /*$this->mailer->sendEmailToUser(
            'Вы успешно записались на курс!',
            $user,
            'emails/course.start.html.twig',
            [
                'host'       => $this->container->getParameter('http_domain'),
                'username'   => $user->getUsername(),
                'courseName' => $course->getName(),
                'courseId'   => $course->getId(),
            ]
        );*/
    }


    /**
     * @param LessonUserEvent $lessonEvent
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onUserLessonFinish(LessonUserEvent $lessonEvent)
    {
        $user   = $lessonEvent->getUser();
        $course = $lessonEvent->getLesson()->getCourse();

        $this->courseUserService->getUserCurrentCourseLessonLink($user, $course);
        $this->courseUserService->setFinishCourse($user, $course);

        $this->userService->regenerateStats($user);
        $this->gestaltService->regenerateGestaltStats($user, $course->getProgramm());
    }

    /**
     * @param LessonUserEvent $lessonEvent
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onUserLessonSetSystemCurrent(LessonUserEvent $lessonEvent)
    {
        $user = $lessonEvent->getUser();
        $course = $lessonEvent->getLesson()->getCourse();

        $this->userService->regenerateStats($user);
        $this->gestaltService->regenerateGestaltStats($user, $course->getProgramm());
    }

    /**
     * @param LessonUserEvent $lessonEvent
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onUserLessonSetSystemOverdue(LessonUserEvent $lessonEvent)
    {
        $user = $lessonEvent->getUser();
        $course = $lessonEvent->getLesson()->getCourse();

        $this->userService->regenerateStats($user);
        $this->gestaltService->regenerateGestaltStats($user, $course->getProgramm());
    }
}