<?php

namespace CoursesBundle\EventListener;

use App\Service\MailSenderQueue;
use CoursesBundle\Event\CourseUserEvent;
use CoursesBundle\Service\CourseUserService;
use CoursesBundle\Service\UserService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UserSubscriber
 *
 * @package App\EventListener
 */
class CourseSubscriber implements EventSubscriberInterface
{
    /**
     * @var MailSenderQueue
     */
    private $mailer;

    /** @var ContainerInterface */
    private $container;

    /** @var CourseUserService */
    private $courseUserService;

    /** @var UserService */
    private $userService;

    /**
     * @param MailSenderQueue $mailer
     */
    public function __construct(MailSenderQueue $mailer, ContainerInterface $container, CourseUserService $courseUserService, UserService $userService)
    {
        $this->mailer        = $mailer;
        $this->courseUserService = $courseUserService;
        $this->userService     = $userService;
        $this->container     = $container;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            CourseUserEvent::NAME_COURSE_USER_START  => 'onUserCourseStart',
            CourseUserEvent::NAME_COURSE_USER_FINISH => 'onUserCourseFinish',
            CourseUserEvent::NAME_COURSE_USER_CANCEL => 'onUserCourseCancel',
        ];
    }

    /**
     * @param CourseUserEvent $courseEvent
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onUserCourseStart(CourseUserEvent $courseEvent)
    {
        $user   = $courseEvent->getUser();
        $course = $courseEvent->getCourse();

        $this->userService->regenerateStats($user);

        $this->mailer->sendEmailToUser(
            'Вы успешно записались на курс!',
            $user,
            'emails/course.start.html.twig',
            [
                'host'       => $this->container->getParameter('http_domain'),
                'username'   => $user->getUsername(),
                'courseName' => $course->getName(),
                'courseId'   => $course->getId(),
            ]
        );
    }


    /**
     * @param CourseUserEvent $courseEvent
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onUserCourseFinish(CourseUserEvent $courseEvent)
    {
        $user   = $courseEvent->getUser();
        $course = $courseEvent->getCourse();

        $this->userService->regenerateStats($user);

        $this->mailer->sendEmailToUser(
            'Вы успешно закончили курс!',
            $user,
            'emails/course.finish.html.twig',
            [
                'host'       => $this->container->getParameter('http_domain'),
                'username'   => $user->getUsername(),
                'courseName' => $course->getName(),
                'courseId'   => $course->getId(),
            ]
        );
    }

    /**
     * @param CourseUserEvent $registeredUserEvent
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function onUserCourseCancel(CourseUserEvent $courseEvent)
    {
        $user   = $courseEvent->getUser();
        $course = $courseEvent->getCourse();
        /*$this->mailer->sendEmailToUser(
            'Вы успешно закончили курс!',
            $user,
            'emails/course.finish.html.twig',
            [
                'host'       => $this->container->getParameter('http_domain'),
                'username'   => $user->getUsername(),
                'courseName' => $course->getName(),
                'courseId'   => $course->getId(),
            ]
        );*/
    }
}