<?php

namespace CoursesBundle\Repository;

use CoursesBundle\Entity\CourseLessonUserStudent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CourseLessonUserStudent|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseLessonUserStudent|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseLessonUserStudent[]    findAll()
 * @method CourseLessonUserStudent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseLessonUserStudentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourseLessonUserStudent::class);
    }

    // /**
    //  * @return CourseLessonUserStudent[] Returns an array of CourseLessonUserStudent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CourseLessonUserStudent
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
