<?php

namespace CoursesBundle\Repository;

use CoursesBundle\Entity\CourseLesson;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CourseLesson|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseLesson|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseLesson[]    findAll()
 * @method CourseLesson[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseLessonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourseLesson::class);
    }

    // /**
    //  * @return CourseLesson[] Returns an array of CourseLesson objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CourseLesson
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
