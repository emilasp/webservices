<?php

namespace CoursesBundle\Repository;

use CoursesBundle\Entity\CourseLessonPart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CourseLessonPart|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseLessonPart|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseLessonPart[]    findAll()
 * @method CourseLessonPart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseLessonPartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourseLessonPart::class);
    }

    // /**
    //  * @return CourseLessonPart[] Returns an array of CourseLessonPart objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CourseLessonPart
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
