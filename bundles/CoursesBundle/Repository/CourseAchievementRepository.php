<?php

namespace CoursesBundle\Repository;

use CoursesBundle\Entity\CourseAchievement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CourseAchievement|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseAchievement|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseAchievement[]    findAll()
 * @method CourseAchievement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseAchievementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourseAchievement::class);
    }

    // /**
    //  * @return CourseAchievement[] Returns an array of CourseAchievement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CourseAchievement
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
