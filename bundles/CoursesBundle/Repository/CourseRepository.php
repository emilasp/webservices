<?php

namespace CoursesBundle\Repository;

use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseUserStudent;
use CoursesBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Course|null find($id, $lockMode = null, $lockVersion = null)
 * @method Course|null findOneBy(array $criteria, array $orderBy = null)
 * @method Course[]    findAll()
 * @method Course[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Course::class);
    }

    /**
     * Возвращаем QB для всех доступных курсов для пользователя(пописанных или открытых)
     *
     * @param User  $user
     * @param array $types
     *
     * @return QueryBuilder
     */
    public function getAllowCoursesQueryBilder(array $types, User $user = null): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('base')
            ->andWhere('base.type IN (:types)')
            ->setParameter('types', $types, Connection::PARAM_INT_ARRAY);

        if ($user) {
            $queryBuilder
                ->leftJoin('base.students', 'student')
                ->andWhere('base.status = :status OR (student.user = :user AND student.status=:studentStatus)')
                ->setParameter(':user', $user)
                ->setParameter('studentStatus', CourseUserStudent::STATUS_CURRENT);
        } else {
            $queryBuilder
                ->andWhere('base.status = :status');
        }

        $queryBuilder->setParameter('status', Course::STATUS_ACTIVE);

        return $queryBuilder;
    }

    /**
     * Возвращаем QB для всех курсов для которых пользователь автор
     *
     * @param User  $user
     * @param array $types
     *
     * @return QueryBuilder
     */
    public function getMyCoursesQueryBilder(array $types, User $user = null): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('base')
            ->andWhere('base.createdBy = :user')->setParameter('user', $user)
            ->andWhere('base.type IN (:types)')
            ->setParameter('types', $types, Connection::PARAM_INT_ARRAY);

        return $queryBuilder;
    }
}
