<?php

namespace CoursesBundle\Repository;

use CoursesBundle\Entity\CourseSection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CourseSection|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseSection|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseSection[]    findAll()
 * @method CourseSection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseSectionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourseSection::class);
    }

    // /**
    //  * @return CourseSection[] Returns an array of CourseSection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CourseSection
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
