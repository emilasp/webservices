<?php

namespace CoursesBundle\Repository;

use CoursesBundle\Entity\CourseAchievementUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CourseAchievementUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseAchievementUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseAchievementUser[]    findAll()
 * @method CourseAchievementUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseAchievementUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourseAchievementUser::class);
    }

    // /**
    //  * @return CourseAchievementUser[] Returns an array of CourseAchievementUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CourseAchievementUser
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
