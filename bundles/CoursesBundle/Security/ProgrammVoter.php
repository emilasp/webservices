<?php


namespace CoursesBundle\Security;


use App\Core\Abstracts\AVoterCrudEntity;
use CoursesBundle\Entity\Course;

/**
 * Class ProgrammVoter
 *
 * @package CoursesBundle\Security
 */
class ProgrammVoter extends AVoterCrudEntity
{
    protected function getEntityClass(): string
    {
        return Course::class;
    }
}