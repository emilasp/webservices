<?php


namespace CoursesBundle\Security;


use App\Core\Abstracts\AVoterCrudEntity;
use CoursesBundle\Entity\Course;

/**
 * Class CourseVoter
 *
 * @package CoursesBundle\Security
 */
class CourseVoter extends AVoterCrudEntity
{
    protected function getEntityClass(): string
    {
        return Course::class;
    }
}