<?php
namespace CoursesBundle\Security;

use App\Core\Abstracts\AVoterCrudEntity;
use App\Core\Interfaces\IEntityCore;
use CoursesBundle\Entity\User;
use CoursesBundle\Entity\CourseLesson;
use CoursesBundle\Entity\CourseLessonUserStudent;
use CoursesBundle\Service\CourseUserService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

/**
 * Class LessonAuditoriumVoter
 *
 * @package CoursesBundle\Security
 */
class LessonAuditoriumVoter extends Voter
{
    public const VIEW   = 'view';

    protected const ATTRIBUTES = [
        self::VIEW,
    ];

    /** @var Security */
    protected $security;
    /** @var CourseUserService  */
    protected $courseService;

    /**
     * CourseVoter constructor.
     *
     * @param Security $security
     */
    public function __construct(Security $security, CourseUserService $courseService)
    {
        $this->security = $security;
        $this->courseService = $courseService;
    }

    /**
     * @param string $attribute
     * @param CourseLesson $lesson
     *
     * @return bool
     */
    protected function supports($attribute, $lesson)
    {
        return $lesson instanceof CourseLesson && in_array($attribute, self::ATTRIBUTES);
    }

    /**
     * @param string         $attribute
     * @param IEntityCore    $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            return true;
        }

        /** @var User $user */
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($subject, $user);
        }

        throw new \LogicException('Invalid attribute: ' . $attribute);
    }

    /**
     * @param CourseLesson $lessont
     * @param User   $user
     *
     * @return bool
     */
    protected function canView(CourseLesson $lesson, User $user)
    {
        $status = $this->courseService->getUserLessonStatus($user, $lesson);
        return in_array($status, [CourseLessonUserStudent::STATUS_FINISH, CourseLessonUserStudent::STATUS_CURRENT]);
    }
}