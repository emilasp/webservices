<?php
namespace CoursesBundle\Security;

use App\Core\Abstracts\AVoterCrudEntity;
use CoursesBundle\Entity\CourseLesson;

/**
 * Class CourseVoter
 *
 * @package CoursesBundle\Security
 */
class LessonManageVoter extends AVoterCrudEntity
{
    protected function getEntityClass(): string
    {
        return CourseLesson::class;
    }
}