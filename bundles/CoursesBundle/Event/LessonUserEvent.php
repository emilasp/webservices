<?php

declare(strict_types=1);

namespace CoursesBundle\Event;

use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseLesson;
use CoursesBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class LessonUserEvent
 *
 * @package CoursesBundle\Event
 */
class LessonUserEvent extends Event
{
    public const NAME_SYSTEM_LESSON_SET_OVERDUE    = 'system.lesson.set.overdue';
    public const NAME_SYSTEM_LESSON_SET_CURRENT    = 'system.lesson.set.current';
    public const NAME_USER_LESSON_START            = 'user.lesson.start';
    public const NAME_USER_LESSON_FINISH           = 'user.lesson.finish';

    /** @var User */
    private $user;
    /** @var CourseLesson */
    private $lesson;

    /**
     * @param CourseLesson $lesson
     * @param User         $user
     */
    public function __construct(CourseLesson $lesson, User $user)
    {
        $this->lesson = $lesson;
        $this->user   = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return CourseLesson
     */
    public function getLesson(): CourseLesson
    {
        return $this->lesson;
    }
}