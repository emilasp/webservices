<?php

declare(strict_types=1);

namespace CoursesBundle\Event;

use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class CourseUserEvent
 *
 * @package CoursesBundle\Event
 */
class CourseUserEvent extends Event
{
    public const NAME_COURSE_USER_START  = 'course.user.start';
    public const NAME_COURSE_USER_FINISH = 'course.user.finish';
    public const NAME_COURSE_USER_CANCEL = 'course.user.cancel';

    /**
     * @var User
     */
    private $user;
    /** @var Course */
    private $course;

    /**
     * @param Course $course
     * @param User   $user
     */
    public function __construct(Course $course, User $user)
    {
        $this->course = $course;
        $this->user   = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Course
     */
    public function getCourse(): Course
    {
        return $this->course;
    }
}