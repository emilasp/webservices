<?php
namespace CoursesBundle;

use CoursesBundle\DependencyInjection\CoursesExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Бандл с курсами
 *
 * Class CoursesBundle
 *
 * @package CoursesBundle
 */
class CoursesBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new CoursesExtension();
    }
}