<?php

namespace CoursesBundle\Form;

use App\Form\base\ImageType;
use App\Transformers\ArrayToJSONStringTransformer;
use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseLesson;
use CoursesBundle\Entity\CourseSection;
use CoursesBundle\Repository\CourseRepository;
use CoursesBundle\Repository\CourseSectionRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class LessonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();

        $builder
            ->add('name', TextType::class, ['label' => 'Наименование'])
            ->add('purpose', TextareaType::class, ['label' => 'Цель/Зачем'])
            ->add('clone', IntegerType::class, [
                'label'    => false,
                'mapped'   => false,
                'required' => false,
                'attr'     => ['placeholder' => 'кол-во клонов']
            ])
            ->add('bested', TextareaType::class, ['label' => 'Стнем лучше', 'required' => false])
            ->add('entry', CKEditorType::class, ['label' => 'Вступление'])
            ->add('conclusion', CKEditorType::class, ['label' => 'Заключение/вывод', 'required' => false])
            ->add('status', ChoiceType::class, ['label' => 'Статус', 'choices' => array_flip(CourseLesson::STATUSES)])
            //->add('startAt', DateTimeType::class, ['label' => 'Дата начала', 'required' => false])
            ->add('startAt', TextType::class, [
                'required' => false,
                'label'    => 'Дата начала',
                'attr'     => [
                    'class'        => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datepicker',
                    'data-format'  => 'yyyy-mm-dd HH:ii',
                ],
            ])

            ->add('endPeriod', IntegerType::class, ['label' => 'Действует(дней)', 'required' => false])
            ->add('duration', IntegerType::class, ['label' => 'Длительность(мин)'])
            //->add('answers', TextareaType::class, ['label' => 'Верные ответы на вопросы в конце урока', 'required' => false])
            ->add('difficulty', TextType::class, ['label' => 'Сложность/опытные баллы'])
            ->add('typeAdmission', ChoiceType::class, [
                'label'   => 'Тип доступа к уроку после прохождения предыдущего',
                'choices' => array_flip(CourseLesson::ADMISSION_TYPES)
            ])
            ->add('typeAnswer', ChoiceType::class, [
                'label'   => 'Тип ответа(студентом)',
                'choices' => array_flip(CourseLesson::ANSWER_TYPES)
            ])
            ->add('typeFinish', ChoiceType::class, [
                'label'   => 'Тип завершения',
                'choices' => array_flip(CourseLesson::FINISH_TYPES)
            ])
            //->add('position')
            ->add('course', EntityType::class, [
                'placeholder'   => 'Выберите курс',
                'class'         => Course::class,
                'data'          => $entity->getCourse(),
                'choice_label'  => 'name',
                'attr'          => ['readonly' => true],
                'query_builder' => function (CourseRepository $courseRepository) use ($options) {
                    return $courseRepository->createQueryBuilder('c')
                        //->where('c.createdBy = :user')
                        //->setParameter('user', $options['user']) //@TODO after test remove comment
                        ->orderBy('c.createdAt', 'DESC');
                },
            ])
            ->add('section', EntityType::class, [
                'placeholder'   => 'Выберите секцию/блок',
                'class'         => CourseSection::class,
                'choice_label'  => 'name',
                'query_builder' => function (CourseSectionRepository $sectionRepository) use ($options) {
                    return $sectionRepository->createQueryBuilder('s')
                        ->where('s.course = :course')
                        ->setParameter('course', $options['course'])
                        ->orderBy('s.position', 'ASC');
                },
            ])
            ->add('image', ImageType::class, [
                'label'       => 'Изображение',
                'mapped'      => false,
                'required'    => false,
                'width'       => '150px',
                'file'        => $entity->getImage(),
                'constraints' => [
                    new Image([
                        'maxSize' => '2M'
                    ])
                ]
            ])
            ->add('checklist', CollectionType::class, [
                'allow_add'     => true,
                'allow_delete'  => true,
                'entry_type'    => LessonCheckListItemType::class,
                'prototype'     => true,
                'entry_options' => ['label' => false],
                'attr'          => array(
                    'class' => 'my-selector',
                ),
            ]);

        $builder->add('parts', CollectionType::class, [
            'entry_type'    => LessonPartType::class,
            'entry_options' => ['label' => false],
            'allow_add'     => true,
            'allow_delete'  => true,
            'prototype'     => true,
            'attr'          => array(
                'class' => 'my-selector',
            ),
        ]);

        $builder->add('periodic', HiddenType::class, [
            'attr' => ['id' => 'periodic-id'],
        ]);

        $builder->get('periodic')->addModelTransformer(new ArrayToJSONStringTransformer());
        $builder->get('startAt')->addModelTransformer(new DateTimeTransformer());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CourseLesson::class,
            'user'       => null,
            'course'     => null,
        ]);
    }
}
