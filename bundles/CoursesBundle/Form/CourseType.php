<?php

namespace CoursesBundle\Form;

use App\Form\base\ImageType;
use CoursesBundle\Entity\Course;
use CoursesBundle\Repository\CourseRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class CourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();

        $builder
            ->add('name', TextType::class, ['label' => 'Наименование'])
            ->add('annotation', CKEditorType::class, ['label' => 'Аннотация'])
            ->add('description', CKEditorType::Class, ['label' => 'Описание'])
            ->add('type', ChoiceType::class, ['label' => 'Тип', 'choices' => array_flip(Course::TYPES)])
            ->add('status', ChoiceType::class, ['label' => 'Статус', 'choices' => array_flip(Course::STATUSES)])
            ->add('programm', EntityType::class, [
                'label'         => 'Программа',
                'placeholder'   => 'Выберите программу',
                'class'         => Course::class,
                'choice_label'  => 'name',
                'required'      => false,
                'query_builder' => function (CourseRepository $courseRepository) use ($options) {
                    return $courseRepository->createQueryBuilder('c')
                        ->where('c.createdBy = :user')
                        ->andWhere('c.type = :type')
                        ->setParameter('user', $options['user'])
                        ->setParameter('type', Course::TYPE_PROGRAMM)
                        ->orderBy('c.createdAt', 'DESC');
                },
            ])
            ->add('imageBig', ImageType::class, [
                'label'       => 'Промо',
                'mapped'      => false,
                'required'    => false,
                'width'       => '150px',
                'file'        => $entity->getImageBig(),
                'constraints' => [
                    new Image([
                        'maxSize'   => '5M',
                        'minHeight' => 100,
                        'maxHeight' => 250,
                        'minWidth'  => 900,
                        'maxWidth'  => 960,
                    ])
                ]
            ])
            ->add('image', ImageType::class, [
                'label'       => 'Изображение',
                'mapped'      => false,
                'required'    => false,
                'width'       => '150px',
                'file'        => $entity->getImage(),
                'constraints' => [
                    new Image([
                        'maxSize' => '2M'
                    ])
                ]
            ]);
        $builder->add('sections', CollectionType::class, [
            'entry_type'    => CourseSectionType::class,
            'entry_options' => ['label' => false],
            'allow_add'     => true,
            'allow_delete'  => true,
            'prototype'     => true,
            'attr'          => array(
                'class' => 'my-selector',
            ),
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
            'user'       => null,
        ]);
    }
}
