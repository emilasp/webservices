<?php

namespace CoursesBundle\Form;

use App\Form\base\ImageType;
use CoursesBundle\Entity\Course;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class ProgrammType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();

        $builder
            ->add('name', TextType::class,['label' => 'Наименование'])
            ->add('annotation', CKEditorType::class,['label' => 'Аннотация'])
            ->add('description', CKEditorType::Class, ['label' => 'Описание'])
            ->add('type', ChoiceType::class, ['label' => 'Тип', 'choices' => array_flip(Course::TYPES)])
            ->add('status', ChoiceType::class, ['label' => 'Статус', 'choices' => array_flip(Course::STATUSES)])
            ->add('imageBig', ImageType::class, [
                'label' => 'Промо',
                'mapped' => false,
                'required' => false,
                'width' => '150px',
                'file' => $entity->getImageBig(),
                'constraints' => [
                    new Image([
                        'maxSize' => '5M',
                        'minHeight' => 100,
                        'maxHeight' => 250,
                        'minWidth'  => 900,
                        'maxWidth'  => 960,
                    ])
                ]
            ])
            ->add('image', ImageType::class, [
                'label' => 'Изображение',
                'mapped' => false,
                'required' => false,
                'width' => '150px',
                'file' => $entity->getImage(),
                'constraints' => [
                    new Image([
                        'maxSize' => '2M'
                    ])
                ]
            ])
            ->add('maxDifficultly', IntegerType::class, ['label' => 'Максимальное значение для 100%', 'required' => false])
            ->add('negativeRatio', IntegerType::class, ['label' => 'Коэффициент для пропусков', 'required' => false]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
        ]);
    }
}
