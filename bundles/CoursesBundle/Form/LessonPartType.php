<?php

namespace CoursesBundle\Form;

use App\Form\base\ImageType;
use CoursesBundle\Entity\CourseLessonPart;
use CoursesBundle\Entity\CourseSection;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LessonPartType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['attr' => ['placeholder' => 'Загаловок']])
            ->add('text', CKEditorType::class, ['label' => 'Текст'])
            ->add('link', TextType::class, ['required' => false, 'attr' => ['placeholder' => 'Ссылка/приложение']])
            ->add('type', ChoiceType::class, [
                'choices' => array_flip(CourseLessonPart::TYPES),
                'attr' => ['placeholder' => 'Тип']
            ])
        ;

        $builder->add('position', HiddenType::class, [
            'attr' => [
                'class' => 'my-position',
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CourseLessonPart::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'LessonPartType';
    }
}
