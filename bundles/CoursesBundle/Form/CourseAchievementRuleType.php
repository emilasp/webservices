<?php

namespace CoursesBundle\Form;

use App\Form\base\ImageType;
use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseAchievement;
use CoursesBundle\Entity\CourseAchievementRule;
use CoursesBundle\Repository\CourseRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

/**
 * Class CourseAchievementRuleType
 *
 * @package CoursesBundle\Form
 */
class CourseAchievementRuleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rule', TextType::class, ['label' => 'Правило'])
        ;

       /* $builder
            ->add('rule', Select2EntityType::class, [
                'label'                => false,
                'required'             => true,
                'mapped'               => false,
                'multiple'             => false,
                'remote_route'         => 'achievements_rules_ajax_search',
                'class'                => TextType::class,
                'minimum_input_length' => 0,
                'page_limit'           => 10,
                'scroll'               => true,
                'allow_clear'          => false,
                'allow_add'            => [
                    'enabled'        => true,
                    'new_tag_text'   => ' (новый)',
                    'new_tag_prefix' => '__',
                    'tag_separators' => '[",", " "]'
                ],
            ]);

        $builder->get('rule')
            ->addModelTransformer(new CallbackTransformer(
                function ($tagsAsArray) {

                    if (!$tagsAsArray) {
                        return null;
                    }
                    // transform the array to a string
                    return implode(', ', $tagsAsArray);
                },
                function ($tagsAsString) {
                    // transform the string back to an array
                    return explode(', ', $tagsAsString);
                }
            ))
        ;*/
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    public function getBlockPrefix()
    {
        return 'CourseAchievementRuleType';
    }
}
