<?php

namespace CoursesBundle\Form;

use App\Form\base\ImageType;
use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseAchievement;
use CoursesBundle\Repository\CourseRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class CourseAchievementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();

        $builder
            ->add('name', TextType::class, ['label' => 'Наименование'])
            ->add('category', TextType::class, ['label' => 'Катеория'])
            ->add('type', ChoiceType::class, ['label' => 'Тип', 'choices' => array_flip(CourseAchievement::TYPES)])
            ->add('jewel', ChoiceType::class, ['label' => 'Ценность', 'choices' => array_flip(CourseAchievement::JEWELS)])
            ->add('difficulty', TextType::class, ['label' => 'Сложность'])
            ->add('description', CKEditorType::class, ['label' => 'Описание'])
            //->add('rules', TextareaType::class, ['label' => 'Правила'])

            ->add('course', EntityType::class, [
                'required'   => false,
                'placeholder'   => 'Выберите курс',
                'class'         => Course::class,
                'choice_label'  => 'name',
                'query_builder' => function (CourseRepository $courseRepository) use ($options) {
                    return $courseRepository->createQueryBuilder('c')
                        ->where('c.createdBy = :user')
                        ->setParameter('user', $options['user'])
                        ->orderBy('c.createdAt', 'DESC');
                },
            ])
            ->add('image', ImageType::class, [
                'label'    => 'Изображение',
                'mapped'   => false,
                'required' => false,
                'width'    => '150px',
                'file'     => $entity->getImage(),
                'constraints' => [
                    new Image([
                        'maxSize' => '1M'
                    ])
                ]
            ])
            ->add('rules', CollectionType::class, [
                'allow_add'     => true,
                'allow_delete'  => true,
                'entry_type'    => CourseAchievementRuleType::class,
                'prototype'     => true,
                'entry_options' => ['label' => false],
                'attr'          => array(
                    'class' => 'my-selector',
                ),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CourseAchievement::class,
            'user'       => null,
        ]);
    }
}
