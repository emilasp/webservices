<?php

namespace CoursesBundle\Form;

use App\Form\base\ImageType;
use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseAchievement;
use CoursesBundle\Entity\CourseAchievementRule;
use CoursesBundle\Repository\CourseRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

/**
 * Class LessonCheckListItemType
 *
 * @package CoursesBundle\Form
 */
class LessonCheckListItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label', TextType::class, ['label' => false, 'attr' => ['placeholder' => 'Наименование']])
            ->add('check', HiddenType::class, ['label' => false, 'required' => false, 'data' => 0])
        ;

        /*$builder->get('rule')
            ->addModelTransformer(new CallbackTransformer(
                function ($tagsAsArray) {

                    if (!$tagsAsArray) {
                        return null;
                    }
                    // transform the array to a string
                    return implode(', ', $tagsAsArray);
                },
                function ($tagsAsString) {
                    // transform the string back to an array
                    return explode(', ', $tagsAsString);
                }
            ))
        ;*/
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    public function getBlockPrefix()
    {
        return 'LessonCheckListItemType';
    }
}
