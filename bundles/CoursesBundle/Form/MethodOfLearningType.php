<?php

namespace CoursesBundle\Form;

use CoursesBundle\Entity\MethodOfLearning;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MethodOfLearningType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Наименование'])
            ->add('description', CKEditorType::class, ['label' => 'Описание'])
            ->add('comment', TextareaType::class, ['label' => 'Комментарий', 'required' => false,'attr' => ['rows' => 12]])
            ->add('purpose', TextareaType::class, ['label' => 'Цель/Зачем'])
            ->add('type', ChoiceType::class, ['label' => 'Тип', 'choices' => array_flip(MethodOfLearning::TYPES)])
            ->add('status', ChoiceType::class, ['label' => 'Статус', 'choices' => array_flip(MethodOfLearning::STATUSES)]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MethodOfLearning::class,
        ]);
    }
}
