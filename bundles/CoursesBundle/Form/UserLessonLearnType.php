<?php

namespace CoursesBundle\Form;

use App\Form\base\ImageType;
use CoursesBundle\Entity\Course;
use CoursesBundle\Entity\CourseLesson;
use CoursesBundle\Entity\CourseSection;
use CoursesBundle\Repository\CourseRepository;
use CoursesBundle\Repository\CourseSectionRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserLessonLearnType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();

        $builder
            ->add('name', TextType::class, ['label' => 'Наименование'])
            ->add('purpose', TextareaType::class, ['label' => 'Цель/Зачем'])
            ->add('bested', TextareaType::class, ['label' => 'Стнем лучше', 'required' => false])
            ->add('entry', CKEditorType::class, ['label' => 'Вступление'])
            ->add('conclusion', CKEditorType::class, ['label' => 'Заключение/вывод', 'required' => false])
            ->add('status', ChoiceType::class, ['label' => 'Статус', 'choices' => array_flip(CourseLesson::STATUSES)])
            ->add('answers', TextareaType::class, ['label' => 'Верные ответы на вопросы в конце урока', 'required' => false])
            ->add('difficulty', TextType::class, ['label' => 'Сложность/опытные баллы'])
            ->add('typeAdmission', ChoiceType::class, [
                'label'   => 'Тип доступа к уроку после прохождения предыдущего',
                'choices' => array_flip(CourseLesson::ADMISSION_TYPES)
            ])
            ->add('typeAnswer', ChoiceType::class, [
                'label'   => 'Тип ответа(студентом)',
                'choices' => array_flip(CourseLesson::ANSWER_TYPES)
            ])
            //->add('position')
            ->add('course', EntityType::class, [
                'placeholder'   => 'Выберите курс',
                'class'         => Course::class,
                'choice_label'  => 'name',
                'query_builder' => function (CourseRepository $courseRepository) use ($options) {
                    return $courseRepository->createQueryBuilder('c')
                        ->where('c.createdBy = :user')
                        ->setParameter('user', $options['user'])
                        ->orderBy('c.createdAt', 'DESC');
                },
            ])
            ->add('section', EntityType::class, [
                'placeholder'   => 'Выберите секцию/блок',
                'class'         => CourseSection::class,
                'choice_label'  => 'name',
                'query_builder' => function (CourseSectionRepository $sectionRepository) use ($options) {
                    return $sectionRepository->createQueryBuilder('s')
                        ->where('s.course = :course')
                        ->setParameter('course', $options['course'])
                        ->orderBy('s.position', 'DESC');
                },
            ])
            ->add('image', ImageType::class, [
                'label'    => 'Изображение',
                'mapped'   => false,
                'required' => false,
                'width'    => '150px',
                'file'     => $entity->getImage(),
            ]);

        $builder->add('parts', CollectionType::class, [
            'entry_type'    => LessonPartType::class,
            'entry_options' => ['label' => false],
            'allow_add'     => true,
            'allow_delete'  => true,
            'prototype'     => true,
            'attr'          => array(
                'class' => 'my-selector',
            ),
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CourseLesson::class,
            'user'       => null,
            'course'     => null,
        ]);
    }
}
