<?php

namespace CoursesBundle\Form;

use App\Form\base\ImageType;
use CoursesBundle\Entity\CourseSection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class CourseSectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();

        $builder
            ->add('name', TextType::class, ['attr' => ['placeholder' => 'Наименование']])
            ->add('description', TextareaType::class, ['attr' => ['placeholder' => 'Описание']])
            //->add('course')
            ->add('image', ImageType::class, [
                'label'    => 'Изображение',
                'mapped'   => false,
                'required' => false,
                'width'    => '150px',
                'file'     => $entity ? $entity->getImage() : null,
                'constraints' => [
                    new Image([
                        'maxSize' => '1M'
                    ])
                ]
            ]);

        $builder->add('position', HiddenType::class, [
            'attr' => [
                'class' => 'my-position',
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CourseSection::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'CourseSectionType';
    }
}
