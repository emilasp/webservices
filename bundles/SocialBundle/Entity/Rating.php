<?php

namespace SocialBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="SocialBundle\Repository\RatingRepository")
 */
class Rating
{
    public const STATUS_VISIBLE = 1;

    public const TYPE_UP_DOWN = 1;
    public const TYPE_STARS   = 2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"appComments"})
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $votes = 0;

    /**
     * @Groups({"appComments"})
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $rateSum = 0;

    /**
     * @Groups({"appComments"})
     *
     * @ORM\Column(type="float", options={"default": 0})
     */
    private $rate = 0;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $entityName;

    /**
     * @ORM\Column(type="integer")
     */
    private $entityId;

    /**
     * @Groups({"appComments"})
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="SocialBundle\Entity\RatingVote", mappedBy="rating", orphanRemoval=true)
     */
    private $ratingVotes;

    public function __construct()
    {
        $this->ratingVotes = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getVotes(): int
    {
        return $this->votes;
    }

    public function setVotes(int $votes): self
    {
        $this->votes = $votes;

        return $this;
    }

    public function getRateSum(): int
    {
        return $this->rateSum;
    }

    public function setRateSum(int $rateSum): self
    {
        $this->rateSum = $rateSum;

        return $this;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getEntityName(): ?string
    {
        return $this->entityName;
    }

    public function setEntityName(string $entityName): self
    {
        $this->entityName = $entityName;

        return $this;
    }

    public function getEntityId(): ?int
    {
        return $this->entityId;
    }

    public function setEntityId(int $entityId): self
    {
        $this->entityId = $entityId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|RatingVote[]
     */
    public function getRatingVotes(): Collection
    {
        return $this->ratingVotes;
    }

    public function addRatingVote(RatingVote $ratingVote): self
    {
        if (!$this->ratingVotes->contains($ratingVote)) {
            $this->ratingVotes[] = $ratingVote;
            $ratingVote->setRating($this);
        }

        return $this;
    }

    public function removeRatingVote(RatingVote $ratingVote): self
    {
        if ($this->ratingVotes->contains($ratingVote)) {
            $this->ratingVotes->removeElement($ratingVote);
            // set the owning side to null (unless already changed)
            if ($ratingVote->getRating() === $this) {
                $ratingVote->setRating(null);
            }
        }

        return $this;
    }

    /**
     * Обновлем стату
     */
    public function recalculateStats()
    {
        $rateSum   = 0;
        $rateCount = 0;
        /** @var RatingVote $vote */
        foreach ($this->ratingVotes as $vote) {
            $rateCount++;
            $rateSum += $vote->getVote();
        }

        $this->setRateSum($rateSum);
        $this->setVotes($rateCount);
        $this->setRate(round($rateSum / $rateCount, 2));
    }
}
