<?php

namespace SocialBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\EntityTrait;
use CoursesBundle\Entity\CourseLesson;
use CoursesBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="social_comments", indexes={@ORM\Index(columns={"entity_name", "entity_id"})})
 * @ORM\Entity(repositoryClass="SocialBundle\Repository\CommentRepository")
 */
class Comment implements IEntityCore
{
    use EntityTrait;

    public const STATUS_DELETE    = 0;
    public const STATUS_PUBLISHED = 1;
    public const STATUS_MODERATE  = 2;


    public const STATUSES = [
        self::STATUS_DELETE    => 'Удален',
        self::STATUS_PUBLISHED => 'Опубликован',
        self::STATUS_MODERATE  => 'На модерации',
    ];

    public const TYPE_DISCUSSION = 1;
    public const TYPE_PRIVATE    = 2;

    public const TYPES = [
        self::TYPE_DISCUSSION => 'Дискуссия',
        self::TYPE_PRIVATE    => 'Проиватный',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"appComments"})
     */
    private $id;

    /**
     * @Groups({"appComments"})
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $title;

    /**
     * @Groups({"appComments"})
     *
     * @ORM\Column(type="text")
     */
    private $comment;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $entityName;

    /**
     * @ORM\Column(type="integer")
     */
    private $entityId;

    /**
     * @Groups({"appComments"})
     *
     * @var User $createdBy
     *
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @Groups({"appComments"})
     *
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @Groups({"appComments"})
     *
     * @ORM\Column(type="smallint", options={"default": 1})
     */
    private $type = 1;

    /**
     * @Groups({"appComments"})
     *
     * @ORM\Column(type="datetime", nullable=true, options={"default": null})
     */
    private $createdAt;


    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $left;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $right;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="SocialBundle\Entity\Comment")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="SocialBundle\Entity\Comment", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="SocialBundle\Entity\Comment", mappedBy="parent")
     * @ORM\OrderBy({"left" = "ASC"})
     */
    private $children;

    /**
     * @ORM\OneToOne(targetEntity="SocialBundle\Entity\Rating", cascade={"persist", "remove"})
     *
     * @Groups({"appComments"})
     *
     */
    private $rating;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->children  = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getEntityName(): ?string
    {
        return $this->entityName;
    }

    public function setEntityName(string $entityName): self
    {
        $this->entityName = $entityName;

        return $this;
    }

    public function getEntityId(): ?int
    {
        return $this->entityId;
    }

    public function setEntityId(int $entityId): self
    {
        $this->entityId = $entityId;

        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }


    public function getRoot(): Comment
    {
        return $this->root;
    }

    public function setParent(Comment $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent(): Comment
    {
        return $this->parent;
    }

    public function getLeft(): ?int
    {
        return $this->left;
    }

    public function setLeft(int $left): self
    {
        $this->left = $left;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getRight(): ?int
    {
        return $this->right;
    }

    public function setRight(int $right): self
    {
        $this->right = $right;

        return $this;
    }

    public function setRoot(?self $root): self
    {
        $this->root = $root;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(Comment $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(Comment $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getRating(): ?Rating
    {
        return $this->rating;
    }

    public function setRating(?Rating $rating): self
    {
        $this->rating = $rating;

        return $this;
    }
}
