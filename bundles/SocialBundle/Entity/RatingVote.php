<?php

namespace SocialBundle\Entity;

use App\Entity\UserBase;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="SocialBundle\Repository\RatingVoteRepository")
 */
class RatingVote
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $vote;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $ip;

    /**
     * @ORM\ManyToOne(targetEntity="SocialBundle\Entity\Rating", inversedBy="ratingVotes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rating;

    /**
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $createdBy;

    public function getId(): int
    {
        return $this->id;
    }

    public function getVote(): int
    {
        return $this->vote;
    }

    public function setVote(int $vote): self
    {
        $this->vote = $vote;

        return $this;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getRating(): Rating
    {
        return $this->rating;
    }

    public function setRating(Rating $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getCreatedBy(): ?UserBase
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?UserBase $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
