<?php

namespace SocialBundle\Controller;

use App\Core\Abstracts\AController;
use Doctrine\ORM\EntityManagerInterface;
use SocialBundle\Entity\Comment;
use SocialBundle\Service\SocialApiService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 *
 * @package SocialBundle\Controller
 */
class CommentsController extends AController
{
    /**
     * @Route("/social/comments/api/get/list", name="social.comment.list", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function listAction(Request $request, EntityManagerInterface $em, SocialApiService $apiService): Response
    {
        $data   = json_decode($request->getContent());
        $entity = $em->getRepository($data->entityClass)->find($data->entityId);

        $repositoryComment = $em->getRepository(Comment::class);
        $comments          = $repositoryComment->getComments($entity, $this->getUser(), $data->type);

        $data = $this->get('serializer')->serialize(
            ['comments' => $apiService->getCommetnsForDashboardApp($comments)],
            'json',
            ['groups' => ['appComments']]
        );
        return new Response($data);
    }

    /**
     * @Route("/social/comments/api/add", name="social.comment.add", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addAction(Request $request, EntityManagerInterface $em): Response
    {
        $data              = json_decode($request->getContent());
        $repositoryComment = $em->getRepository(Comment::class);

        $entity = $em->getRepository($data->entityClass)->find($data->entityId);

        if ($data->replyId) {
            $parent = $repositoryComment->find($data->replyId);
            $newComment = $repositoryComment->addComment($data->type, $entity, $this->getUser(), $data->comment, null, Comment::STATUS_MODERATE, $parent);
        } else {
            $newComment = $repositoryComment->addComment($data->type, $entity, $this->getUser(), $data->comment);
        }

        $em->flush($newComment);

        return new JsonResponse(['status' => 1, 'id' => $newComment->getId(), 'message' => 'комментарий успешно сохранен']);
    }
}
