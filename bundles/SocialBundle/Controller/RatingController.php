<?php

namespace SocialBundle\Controller;

use App\Core\Abstracts\AController;
use Doctrine\ORM\EntityManagerInterface;
use SocialBundle\Entity\Comment;
use SocialBundle\Entity\Rating;
use SocialBundle\Service\SocialApiService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RatingController
 *
 * @package SocialBundle\Controller
 */
class RatingController extends AController
{
    /**
     * @Route("/social/rating/api/vote", name="social.rating.vote", methods={"POST"})
     *
     * @param Request                $request
     * @param EntityManagerInterface $em
     * @param SocialApiService       $apiService
     *
     * @return Response
     * @throws \Exception
     */
    public function addAction(Request $request, EntityManagerInterface $em, SocialApiService $apiService): Response
    {
        $data   = json_decode($request->getContent());
        $rating = $apiService->getRating($data->entityName, $data->entityId, $data->type);

        $oldScore = $rating->getRateSum();

        $apiService->vote($request, $rating, $this->getUser(), $data->vote);

        $em->flush();

        if ($oldScore === $rating->getRateSum()) {
            return new JsonResponse(['status' => 1, 'rate' => $rating->getRate(), 'message' => 'Вы уже голосовали']);
        }
        return new JsonResponse(['status' => 1, 'rate' => $rating->getRate(), 'message' => 'Ваш голос учтен']);
    }
}
