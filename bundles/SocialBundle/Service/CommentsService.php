<?php


namespace SocialBundle\Service;

use App\Core\Interfaces\IEntityCore;

/**
 * Class CommentsService
 *
 * @package SocialBundle\Service
 */
class CommentsService
{
    /**
     * Получаем комментарии для entity
     *
     * @param IEntityCore $entity
     *
     * @return array
     */
    public function getCommentsForEntity(IEntityCore $entity): array
    {

    }

    public function getCommentsForEntities(string $entityClass, array $ids): array
    {

    }
}