<?php


namespace SocialBundle\Service;

use App\Core\Abstracts\AInnerApiService;
use App\Entity\UserBase;
use CoursesBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use FileBundle\Service\FileService;
use SocialBundle\Entity\Comment;
use SocialBundle\Entity\Rating;
use SocialBundle\Entity\RatingVote;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SocialBundle
 *
 * @package SocialBundle\Service
 */
class SocialApiService extends AInnerApiService
{
    private const IMAGE_TYPES = [FileService::IMAGE_FORMAT_ICON, FileService::IMAGE_FORMAT_SMALL, FileService::IMAGE_FORMAT_SQUARE];

    /** @var CommentsService */
    private $commentsService;

    /**
     * SocialApiService constructor.
     *
     * @param CommentsService        $commentsService
     * @param EntityManagerInterface $em
     * @param FileService            $fileService
     */
    public function __construct(CommentsService $commentsService, EntityManagerInterface $em, FileService $fileService)
    {
        parent::__construct($em, $fileService);

        $this->commentsService = $commentsService;
    }


    /**
     * Получаем информацию по ачивкам юзера
     *
     * @param User $user
     *
     * @return array
     */
    public function getCommetnsForDashboardApp(array $comments): array
    {
        $data = [];
        /** @var Comment $comment */
        foreach ($comments as $comment) {
            $data[] = [
                'rating'   => $this->getRating(Comment::class, $comment->getId(), Rating::TYPE_UP_DOWN),
                'comment'  => $comment,
                'user'     => [
                    'id'     => $comment->getCreatedBy()->getId(),
                    'name'   => $comment->getCreatedBy()->getNameFull(),
                    'avatar' => $this->getEntityImages($comment->getCreatedBy(), 'avatar', self::IMAGE_TYPES),
                ],
                'children' => $this->getCommetnsForDashboardApp($comment->getChildren()->getValues())
            ];
        }
        return $data;
    }


    /**
     * Получаем Rating для Entity
     *
     * @param string $entityClass
     * @param int    $entityId
     * @param int    $type
     *
     * @return Rating
     */
    public function getRating(string $entityClass, int $entityId, int $type): Rating
    {
        $ratingRepository = $this->em->getRepository(Rating::class);

        if (!$rating = $ratingRepository->findOneBy(['entityName' => $entityClass, 'entityId' => $entityId])) {
            $rating = new Rating();
            $rating->setStatus(Rating::STATUS_VISIBLE);
            $rating->setType($type);
            $rating->setEntityId($entityId);
            $rating->setEntityName($entityClass);
            $this->em->persist($rating);
        }
        return $rating;
    }

    /**
     * Сохраняем голос
     *
     * @param Request  $request
     * @param Rating   $rating
     * @param UserBase $user
     * @param int      $voteValue
     *
     * @throws \Exception
     */
    public function vote(Request $request, Rating $rating, UserBase $user, int $voteValue)
    {
        $ip = $request->getClientIp();

        $voteRepository = $this->em->getRepository(RatingVote::class);
        $vote           = $voteRepository->findOneBy(['rating' => $rating, 'createdBy' => $user]);

        if (!$vote) {
            $vote = $voteRepository->findOneBy(['rating' => $rating, 'ip' => $ip]);
        }

        if (!$vote) {
            $vote = new RatingVote();
            $vote->setRating($rating);
            $vote->setCreatedBy($user);
            $vote->setCreatedAt(new \DateTime());
            $vote->setIp($ip);
            $vote->setVote($voteValue);

            $this->em->persist($vote);
        } else {
            $vote->setCreatedAt(new \DateTime());
            $vote->setVote($voteValue);
            $vote->setIp($ip);
        }

        $this->em->flush();
        $this->em->refresh($rating);

        $rating->recalculateStats();
        $this->em->flush();
    }
}
