'use strict';

import Vue from 'vue';
import Vuelidate from 'vuelidate'
import CommentsApp from "./CommentsApp.vue";

Vue.config.productionTip = false;

Vue.use(Vuelidate);

require('./css/style.scss');

new Vue(CommentsApp);

