<?php
namespace SocialBundle;

use SocialBundle\DependencyInjection\SocialExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Бандл с курсами
 *
 * Class SocialBundle
 *
 * @package SocialBundle
 */
class SocialBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new SocialExtension();
    }
}