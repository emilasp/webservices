<?php

namespace SocialBundle\Repository;

use App\Core\Interfaces\IEntityCore;
use App\Entity\UserBase;
use CoursesBundle\Entity\User;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use SocialBundle\Entity\Comment;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends NestedTreeRepository
{
    /**
     * Заполняем новый коммент
     *
     * @param int         $type
     * @param IEntityCore $entity
     * @param UserBase    $user
     * @param string      $text
     * @param string|null $title
     * @param int         $status
     * @param Comment     $parent
     *
     * @return Comment
     */
    public function addComment(
        int $type,
        IEntityCore $entity,
        UserBase $user,
        string $text,
        string $title = null,
        int $status = Comment::STATUS_MODERATE,
        Comment $parent = null
    ): Comment {
        $comment = new Comment();
        $comment->setTitle($title);
        $comment->setComment($text);
        $comment->setCreatedBy($user);
        $comment->setEntityName(get_class($entity));
        $comment->setEntityId($entity->getId());
        $comment->setType($type);
        $comment->setStatus($status);

        if ($parent) {
            $this->persistAsLastChildOf($comment, $parent);
        } else {
            $this->persistAsLastChild($comment);
        }

        return $comment;
    }

    /**
     * Получаем список комментариев
     *
     * @param IEntityCore $entity
     * @param User $user
     * @param int         $rootType
     *
     * @return array
     */
    public function getComments(IEntityCore $entity, User $user, int $rootType): array
    {
        $comments = $this->getRootComments($entity, $user, $rootType);

        return $comments;
    }

    /**
     * Получаем корневые комментарии
     *
     * @param IEntityCore $entity
     * @param User $user
     * @param int         $rootType
     *
     * @return Comment|null
     */
    private function getRootComments(IEntityCore $entity, User $user, int $rootType): array
    {
        $qb = $this->getQueryBuilder()
            ->select('c')
            ->from('SocialBundle:Comment', 'c')
            ->where('c.entityName=:entityName AND c.entityId=:entityId')
            ->andWhere('c.parent is NULL and c.type=:type')
            ->setParameter(':type', $rootType)
            ->setParameter(':entityName', get_class($entity))
            ->setParameter(':entityId', $entity->getId())
            ->orderBy('c.left', 'ASC');

        if ($rootType === Comment::TYPE_PRIVATE) {
            $qb->andWhere('c.createdBy=:user')
                ->setParameter('user', $user);
        }

        return $qb->getQuery()->getResult();
    }
}
