<?php

namespace SocialBundle\Repository;

use Doctrine\Persistence\ManagerRegistry;
use SocialBundle\Entity\RatingVote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method RatingVote|null find($id, $lockMode = null, $lockVersion = null)
 * @method RatingVote|null findOneBy(array $criteria, array $orderBy = null)
 * @method RatingVote[]    findAll()
 * @method RatingVote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RatingVoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RatingVote::class);
    }

    // /**
    //  * @return RatingVote[] Returns an array of RatingVote objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RatingVote
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
