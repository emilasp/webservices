<?php

namespace AffirmationsBundle\Entity;

use App\Core\Traits\BlameableEntityTrait;
use App\Core\Traits\TimestampableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FileBundle\Entity\File;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="AffirmationsBundle\Repository\AffirmCategoryRepository")
 */
class AffirmCategory
{
    use TimestampableEntityTrait;
    use BlameableEntityTrait;

    public const STATUS_ACTIVE    = 1;
    public const STATUS_NO_ACTIVE = 2;
    public const STATUS_DELETED   = 3;

    public const STATUSES = [
        self::STATUS_ACTIVE    => 'активный',
        self::STATUS_NO_ACTIVE => 'не активный',
        self::STATUS_DELETED   => 'удаленный',
    ];

    public const TYPE_CATEGORY = 1;
    public const TYPE_SPHERE   = 2;

    public const TYPES = [
        self::TYPE_CATEGORY => 'Категория',
        self::TYPE_SPHERE   => 'Сфера',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"appAffirmation"})
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="FileBundle\Entity\File", cascade={"persist", "remove"})
     * @Groups({"appAffirmation"})
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=1000)
     * @Groups({"appAffirmation"})
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"appAffirmation"})
     */
    private $description;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @Gedmo\Mapping\Annotation\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @ORM\OneToMany(targetEntity="AffirmationsBundle\Entity\AffirmAffirmation", mappedBy="sphere")
     */
    private $affirmationsForSphere;

    /**
     * @ORM\OneToMany(targetEntity="AffirmationsBundle\Entity\AffirmAffirmation", mappedBy="category")
     */
    private $affirmationsForCategory;

    public function __construct()
    {
        $this->affirmationsForSphere   = new ArrayCollection();
        $this->affirmationsForCategory = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(?File $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|AffirmAffirmation[]
     */
    public function getAffirmationsForSphere(): Collection
    {
        return $this->affirmationsForSphere;
    }

    /**
     * @return Collection|AffirmAffirmation[]
     */
    public function getAffirmationsForCategory(): Collection
    {
        return $this->affirmationsForCategory;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function addAffirmationsForSphere(AffirmAffirmation $affirmationsForSphere): self
    {
        if (!$this->affirmationsForSphere->contains($affirmationsForSphere)) {
            $this->affirmationsForSphere[] = $affirmationsForSphere;
            $affirmationsForSphere->setSphere($this);
        }

        return $this;
    }

    public function removeAffirmationsForSphere(AffirmAffirmation $affirmationsForSphere): self
    {
        if ($this->affirmationsForSphere->contains($affirmationsForSphere)) {
            $this->affirmationsForSphere->removeElement($affirmationsForSphere);
            // set the owning side to null (unless already changed)
            if ($affirmationsForSphere->getSphere() === $this) {
                $affirmationsForSphere->setSphere(null);
            }
        }

        return $this;
    }

    public function addAffirmationsForCategory(AffirmAffirmation $affirmationsForCategory): self
    {
        if (!$this->affirmationsForCategory->contains($affirmationsForCategory)) {
            $this->affirmationsForCategory[] = $affirmationsForCategory;
            $affirmationsForCategory->setCategory($this);
        }

        return $this;
    }

    public function removeAffirmationsForCategory(AffirmAffirmation $affirmationsForCategory): self
    {
        if ($this->affirmationsForCategory->contains($affirmationsForCategory)) {
            $this->affirmationsForCategory->removeElement($affirmationsForCategory);
            // set the owning side to null (unless already changed)
            if ($affirmationsForCategory->getCategory() === $this) {
                $affirmationsForCategory->setCategory(null);
            }
        }

        return $this;
    }
}
