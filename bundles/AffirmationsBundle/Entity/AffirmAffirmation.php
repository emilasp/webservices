<?php

namespace AffirmationsBundle\Entity;

use App\Core\Traits\BlameableEntityTrait;
use App\Core\Traits\TimestampableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FileBundle\Entity\File;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="AffirmationsBundle\Repository\AffirmAffirmationRepository")
 */
class AffirmAffirmation
{
    use TimestampableEntityTrait;
    use BlameableEntityTrait;

    public const STATUS_ACTIVE    = 1;
    public const STATUS_NO_ACTIVE = 2;
    public const STATUS_PRIVATE   = 3; // Кода пользователь создает новую аффиомацию, то она будет приватной и доступной только ему, пока не пройдет модерацию
    public const STATUS_DELETED   = 4;
    public const STATUS_MODERATE  = 6;

    public const STATUSES = [
        self::STATUS_ACTIVE    => 'активный',
        self::STATUS_NO_ACTIVE => 'не активный',
        self::STATUS_PRIVATE   => 'приватный',
        self::STATUS_DELETED   => 'удаленный',
        self::STATUS_MODERATE  => 'на модерации',
    ];

    public const TYPE_TEXT  = 1;
    public const TYPE_AUDIO = 2;
    public const TYPE_VIDEO = 3;

    public const TYPES = [
        self::TYPE_TEXT  => 'Текст',
        self::TYPE_AUDIO => 'Аудио',
        self::TYPE_VIDEO => 'Видео',
    ];


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"appAffirmation"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=300)
     * @Groups({"appAffirmation"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"appAffirmation"})
     */
    private $reason;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $uses = 0;

    /**
     * @ORM\Column(type="smallint")
     * @Groups({"appAffirmation"})
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="AffirmationsBundle\Entity\AffirmCategory", inversedBy="affirmations")
     * @Groups({"appAffirmation"})
     */
    private $sphere;

    /**
     * @ORM\ManyToOne(targetEntity="AffirmationsBundle\Entity\AffirmCategory", inversedBy="affirmationsForCategory")
     * @Groups({"appAffirmation"})
     */
    private $category;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $single = false;

    /**
     * @ORM\OneToMany(targetEntity="AffirmationsBundle\Entity\AffirmAffirmationUser", mappedBy="affirmation")
     */
    private $affirmationUserLinks;

    /**
     * @ORM\OneToMany(targetEntity="AffirmationsBundle\Entity\AffirmAffirmationVariate", mappedBy="affirmation")
     */
    private $variates;

    /**
     * @ORM\OneToOne(targetEntity="FileBundle\Entity\File", cascade={"persist", "remove"})
     */
    private $image;


    public function __construct()
    {
        $this->affirmationUserLinks = new ArrayCollection();
        $this->variates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setReason(?string $reason): self
    {
        $this->reason = $reason;

        return $this;
    }

    public function getUses(): ?int
    {
        return $this->uses;
    }

    public function setUses(int $uses): self
    {
        $this->uses = $uses;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getSphere(): ?AffirmCategory
    {
        return $this->sphere;
    }

    public function setSphere(?AffirmCategory $sphere): self
    {
        $this->sphere = $sphere;

        return $this;
    }

    public function getCategory(): ?AffirmCategory
    {
        return $this->category;
    }

    public function setCategory(?AffirmCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|AffirmAffirmationUser[]
     */
    public function getAffirmationUserLinks(): Collection
    {
        return $this->affirmationUserLinks;
    }

    public function addAffirmationUserLink(AffirmAffirmationUser $affirmationUserLink): self
    {
        if (!$this->affirmationUserLinks->contains($affirmationUserLink)) {
            $this->affirmationUserLinks[] = $affirmationUserLink;
            $affirmationUserLink->setAffirmation($this);
        }

        return $this;
    }

    public function removeAffirmationUserLink(AffirmAffirmationUser $affirmationUserLink): self
    {
        if ($this->affirmationUserLinks->contains($affirmationUserLink)) {
            $this->affirmationUserLinks->removeElement($affirmationUserLink);
            // set the owning side to null (unless already changed)
            if ($affirmationUserLink->getAffirmation() === $this) {
                $affirmationUserLink->setAffirmation(null);
            }
        }

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|AffirmAffirmationVariate[]
     */
    public function getVariates(): Collection
    {
        return $this->variates;
    }

    public function addVariate(AffirmAffirmationVariate $variate): self
    {
        if (!$this->variates->contains($variate)) {
            $this->variates[] = $variate;
            $variate->setAffirmation($this);
        }

        return $this;
    }

    public function removeVariate(AffirmAffirmationVariate $variate): self
    {
        if ($this->variates->contains($variate)) {
            $this->variates->removeElement($variate);
            // set the owning side to null (unless already changed)
            if ($variate->getAffirmation() === $this) {
                $variate->setAffirmation(null);
            }
        }

        return $this;
    }

    public function getSingle(): ?bool
    {
        return $this->single;
    }

    public function setSingle(bool $single): self
    {
        $this->single = $single;

        return $this;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(?File $image): self
    {
        $this->image = $image;

        return $this;
    }
}
