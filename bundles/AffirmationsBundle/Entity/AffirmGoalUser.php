<?php

namespace AffirmationsBundle\Entity;

use CoursesBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AffirmationsBundle\Repository\AffirmGoalUserRepository")
 */
class AffirmGoalUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AffirmationsBundle\Entity\AffirmGoal", inversedBy="goalUserLinks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $goal;

    /**
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\User", inversedBy="goals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGoal(): ?AffirmGoal
    {
        return $this->goal;
    }

    public function setGoal(?AffirmGoal $goal): self
    {
        $this->goal = $goal;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
