<?php

namespace AffirmationsBundle\Entity;

use App\Core\Traits\BlameableEntityTrait;
use App\Core\Traits\TimestampableEntityTrait;
use CoursesBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AffirmationsBundle\Repository\AffirmAffirmationUserRepository")
 */
class AffirmAffirmationUser
{
    use TimestampableEntityTrait;
    use BlameableEntityTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AffirmationsBundle\Entity\AffirmAffirmation", inversedBy="affirmationUserLinks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $affirmation;

    /**
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\User", inversedBy="affirmationUserLinks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="AffirmationsBundle\Entity\AffirmGoal", inversedBy="affirmationUserLinks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $goal;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="json")
     */
    private $settingsWeb = [];

    /**
     * @ORM\Column(type="json")
     */
    private $settingsMobile = [];

    /**
     * @ORM\Column(type="json")
     */
    private $settingsDesktop = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAffirmation(): ?AffirmAffirmation
    {
        return $this->affirmation;
    }

    public function setAffirmation(?AffirmAffirmation $affirmation): self
    {
        $this->affirmation = $affirmation;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getGoal(): ?AffirmGoal
    {
        return $this->goal;
    }

    public function setGoal(?AffirmGoal $goal): self
    {
        $this->goal = $goal;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getSettingsWeb(): array
    {
        return $this->settingsWeb;
    }

    public function setSettingsWeb(array $settingsWeb): self
    {
        $this->settingsWeb = $settingsWeb;

        return $this;
    }

    public function getSettingsMobile(): array
    {
        return $this->settingsMobile;
    }

    public function setSettingsMobile(array $settingsMobile): self
    {
        $this->settingsMobile = $settingsMobile;

        return $this;
    }

    public function getSettingsDesktop(): array
    {
        return $this->settingsDesktop;
    }

    public function setSettingsDesktop(array $settingsDesktop): self
    {
        $this->settingsDesktop = $settingsDesktop;

        return $this;
    }
}
