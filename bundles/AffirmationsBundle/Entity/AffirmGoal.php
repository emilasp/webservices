<?php

namespace AffirmationsBundle\Entity;

use App\Core\Traits\BlameableEntityTrait;
use App\Core\Traits\TimestampableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FileBundle\Entity\File;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="AffirmationsBundle\Repository\AffirmGoalRepository")
 */
class AffirmGoal
{
    use TimestampableEntityTrait;
    use BlameableEntityTrait;

    public const STATUS_ACTIVE    = 1;
    public const STATUS_NO_ACTIVE = 2;
    public const STATUS_DELETED   = 3;

    public const STATUSES = [
        self::STATUS_ACTIVE    => 'активный',
        self::STATUS_NO_ACTIVE => 'не активный',
        self::STATUS_DELETED   => 'удаленный',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"appAffirmation"})
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="FileBundle\Entity\File", cascade={"persist", "remove"})
     * @Groups({"appAffirmation"})
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"appAffirmation"})
     */
    private $name;

    /**
     * @ORM\Column(name="changes_in_me", type="text", nullable=true)
     * @Groups({"appAffirmation"})
     */
    private $changesInMe;

    /**
     * @ORM\Column(name="changes_in_outside", type="text", nullable=true)
     * @Groups({"appAffirmation"})
     */
    private $changesInOutside;

    /**
     * @ORM\Column(type="smallint")
     * @Groups({"appAffirmation"})
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="AffirmationsBundle\Entity\AffirmAffirmationUser", mappedBy="goal")
     */
    private $affirmationUserLinks;

    /**
     * @ORM\OneToMany(targetEntity="AffirmationsBundle\Entity\AffirmGoalUser", mappedBy="goal")
     */
    private $goalUserLinks;

    public function __construct()
    {
        $this->affirmationUserLinks = new ArrayCollection();
        $this->goalUserLinks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(?File $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getChangesInMe(): ?string
    {
        return $this->changesInMe;
    }

    public function setChangesInMe(?string $changesInMe): self
    {
        $this->changesInMe = $changesInMe;

        return $this;
    }

    public function getChangesInOutside(): ?string
    {
        return $this->changesInOutside;
    }

    public function setChangesInOutside(?string $changesInOutside): self
    {
        $this->changesInOutside = $changesInOutside;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|AffirmAffirmationUser[]
     */
    public function getAffirmationUserLinks(): Collection
    {
        return $this->affirmationUserLinks;
    }

    public function addAffirmationUserLink(AffirmAffirmationUser $affirmationUserLink): self
    {
        if (!$this->affirmationUserLinks->contains($affirmationUserLink)) {
            $this->affirmationUserLinks[] = $affirmationUserLink;
            $affirmationUserLink->setGoal($this);
        }

        return $this;
    }

    public function removeAffirmationUserLink(AffirmAffirmationUser $affirmationUserLink): self
    {
        if ($this->affirmationUserLinks->contains($affirmationUserLink)) {
            $this->affirmationUserLinks->removeElement($affirmationUserLink);
            // set the owning side to null (unless already changed)
            if ($affirmationUserLink->getGoal() === $this) {
                $affirmationUserLink->setGoal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AffirmGoalUser[]
     */
    public function getGoalUserLinks(): Collection
    {
        return $this->goalUserLinks;
    }

    public function addGoalUserLink(AffirmGoalUser $goalUserLink): self
    {
        if (!$this->goalUserLinks->contains($goalUserLink)) {
            $this->goalUserLinks[] = $goalUserLink;
            $goalUserLink->setGoal($this);
        }

        return $this;
    }

    public function removeGoalUserLink(AffirmGoalUser $goalUserLink): self
    {
        if ($this->goalUserLinks->contains($goalUserLink)) {
            $this->goalUserLinks->removeElement($goalUserLink);
            // set the owning side to null (unless already changed)
            if ($goalUserLink->getGoal() === $this) {
                $goalUserLink->setGoal(null);
            }
        }

        return $this;
    }
}
