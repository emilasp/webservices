<?php

namespace AffirmationsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="affirm_affirmation_variate",
 *     indexes={
 *         @ORM\Index(name="idx_affirm_affirmation_variate_name", columns={"name"}),
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="AffirmationsBundle\Repository\AffirmAffirmationVariateRepository")
 */
class AffirmAffirmationVariate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=300)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="AffirmationsBundle\Entity\AffirmAffirmation", inversedBy="variates")
     * @ORM\JoinColumn(nullable=false)
     */
    private $affirmation;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isMain = false;

    /**
     * @Gedmo\Mapping\Annotation\SortablePosition
     * @ORM\Column(name="position", type="integer", options={"default": 0})
     */
    private $position = 0;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAffirmation(): ?AffirmAffirmation
    {
        return $this->affirmation;
    }

    public function setAffirmation(?AffirmAffirmation $affirmation): self
    {
        $this->affirmation = $affirmation;

        return $this;
    }

    public function getIsMain(): bool
    {
        return $this->isMain;
    }

    public function setIsMain(bool $isMain): self
    {
        $this->isMain = $isMain;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }
}
