<?php

namespace AffirmationsBundle\Repository;

use AffirmationsBundle\Entity\AffirmGoalUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AffirmGoalUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method AffirmGoalUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method AffirmGoalUser[]    findAll()
 * @method AffirmGoalUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AffirmGoalUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AffirmGoalUser::class);
    }

    // /**
    //  * @return AffirmGoalUser[] Returns an array of AffirmGoalUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AffirmGoalUser
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
