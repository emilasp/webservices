<?php

namespace AffirmationsBundle\Repository;

use AffirmationsBundle\Entity\AffirmGoal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AffirmGoal|null find($id, $lockMode = null, $lockVersion = null)
 * @method AffirmGoal|null findOneBy(array $criteria, array $orderBy = null)
 * @method AffirmGoal[]    findAll()
 * @method AffirmGoal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AffirmGoalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AffirmGoal::class);
    }

    // /**
    //  * @return AffirmGoal[] Returns an array of AffirmGoal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AffirmGoal
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
