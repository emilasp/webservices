<?php

namespace AffirmationsBundle\Repository;

use AffirmationsBundle\Entity\AffirmAffirmation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AffirmAffirmation|null find($id, $lockMode = null, $lockVersion = null)
 * @method AffirmAffirmation|null findOneBy(array $criteria, array $orderBy = null)
 * @method AffirmAffirmation[]    findAll()
 * @method AffirmAffirmation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AffirmAffirmationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AffirmAffirmation::class);
    }

    // /**
    //  * @return AffirmAffirmation[] Returns an array of AffirmAffirmation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AffirmAffirmation
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
