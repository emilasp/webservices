<?php

namespace AffirmationsBundle\Repository;

use AffirmationsBundle\Entity\AffirmCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AffirmCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method AffirmCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method AffirmCategory[]    findAll()
 * @method AffirmCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AffirmCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AffirmCategory::class);
    }

    // /**
    //  * @return AffirmCategory[] Returns an array of AffirmCategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AffirmCategory
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
