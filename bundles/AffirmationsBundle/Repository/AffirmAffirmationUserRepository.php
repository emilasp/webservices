<?php

namespace AffirmationsBundle\Repository;

use AffirmationsBundle\Entity\AffirmAffirmationUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AffirmAffirmationUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method AffirmAffirmationUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method AffirmAffirmationUser[]    findAll()
 * @method AffirmAffirmationUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AffirmAffirmationUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AffirmAffirmationUser::class);
    }

    // /**
    //  * @return AffirmAffirmationUser[] Returns an array of AffirmAffirmationUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AffirmAffirmationUser
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
