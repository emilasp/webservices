<?php

namespace AffirmationsBundle\Repository;

use AffirmationsBundle\Entity\AffirmAffirmationVariate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AffirmAffirmationVariate|null find($id, $lockMode = null, $lockVersion = null)
 * @method AffirmAffirmationVariate|null findOneBy(array $criteria, array $orderBy = null)
 * @method AffirmAffirmationVariate[]    findAll()
 * @method AffirmAffirmationVariate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AffirmAffirmationVariateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AffirmAffirmationVariate::class);
    }

    // /**
    //  * @return AffirmAffirmationVariate[] Returns an array of AffirmAffirmationVariate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AffirmAffirmationVariate
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
