<?php

namespace AffirmationsBundle\Twig;

use AffirmationsBundle\Entity\File;
use AffirmationsBundle\Service\AffirmationService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class FileExtension
 *
 * @package AffirmationsBundle\Twig
 */
class AffirmationExtension extends AbstractExtension
{
    private $fileService;

    /**
     * FileExtension constructor.
     *
     * @param AffirmationService $fileService
     */
    public function __construct(AffirmationService $fileService)
    {
        $this->fileService = $fileService;
    }
}
