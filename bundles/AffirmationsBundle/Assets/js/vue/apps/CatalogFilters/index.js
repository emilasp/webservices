'use strict';

import Vue from 'vue';
import App from "./App";

import './css/app.scss';

Vue.config.productionTip = false;

// Validate - https://vuetifyjs.com/ru/components/dialogs

// Vuetify
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify);
App.vuetify =  new Vuetify({});

new Vue(App);
