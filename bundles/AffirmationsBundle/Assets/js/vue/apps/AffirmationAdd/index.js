'use strict';

import Vue from 'vue';
import App from "./App";
import SphereSelect from "./components/SphereSelect";

import './css/app.scss';

Vue.config.productionTip = false;

// Validate - https://vuetifyjs.com/ru/components/dialogs
import Vuelidate from "vuelidate";
Vue.use(Vuelidate);

// Vuetify
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify);
App.vuetify =  new Vuetify({});


// Store - https://habr.com/ru/post/421551/
import {store} from '../_store';
App.store = store;

new Vue(App);
