import Vue from 'vue';
import Vuex from 'vuex';
import Axios from "axios";

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        spheres: null,
        sphereCur: null,
    },
    getters: {
        SPHERES: state => state.spheres,
        SPHERE_INDEX: state => state.sphereCur,
        //
        GOALS_MY: state => state.spheres,
        GOALS_MY_CUR: state => state.sphereCur,
    },
    mutations: {
        SET_SPHERES: (state,payload) => {
            state.spheres = payload;
        },
        SET_SPHERE_INDEX: (state,payload) => {
            state.sphereCur = payload;
        },
        //

        SET_GOALS_MY: (state,payload) => {
            state.spheres = payload;
        },
        SET_GOALS_MY_CUR: (state,payload) => {
            state.sphereCur = payload;
        }
    },
    actions: {
        GET_SPHERES: async (context, payload) => {
            console.log('Get Spheres')

            let {data} = await Axios.get(process.env.PARAMS.HOST + '/api/affirmations/get-spheres')
            context.commit('SET_SPHERES', data);
        },
        SET_GOALS_MY: async (context, payload) => {
            console.log('Get Goals')

            let {data} = await Axios.get(process.env.PARAMS.HOST + '/api/affirmations/get-goals-my')
            context.commit('SET_GOALS_MY', data);
        },
    },
});