<?php

namespace AffirmationsBundle\Service;

use AffirmationsBundle\Entity\AffirmAffirmation;
use AffirmationsBundle\Entity\AffirmAffirmationVariate;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Сервис отвечает за взаимодействие между Entity проекта и File entity бандла
 *
 * Class FileRelationService
 *
 * @package AffirmationsBundle\Service
 */
class AffirmationService
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /**
     * CourseUserService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $eventDispatcher)
    {
        $this->em              = $em;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Создаем Аффирмацию
     *
     * @param string $text
     * @param bool   $isSingle
     * @param int    $type
     * @param int    $status
     *
     * @return AffirmAffirmation
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAffirmation(
        string $text,
        bool $isSingle = false,
        int $type = AffirmAffirmation::TYPE_TEXT,
        int $status = AffirmAffirmation::STATUS_ACTIVE
    ): AffirmAffirmation {
        $affirmation = new AffirmAffirmation();
        $affirmation->setType($type);
        $affirmation->setStatus($status);
        $affirmation->setSingle($isSingle);
        $affirmation->setName(trim($text));
        $this->em->persist($affirmation);

        return $affirmation;
    }

    /**
     * Создаем вариант аффирмации
     *
     * @param string            $text
     * @param AffirmAffirmation $affirmation
     * @param int               $position
     * @param bool              $isMain
     *
     * @return AffirmAffirmationVariate
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAffirmationVariate(
        string $text,
        AffirmAffirmation $affirmation,
        int $position,
        bool $isMain = false
    ): AffirmAffirmationVariate {
        $variate = new AffirmAffirmationVariate();
        $variate->setAffirmation($affirmation);
        $variate->setPosition($position);
        $variate->setIsMain($isMain);
        $variate->setName(trim($text));

        $this->em->persist($variate);

        return $variate;
    }
}