<?php

namespace AffirmationsBundle\Command;

use AffirmationsBundle\Entity\AffirmAffirmationVariate;
use AffirmationsBundle\Service\AffirmationService;
use App\Core\Abstracts\ACommandActions;
use App\Service\ElasticSearchService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class ParseCommands
 *
 * @package AffirmationsBundle\Command
 */
class ParseCommand extends ACommandActions
{
    //EAV
    //- https://xn----8sbafpuba7a2bg3ba6a.xn--p1ai/products

    //Affirmations
    //- https://metodorf.ru/affirm/na_dengi_bolshoy_sbornik.php
    //- https://metodorf.ru/affirm/na_dengi_bolshoy_sbornik.php

    /** @var ElasticSearchService */
    private $elasticSearch;

    /** @var AffirmationService */
    private $affirmationService;

    /**
     * ACommandActions constructor.
     *
     * @param string|null            $name
     * @param EntityManagerInterface $em
     */
    public function __construct(
        string $name = null,
        EntityManagerInterface $em = null,
        EventDispatcherInterface $eventDispatcher = null,
        Filesystem $fileSystem = null,
        KernelInterface $kernel = null,
        ContainerInterface $container = null,
        ElasticSearchService $elasticSearch = null,
        AffirmationService $affirmationService = null
    ) {
        parent::__construct($name, $em, $eventDispatcher, $fileSystem, $kernel, $container);

        $this->elasticSearch      = $elasticSearch;
        $this->affirmationService = $affirmationService;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $this->setName('parser:affirmations:affirmations');
        $this->setDescription("Парсим данные");
    }

    /**
     * Устанавливаем текущие уроки(periodical) для полоьзователей
     *
     * @param InputInterface $input
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function allAffirmationsRfAction(InputInterface $input)
    {
        //$this->getSimilarityData('Мой канал изобилия расширяется с каждым днем');


        $host  = 'https://все-аффирмации.рф';
        $links = [
            '/tekst-zhenschiny',
            //'/muzhchiny',
            //'/zhenschiny',
        ];

        $this->display("Начинаем сбор категорий");

        //Сбор страниц с категориями
        $allCategoriesCount = count($links);
        $categoriesLinks    = [];
        foreach ($links as $index => $link) {
            $url     = $this->getUrl($host . $link);
            $html    = file_get_contents($url);
            $crawler = new Crawler(null, $url);
            $crawler->addHtmlContent($html, 'UTF-8');

            $categoriesLinks = array_merge($categoriesLinks,
                $crawler->filter('#aside1 .b-sidebar .b-accordion a')->each(function (Crawler $node, $i) {
                    return $node->attr('href');
                }));

            $this->display("{$index}/{$allCategoriesCount}: {$link}, уже " . count($categoriesLinks) . 'ссылок');
        }

        $this->display("Начинаем сбор страниц - парсинг списков");

        // Сбор страниц для парсинга со списков
        $allListsCount   = count($categoriesLinks);
        $categoriesLinks = array_diff($categoriesLinks, ['', '/']);
        $categoriesLinks = array_unique($categoriesLinks);
        $pageLinks       = [];
        foreach ($categoriesLinks as $index => $link) {
            $this->display("{$allListsCount}/{$index}: {$link}, уже " . count($pageLinks) . 'ссылок');
            // Сбор паджинации
            $maxPage        = 2;// Сделать Callback
            $getPagePattern = '?page={page}';

            for ($page = 1; $page <= $maxPage; $page++) {
                $url     = $this->getUrl($host . $link) . str_replace('{page}', $page, $getPagePattern);
                $html    = file_get_contents($url);
                $crawler = new Crawler(null, $url);
                $crawler->addHtmlContent($html, 'UTF-8');

                $pageLinks = array_merge($pageLinks, $crawler->filter('.b-content .b-att__item .el-att__tittle')->each(function (Crawler $node, $i) {
                    return $node->attr('href');
                }));

                $this->display("{$link}: {$url} Pages" . count($pageLinks));
            }
        }

        // Сбор аффирмаций со страниц просмотра
        $pageLinks     = array_unique($pageLinks);
        $allPagesCount = count($pageLinks);
        $affirmations  = [];
        foreach ($pageLinks as $index => $pageLink) {
            $this->display("{$allPagesCount}/{$index}: {$pageLink}");

            $url     = $this->getUrl($host . $pageLink);
            $html    = file_get_contents($url);
            $crawler = new Crawler(null, $url);
            $crawler->addHtmlContent($html, 'UTF-8');

            $affirmations[$index]['affirmations'] = [];
            $affirmations[$index]['title']        = $crawler->filter('h1')->text();

            if ($crawler->filter('.b-content__description')->count()) {
                $html  = $crawler->filter('.b-content__description')->html();
                $html  = str_ireplace(["<br />", "<br>", "<br/>"], "\r\n", $html);
                $lines = preg_split('/\\r\\n?|\\n/', $html);

                foreach ($lines as $line) {
                    $line = trim(strip_tags($line));
                    if (strlen($line) > 10 && strlen($line) < 300 && strpos($line, 'Текст аффирмации') === false) {
                        $affirmations[$index]['affirmations'][] = trim(trim($line, ',.!?-;:'));
                    }
                }
            }
        }

        $this->affitmationsProcess($affirmations);
    }

    /**
     * Записываем аффирмации
     *
     * @param array $affirmations
     *
     * @throws \Doctrine\ORM\ORMException
     */
    private function affitmationsProcess(array $affirmations)
    {
        foreach ($affirmations as $data) {
            if($affirmationData = $data['affirmations']) {
                $single          = count($affirmationData) === 1 ? true : false;

                $affirmationEntity = $this->affirmationService->createAffirmation($data['title'], $single);

                if (!$this->em->getRepository(AffirmAffirmationVariate::class)->findOneBy(['name' => $affirmationData[0]])) {
                    $this->display("Create affirmation: " . count($affirmationData), self::COLOR_GREEN);

                    foreach ($affirmationData as $index => $affirmation) {
                        $this->affirmationService->createAffirmationVariate($affirmation, $affirmationEntity, $index, !$index);
                    }
                    $this->em->flush();
                } else {
                    $this->display("Isset ", self::COLOR_YELLOW);
                }
            }
        }
    }


    /**
     * Получаем даныне по схожести
     *
     * @param string $text
     *
     * @return array
     */
    private function getSimilarityPercent(string $text): array
    {
        // Каждый воспринимает эту часть жизни на своём уровне. - 2.85
        // Каждый воспринимает жизнь на уровне для себя эту часть . - 0.925
        // Каждый воспринимает эту часть жизни по своему - 0.873

        $data = $this->elasticSearch->getSearchMeta($text, 'affirmation', ['affirmation_variate'],
            ['name' => ['fuzziness' => 'AUTO', 'analyzer' => 'ru_analyzer']], 10);

        if ($data) {
            similar_text(mb_strtolower(trim($text)), mb_strtolower(trim($data[0]['item']['name'])), $percent);
            $data[0]['percent'] = $percent;
        }

        return $data;
    }

    /**
     * @param string $link
     *
     * @return string
     */
    private function getUrl(string $link): string
    {
        if (preg_match('#^([\w\d]+://)([^/]+)(.*)$#iu', $link, $m)) {
            $link = $m[1] . idn_to_ascii($m[2], IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46) . $m[3];
        }
        $link = urldecode($link);
        $link = rawurlencode($link);
        $link = str_replace(array('%3A', '%2F'), array(':', '/'), $link);
        return $link;
    }
}
