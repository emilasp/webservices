<?php
namespace AffirmationsBundle;

use AffirmationsBundle\DependencyInjection\AffirmationsExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AffirmationsBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new AffirmationsExtension();
    }
}