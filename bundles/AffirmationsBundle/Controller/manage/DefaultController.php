<?php

namespace AffirmationsBundle\Controller\manage;

use App\Core\Abstracts\AController;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class DefaultController
 *
 * @Route("/affirmations")
 * @package EavBundle\Controller
 *
 * @Security("is_granted('ROLE_USER')")
 */
class DefaultController extends AController
{
    /**
     * @Route("/affirmation-add", name="affirmations_affirmation_add" , options={"expose"=true})
     *
     * @Template(template="/Default/affirmation.add.html.twig")
     * @return Response
     */
    public function affirmationsAddAction(EntityManagerInterface $entityManager, MessageBusInterface $bus)
    {
        return [];
    }
}
