<?php

namespace AffirmationsBundle\Controller;

use AffirmationsBundle\Entity\AffirmAffirmation;
use AffirmationsBundle\Entity\AffirmCategory;
use App\Core\Abstracts\AController;
use App\Core\Helpers\EntityHelper;
use EavBundle\Entity\EavAttribute;
use EavBundle\Entity\EavAttributeEntity;
use EavBundle\Service\EavService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiController
 *
 * @Route("/api/affirmations")
 *
 * @Security("is_granted('ROLE_USER')")
 *
 * @package App\Controller
 */
class ApiController extends AController
{
    /**
     * @Route("/get-attributes", name="affirmations_api_get_attributes")
     *
     * @return Response
     * @throws \Exception
     */
    public function getAttributesApi(): Response
    {
        $attributes = $this->getEm()->getRepository(EavAttribute::class)->findAll();

        $data = $this->get('serializer')->serialize(
            [
                'attributes' => EntityHelper::setFieldAsKeyWithGroup($attributes, 'grouped'),
                'types'      => EavAttribute::TYPES
            ],
            'json',
            ['groups' => ['appEav']]
        );
        return new Response($data);
    }

    /**
     * @Route("/set-attribute", name="eav_api_set_attribute")
     *
     * @param Request    $request
     * @param EavService $service
     *
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function setAttributeApi(Request $request, EavService $service): Response
    {
        $data = json_decode($request->getContent());

        if ($data->id) {
            $attribute = $this->getEm()->getRepository(EavAttribute::class)->find($data->id);
        } else {
            $attribute = new EavAttribute();

            $this->getEm()->persist($attribute);
        }

        $attribute->setName($data->name);
        $attribute->setGrouped($data->grouped);
        $attribute->setType($data->type);

        $service->removeAttributeValues($attribute, $data->values);
        $service->addAttributeValues($attribute, $data->values);

        $this->getEm()->flush();

        return new JsonResponse(['success' => true, 'message' => 'Good']);
    }

    /**
     * @Route("/set-attributes-entity", name="eav_api_set_entity_attribute")
     *
     * @param Request    $request
     * @param EavService $service
     *
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function setEntityAttributesApi(Request $request, EavService $service): Response
    {
        $data = json_decode($request->getContent());

        if ($data->id) {
            $entity            = $this->getEm()->getRepository(EavAttributeEntity::class)->find($data->id);
            $attributeEntities = $this->getEm()->getRepository(EavAttributeEntity::class)->findBy(['name' => $entity->getName()]);

            foreach ($attributeEntities as $attributeEntity) {
                $attributeEntity->setName($data->name);
                $attributeEntity->setEntityClass($data->entityClass);
            }
            $this->getEm()->flush();
        } else {
            $attributeEntities = $this->getEm()->getRepository(EavAttributeEntity::class)->findBy(['name' => $data->name]);
        }

        $issetAttributeIds = array_map(function ($item) {
            return $item->getAttribute()->getId();
        }, $attributeEntities);

        foreach ($issetAttributeIds as $attributeId) {
            if (array_search($attributeId, $data->attributes) === false) {
                $attribute = $this->getEm()->getRepository(EavAttribute::class)->find($attributeId);
                $entity    = $this->getEm()->getRepository(EavAttributeEntity::class)->findOneBy(['name' => $data->name, 'attribute' => $attribute]);
                $this->getEm()->remove($entity);
            }
        }

        foreach ($data->attributes as $attributeId) {
            if (array_search($attributeId, $issetAttributeIds) === false) {
                $attribute = $this->getEm()->getRepository(EavAttribute::class)->find($attributeId);

                $newEntity = new EavAttributeEntity();
                $newEntity->setName($data->name);
                $newEntity->setEntityClass($data->entityClass);
                $newEntity->setAttribute($attribute);

                $this->getEm()->persist($newEntity);
            }
        }

        $this->getEm()->flush();

        return new JsonResponse(['success' => true, 'message' => 'Good']);
    }

    /**
     * @Route("/get-entity-attributes", name="eav_api_get_entity_attributes")
     *
     * @return Response
     * @throws \Exception
     */
    public function getEntityAttributesApi(): Response
    {
        $entityAttributes = $this->getEm()->getRepository(EavAttributeEntity::class)->findAll();
        $attributes       = $this->getEm()->getRepository(EavAttribute::class)->findAll();

        $data = $this->get('serializer')->serialize(
            [
                'entities'   => EntityHelper::setFieldAsKeyWithGroup($entityAttributes, 'name'),
                'attributes' => EntityHelper::setFieldAsKeyWithGroup($attributes, 'grouped'),
            ],
            'json',
            ['groups' => ['appEav']]
        );
        return new Response($data);
    }

    /**
     * @Route("/get-spheres", name="affirmations_api_get_spheres")
     *
     * @return Response
     * @throws \Exception
     */
    public function getAffirmationsSpheresApi(): Response
    {
        $attributes = $this->getEm()->getRepository(AffirmCategory::class)->findBy([
            'type'   => AffirmCategory::TYPE_SPHERE,
            'status' => AffirmCategory::STATUS_ACTIVE,
        ]);

        $data = $this->get('serializer')->serialize($attributes, 'json', ['groups' => ['appAffirmation']]);

        return new Response($data);
    }
    /**
     * @Route("/get-catalog-filters", name="affirmations_api_get_catalog_filters")
     *
     * @return Response
     * @throws \Exception
     */
    public function getCatalogFiltersApi(): Response
    {
        $entityAttributes = $this->getEm()->getRepository(EavAttributeEntity::class)->findBy(['entityClass' => AffirmAffirmation::class]);

        /*$data = [];
        foreach ($entityAttributes as $entityAttribute) {
            $attribute = $entityAttribute->getAttribute();
            $values = $attribute->getValues();

            $data[$entityAttribute]
        }*/

        $data = $this->get('serializer')->serialize(['data' => $entityAttributes], 'json', ['groups' => ['appEav']]);

        return new Response($data);
    }
}
