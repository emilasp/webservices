<?php

namespace AffirmationsBundle\Controller;

use App\Core\Abstracts\AController;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class DefaultController
 *
 * @Route("/affirmations")
 * @package EavBundle\Controller
 *
 */
class QtController extends AController
{
    /**
     * @Route("/affirmations", name="affirmations_affirmations" , options={"expose"=true})
     *
     * @Template(template="/Catalog/affirmations.catalog.html.twig")
     * @return Response
     */
    public function affirmationsCatalogAction(EntityManagerInterface $entityManager)
    {
        return [];
    }
}
