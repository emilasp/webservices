<?php

namespace TradeBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\BlameableEntityTrait;
use App\Core\Traits\TimestampableEntityTrait;
use FileBundle\Entity\File;
use TradeBundle\Repository\TradingStrategyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TradingStrategyRepository::class)
 */
class TradingStrategy implements IEntityCore
{
    use TimestampableEntityTrait;
    use BlameableEntityTrait;

    const TREND_TYPE_NONE  = 0;
    const TREND_TYPE_FLET  = 1;
    const TREND_TYPE_LONG  = 2;
    const TREND_TYPE_SHORT = 3;

    const TREND_TYPES = [
        self::TREND_TYPE_NONE  => 'none',
        self::TREND_TYPE_FLET  => 'flet',
        self::TREND_TYPE_LONG  => 'long',
        self::TREND_TYPE_SHORT => 'short',
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $market;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $timeframe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $instrument;

    /**
     * @ORM\Column(type="smallint", options={"unsigned":true})
     */
    private $trendType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $enterRules;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $exitRules;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $riskManagement;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $profitPercent = 0;

    /**
     * @ORM\Column(type="boolean", options={"dafault": false})
     */
    private $isRobotTest = false;

    /**
     * @ORM\Column(type="smallint", options={"unsigned":true, "dafault": 3})
     */
    private $risk = 3;

    /**
     * @ORM\Column(type="smallint", options={"unsigned":true, "dafault": 3})
     */
    private $difficultly = 3;

    /**
     * @ORM\Column(type="smallint", options={"unsigned":true, "dafault": 0})
     */
    private $rating = 0;

    /**
     * Изображение
     *
     * @ORM\OneToOne(targetEntity="FileBundle\Entity\File", cascade={"persist", "remove"})
     */
    private $image;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMarket(): ?string
    {
        return $this->market;
    }

    public function setMarket(?string $market): self
    {
        $this->market = $market;

        return $this;
    }

    public function getTimeframe(): ?string
    {
        return $this->timeframe;
    }

    public function setTimeframe(?string $timeframe): self
    {
        $this->timeframe = $timeframe;

        return $this;
    }

    public function getInstrument(): ?string
    {
        return $this->instrument;
    }

    public function setInstrument(?string $instrument): self
    {
        $this->instrument = $instrument;

        return $this;
    }

    public function getTrendType(): int
    {
        return $this->trendType;
    }

    public function setTrendType(int $trendType): self
    {
        $this->trendType = $trendType;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEnterRules(): ?string
    {
        return $this->enterRules;
    }

    public function setEnterRules(?string $enterRules): self
    {
        $this->enterRules = $enterRules;

        return $this;
    }

    public function getExitRules(): ?string
    {
        return $this->exitRules;
    }

    public function setExitRules(?string $exitRules): self
    {
        $this->exitRules = $exitRules;

        return $this;
    }

    public function getRiskManagement(): ?string
    {
        return $this->riskManagement;
    }

    public function setRiskManagement(?string $riskManagement): self
    {
        $this->riskManagement = $riskManagement;

        return $this;
    }

    public function getProfitPercent(): string
    {
        return $this->profitPercent;
    }

    public function setProfitPercent(string $profitPercent): self
    {
        $this->profitPercent = $profitPercent;

        return $this;
    }

    public function getIsRobotTest(): bool
    {
        return $this->isRobotTest;
    }

    public function setIsRobotTest(bool $isRobotTest): self
    {
        $this->isRobotTest = $isRobotTest;

        return $this;
    }

    public function getRisk(): int
    {
        return $this->risk;
    }

    public function setRisk(int $risk): self
    {
        $this->risk = $risk;

        return $this;
    }

    public function getDifficultly(): int
    {
        return $this->difficultly;
    }

    public function setDifficultly(int $difficultly): self
    {
        $this->difficultly = $difficultly;

        return $this;
    }

    public function getRating(): int
    {
        return $this->rating;
    }

    public function setRating(int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }


    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(?File $image): self
    {
        $this->image = $image;

        return $this;
    }
}
