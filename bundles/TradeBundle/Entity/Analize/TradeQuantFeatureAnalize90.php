<?php

namespace TradeBundle\Entity\Analize;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="trade_quant_feature_analize_90",
 *     indexes={
 *      @ORM\Index(name="searchIdx", columns={"timeframes", "symbol", "type_position","feature","number"}),
 *      @ORM\Index(name="search2Idx", columns={"timeframes", "symbol", "type_position", "math_probe"}),
 *      @ORM\Index(name="search3Idx", columns={"hash"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="TradeBundle\Repository\Analize\TradeQuantFeatureAnalize90Repository")
 */
class TradeQuantFeatureAnalize90 extends ATradeQuantFeatureAnalizeBase
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
