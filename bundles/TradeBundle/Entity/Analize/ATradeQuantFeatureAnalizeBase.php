<?php

namespace TradeBundle\Entity\Analize;

use App\Core\Interfaces\IEntityCore;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/** @ORM\MappedSuperclass() */
abstract class ATradeQuantFeatureAnalizeBase implements IEntityCore
{
    /**
     * @ORM\Column(type="string", length=42, nullable=false)
     * @var string
     */
    private $hash;

    /**
     * @ORM\Column(type="smallint", nullable=false, options={"unsigned":true})
     * @var string
     */
    private $timeframes;
    
    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     * @var string
     */
    private $symbol;

    /**
     * @Groups({"trading"})
     * @ORM\Column(type="smallint", nullable=false, options={"unsigned":true})
     * @var int
     */
    private $typePosition;

    /**
     * @Groups({"trading"})
     * @ORM\Column(type="string", length=100, nullable=false)
     * @var string
     */
    private $feature;

    /**
     * @Groups({"trading"})
     * @ORM\Column(type="string", length=200, nullable=false)
     * @var int
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=1000, nullable=false, options={"unsigned":true})
     * @var string
     */
    private $config;

    /**
     * @Groups({"trading"})
     * @ORM\Column(type="decimal", scale=5, precision=10, nullable=false)
     * @var float
     */
    private $profit;
    /**
     * @ORM\Column(type="decimal", scale=5, precision=10, nullable=false)
     * @var float
     */
    private $profitPlus;
    /**
     * @ORM\Column(type="decimal", scale=5, precision=10, nullable=false)
     * @var float
     */
    private $profitMinus;

    /**
     * @Groups({"trading"})
     * @ORM\Column(type="decimal", scale=5, precision=10, nullable=false)
     * @var float
     */
    private $slumpAvg;

    /**
     * @Groups({"trading"})
     * @ORM\Column(type="integer", nullable=false)
     * @var int
     */
    private $trades;
    /**
     * @ORM\Column(type="integer", nullable=false)
     * @var int
     */
    private $tradesPlus;
    /**
     * @ORM\Column(type="integer", nullable=false)
     * @var int
     */
    private $tradesMinus;

    /**
     * @Groups({"trading"})
     * @ORM\Column(type="decimal", scale=2, precision=4, nullable=false)
     * @var float
     */
    private $probeTrades;


    /**
     * @Groups({"trading"})
     * @ORM\Column(type="decimal", scale=2, precision=4, nullable=false)
     * @var float
     */
    private $profitRate;

    /**
     * @Groups({"trading"})
     * @ORM\Column(type="decimal", scale=2, precision=4, nullable=false)
     * @var float
     */
    private $mathProbe;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @var \DateTime
     */
    protected $dateTimeMax;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @var \DateTime
     */
    protected $dateTimeMin;

    /**
     * @Groups({"trading"})
     * @ORM\Column(type="smallint", nullable=false)
     * @var int
     */
    private $tp;

    /**
     * @Groups({"trading"})
     * @ORM\Column(type="smallint", nullable=false)
     * @var int
     */
    private $sl;


    public function getTypePosition(): ?int
    {
        return $this->typePosition;
    }

    public function setTypePosition(int $typePosition): self
    {
        $this->typePosition = $typePosition;

        return $this;
    }

    public function getFeature(): ?string
    {
        return $this->feature;
    }

    public function setFeature(string $feature): self
    {
        $this->feature = $feature;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getConfig(): ?string
    {
        return $this->config;
    }

    public function setConfig(string $config): self
    {
        $this->config = $config;

        return $this;
    }

    public function getProfit(): ?string
    {
        return $this->profit;
    }

    public function setProfit(string $profit): self
    {
        $this->profit = $profit;

        return $this;
    }

    public function getProfitPlus(): ?string
    {
        return $this->profitPlus;
    }

    public function setProfitPlus(string $profitPlus): self
    {
        $this->profitPlus = $profitPlus;

        return $this;
    }

    public function getProfitMinus(): ?string
    {
        return $this->profitMinus;
    }

    public function setProfitMinus(string $profitMinus): self
    {
        $this->profitMinus = $profitMinus;

        return $this;
    }

    public function getSlumpAvg(): ?string
    {
        return $this->slumpAvg;
    }

    public function setSlumpAvg(string $slumpAvg): self
    {
        $this->slumpAvg = $slumpAvg;

        return $this;
    }

    public function getTrades(): ?int
    {
        return $this->trades;
    }

    public function setTrades(int $trades): self
    {
        $this->trades = $trades;

        return $this;
    }

    public function getTradesPlus(): ?int
    {
        return $this->tradesPlus;
    }

    public function setTradesPlus(int $tradesPlus): self
    {
        $this->tradesPlus = $tradesPlus;

        return $this;
    }

    public function getTradesMinus(): ?int
    {
        return $this->tradesMinus;
    }

    public function setTradesMinus(int $tradesMinus): self
    {
        $this->tradesMinus = $tradesMinus;

        return $this;
    }

    public function getProbeTrades(): ?string
    {
        return $this->probeTrades;
    }

    public function setProbeTrades(string $probeTrades): self
    {
        $this->probeTrades = $probeTrades;

        return $this;
    }

    public function getProfitRate(): ?string
    {
        return $this->profitRate;
    }

    public function setProfitRate(string $profitRate): self
    {
        $this->profitRate = $profitRate;

        return $this;
    }

    public function getMathProbe(): ?string
    {
        return $this->mathProbe;
    }

    public function setMathProbe(string $mathProbe): self
    {
        $this->mathProbe = $mathProbe;

        return $this;
    }

    public function getTp(): ?int
    {
        return $this->tp;
    }

    public function setTp(int $tp): self
    {
        $this->tp = $tp;

        return $this;
    }

    public function getSl(): ?int
    {
        return $this->sl;
    }

    public function setSl(int $sl): self
    {
        $this->sl = $sl;

        return $this;
    }

    public function getTimeframes(): ?string
    {
        return $this->timeframes;
    }

    public function setTimeframes(string $timeframes): self
    {
        $this->timeframes = $timeframes;

        return $this;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    public function getDateTimeMax(): ?\DateTimeInterface
    {
        return $this->dateTimeMax;
    }

    public function setDateTimeMax(\DateTimeInterface $dateTimeMax): self
    {
        $this->dateTimeMax = $dateTimeMax;

        return $this;
    }

    public function getDateTimeMin(): ?\DateTimeInterface
    {
        return $this->dateTimeMin;
    }

    public function setDateTimeMin(\DateTimeInterface $dateTimeMin): self
    {
        $this->dateTimeMin = $dateTimeMin;

        return $this;
    }
}
