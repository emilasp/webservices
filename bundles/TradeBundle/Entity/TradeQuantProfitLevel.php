<?php

namespace TradeBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\TimestampableEntityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="trade_quant_profit_level",
 *     indexes={
 *      @ORM\Index(name="tpSlIdx", columns={"tp","sl"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="TradeBundle\Repository\TradeQuantProfitLevelRepository")
 */
class TradeQuantProfitLevel implements IEntityCore
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint", nullable=false)
     * @var int
     */
    private $tp;

    /**
     * @ORM\Column(type="smallint", nullable=false)
     * @var int
     */
    private $sl;

    /**
     * @ORM\Column(type="decimal", scale=5, precision=10, nullable=false)
     * @var float
     */
    private $tpPrice;

    /**
     * @ORM\Column(type="decimal", scale=5, precision=10, nullable=false)
     * @var float
     */
    private $slPrice;

    /**
     * @ORM\Column(type="decimal", scale=5, precision=10, nullable=false)
     * @var float
     */
    private $min;

    /**
     * @ORM\Column(type="decimal", scale=5, precision=10, nullable=false)
     * @var float
     */
    private $max;

    /**
     * @ORM\Column(type="decimal", scale=5, precision=10, nullable=false)
     * @var float
     */
    private $profit;
    /**
     * @ORM\Column(type="decimal", scale=5, precision=10, nullable=false)
     * @var float
     */
    private $slump;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @var \DateTime
     */
    protected $finished;

    /**
     * @ORM\ManyToOne(targetEntity="TradeBundle\Entity\TradeQuant", inversedBy="levels")
     */
    private $quant;

  
    public function getQuant(): ?TradeQuant
    {
        return $this->quant;
    }

    public function setQuant(?TradeQuant $quant): self
    {
        $this->quant = $quant;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTp(): ?int
    {
        return $this->tp;
    }

    public function setTp(int $tp): self
    {
        $this->tp = $tp;

        return $this;
    }

    public function getSl(): ?int
    {
        return $this->sl;
    }

    public function setSl(int $sl): self
    {
        $this->sl = $sl;

        return $this;
    }

    public function getTpPrice(): ?string
    {
        return $this->tpPrice;
    }

    public function setTpPrice(string $tpPrice): self
    {
        $this->tpPrice = $tpPrice;

        return $this;
    }

    public function getSlPrice(): ?string
    {
        return $this->slPrice;
    }

    public function setSlPrice(string $slPrice): self
    {
        $this->slPrice = $slPrice;

        return $this;
    }

    public function getMin(): ?string
    {
        return $this->min;
    }

    public function setMin(string $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMax(): ?string
    {
        return $this->max;
    }

    public function setMax(string $max): self
    {
        $this->max = $max;

        return $this;
    }

    public function getProfit(): ?string
    {
        return $this->profit;
    }

    public function setProfit(string $profit): self
    {
        $this->profit = $profit;

        return $this;
    }

    public function getSlump(): ?string
    {
        return $this->slump;
    }

    public function setSlump(string $slump): self
    {
        $this->slump = $slump;

        return $this;
    }

    public function getFinished(): ?\DateTimeInterface
    {
        return $this->finished;
    }

    public function setFinished(\DateTimeInterface $finished): self
    {
        $this->finished = $finished;

        return $this;
    }
}
