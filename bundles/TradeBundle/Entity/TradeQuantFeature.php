<?php

namespace TradeBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\TimestampableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="trade_quant_feature",
 *     indexes={
 *      @ORM\Index(name="nameIdx", columns={"name"}),
 *      @ORM\Index(name="hashIdx", columns={"hash", "number3"}),
 *      @ORM\Index(name="number1Idx", columns={"number1"}),
 *      @ORM\Index(name="number2Idx", columns={"number2"}),
 *      @ORM\Index(name="number3Idx", columns={"number3"}),
 *      @ORM\Index(name="number4Idx", columns={"number4"}),
 *      @ORM\Index(name="number5Idx", columns={"number5"}),
 *      @ORM\Index(name="number6Idx", columns={"number6"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="TradeBundle\Repository\TradeQuantFeatureRepository")
 */
class TradeQuantFeature implements IEntityCore
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=42, nullable=false)
     * @var string
     */
    private $hash;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="smallint", nullable=false, options={"unsigned":true})
     * @var string
     */
    private $timeframe;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @var string
     */
    private $rawData = [];

    /**
     * @ORM\Column(type="string", length=1000, nullable=false, options={"dafault": "empty:0"})
     * @var string
     */
    private $config;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     * @var string
     */
    private $number1;
    /**
     * @ORM\Column(type="string", length=30, nullable=false)
     * @var string
     */
    private $number2;
    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     * @var string
     */
    private $number3;
    /**
     * @ORM\Column(type="string", length=70, nullable=false)
     * @var string
     */
    private $number4;
    /**
     * @ORM\Column(type="string", length=90, nullable=false)
     * @var string
     */
    private $number5;
    /**
     * @ORM\Column(type="string", length=110, nullable=false)
     * @var string
     */
    private $number6;


    /**
     * @ORM\ManyToOne(targetEntity="TradeBundle\Entity\TradeQuant", inversedBy="levels")
     */
    private $quant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTimeframe(): ?int
    {
        return $this->timeframe;
    }

    public function setTimeframe(int $timeframe): self
    {
        $this->timeframe = $timeframe;

        return $this;
    }

    public function getRawData(): ?array
    {
        return $this->rawData;
    }

    public function setRawData(?array $rawData): self
    {
        $this->rawData = $rawData;

        return $this;
    }

    public function getConfig(): ?string
    {
        return $this->config;
    }

    public function setConfig(string $config): self
    {
        $this->config = $config;

        return $this;
    }

    public function getNumber1(): ?string
    {
        return $this->number1;
    }

    public function setNumber1(string $number1): self
    {
        $this->number1 = $number1;

        return $this;
    }

    public function getNumber2(): ?string
    {
        return $this->number2;
    }

    public function setNumber2(string $number2): self
    {
        $this->number2 = $number2;

        return $this;
    }

    public function getNumber3(): ?string
    {
        return $this->number3;
    }

    public function setNumber3(string $number3): self
    {
        $this->number3 = $number3;

        return $this;
    }

    public function getNumber4(): ?string
    {
        return $this->number4;
    }

    public function setNumber4(string $number4): self
    {
        $this->number4 = $number4;

        return $this;
    }

    public function getNumber5(): ?string
    {
        return $this->number5;
    }

    public function setNumber5(string $number5): self
    {
        $this->number5 = $number5;

        return $this;
    }

    public function getNumber6(): ?string
    {
        return $this->number6;
    }

    public function setNumber6(string $number6): self
    {
        $this->number6 = $number6;

        return $this;
    }

    public function getQuant(): ?TradeQuant
    {
        return $this->quant;
    }

    public function setQuant(?TradeQuant $quant): self
    {
        $this->quant = $quant;

        return $this;
    }
    
}
