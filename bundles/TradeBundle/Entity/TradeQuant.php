<?php

namespace TradeBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\TimestampableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="trade_quant",
 *     indexes={
 *      @ORM\Index(name="searchIdx", columns={"type_position", "date_time"}),
 *      @ORM\Index(name="searchIdx", columns={"symbol", "type_position", "date_time"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="TradeBundle\Repository\TradeQuantRepository")
 */
class TradeQuant implements IEntityCore
{
    use TimestampableEntityTrait;

    const TYPE_POSITION_BUY = 1;
    const TYPE_POSITION_SELL = 2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @var string
     */
    private $apikey;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @var string
     */
    private $account;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     * @var string
     */
    private $symbol;

    /**
     * @ORM\Column(type="smallint", nullable=false, options={"unsigned":true})
     * @var int
     */
    private $typePosition;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @var \DateTime
     */
    protected $dateTime;
    
    /**
     * @ORM\Column(type="string", length=32, nullable=false)
     * @var string
     */
    private $hash;

    /**
     * @ORM\Column(type="string", length=1024, nullable=false)
     * @var string
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=20, nullable=false)
     * @var string
     */
    private $ip;

    /**
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\TradeQuantFeature", mappedBy="quant")
     */
    private $features;

    /**
     * @ORM\OneToMany(targetEntity="TradeBundle\Entity\TradeQuantProfitLevel", mappedBy="quant")
     */
    private $levels;

    public function __construct()
    {
        $this->features = new ArrayCollection();
        $this->levels   = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApikey(): ?string
    {
        return $this->apikey;
    }

    public function setApikey(string $apikey): self
    {
        $this->apikey = $apikey;

        return $this;
    }

    public function getAccount(): ?string
    {
        return $this->account;
    }

    public function setAccount(string $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getTypePosition(): ?int
    {
        return $this->typePosition;
    }

    public function setTypePosition(int $typePosition): self
    {
        $this->typePosition = $typePosition;

        return $this;
    }

    public function getDateTime(): ?\DateTimeInterface
    {
        return $this->dateTime;
    }

    public function setDateTime(\DateTimeInterface $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return Collection|TradeQuantProfitLevel[]
     */
    public function getLevels(): Collection
    {
        return $this->levels;
    }

    public function addLevel(TradeQuantProfitLevel $level): self
    {
        if (!$this->levels->contains($level)) {
            $this->levels[] = $level;
            $level->setQuant($this);
        }

        return $this;
    }

    public function removeLevel(TradeQuantProfitLevel $level): self
    {
        if ($this->levels->contains($level)) {
            $this->levels->removeElement($level);
            if ($level->getQuant() === $this) {
                $level->setQuant(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|TradeQuantFeature[]
     */
    public function getFeatures(): Collection
    {
        return $this->features;
    }

    public function addFeature(TradeQuantFeature $feature): self
    {
        if (!$this->features->contains($feature)) {
            $this->features[] = $feature;
            $feature->setQuant($this);
        }
        return $this;
    }

    public function removeFeature(TradeQuantFeature $feature): self
    {
        if ($this->features->contains($feature)) {
            $this->features->removeElement($feature);
            if ($feature->getQuant() === $this) {
                $feature->setQuant(null);
            }
        }
        return $this;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

}
