<?php

namespace TradeBundle\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\BlameableEntityTrait;
use App\Core\Traits\TimestampableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FileBundle\Entity\File;
use TradeBundle\Repository\TradingTradeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TradingTradeRepository::class)
 */
class TradingTrade implements IEntityCore
{
    use TimestampableEntityTrait;
    use BlameableEntityTrait;

    const TYPE_IDEA  = 1;
    const TYPE_TRADE = 2;
    const TYPES      = [
        self::TYPE_IDEA  => 'Идея',
        self::TYPE_TRADE => 'Трейд'
    ];

    const STATUS_NEW    = 1;
    const STATUS_CLOSE  = 2;
    const STATUS_DELETE = 0;
    const STATUSES      = [
        self::STATUS_NEW    => 'Новый',
        self::STATUS_CLOSE  => 'Завершен',
        self::STATUS_DELETE => 'Удален'
    ];

    const TREND_TYPE_NONE  = 0;
    const TREND_TYPE_LONG  = 1;
    const TREND_TYPE_SHORT = 2;
    const TREND_TYPE_FLET = 3;
    const TREND_TYPES      = [
        self::TREND_TYPE_NONE  => 'none',
        self::TREND_TYPE_FLET  => 'flet',
        self::TREND_TYPE_LONG  => 'long',
        self::TREND_TYPE_SHORT => 'short'
    ];

    const TREND_PHASE_NONE        = 0;
    const TREND_PHASE_GENERATION  = 1;
    const TREND_PHASE_LIVE        = 2;
    const TREND_PHASE_CULMINATION = 3;
    const TREND_PHASE_DEATH       = 4;
    const TREND_PHASES            = [
        self::TREND_PHASE_GENERATION  => 'Зарождение',
        self::TREND_PHASE_LIVE        => 'Жизнь',
        self::TREND_PHASE_CULMINATION => 'Кульминация',
        self::TREND_PHASE_DEATH       => 'Смерть',
        self::TREND_PHASE_NONE        => 'Не определено',
    ];


    const PERIOD_M1  = 'M1';
    const PERIOD_M5  = 'M5';
    const PERIOD_M15 = 'M15';
    const PERIOD_M30 = 'M30';
    const PERIOD_H4  = 'H4';
    const PERIOD_H12 = 'H12';
    const PERIOD_D1  = 'D1';
    const PERIOD_W1  = 'W1';
    const PERIOD_MN1 = 'MN1';
    const PERIODS    = [
        self::PERIOD_M1  => 'M1',
        self::PERIOD_M5  => 'M5',
        self::PERIOD_M15 => 'M15',
        self::PERIOD_M30 => 'M30',
        self::PERIOD_H4  => 'H4',
        self::PERIOD_H12 => 'H12',
        self::PERIOD_D1  => 'D1',
        self::PERIOD_W1  => 'W1',
        self::PERIOD_MN1 => 'MN1',
    ];

    const MARKET_FOREX   = 'forex';
    const MARKET_STOCKS  = 'stocks';
    const MARKET_OPTION  = 'option';
    const MARKET_FUTURES = 'futures';
    const MARKET_INDEX   = 'index';

    const MARKETS = [
        self::MARKET_FOREX   => 'Forex',
        self::MARKET_STOCKS  => 'Акция',
        self::MARKET_INDEX   => 'Индекс',
        self::MARKET_FUTURES => 'Фьючерс',
        self::MARKET_OPTION  => 'Опцион',
    ];

    const RISK_CALC_TYPE_ABT    = 1;
    const RISK_CALC_TYPE_IDNONE = 2;
    const RISK_CALC_TYPES       = [
        self::RISK_CALC_TYPE_ABT    => 'ABT',
        self::RISK_CALC_TYPE_IDNONE => 'Безиндиакаторная',
    ];


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint", options={"unsigned":true})
     */
    private $type = self::TYPE_TRADE;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status = self::STATUS_NEW;

    /**
     * forex, moex
     * @ORM\Column(type="string", length=20)
     */
    private $market;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $timeframe;

    /**
     * аббревиатура акции, валютной пары
     * @ORM\Column(type="string", length=100)
     */
    private $instrument;

    /** long/short
     * @ORM\Column(type="smallint", options={"unsigned":true})
     */
    private $side = 0;

    /** Цена на момент покупки
     * @ORM\Column(type="decimal", precision=12, scale=5)
     */
    private $price;

    /** Объем
     * @ORM\Column(type="decimal", precision=6, scale=3)
     */
    private $volume;

    /** Баланс на текущий момент
     * @ORM\Column(type="decimal", precision=12, scale=2)
     */
    private $balance;

    /** Цель по профиту
     * @ORM\Column(type="decimal", precision=12, scale=5, nullable=true)
     */
    private $takeProfit;

    /** Стоп
     * @ORM\Column(type="decimal", precision=12, scale=5, nullable=true)
     */
    private $stopLoss;

    /** Потенциал прибыли
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=true)
     */
    private $profitPotentialPercent;

    /** TP/SL
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=true)
     */
    private $profitPercent;

    /** результат сделки в $
     * @ORM\Column(type="decimal", precision=10, scale=5, nullable=true)
     */
    private $profitResult;

    /** максимальный плавающий профит
     * @ORM\Column(type="decimal", precision=10, scale=5, nullable=true)
     */
    private $profitResultHighest;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isResultKnock = false;

    /** риск от баланса
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=true)
     */
    private $riskBalancePercent;

    /** тип расчета риска: Риск/(покупка минус стоп), из Безиндикатораня торговля
     * @ORM\Column(type="smallint")
     */
    private $riskCalcType = self::RISK_CALC_TYPE_ABT;

    /** Информация по входу
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentEnter;

    /** Информация по выходу
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentExit;

    /** Информация по результату
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentResult;

    /** Информация по возможному улучшению: что еще можно было учесть
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentResultImprove;

    /** психологическое состояние 1-5
     * @ORM\Column(type="smallint", options={"unsigned":true})
     */
    private $psyhologyRate = 3;

    /** json - перечисление подтверждающих паттернов
     * @ORM\Column(type="json")
     */
    private $enterReasons;

    /** направление тренда
     * @ORM\Column(type="smallint")
     */
    private $trendType;

    /** фаза тренда
     * @ORM\Column(type="smallint")
     */
    private $trendPhase = self::TREND_PHASE_NONE;

    /** направление тренда следующего старшего ТФ
     * @ORM\Column(type="smallint")
     */
    private $trendNxtTfType = self::TREND_TYPE_NONE;

    /** направление тренда следующего старшего ТФ 2
     * @ORM\Column(type="smallint")
     */
    private $trendNxt2TfType = self::TREND_TYPE_NONE;

    /** сила тренда
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $trendStrenght;

    /** сколько уже было паттернов в текущем тренде
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $trendPatternCount;

    /**
     * @ORM\ManyToOne(targetEntity=TradingStrategy::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $strategy;

    /** выбрать из текущих открытых трейдов по данному инструменту.
     * @ORM\ManyToOne(targetEntity=TradingTrade::class, inversedBy="trades")
     */
    private $tradeBase;

    /**
     * @ORM\OneToMany(targetEntity=TradingTrade::class, mappedBy="tradeBase")
     */
    private $trades;

    /**
     * Изображение
     *
     * @ORM\OneToOne(targetEntity="FileBundle\Entity\File", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * Изображение
     *
     * @ORM\OneToOne(targetEntity="FileBundle\Entity\File", cascade={"persist", "remove"})
     */
    private $imageResult;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2, nullable=true)
     */
    private $atrValue;


    /** фильтрация по АТР: risk pt < 3 ATR[15] || agress : risk pt < 1 ATR[15](с коротким стопом)
     * @ORM\Column(type="boolean")
     */
    private $isFilterAtr = false;

    /** подтверждение объемами - не входим, если объемы стоят на месте
     * @ORM\Column(type="boolean")
     */
    private $isFilterVolume = false;

    /** СОТ нетто показание индикатора. 0 - неопределенность, 1 - устойчивы тренд, 2 - формируется разворот(-1, -2)
     * @ORM\Column(type="boolean")
     */
    private $isFilterCftcType = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAnalized = false;


    public function __construct()
    {
        $this->trades = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMarket(): ?string
    {
        return $this->market;
    }

    public function setMarket(string $market): self
    {
        $this->market = $market;

        return $this;
    }

    public function getTimeframe(): ?string
    {
        return $this->timeframe;
    }

    public function setTimeframe(string $timeframe): self
    {
        $this->timeframe = $timeframe;

        return $this;
    }

    public function getInstrument(): ?string
    {
        return $this->instrument;
    }

    public function setInstrument(string $instrument): self
    {
        $this->instrument = $instrument;

        return $this;
    }

    public function getSide(): ?int
    {
        return $this->side;
    }

    public function setSide(int $side): self
    {
        $this->side = $side;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getVolume(): ?string
    {
        return $this->volume;
    }

    public function setVolume(string $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    public function getBalance(): ?string
    {
        return $this->balance;
    }

    public function setBalance(string $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getTakeProfit(): ?string
    {
        return $this->takeProfit;
    }

    public function setTakeProfit(?string $takeProfit): self
    {
        $this->takeProfit = $takeProfit;

        return $this;
    }

    public function getStopLoss(): ?string
    {
        return $this->stopLoss;
    }

    public function setStopLoss(?string $stopLoss): self
    {
        $this->stopLoss = $stopLoss;

        return $this;
    }

    public function getProfitPotentialPercent(): ?string
    {
        return $this->profitPotentialPercent;
    }

    public function setProfitPotentialPercent(?string $profitPotentialPercent): self
    {
        $this->profitPotentialPercent = $profitPotentialPercent;

        return $this;
    }

    public function getProfitPercent(): ?string
    {
        return $this->profitPercent;
    }

    public function setProfitPercent(?string $profitPercent): self
    {
        $this->profitPercent = $profitPercent;

        return $this;
    }

    public function getProfitResult(): ?string
    {
        return $this->profitResult;
    }

    public function setProfitResult(?string $profitResult): self
    {
        $this->profitResult = $profitResult;

        return $this;
    }

    public function getRiskBalancePercent(): ?string
    {
        return $this->riskBalancePercent;
    }

    public function setRiskBalancePercent(?string $riskBalancePercent): self
    {
        $this->riskBalancePercent = $riskBalancePercent;

        return $this;
    }

    public function getCommentEnter(): ?string
    {
        return $this->commentEnter;
    }

    public function setCommentEnter(?string $commentEnter): self
    {
        $this->commentEnter = $commentEnter;

        return $this;
    }

    public function getCommentExit(): ?string
    {
        return $this->commentExit;
    }

    public function setCommentExit(?string $commentExit): self
    {
        $this->commentExit = $commentExit;

        return $this;
    }

    public function getCommentResult(): ?string
    {
        return $this->commentResult;
    }

    public function setCommentResult(?string $commentResult): self
    {
        $this->commentResult = $commentResult;

        return $this;
    }

    public function getPsyhologyRate(): ?int
    {
        return $this->psyhologyRate;
    }

    public function setPsyhologyRate(int $psyhologyRate): self
    {
        $this->psyhologyRate = $psyhologyRate;

        return $this;
    }

    public function getEnterReasons(): ?string
    {
        return $this->enterReasons;
    }

    public function setEnterReasons(string $enterReasons): self
    {
        $this->enterReasons = $enterReasons;

        return $this;
    }

    public function getStrategy(): ?TradingStrategy
    {
        return $this->strategy;
    }

    public function setStrategy(?TradingStrategy $strategy): self
    {
        $this->strategy = $strategy;

        return $this;
    }

    public function getTradeBase(): ?self
    {
        return $this->tradeBase;
    }

    public function setTradeBase(?self $tradeBase): self
    {
        $this->tradeBase = $tradeBase;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getTrades(): Collection
    {
        return $this->trades;
    }

    public function addTrade(self $trade): self
    {
        if (!$this->trades->contains($trade)) {
            $this->trades[] = $trade;
            $trade->setTradeBase($this);
        }

        return $this;
    }

    public function removeTrade(self $trade): self
    {
        if ($this->trades->removeElement($trade)) {
            // set the owning side to null (unless already changed)
            if ($trade->getTradeBase() === $this) {
                $trade->setTradeBase(null);
            }
        }

        return $this;
    }


    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(?File $image): self
    {
        $this->image = $image;
        return $this;
    }

    public function getImageResult(): ?File
    {
        return $this->imageResult;
    }

    public function setImageResult(?File $imageResult): self
    {
        $this->imageResult = $imageResult;
        return $this;
    }

    public function getAtrValue(): ?string
    {
        return $this->atrValue;
    }

    public function setAtrValue(?string $atrValue): self
    {
        $this->atrValue = $atrValue;

        return $this;
    }

    public function getProfitResultHighest(): ?string
    {
        return $this->profitResultHighest;
    }

    public function setProfitResultHighest(?string $profitResultHighest): self
    {
        $this->profitResultHighest = $profitResultHighest;

        return $this;
    }

    public function getIsResultKnock(): ?bool
    {
        return $this->isResultKnock;
    }

    public function setIsResultKnock(bool $isResultKnock): self
    {
        $this->isResultKnock = $isResultKnock;

        return $this;
    }

    public function getIsFilterAtr(): ?bool
    {
        return $this->isFilterAtr;
    }

    public function setIsFilterAtr(bool $isFilterAtr): self
    {
        $this->isFilterAtr = $isFilterAtr;

        return $this;
    }

    public function getIsFilterVolume(): ?bool
    {
        return $this->isFilterVolume;
    }

    public function setIsFilterVolume(bool $isFilterVolume): self
    {
        $this->isFilterVolume = $isFilterVolume;

        return $this;
    }

    public function getIsFilterCftcType(): ?bool
    {
        return $this->isFilterCftcType;
    }

    public function setIsFilterCftcType(bool $isFilterCftcType): self
    {
        $this->isFilterCftcType = $isFilterCftcType;

        return $this;
    }

    public function getAtr(): ?string
    {
        return $this->atr;
    }

    public function setAtr(?string $atr): self
    {
        $this->atr = $atr;

        return $this;
    }

    public function getRiskCalcType(): ?int
    {
        return $this->riskCalcType;
    }

    public function setRiskCalcType(int $riskCalcType): self
    {
        $this->riskCalcType = $riskCalcType;

        return $this;
    }

    public function getCommentResultImprove(): ?string
    {
        return $this->commentResultImprove;
    }

    public function setCommentResultImprove(?string $commentResultImprove): self
    {
        $this->commentResultImprove = $commentResultImprove;

        return $this;
    }

    public function getTrendType(): ?int
    {
        return $this->trendType;
    }

    public function setTrendType(int $trendType): self
    {
        $this->trendType = $trendType;

        return $this;
    }

    public function getTrendPhase(): ?int
    {
        return $this->trendPhase;
    }

    public function setTrendPhase(int $trendPhase): self
    {
        $this->trendPhase = $trendPhase;

        return $this;
    }

    public function getTrendNxtTfType(): ?int
    {
        return $this->trendNxtTfType;
    }

    public function setTrendNxtTfType(?int $trendNxtTfType): self
    {
        $this->trendNxtTfType = $trendNxtTfType;

        return $this;
    }

    public function getTrendNxt2TfType(): ?int
    {
        return $this->trendNxt2TfType;
    }

    public function setTrendNxt2TfType(?int $trendNxt2TfType): self
    {
        $this->trendNxt2TfType = $trendNxt2TfType;

        return $this;
    }

    public function getTrendStrenght(): ?int
    {
        return $this->trendStrenght;
    }

    public function setTrendStrenght(?int $trendStrenght): self
    {
        $this->trendStrenght = $trendStrenght;

        return $this;
    }

    public function getTrendPatternCount(): ?int
    {
        return $this->trendPatternCount;
    }

    public function setTrendPatternCount(?int $trendPatternCount): self
    {
        $this->trendPatternCount = $trendPatternCount;

        return $this;
    }

    public function getIsAnalized(): ?bool
    {
        return $this->isAnalized;
    }

    public function setIsAnalized(bool $isAnalized): self
    {
        $this->isAnalized = $isAnalized;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getName(): string
    {
        return $this->getMarket() . ':' . $this->getInstrument() . ':' . $this->getTimeframe() . ':' . $this->getPrice()
            . ':' . $this->getCreatedAt()->format('Y-m-d H:i');
    }
}
