<?php

namespace TradeBundle\Command;

use App\Core\Abstracts\ACommandActions;
use App\Core\Helpers\DateHelper;
use App\Core\Helpers\FileHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TradeBundle\Entity\TradeQuant;
use TradeBundle\Entity\TradeQuantFeatureAnalize;
use TradeBundle\Repository\Analize\TradeQuantFeatureAnalize5Repository;
use TradeBundle\Repository\TradeQuantFeatureAnalizetRepository;
use TradeBundle\Service\TradeAnalizeService;
use TradeBundle\Service\TradePredictService;
use TradeBundle\Service\TradeQuantService;

/**
 * Class QuantsImportCommand
 * @package TradeBundle\Command
 */
class QuantsImportCommand extends ACommandActions
{
    private const ARG_FILE_PATH = 'file';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('trade:quant')
            ->setDescription('Трейдинг: кванты');
        $this->addOption(self::ARG_FILE_PATH, null, InputOption::VALUE_OPTIONAL);
    }

    /**
     * Импортируем данные из файла
     */
    public function importQuantsFromFileAction(InputInterface $input, OutputInterface $output)
    {
        $this->display('Start set current periodical lesson links');

        if (!$filePath = $input->getOption(self::ARG_FILE_PATH)) {
            throw new InvalidArgumentException('Для данной команды указание пути до файла с данными обязательно');
        }

        $progressBar = new ProgressBar($output, filesize($filePath) / 1000000);
        $progressBar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%');

        $progressBar->setMessage('Start');
        $progressBar->start();

        $service = $this->container->get(TradeQuantService::class);
        foreach (FileHelper::getRowsFromFileGenerator($filePath) as $index => $row) {
            $progressBar->advance(strlen($row) / 1000000);

            if ($row) {
                $batchData = json_decode($row, true);

                $apiKey       = $batchData['apiKey'];
                $account      = $batchData['account'];
                $symbol       = $batchData['symbol'] ?? 'EURUSD';
                $typePosition = $batchData['typePosition'];
                $quants       = $batchData['quants'];
                $ip           = '127.0.0.1';

                $service->addQuants($apiKey, $account, $symbol, $typePosition, $quants, $ip);
            }
        }
        $progressBar->finish();
    }

    /**
     * Обновляем данные для анализа квантов
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    public function updateAnaliticsAction(InputInterface $input, OutputInterface $output)
    {
        $this->display('Start set current periodical lesson links');

        //$service = $this->container->get(TradeAnalizeService::class);

        $dateEnd = new \DateTime('-1 month');

        $symbols      = ['EURUSD'];
        $types        = [0, 1];
        $numbersParts = [1, 2, 3, 6];
        $daysFrom     = [3, 5, 10, 30, 90];
        foreach ($daysFrom as $days) {
            $this->em->getConnection()->exec("TRUNCATE TABLE trade_quant_feature_analize_{$days}");
            $repoName   = "TradeBundle\Entity\Analize\TradeQuantFeatureAnalize{$days}";
            $repository = $this->em->getRepository($repoName);
            foreach ($types as $type) {
                foreach ($symbols as $symbol) {
                    foreach ($numbersParts as $numbersPart) {
                        $this->display("Import symbol: {$symbol}, type: {$type}, period: {$days}, numbers: {$numbersPart}..", 'green');
                        $repository->updateAnaliticsTable($symbol, $type, $numbersPart, $dateEnd, $days);
                    }
                }
            }
        }
    }

    /**
     * Обновляем данные для анализа квантов
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    public function testAction(InputInterface $input, OutputInterface $output)
    {
        $this->display('Start set current periodical lesson links');

        $service        = $this->container->get(TradeQuantService::class);
        $predictService = $this->container->get(TradePredictService::class);

        $symbol     = 'EURUSD';
        $type       = 0;
        $timeframes = 3;
        // Даты за которые тестируем
        $start    = new \DateTime('2020-11-15 00:00:00');
        $end      = new \DateTime('2020-12-05 23:59:50');
        $tableNum = 3;

        $dates = DateHelper::getDatesFromRange($start, $end);
        $featuresProfit = [];
        $trades = ['good' => 0, 'bad' => 0, 'all' => 0];
        $goodTrades = 0;
        $profit = 0;
        foreach ($dates as $date) {
            $minutes = range(1, 1080);
            foreach ($minutes as $minute) {
                $date->add(new \DateInterval('PT1M'));

                // Get current feature
                $quant = $this->em->getRepository(TradeQuant::class)->findOneBy(['symbol' => $symbol, 'typePosition' => $type, 'dateTime' => $date]);
                if ($quant) {
                    // Predict
                    $dataToPredict = $service->getQuantsDataFromEntity([$quant]);
                    $startPredict  = (clone $date)->modify('-5 days');
                    $endPredict    = (clone $date)->modify('-1 days');
                    $predictResult = $predictService->getMaxMinMatProbe(
                        $symbol, $type, $dataToPredict[0]['calculators'], $timeframes, $startPredict, $endPredict, 15, ['time']
                    );

                    $this->display("Predict minute({$profit}): {$date->format('Y-m-d H:i')}, count:{$predictResult['countFull']}/{$predictResult['count']}", 'cyan');


                    if ((float)$predictResult['maxMatProbe'] > 0.7) {
                        $level  = $predictService->getQuantLevelByTpSl($quant, $predictResult['max']['tp'], $predictResult['max']['sl']);
                        $profit += $level->getProfit();

                        $featuresProfit[$predictResult['max']['feature']] = $featuresProfit[$predictResult['max']['feature']] ?? 0;
                        $featuresProfit[$predictResult['max']['feature']] += $level->getProfit();

                        if ($level->getProfit() > 0) {
                            $trades['good']++;
                        } else {
                            $trades['bad']++;
                        }
                        $trades['all']++;

                        $rate = round($trades['good'] / ($trades['all']), 2);

                        $goodTrades += $level->getProfit() > 0 ? 1 : -1;

                        $this->display("PREDICT: Feature: {$predictResult['max']['feature']}|{$featuresProfit[$predictResult['max']['feature']]}| - {$predictResult['max']['profit']}, real: {$level->getProfit()}",
                            'blue');
                        $this->display("Profit ({$rate}): {$profit}", 'green');
                    }
                }
            }
        }
    }

    /**
     * @param string    $symbol
     * @param int       $type
     * @param int       $timeframes
     * @param \DateTime $dateItem
     * @param int       $tableNum
     */
    private function updateAnalizeTable(string $symbol, int $type, int $timeframes, \DateTime $dateItem, int $tableNum)
    {
        $dateEndForUpdateTable = (clone $dateItem)->sub(new \DateInterval('P1D'));
        $repoName              = "TradeBundle\Entity\Analize\TradeQuantFeatureAnalize{$tableNum}";
        $repository            = $this->em->getRepository($repoName);
        $repository->updateAnaliticsTable($symbol, $type, $timeframes, $dateEndForUpdateTable, $tableNum);
    }
}
