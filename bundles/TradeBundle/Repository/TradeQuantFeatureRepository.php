<?php

namespace TradeBundle\Repository;

use App\Core\Helpers\ArrayHelper;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use TradeBundle\Entity\TradeQuantFeature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method TradeQuantFeature|null find($id, $lockMode = null, $lockVersion = null)
 * @method TradeQuantFeature|null findOneBy(array $criteria, array $orderBy = null)
 * @method TradeQuantFeature[]    findAll()
 * @method TradeQuantFeature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TradeQuantFeatureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TradeQuantFeature::class);
    }

    /**
     * Создаем новый признак
     *
     * @param string      $hash
     * @param string      $name
     * @param array       $numberParts
     * @param int         $timeFrame
     * @param string|null $config
     * @param array       $rawData
     * @return TradeQuantFeature
     * @throws \Doctrine\ORM\ORMException
     */
    public function createQuantFeature(string $hash, string $name, array $numberParts, int $timeFrame, ?string $config, array $rawData): TradeQuantFeature
    {
        $feature = new TradeQuantFeature();
        $feature->setHash($hash);
        $feature->setName($name);
        $feature->setTimeframe($timeFrame);
        $feature->setConfig($config);
        $feature->setRawData($rawData);

        foreach ($numberParts as $index => $part){
            $setter = 'setNumber' . ($index + 1);
            $feature->{$setter}($part);
        }
        $this->_em->persist($feature);
        return $feature;
    }

    /**
     * Получаем если есть фичу
     *
     * @param \DateTime   $dateTime
     * @param string      $name
     * @param string      $number
     * @param int         $timeframe
     * @param string|null $config
     * @return TradeQuantFeature|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getFeatureUnique(\DateTime $dateTime, string $name, string $number, int $timeframe, ?string $config):? TradeQuantFeature
    {
        return $this->createQueryBuilder('f')
            ->where('f.dateTime=:dateTime')
            ->andWhere('f.name=:name')
            ->andWhere('f.number=:number')
            ->andWhere('f.timeframe=:timeframe')
            ->andWhere('f.config=:config')
            ->setParameter('dateTime', $dateTime)
            ->setParameter('name', $name)
            ->setParameter('number', $number)
            ->setParameter('timeframe', $timeframe)
            ->setParameter('config', $config)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Обновляем таблицу со статистикой частот по признакам
     *
     * @param string    $symbol
     * @param int       $typePosition
     * @param int       $numbersPart
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function getFeatureStat(string $symbol, int $typePosition, int $numbersPart, array $hashs, \DateTime $dateStart = null, \DateTime $dateEnd = null)
    {
        $numberField = 'number'.$numbersPart;

        $dateStart = $dateStart ?: new \DateTime('-1 year');
        $dateEnd = $dateEnd ?: new \DateTime();

        $sql = "
            SELECT '{$numbersPart}' as timeframes, hash, symbol, type_position, feature, number, config, profit, profit_plus, profit_minus, slump_avg, trades, trades_plus, trades_minus,
                   (trades_plus / (trades_plus + trades_minus)) probe_trades, profit_rate, (trades_plus / (trades_plus + trades_minus / profit_rate)) as math_probe, date_time_max, date_time_min, tp, sl
            FROM (
               SELECT MAX(feature.hash) as hash, symbol, quant.type_position, feature.name as feature, feature.{$numberField} as number, feature.config as config,
                    SUM(profit.profit) as profit, SUM(IF(profit.profit > 0, profit.profit, 0)) as profit_plus, SUM(IF(profit.profit < 0, profit.profit, 0)) as profit_minus,
                    SUM(profit.slump) / COUNT(*) as slump_avg, COUNT(*) as trades,
                    SUM(IF(profit.profit > 0, 1, 0)) as trades_plus, SUM(IF(profit.profit < 0, 1, 0)) as trades_minus,
                    profit.tp, profit.sl, (profit.tp / profit.sl) as profit_rate, MAX(quant.date_time) as date_time_max, MIN(quant.date_time) as date_time_min
                FROM trade_quant_feature feature
                  INNER JOIN trade_quant quant on feature.quant_id = quant.id
                  INNER JOIN trade_quant_profit_level profit on profit.quant_id = quant.id
                WHERE feature.hash IN(:hashs)
                  AND quant.symbol=:symbol
                  AND quant.type_position=:typePosition
                  AND feature.timeframe = :timeframe
                  AND quant.date_time > :start
                  AND quant.date_time < :end
               
                GROUP BY feature.hash, feature.{$numberField}, profit.tp, profit.sl
            ) as predictData
;";

        $stat = $this->_em->getConnection()->executeQuery($sql, [
            'symbol'       => $symbol,
            'typePosition' => $typePosition,
            'timeframe'    => 1,
            'start'        => $dateStart->format('Y-m-d 23:59:59'),
            'end'          => $dateEnd->format('Y-m-d H:i:s'),
            'hashs'        => $hashs,
        ], ['hashs' => Connection::PARAM_STR_ARRAY])->fetchAllAssociative();

        return ArrayHelper::setFieldAsKeyWithGroup($stat, 'hash');
    }

}
