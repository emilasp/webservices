<?php

namespace TradeBundle\Repository;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\DBAL\FetchMode;
use TradeBundle\Entity\TradeQuant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method TradeQuant|null find($id, $lockMode = null, $lockVersion = null)
 * @method TradeQuant|null findOneBy(array $criteria, array $orderBy = null)
 * @method TradeQuant[]    findAll()
 * @method TradeQuant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TradeQuantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TradeQuant::class);
    }

    /**
     * Получаем кванты для предсказания
     *
     * @param string         $quantName
     * @param int            $typePosition
     * @param string         $quant
     * @param \DateTime      $from
     * @param \DateTime|null $to
     * @return array
     */
    public function getQuantStatistic(string $quantName, int $typePosition, string $quant, \DateTime $from, \DateTime $to = null): array
    {
        $to = $to ?: new \DateTime();

        $sql       = "SELECT SUM(profit) as profit, SUM(slump) as slump, COUNT(id) as total FROM trade_quant_level
                WHERE quant_name=:quantName AND type_position=:typePosition AND quant=:quant
                 AND created_at >= :startDate AND created_at <= :endDate";
        $quantStat = $this->_em->getConnection()->executeQuery($sql, [
            'quantName'    => $quantName,
            'typePosition' => $typePosition,
            'quant'        => $quant,
            'startDate'    => $from->format('Y-m-d H:i:s'),
            'endDate'      => $to->format('Y-m-d H:i:s'),
        ])->fetchAll()[0];

        $sql          = "SELECT (SUM(profit)/COUNT(*)) as profit, (SUM(slump)/COUNT(*)) as slump, COUNT(*) as total FROM (
                   SELECT SUM(profit) as profit, SUM(slump) as slump, COUNT(id) as total FROM trade_quant_level
                   WHERE quant_name=:quantName AND type_position=:typePosition
                     AND created_at >= :startDate AND created_at <= :endDate
                   GROUP BY quant
                ) as avg";
        $quantAllStat = $this->_em->getConnection()->executeQuery($sql, [
            'quantName'    => $quantName,
            'typePosition' => $typePosition,
            'quant'        => $quant,
            'startDate'    => $from->format('Y-m-d H:i:s'),
            'endDate'      => $to->format('Y-m-d H:i:s'),
        ])->fetchAll()[0];

        return ['quant' => $quantStat, 'all' => $quantAllStat];
    }

    /**
     * Получаем кванты для предсказания
     *
     * @param string         $quantName
     * @param \DateTime      $from
     * @param \DateTime|null $to
     * @return array
     */
    public function getQuantsToPredict(string $quantName, \DateTime $from, \DateTime $to = null): array
    {
        $to = $to ?: new \DateTime();

        $quants = $this->createQueryBuilder('q')
            ->where('q.quantName=:quantName')
            ->andWhere('q.createdAt>=:start AND q.createdAt <=:end')
            ->setParameter('quantName', $quantName)
            ->setParameter('start', $from)
            ->setParameter('end', $to)
            ->getQuery()
            ->getResult();

        return $quants;
    }

    /**
     * Проверяем, что в базе нет кванта с переданными свойствами
     *
     * @param \DateTime $dateTime
     * @param string    $hash
     * @param string    $number
     * @param int       $typePosition
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getQuantUnique(\DateTime $dateTime, string $hash, string $number, int $typePosition): ?TradeQuant
    {
        return $this->createQueryBuilder('q')
            ->where('q.dateTime=:dateTime')
            ->andWhere('q.hash=:hash')
            ->andWhere('q.number=:number')
            ->andWhere('q.typePosition=:position')
            ->setParameter('dateTime', $dateTime)
            ->setParameter('hash', $hash)
            ->setParameter('number', $number)
            ->setParameter('position', $typePosition)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }


    /**
     * Создаем новый квант
     *
     * @param string    $apiKey
     * @param string    $account
     * @param string    $symbol
     * @param \DateTime $dateTime
     * @param string    $name
     * @param string    $number
     * @param int       $typePosition
     * @param float     $volume
     * @return TradeQuant
     * @throws \Doctrine\ORM\ORMException
     */
    public function createQuant(
        string $apiKey,
        string $account,
        string $symbol,
        \DateTime $dateTime,
        string $hash,
        string $number,
        int $typePosition,
        string $ip
    ): TradeQuant {
        $quant = new TradeQuant();
        $quant->setApikey($apiKey);
        $quant->setAccount($account);
        $quant->setSymbol($symbol);
        $quant->setDateTime($dateTime);
        $quant->setHash($hash);
        $quant->setNumber($number);
        $quant->setTypePosition($typePosition);
        $quant->setIp($ip);

        $this->_em->persist($quant);

        return $quant;
    }
}
