<?php

namespace TradeBundle\Repository\Analize;

use TradeBundle\Entity\Analize\TradeQuantFeatureAnalize30;
use TradeBundle\Entity\TradeQuant;

/**
 * @method TradeQuant|null find($id, $lockMode = null, $lockVersion = null)
 * @method TradeQuant|null findOneBy(array $criteria, array $orderBy = null)
 * @method TradeQuant[]    findAll()
 * @method TradeQuant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TradeQuantFeatureAnalize30Repository extends ATradeQuantFeatureAnalizeRepositoryBase
{
    protected static $entityClass = TradeQuantFeatureAnalize30::class;
}
