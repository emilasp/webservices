<?php

namespace TradeBundle\Repository\Analize;

use App\Core\Helpers\ArrayHelper;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use TradeBundle\Entity\Analize\TradeQuantFeatureAnalize3;
use TradeBundle\Entity\TradeQuant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method TradeQuant|null find($id, $lockMode = null, $lockVersion = null)
 * @method TradeQuant|null findOneBy(array $criteria, array $orderBy = null)
 * @method TradeQuant[]    findAll()
 * @method TradeQuant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ATradeQuantFeatureAnalizeRepositoryBase extends ServiceEntityRepository
{
    protected static $entityClass;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, static::$entityClass);
    }

    /**
     * Обновляем таблицу со статистикой частот по признакам
     *
     * @param int       $typePosition
     * @param int       $numbersPart
     * @param \DateTime $dateEnd
     * @param int       $days
     */
    public function updateAnaliticsTable(string $symbol, int $typePosition, int $numbersPart, \DateTime $dateEnd, int $days)
    {
        $end         = $dateEnd->format('Y-m-d 23:59:59');
        $dateStart   = $dateEnd->modify("-{$days} days")->modify('+1 days');
        $start       = $dateStart->format('Y-m-d H:i:s');
        $numberField = 'number'.$numbersPart;

        $sql = "
            CREATE OR REPLACE VIEW trade_features_analitic_frequency AS
            SELECT '{$numbersPart}' as timeframes, hash, symbol, type_position, feature, number, config, profit, profit_plus, profit_minus, slump_avg, trades, trades_plus, trades_minus,
                   (trades_plus / (trades_plus + trades_minus)) probe_trades, profit_rate, (trades_plus / (trades_plus + trades_minus / profit_rate)) as math_probe, date_time_max, date_time_min, tp, sl
            FROM (
                     SELECT  MAX(feature.hash) as hash, symbol, quant.type_position, feature.name as feature, feature.{$numberField} as number, feature.config as config,
                            SUM(profit.profit) as profit, SUM(IF(profit.profit > 0, profit.profit, 0)) as profit_plus, SUM(IF(profit.profit < 0, profit.profit, 0)) as profit_minus,
                            SUM(profit.slump) / COUNT(*) as slump_avg, COUNT(*) as trades,
                            SUM(IF(profit.profit > 0, 1, 0)) as trades_plus, SUM(IF(profit.profit < 0, 1, 0)) as trades_minus,
                            profit.tp, profit.sl, (profit.tp / profit.sl) as profit_rate, MAX(quant.date_time) as date_time_max, MIN(quant.date_time) as date_time_min
                     FROM trade_quant quant
                              INNER JOIN trade_quant_feature feature ON feature.quant_id=quant.id
                              INNER JOIN trade_quant_profit_level profit ON profit.quant_id=quant.id
                     WHERE quant.symbol='{$symbol}' AND quant.type_position={$typePosition}
                      AND quant.date_time >= '{$start}' AND quant.date_time <= '{$end}'
                     GROUP BY feature.hash, feature.{$numberField}, profit.tp, profit.sl
                 ) as view
            ORDER BY profit DESC;
        ";//                     --GROUP BY CONCAT(feature.hash, feature.{$numberField}, profit.tp, 'X', profit.sl)

        $this->_em->getConnection()->exec($sql);

        $sql = "INSERT INTO trade_quant_feature_analize_{$days} (hash, timeframes, symbol, type_position, feature, number, config,profit,profit_plus,profit_minus,slump_avg,trades,trades_plus,trades_minus,probe_trades, profit_rate, math_probe, date_time_max, date_time_min,tp,sl) 
                SELECT hash, timeframes, symbol,type_position, feature, number, config,profit,profit_plus,profit_minus,slump_avg,trades,trades_plus,trades_minus,probe_trades, profit_rate, math_probe, date_time_max, date_time_min,tp,sl from `trade_features_analitic_frequency`";
        $this->_em->getConnection()->exec($sql);
    }
}
