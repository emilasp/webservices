<?php

namespace TradeBundle\Repository\Analize;

use TradeBundle\Entity\Analize\TradeQuantFeatureAnalize10;
use TradeBundle\Entity\TradeQuant;

/**
 * @method TradeQuant|null find($id, $lockMode = null, $lockVersion = null)
 * @method TradeQuant|null findOneBy(array $criteria, array $orderBy = null)
 * @method TradeQuant[]    findAll()
 * @method TradeQuant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TradeQuantFeatureAnalize10Repository extends ATradeQuantFeatureAnalizeRepositoryBase
{
    protected static $entityClass = TradeQuantFeatureAnalize10::class;
}
