<?php

namespace TradeBundle\Repository;

use Doctrine\Persistence\ManagerRegistry;
use TradeBundle\Entity\TradeQuantProfitLevel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method TradeQuantProfitLevel|null find($id, $lockMode = null, $lockVersion = null)
 * @method TradeQuantProfitLevel|null findOneBy(array $criteria, array $orderBy = null)
 * @method TradeQuantProfitLevel[]    findAll()
 * @method TradeQuantProfitLevel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TradeQuantProfitLevelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TradeQuantProfitLevel::class);
    }

    /**
     * Создавем уровень профита
     *
     * @param int   $tp
     * @param int   $sl
     * @param float $tpPrice
     * @param float $slPrice
     * @param float $min
     * @param float $max
     * @param float $profit
     * @param float $slump
     * @return TradeQuantProfitLevel
     * @throws \Doctrine\ORM\ORMException
     */
    public function createLevel(int $tp, int $sl, float $tpPrice, float $slPrice, float $min, float $max, float $profit, float $slump, \DateTime $finishedTime): TradeQuantProfitLevel
    {
        $profitLevel = new TradeQuantProfitLevel();
        $profitLevel->setTp($tp);
        $profitLevel->setSl($sl);
        $profitLevel->setTpPrice($tpPrice);
        $profitLevel->setSlPrice($slPrice);
        $profitLevel->setMin($min);
        $profitLevel->setMax($max);
        $profitLevel->setProfit($profit);
        $profitLevel->setSlump($slump);
        $profitLevel->setFinished($finishedTime);

        $this->_em->persist($profitLevel);
        return $profitLevel;
    }
}
