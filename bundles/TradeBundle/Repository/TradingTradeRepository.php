<?php

namespace TradeBundle\Repository;

use TradeBundle\Entity\TradingTrade;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TradingTrade|null find($id, $lockMode = null, $lockVersion = null)
 * @method TradingTrade|null findOneBy(array $criteria, array $orderBy = null)
 * @method TradingTrade[]    findAll()
 * @method TradingTrade[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TradingTradeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TradingTrade::class);
    }

    // /**
    //  * @return TradingTrade[] Returns an array of TradingTrade objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TradingTrade
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
