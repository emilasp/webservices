<?php

namespace TradeBundle\Repository;

use TradeBundle\Entity\TradingStrategy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TradingStrategy|null find($id, $lockMode = null, $lockVersion = null)
 * @method TradingStrategy|null findOneBy(array $criteria, array $orderBy = null)
 * @method TradingStrategy[]    findAll()
 * @method TradingStrategy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TradingStrategyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TradingStrategy::class);
    }

    // /**
    //  * @return TradingStrategy[] Returns an array of TradingStrategy objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TradingStrategy
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
