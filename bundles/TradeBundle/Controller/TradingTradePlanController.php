<?php

namespace TradeBundle\Controller;

use App\Core\Abstracts\AController;
use App\Core\Helpers\EntityHelper;
use App\Http\ApiResponse;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use TradeBundle\Entity\TradeQuant;
use TradeBundle\Entity\TradingStrategy;
use TradeBundle\Entity\TradingTrade;
use TradeBundle\Service\TradePredictService;
use TradeBundle\Service\TradeQuantService;
use Symfony\Component\Routing\Annotation\Route;
use TradeBundle\Service\TradingTradeService;

/**
 * Class TradingTradePlan
 *
 * @Route("/trading/trade/plan")
 * @Security("is_granted('ROLE_ADMIN')")
 *
 * @package TradeBundle\Controller
 */
class TradingTradePlanController extends AController
{
    /**
     * Список трейдов
     * @Route("/list", name="trading.trade.dashboard")
     *
     * @Template(template="/Trade/plan/index.html.twig")
     *
     * @return array
     */
    public function indexAction(TradeQuantService $service, EntityManagerInterface $em): array
    {
        return [];
    }

    /**
     * Новый трейд
     * @Route("/new", name="trading.trade.new")
     * @Route("/update/{trade}/", name="trading.trade.update")
     *
     * @Template(template="/Trade/plan/new.trade.html.twig")
     *
     * @return array
     */
    public function updateTradeAction(TradeQuantService $service, EntityManagerInterface $em, TradingTrade $trade = null): array
    {
        return ['tradeId' => $trade ? $trade->getId() : null];
    }

    /**
     * Загружаем трейд
     * @Route("/load/{trade}/", name="trading.trade.load", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function loadTradeAction(Request $request, TradingTradeService $tradeService, TradingTrade $trade): JsonResponse
    {
        $this->checkAllow($request);

        return new ApiResponse(true, 'Успешно загружено', ['trade' => $tradeService->getTradeFormData($trade)]);
    }

    /**
     * Сохраняем трейд
     * @Route("/save", name="trading.trade.save", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function saveTradeAction(Request $request, TradingTradeService $tradeService): JsonResponse
    {
        $this->checkAllow($request);

        try {
            $tradeData = json_decode($request->get('trade'), true);
            $trade = $tradeService->saveTrade($tradeData);
        } catch (\Exception $e) {
            return new ApiResponse(false, $e->getMessage());
        }

        return new ApiResponse(true, 'Успешно сохранено', ['trade' => $tradeService->getTradeFormData($trade)]);
    }


    /**
     * Получаем параметры формы
     * @Route("/params", name="trading.trade.get.params", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function getTradingParametersAction(Request $request, TradingTradeService $tradeService): JsonResponse
    {
        $this->checkAllow($request);

        return new ApiResponse(true, 'Success', $tradeService->getTradeFromParams());
    }

    /**
     * Проверняем права доступа
     *
     * @param Request $request
     */
    private function checkAllow(Request $request)
    {
        $apiKey  = $request->get('apiKey');
        $account = $request->get('account');
        $ip      = $request->getClientIp();

        //if ($apiKey !== 'svr4356yhxdv') {
        //    throw new AccessDeniedHttpException('Доступ запрещен');
        //}
    }
}
