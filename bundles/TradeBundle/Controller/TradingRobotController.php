<?php

namespace TradeBundle\Controller;

use App\Core\Abstracts\AController;
use App\Http\ApiResponse;
use CoursesBundle\Entity\User;
use App\Queue\Message\EmailSenderMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use TradeBundle\Entity\QuantLevel;
use TradeBundle\Service\TradeAnalizeService;
use TradeBundle\Service\TradePredictService;
use TradeBundle\Service\TradeQuantService;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class TradingRobotController
 *
 * @package TradeBundle\Controller
 */
class TradingRobotController extends AController
{
    /**
     * Получаем новый сигнал
     * @Route("/robot/trade/quant/batch/add", name="robot.trade.quant.batch.add", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function addQuantsBatchAction(Request $request, TradeQuantService $service): JsonResponse
    {
        $this->checkAllow($request);

        $apiKey       = $request->get('apiKey');
        $account      = $request->get('account');
        $symbol       = $request->get('symbol');
        $typePosition = $request->get('typePosition');
        $ip           = $request->getClientIp();

        $quants = $request->get('quants');

        $service->addQuants($apiKey, $account, $symbol, $typePosition, $quants, $ip);

        return new ApiResponse(true, 'Success add new quant(levels)', null);
    }

    /**
     * Получаем предсказангие для кванта
     * @Route("/robot/trade/quant/predict", name="robot.trade.quant.prediciton", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function getQuantPredictAction(Request $request, TradePredictService $service): JsonResponse
    {
        $this->checkAllow($request);

        $apiKey       = $request->get('apiKey');
        $account      = $request->get('account');
        $symbol       = $request->get('symbol');
        $typePosition = $request->get('typePosition');
        $ip           = $request->getClientIp();

        $quants = $request->get('quants');

        $data = $service->predictFeatures($apiKey, $account, $symbol, $typePosition, $quants, $ip);

        //$serializer = $this->container->get('serializer');

        //foreach ($analizeItems as $item) {
        //    $data[] = $item = $serializer->serialize($item, 'json', ['groups' => ['trading']]);
        //}

        return new ApiResponse(true, 'Success predict features', ['probe' => $data]);
    }

    /**
     * Получаем предсказангие для кванта
     * @Route("/test", name="robot.trade.quant.test", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function testAction(Request $request): JsonResponse
    {
        if ($data = $request->get('dataInput')['first']) {
            return new ApiResponse(true, 'Данные успешно получены, можно сдавать работу :)', ['data' => $data]);
        }

        return new ApiResponse(false, 'Не все данные пришли');
    }


    /**
     * Проверняем права доступа
     *
     * @param Request $request
     */
    private function checkAllow(Request $request)
    {
        $apiKey       = $request->get('apiKey');
        $account      = $request->get('account');
        $ip           = $request->getClientIp();

        if ($apiKey !== 'svr4356yhxdv') {
            throw new AccessDeniedHttpException('Доступ запрещен');
        }
    }
}
