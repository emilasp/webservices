<?php

namespace TradeBundle\Controller;

use App\Core\Abstracts\AController;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TradeBundle\Entity\TradeQuant;
use TradeBundle\Service\TradePredictService;
use TradeBundle\Service\TradeQuantService;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TradingQuantsController
 *
 * @Route("/trading/quants")
 * @Security("is_granted('ROLE_ADMIN')")
 *
 * @package TradeBundle\Controller
 */
class TradingQuantsTesterController extends AController
{
    /**
     * Получаем новый сигнал
     * @Route("/tester", name="trading.quants.tester")
     *
     * @Template(template="/Quants/tester/index.html.twig")
     *
     * @return array
     */
    public function addQuantsBatchAction(TradeQuantService $service, TradePredictService $predictService, EntityManagerInterface $em): array
    {
        $start = new \DateTime('-60 days');
        $end   = new \DateTime('-10 days');

        return [
            'start' => $start,
            'end'   => $end,
        ];
    }

    /**
     * @Route("/predict-quant", name="trade.quant.tester.predict", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function setAttributeApi(Request $request, TradeQuantService $service, TradePredictService $predictService): JsonResponse
    {
        $date        = new \DateTime($request->get('date'));
        $symbol      = $request->get('symbol');
        $minMatProbe = $request->get('matProbe');
        $minTrades   = $request->get('minTrades');
        $timeframes  = $request->get('timeframes');
        $minProbe    = $request->get('minProbe');
        $statDays    = $request->get('statDays');
        $tpRange     = explode('-', $request->get('tpRange'));
        $slRange     = explode('-', $request->get('slRange'));

        // Filter
        $filterLossCount   = $request->get('filterLossCount');
        $filterLossTimeout = $request->get('filterLossTimeout');

        //
        $buyAllow    = $request->get('buyAllow');
        $sellAllow   = $request->get('sellAllow');
        $mmLotEnable = $request->get('mmLotEnable');


        $type = $buyAllow ? 0 : 1; // TODO $sellAllow

        $predicts = [];


        if($date->format('h') < 18) {
            $quant = $this->getEm()->getRepository(TradeQuant::class)
                ->findOneBy(['symbol' => $symbol, 'typePosition' => $type, 'dateTime' => $date,]);


            if ($quant) {
                // Predict
                $dataToPredict = $service->getQuantsDataFromEntity([$quant]);
                $startPredict  = (clone $date)->modify('-'.($statDays + 1).' days');
                $endPredict    = (clone $date)->modify('-1 minutes');

                $predicts = $predictService->predictQuantAllFeatures(
                    $symbol,
                    $type,
                    $dataToPredict[0]['calculators'],
                    $timeframes,
                    $startPredict,
                    $endPredict,
                    $minMatProbe,
                    $minProbe,
                    $minTrades,
                    $tpRange,
                    $slRange
                );

                foreach ($predicts as $index => $item) {
                    $level = $predictService->getQuantLevelByTpSl($quant, $item['tp'], $item['sl']);
                    $predicts[$index]['profitReal'] = $level->getProfit();
                    $predicts[$index]['date']       = $date->format('d H:i');
                    //if ($item['hash'] !== '91935c79ee4016e5772e36538e6d6efb0ba89b1e') {
                    //    unset($predicts[$index]);
                    //}
                }
            }
        }


        return new JsonResponse(['success' => true, 'predicts' => $predicts]);
    }
}
