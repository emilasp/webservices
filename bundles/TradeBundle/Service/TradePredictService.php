<?php

namespace TradeBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use TradeBundle\Entity\TradeQuant;
use TradeBundle\Entity\TradeQuantFeature;
use TradeBundle\Entity\TradeQuantProfitLevel;

/**
 * Class TradePredictService
 *
 * @package TradeBundle\Service
 */
class TradePredictService
{
    /** @var ContainerInterface */
    protected $container;
    /** @var EntityManagerInterface */
    protected $em;
    /** @var TradeQuantService */
    protected $tradeService;
    /** @var TradeAnalizeService */
    protected $tradeAnalizeService;

    /**
     * FileService constructor.
     *
     * @param ContainerInterface     $container
     * @param EntityManagerInterface $em
     */
    public function __construct(
        ContainerInterface $container,
        EntityManagerInterface $em,
        TradeQuantService $tradeService,
        TradeAnalizeService $tradeAnalizeService
    ) {
        $this->container           = $container;
        $this->em                  = $em;
        $this->tradeService        = $tradeService;
        $this->tradeAnalizeService = $tradeAnalizeService;
    }

    /**
     * Прогнощзируем все признаки кванта
     *
     * @param string         $symbol
     * @param int            $typePosition
     * @param array          $featuresData
     * @param int            $numberParts
     * @param \DateTime|null $dateStart
     * @param \DateTime|null $dateEnd
     * @param int            $minTrades
     * @param array          $exclude
     * @return array
     */
    public function predictQuantAllFeatures(
        string $symbol,
        int $typePosition,
        array $featuresData,
        int $numberParts,
        \DateTime $dateStart = null,
        \DateTime $dateEnd = null,
        float $minMatProbe,
        float $minProbe,
        int $minTrades,
        array $tpRange = [],
        array $slRange = [],
        array $exclude = []
    ): array {
        $hashs = [];
        foreach ($featuresData as $feature) {
            if (!in_array($feature['name'], $exclude)) {
                $hashs[] = $this->tradeService->getQuantFeatureHash($symbol, $typePosition, $feature['name'], $feature['config'],
                    $feature['numberFull'], 1);
            }
        }

        $calculators = $this->em->getRepository(TradeQuantFeature::class)
            ->getFeatureStat($symbol, $typePosition, $numberParts, $hashs, $dateStart, $dateEnd);

        $predicts = [];
        foreach ($calculators as $hash => $calculatorLevels) {
            $filtered = $this->getFilteredPredictLevels($calculatorLevels, $minMatProbe, $minProbe, $minTrades, $tpRange, $slRange);
            // Get Max profit calculator level
            if ($filtered) {
                $maxProfitItem = [];
                foreach ($filtered as $item) {
                    if (!$maxProfitItem || (float)$maxProfitItem['profit'] < (float)$item['profit']) {
                        $maxProfitItem = $item;
                    }
                }
                $predicts[] = $maxProfitItem;
            }
        }
        return $predicts;
    }

    /**
     * Отфильтровываем уровни для калькулятора
     * @param array $calculatorLevels
     * @return array
     */
    private function getFilteredPredictLevels(
        array $calculatorLevels,
        float $minMatProbe,
        float $minProbe,
        int $minTrades,
        array $tpRange,
        array $slRange
    ): array {
        $data = [];
        foreach ($calculatorLevels as $levelStat) {
            if ($minTrades && $minTrades > (int)$levelStat['trades']) {
                continue;
            }
            if ($minMatProbe && $minMatProbe > (float)$levelStat['math_probe']) {
                continue;
            }
            if ($minProbe && $minProbe > (float)$levelStat['probe_trades']) {
                continue;
            }
            if ($tpRange && ($tpRange[0] > (int)$levelStat['tp']) || ($tpRange[1] < (int)$levelStat['tp'])) {
                continue;
            }
            if ($slRange && ($slRange[0] > (int)$levelStat['sl']) || ($slRange[1] < (int)$levelStat['sl'])) {
                continue;
            }
            $data[] = $levelStat;
        }
        return $data;
    }

    /**
     * Получаем максимальные и минимальные матожидания по всем предиктам
     * @param array $featuresData
     * @return array
     */
    public function getMaxMinMatProbe(
        string $symbol,
        int $typePosition,
        array $featuresData,
        int $numberParts,
        \DateTime $dateStart = null,
        \DateTime $dateEnd = null,
        int $minTrades = 10,
        array $exclude = []
    ): array {
        $dataReturn = ['max' => null, 'min' => null, 'avg' => 0, 'maxMatProbe' => 0, 'count' => 0, 'countFull' => 0];

        $hashs = [];
        foreach ($featuresData as $feature) {
            if (!in_array($feature['name'], $exclude)) {
                $hashs[] = $this->tradeService->getQuantFeatureHash($symbol, $typePosition, $feature['name'], $feature['config'],
                    $feature['numberFull'], 1);
            }
        }

        $items = $this->em->getRepository(TradeQuantFeature::class)->getFeatureStat($symbol, $typePosition, $numberParts, $hashs, $dateStart,
            $dateEnd);

        $maxByFeature = [];
        foreach ($items as $features) {
            foreach ($features as $item) {
                if (!isset($maxByFeature[$item['feature']]) || $maxByFeature[$item['feature']]['math_probe'] < $item['math_probe']) {
                    $maxByFeature[$item['feature']] = $item;
                }
            }
        }

        $dataReturn['count'] = count($maxByFeature);
        $matProbes           = [];
        foreach ($maxByFeature as $item) {
            if ($item['trades'] > $minTrades && $item['tp'] > 80) {
                $dataReturn['countFull']++;
                $matProbes[] = $item['math_probe'];
                if (!$dataReturn['max'] || (float)$dataReturn['max']['math_probe'] < (float)$item['math_probe']) {
                    $dataReturn['max']         = $item;
                    $dataReturn['maxMatProbe'] = $item['math_probe'];
                }
                if (!$dataReturn['min'] || (float)$dataReturn['min']['math_probe'] > (float)$item['math_probe']) {
                    $dataReturn['min'] = $item;
                }
            }
        }

        $dataReturn['avg'] = $matProbes ? array_sum($matProbes) / count($matProbes) : 0;

        return $dataReturn;
    }

    /**
     * @param TradeQuant $quant
     * @param int        $tp
     * @param int        $sl
     * @return TradeQuantProfitLevel|null
     */
    public function getQuantLevelByTpSl(TradeQuant $quant, int $tp, int $sl): ?TradeQuantProfitLevel
    {
        foreach ($quant->getLevels() as $level) {
            if ($level->getTp() === $tp && $level->getSl() === $sl) {
                return $level;
            }
        }

        return null;
    }


    /**
     * Прогнозируем успех по кванту
     *
     * @param Request $request
     * @return array
     */
    public function predictNeuroNetQuant(Request $request): array
    {
        // Check Api key
        $quantName    = $request->get('quantName');
        $typePosition = $request->get('typePosition');
        $quant        = $request->get('quant');

        $projectRoot = $this->container->get('kernel')->getProjectDir();

        $scriptPath = '/var/www/trade/test.py';

        $program = "python3 {$scriptPath} {$quantName} {$typePosition} {$quant}";

        $descriptorspec = [['pipe', 'r'], ['pipe', 'w'], ['file', '/tmp/webserviceserror-output.txt', 'a']];

        $resultReturn = ['status' => 0, 'message' => null, 'data' => null];
        $process      = proc_open($program, $descriptorspec, $pipes);
        if (is_resource($process)) {
            $result = stream_get_contents($pipes[1]);
            fclose($pipes[1]);
            $returnCode = proc_close($process); // 0 - good, 2 - error

            if ($returnCode === 0) {
                $resultReturn['status'] = 1;
                $resultReturn['data']   = $result;

                return $resultReturn;
            }
        }
        $resultReturn['message'] = 'Не удалось сделать predict';

        return $resultReturn;
    }
}