<?php

namespace TradeBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TradeBundle\Entity\TradeQuant;
use TradeBundle\Entity\TradeQuantFeature;
use TradeBundle\Entity\TradeQuantProfitLevel;

/**
 * Class TradeQuantService
 *
 * @package TradeBundle\Service
 */
class TradeQuantService
{
    /** @var ContainerInterface */
    protected $container;
    /** @var EntityManagerInterface */
    protected $em;

    /**
     * FileService constructor.
     *
     * @param ContainerInterface     $container
     * @param EntityManagerInterface $em
     */
    public function __construct(ContainerInterface $container, EntityManagerInterface $em)
    {
        $this->container = $container;
        $this->em        = $em;
    }

    /**
     * Добавляем записо по сделке для кванта
     *
     * @param string $apiKey
     * @param string $account
     * @param string $symbol
     * @param int    $typePosition
     * @param array  $quants
     * @param string $ip
     */
    public function addQuants(string $apiKey, string $account, string $symbol, int $typePosition, array $quants, string $ip): void
    {
        $repository        = $this->em->getRepository(TradeQuant::class);
        $repositoryFeature = $this->em->getRepository(TradeQuantFeature::class);
        $repositoryLevel   = $this->em->getRepository(TradeQuantProfitLevel::class);

        $quantsData = $this->getQuantsData($apiKey, $account, $symbol, $typePosition, $quants, $ip);

        foreach ($quantsData as $index => $quant) {
            if (!$repository->getQuantUnique($quant['dateTime'], $quant['hash'], $quant['number'], $typePosition)) {
                /** @var TradeQuant $newQuant */
                $newQuant = $repository->createQuant($apiKey, $account, $symbol, $quant['dateTime'], $quant['hash'], $quant['number'], $typePosition, $ip);

                foreach ($quant['calculators'] as $calculator) {

                    $hash         = $this->getQuantFeatureHash(
                        $symbol,
                        $typePosition,
                        $calculator['name'],
                        $calculator['config'],
                        $calculator['numberFull'],
                        1
                    );
                    $quantFeature = $repositoryFeature->createQuantFeature($hash, $calculator['name'], $calculator['numberParts'], 1,
                        $calculator['config'], $calculator['raws']);
                    $newQuant->addFeature($quantFeature);
                }

                foreach ($quant['levels'] as $level) {
                    $quantLevel = $repositoryLevel->createLevel(
                        $level['tp'],
                        $level['sl'],
                        $level['tpPrice'],
                        $level['slPrice'],
                        $level['minPrice'],
                        $level['maxPrice'],
                        $level['profit'],
                        $level['slump'],
                        $level['finishedTime']
                    );
                    $newQuant->addLevel($quantLevel);
                }
                if ($index % 100 === 0) {
                    $this->em->flush();
                    $this->em->clear();
                }
            }
        }
        $this->em->flush();
        $this->em->clear();
    }

    public function getQuantsData(
        string $apiKey,
        string $account,
        string $symbol,
        int $typePosition,
        array $quants,
        string $ip
    ): array {
        $data = [];
        foreach ($quants as $index => $quant) {
            $quantIdentificator = $this->getQuantNameAndNumberByCalculatorsData($quant['calculators']);

            $quantRow = [
                'apiKey'      => $apiKey,
                'account'     => $account,
                'ip'          => $ip,
                'dateTime'    => $this->toCorrectDateTime($quant['dateTime']),
                'hash'        => MD5($quantIdentificator['name'].$quantIdentificator['number']),
                'number'      => $quantIdentificator['number'],
                'calculators' => [],
                'levels'      => [],
            ];

            foreach ($quant['calculators'] as $calculator) {
                $calculator['numberParts'] = $this->getNumberParts($calculator['numbers']);
                $calculator['numberFull']  = $calculator['numberParts'][count($calculator['numberParts']) - 1];
                $calculator['config']      = $calculator['config'] ?: 'empty:0';
                $calculator['hash']        = $this->getQuantFeatureHash(
                    $symbol,
                    $typePosition,
                    $calculator['name'],
                    $calculator['config'],
                    $calculator['numberFull'],
                    1
                );
                $quantRow['calculators'][] = $calculator;
            }

            foreach ($quant['levels'] as $level) {
                $quantRow['levels'][] = [
                    'tp'           => (int)$level['tp'],
                    'sl'           => (int)$level['sl'],
                    'tpPrice'      => (float)$level['tpPrice'],
                    'slPrice'      => (float)$level['slPrice'],
                    'minPrice'     => (float)$level['minPrice'],
                    'maxPrice'     => (float)$level['maxPrice'],
                    'profit'       => (float)$level['profit'],
                    'slump'        => (float)$level['slump'],
                    'finishedTime' => $level['finishedTime'] ? $this->toCorrectDateTime($level['finishedTime']) : null,

                ];
            }
            $data[] = $quantRow;
        }

        return $data;
    }

    /**
     * Формируем массив из объектов
     *
     * @param array $quants
     * @return array
     */
    public function getQuantsDataFromEntity(array $quants): array
    {
        $data = [];
        /** @var TradeQuant $quant */
        foreach ($quants as $quant) {
            $quantRow = [
                'apiKey'      => $quant->getApikey(),
                'account'     => $quant->getAccount(),
                'ip'          => $quant->getIp(),
                'dateTime'    => $quant->getDateTime(),
                'hash'        => $quant->getHash(),
                'number'      => $quant->getNumber(),
                'calculators' => [],
                'levels'      => [],
            ];

            foreach ($quant->getFeatures() as $calculator) {
                $calculatorData                = ['name' => $calculator->getName()];
                $calculatorData['numberParts'] = [
                    $calculator->getNumber1(),
                    $calculator->getNumber2(),
                    $calculator->getNumber3(),
                    $calculator->getNumber4(),
                    $calculator->getNumber5(),
                    $calculator->getNumber6(),
                ];
                $calculatorData['numberFull']  = $calculator->getNumber6();
                $calculatorData['config']      = $calculator->getConfig();
                $calculatorData['hash']        = $calculator->getHash();
                $quantRow['calculators'][]     = $calculatorData;
            }

            foreach ($quant->getLevels() as $level) {
                $quantRow['levels'][] = [
                    'tp'           => $level->getTp(),
                    'sl'           => $level->getSl(),
                    'tpPrice'      => $level->getTpPrice(),
                    'slPrice'      => $level->getSlPrice(),
                    'minPrice'     => $level->getMin(),
                    'maxPrice'     => $level->getMax(),
                    'profit'       => $level->getProfit(),
                    'slump'        => $level->getSlump(),
                    'finishedTime' => $level->getFinished(),
                ];
            }
            $data[] = $quantRow;
        }

        return $data;
    }

    /**
     * Формируем наименование и номер Кванта по данным из калькуляторов
     *
     * @param array $calculators
     * @return array
     */
    private function getQuantNameAndNumberByCalculatorsData(array $calculators): array
    {
        $data = ['name' => '', 'number' => ''];
        foreach ($calculators as $calculator) {
            $data['name']   .= $calculator['name'].":";
            $data['name']   .= implode('-', $calculator['timeframes']).";";
            $data['number'] .= implode('-', $calculator['numbers']).";";
        }

        return $data;
    }

    /**
     * Приводим дату в нужный вид
     *
     * @param string $dateString
     * @return \DateTime
     */
    private function toCorrectDateTime(string $dateString): \DateTime
    {
        return \DateTime::createFromFormat('Y.m.d H:i', $dateString);
    }


    /**
     * Формируем номера кванта
     *
     * @param array $numbers
     * @return array
     */
    private function getNumberParts(array $numbers): array
    {
        $countNumbers = count($numbers);
        $numberParts  = [];
        foreach ($numbers as $index => $number) {
            $nums          = array_slice($numbers, $countNumbers - $index - 1, $countNumbers);
            $numberParts[] = implode('_', array_reverse($nums));
        }
        for ($i = 0; $i < 6; $i++) {
            if (!isset($numberParts[$i])) {
                $numberParts[$i] = $lastNumber;
            } else {
                $lastNumber = $numberParts[$i];
            }
        }

        return $numberParts;
    }

    /**
     * Формируем хеш для кванта
     *
     * @param string $symbol
     * @param int    $typePos
     * @param string $featureName
     * @param string $config
     * @param string $numberFull
     * @param int    $timeFrame
     * @return string
     */
    public function getQuantFeatureHash(string $symbol, int $typePos, string $featureName, string $config, string $numberFull, int $timeFrame): string
    {
        return md5($symbol.$typePos.$featureName.$config.$numberFull.$timeFrame)
            .hash('crc32', $featureName.$numberFull);
    }
}