<?php

namespace TradeBundle\Service;

use App\Core\Helpers\EntityHelper;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use TradeBundle\Entity\TradingStrategy;
use TradeBundle\Entity\TradingTrade;

/**
 * Class TradingTradeService
 *
 * @package TradeBundle\Service
 */
class TradingTradeService
{
    /** @var ContainerInterface */
    protected $container;
    /** @var EntityManagerInterface */
    protected $em;

    /**
     * FileService constructor.
     *
     * @param ContainerInterface     $container
     * @param EntityManagerInterface $em
     */
    public function __construct(ContainerInterface $container, EntityManagerInterface $em)
    {
        $this->container = $container;
        $this->em        = $em;
    }


    /**
     * Сохраняем трейд из vue формы
     *
     * @param array $dataToSave
     * @return TradingTrade
     */
    public function saveTrade(array $dataToSave): TradingTrade
    {
        if (empty($dataToSave['id'])) {
            $trade = new TradingTrade();
            $this->em->persist($trade);
        } else {
            $trade = $this->em->getRepository(TradingTrade::class)->find($dataToSave['id']);
        }

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        foreach ($dataToSave as $field => $value) {
            if (in_array($field, ['id', 'strategyId', 'tradeBaseId', 'image', 'imageResult', 'side', 'psyhologyRate', 'isResultKnock'])) {
                continue;
            }
            if ($propertyAccessor->isWritable($trade, $field)) {
                $propertyAccessor->setValue($trade, $field, $value);
            } else {
                $tt = 10;
            }
        }

        $trade->setStrategy($this->em->getRepository(TradingStrategy::class)->find($dataToSave['strategyId']));
        if ($dataToSave['tradeBaseId']) {
            $trade->setTradeBase($this->em->getRepository(TradingTrade::class)->find($dataToSave['tradeBaseId']));
        }

        $this->calculateTrade($trade);

        $this->em->flush($trade);

        return $trade;
    }

    /** Рассчитываем поля
     * @param TradingTrade $trade
     */
    private function calculateTrade(TradingTrade $trade)
    {
        // Trade type
        $trade->setSide(TradingTrade::TREND_TYPE_NONE);
        if ($trade->getPrice()) {
            if ($trade->getStopLoss()) {
                if ($trade->getPrice() > $trade->getStopLoss()) {
                    $trade->setSide(TradingTrade::TREND_TYPE_LONG);
                } else {
                    $trade->setSide(TradingTrade::TREND_TYPE_SHORT);
                }
            } elseif ($trade->getTakeProfit()) {
                if ($trade->getTakeProfit() > $trade->getPrice()) {
                    $trade->setSide(TradingTrade::TREND_TYPE_LONG);
                } else {
                    $trade->setSide(TradingTrade::TREND_TYPE_SHORT);
                }
            }
        }
    }

    /**
     * Формируем массив дял заполнения vue формы
     *
     * @param TradingTrade $trade
     * @return array
     */
    public function getTradeFormData(TradingTrade $trade): array
    {
        $data = [
            'id'                     => $trade->getId(),
            'strategyId'             => $trade->getStrategy()->getId(),
            'tradeBaseId'            => $trade->getTradeBase() ? $trade->getTradeBase()->getId() : null,
            'type'                   => $trade->getType(),
            'status'                 => $trade->getStatus(),
            'market'                 => $trade->getMarket(),
            'instrument'             => $trade->getInstrument(),
            'timeframe'              => $trade->getTimeframe(),
            'riskCalcType'           => $trade->getRiskCalcType(),
            'trendType'              => $trade->getTrendType(),
            'trendPhase'             => $trade->getTrendPhase(),
            'trendStrenght'          => $trade->getTrendStrenght(),
            'trendPatternCount'      => $trade->getTrendPatternCount(),
            'trendNxtTfType'         => $trade->getTrendNxtTfType(),
            'trendNxt2TfType'        => $trade->getTrendNxt2TfType(),
            'atrValue'               => $trade->getAtrValue(),
            'side'                   => $trade->getSide(),
            'price'                  => $trade->getPrice(),
            'balance'                => $trade->getBalance(),
            'enterReasons'           => json_encode($trade->getEnterReasons(), JSON_UNESCAPED_UNICODE),
            'commentEnter'           => $trade->getCommentEnter(),
            'takeProfit'             => $trade->getTakeProfit(),
            'stopLoss'               => $trade->getStopLoss(),
            'volume'                 => $trade->getVolume(),
            'profitPotentialPercent' => $trade->getProfitPotentialPercent(),
            'profitPercent'          => $trade->getProfitPercent(),
            'profitResult'           => $trade->getProfitResult(),
            'riskBalancePercent'     => $trade->getRiskBalancePercent(),
            'commentExit'            => $trade->getCommentExit(),
            'commentResult'          => $trade->getCommentResult(),
            'commentResultImprove'   => $trade->getCommentResultImprove(),
            'psyhologyRate'          => $trade->getPsyhologyRate(),
            'profitResultHighest'    => $trade->getProfitResultHighest(),
            'isResultKnock'          => $trade->getIsResultKnock(),
            'isFilterAtr'            => $trade->getIsFilterAtr(),
            'isFilterVolume'         => $trade->getIsFilterVolume(),
            'isFilterCftcType'       => $trade->getIsFilterCftcType(),
            //'imageLink'              => $trade->getId(),
            //'imageResultLink'        => $trade->getId(),
        ];

        return $data;
    }

    /**
     * Параметры для vue формы создания/редактирования трейда
     *
     * @return array
     */
    public function getTradeFromParams(): array
    {
        $strategies   = EntityHelper::getItemsForSelect($this->em->getRepository(TradingStrategy::class)->findAll());
        $tradesEntity = $this->em->getRepository(TradingTrade::class)->createQueryBuilder('t')
            ->where('t.status NOT IN(:statusesFinal)')
            ->setParameter('statusesFinal', [TradingTrade::STATUS_CLOSE, TradingTrade::STATUS_DELETE], Connection::PARAM_INT_ARRAY)
            ->getQuery()
            ->getResult();
        $tradesActive = EntityHelper::getItemsForSelect($tradesEntity);

        return [
            'strategies'    => $strategies,
            'trades'        => $tradesActive,
            'types'         => TradingTrade::TYPES,
            'statuses'      => TradingTrade::STATUSES,
            'trendTypes'    => TradingTrade::TREND_TYPES,
            'trendPhases'   => TradingTrade::TREND_PHASES,
            'periods'       => TradingTrade::PERIODS,
            'markets'       => TradingTrade::MARKETS,
            'riskCalcTypes' => TradingTrade::RISK_CALC_TYPES,
        ];
    }
}