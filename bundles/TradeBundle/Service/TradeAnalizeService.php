<?php

namespace TradeBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use TradeBundle\Entity\TradeQuantFeature;
use TradeBundle\Entity\TradeQuantFeatureAnalize;
use TradeBundle\Entity\TradeQuantProfitLevel;

/**
 * Class TradeAnalizeService
 *
 * @package TradeBundle\Service
 */
class TradeAnalizeService
{
    /** @var ContainerInterface */
    protected $container;
    /** @var EntityManagerInterface */
    protected $em;
    /** @var TradeQuantService */
    protected $tradeService;

    /**
     * FileService constructor.
     *
     * @param ContainerInterface     $container
     * @param EntityManagerInterface $em
     */
    public function __construct(ContainerInterface $container, EntityManagerInterface $em, TradeQuantService $tradeService)
    {
        $this->container    = $container;
        $this->em           = $em;
        $this->tradeService = $tradeService;
    }

    /**
     * Получаем статистику по квантам для определенных профитных уровней
     *
     * @param \DateTime $date
     * @param int       $takeProfit
     * @param int       $stopLoss
     */
    public function getMathProbeForQuant(
        string $quantName,
        int $typePosition,
        string $feature,
        int $timeframe,
        string $config,
        string $number,
        int $tp = null,
        int $sl = null
    ): array {
        /** @var QueryBuilder $query */
        $query         = $this->em->getRepository(TradeQuantFeatureAnalize::class)
            ->createQueryBuilder('a');
        $quantsAnalize = $query->where('a.quantName=:quantName')
            ->andWhere('a.typePosition=:typePosition')
            ->andWhere('a.quantName=:quantName')
            ->andWhere('a.feature=:feature')
            ->andWhere('a.timeframe=:timeframe')
            ->andWhere('a.config=:config')
            ->andWhere('a.number=:number')
            ->setParameters([
                'quantName'    => $quantName,
                'typePosition' => $typePosition,
                'feature'      => $feature,
                'timeframe'    => $timeframe,
                'config'       => $config,
                'number'       => $number,
            ])
            ->getQuery()->getResult();

        $serializer = $this->container->get('serializer');

        $maxMathProbe = 0;
        $analize      = ['current' => [], 'best' => [], 'all' => []];
        /** @var TradeQuantFeatureAnalize $quant */
        foreach ($quantsAnalize as $quant) {
            if ($quant->getProfit() > 0) {
                $item = $serializer->serialize($quant, 'json', ['groups' => ['trading']]);
                if ($maxMathProbe < $quant->getMathProbe()) {
                    $maxMathProbe    = $quant->getMathProbe();
                    $analize['best'] = $item;
                }

                if ($quant->getTp() === $tp && $quant->getSl() === $sl) {
                    $analize['current'] = $item;
                }
                $analize['all'][] = $item;
            }
        }

        return $analize;
    }

    /**
     * Анализируем одиночный квант
     *
     * @param string $symbol
     * @param int    $typePos
     * @param string $featureName
     * @param string $config
     * @param string $numberFull
     * @param int    $tf
     * @return array
     */
    public function getAnalizeQuantFeatureSingle(
        string $symbol,
        int $typePos,
        string $featureName,
        string $config,
        string $numberFull,
        int $tf,
        int $fromDays,
        int $numberParts,
        \DateTime $dateStart = null,
        \DateTime $dateEnd = null
    ): array {
        $data   = [];
        $hash   = $this->tradeService->getQuantFeatureHash($symbol, $typePos, $featureName, $config, $numberFull, $tf);

        $repoName              = "TradeBundle\Entity\Analize\TradeQuantFeatureAnalize{$fromDays}";
        $repository            = $this->em->getRepository($repoName);

        $this->em->getRepository(TradeQuantFeature::class)->getFeatureStat($symbol, $typePos, $numberParts, $hashs, $dateStart, $dateEnd);

        return $repository->findBy(['symbol' => $symbol, 'typePosition' => $typePos, 'timeframes' => $numberParts, 'feature' => $featureName, 'hash' => $hash]);
    }


    /**
     * Прогнозируем успех по кванту
     *
     * @param Request $request
     * @return array
     */
    public function predictQuantByStatistics(Request $request): int
    {
        // {"apiKey": "svr4356yhxdv","account": "2342345435534532dsdfs2","quantName": "quant name", "typePosition": 1, "quant": "sdfsd2342dssfdt3453sdfsdf34r3dwsdfsf", "tp": 100, "sl": 100}
        $symbol      = $request->get('symbol');
        $typePpos    = $request->get('typePpos');
        $featureName = $request->get('featureName');
        $config      = $request->get('config');
        $number      = $request->get('number');
        $timeFrame   = $request->get('timeFrame');

        $hash = $this->tradeService->getQuantFeatureHash($symbol, $typePpos, $featureName, $config, $number, $timeFrame);

        return $this->predictByStatisticsInBd(
            $request->get('quantName'),
            $request->get('typePosition'),
            $request->get('quant'),
            new \DateTime('-1 month')
        );
    }


    public function updateAnalizeTable()
    {
        
    }
}