<?php

namespace TradeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('TradeConfig');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->scalarNode('web_folder')->end()
                ->scalarNode('cache_folder')->end()
                ->scalarNode('original_folder')->end()
            ->end();

        return $treeBuilder;
    }
}