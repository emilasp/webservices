<?php
namespace TradeBundle;

use TradeBundle\DependencyInjection\TradeExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class TradeBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new TradeExtension();
    }
}