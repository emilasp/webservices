'use strict';

import moment from 'moment';
import Chart  from 'chart.js';


class Tester {
    constructor() {
        //Settings
        this.startDate;
        this.endDate;
        this.symbol;
        this.matProbe;
        this.minTrades;
        this.minProbe;
        this.timeframes;
        this.statDays;
        this.tp;
        this.sl;
        this.filterLossCount;
        this.filterLossTimeout;
        this.buyAllow;
        this.sellAllow;
        this.mmLotEnable;

        // Calculate
        this.datesToSend = [];

        this.pause = false;

        this.dataToChart = [];
        this.timeAll = 0;
        this.profit = 0;

        // Calculated
        this.featuresNames = [];
        this.featuresprofits = [];

        // Sender Queue Manager
        this.maxWorkers = 20;
        this.workersCurrent = 0;
        this.workersFinished = 0;

        this.chart;
        this.chartFeatures = [];
        this.chartColors = [
            'rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(54, 162, 235)',
            'rgb(153, 102, 255)',
            'rgb(231,233,237)',
            'rgb(109,239,83)',
            'rgb(97,152,153)',
            'rgb(10,11,14)',
            'rgb(210,18,82)',
        ];
    }

    run() {
        this.init();
        this.setDates();

        this.timeAll = this.datesToSend.length;

        console.log('Start sended');

        let $this = this;
        setInterval(function() {$this.senderQueueManager();}, 1);

        $('#btn-start').prop('disabled', true);
    }
    changePause() {
        this.pause = $('#formTesterPause').is(':checked');
    }

    init() {
        this.datesToSend = [];
        this.dataToChart = [];
        this.pause = false;
        this.profit = 0;

        this.startDate = $('#formDateStart').val();
        this.endDate = $('#formDateEnd').val();
        this.symbol = $('#formSymbol').val();
        this.matProbe = $('#formMatProbe').val();
        this.minTrades = $('#formMinTrades').val();
        this.minProbe = $('#formMinProbe').val();
        this.timeframes = $('#formTimeframes').val();
        this.statDays = $('#formStatDays').val();
        this.tp = $('#formTp').val();
        this.sl = $('#formSl').val();
        this.filterLossCount = $('#formFilterLossCount').val();
        this.filterLossTimeout = $('#formFilterLossTimeout').val();
        this.buyAllow = $('#buyAllow').is(':checked') ? 1 : 0;
        this.sellAllow = $('#sellAllow').is(':checked') ? 1 : 0;
        this.mmLotEnable = $('#formMmLotEnable').is(':checked') ? 1 : 0;

        this.initChartJs();
    }

    initChartJs() {
        this.chart = new Chart($('#myChart'), {
            type: 'line',
            data: {
                labels: [],
                datasets: [],
            },
            options: {
                responsive: true,
                layout: {
                    padding: {
                        left: 50,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }
                }
            }
        });
    }

    addChartData(label, value, datasetIndex, featureName) {
        this.chart.data.labels.push(label);
        if (typeof(this.chart.data.datasets[datasetIndex]) === 'undefined') {
            this.chart.data.datasets[datasetIndex] = {
                label: featureName,
                backgroundColor: this.chartColors[datasetIndex],
                borderColor: this.chartColors[datasetIndex],
                data: []
            };
        }
        this.chart.data.datasets[datasetIndex].data.push(value);
        this.chart.update();
    }

    senderQueueManager() {
        if (!this.pause && this.workersCurrent <= this.maxWorkers && this.datesToSend.length) {
            const date = this.datesToSend.shift();
            this.workersCurrent++;

            this.sendTimePredict(date);
        }
    }

    sendTimePredict(date) {
        const data = {
            date: date,
            symbol: this.symbol,
            matProbe: this.matProbe,
            minTrades: this.minTrades,
            timeframes: this.timeframes,
            statDays: this.statDays,
            minProbe: this.minProbe,
            tpRange: this.tp,
            slRange: this.sl,
            filterLossCount: this.filterLossCount,
            filterLossTimeout: this.filterLossTimeout,
            buyAllow: this.buyAllow,
            sellAllow: this.sellAllow,
        };

        let $this = this;

        console.log('Send ', date);
        $.ajax({
                   type: "POST",
                   url: "/trading/quants/predict-quant",
                   data: data,
                   //contentType: "application/json; charset=utf-8",
                   dataType: "json",
                   async: true,
                   success: function (result) {
                       $this.onCompleted(result);
                   },
                   failure: function (errMsg) {
                       console(errMsg);
                       $this.onError();
                   }
               });
    }


    setDates() {
        this.datesToSend = [];

        let start = moment(this.startDate + ' 00:00', 'YYYY-MM-DD HH:mm').toDate();
        let end = moment(this.endDate + ' 00:00', 'YYYY-MM-DD HH:mm').toDate();
        while (start < end) {
            let newDate = start.setDate(start.getDate() + 1);
            for (let i = 0; i < 1440; i++) {
                newDate = moment(newDate).add(1, 'm').toDate();
                start = new Date(newDate);
                this.datesToSend.push(moment(start).format('YYYY-MM-DD HH:mm'));
            }
        }
    }

    onCompleted(result) {
        this.workersFinished++;
        this.workersCurrent--;

        let $this = this;
        $.each(result.predicts, function (index, predict) {
            const featureName = predict.feature + predict.config;
            let indexFeature = $this.chartFeatures.indexOf(featureName);
            if (indexFeature === -1) {
                $this.chartFeatures.push(featureName);
                indexFeature = $this.chartFeatures.length - 1

                $this.featuresNames.push(predict.feature);
                $this.featuresprofits.push(0);
            }
            $this.addChartData(predict.date, predict.profitReal, indexFeature, predict.feature);

            /// Fill zero if isset


            const profit = parseFloat(predict.profitReal);
            $this.profit = $this.profit + parseFloat(profit);
            $this.featuresprofits[indexFeature] += profit;

        });

        this.updateStatSenderManager();
    }
    onError() {
        this.workersFinished++;
        this.workersCurrent--;
        this.updateStatSenderManager();
    }

    updateStatSenderManager () {
        const percent = (this.workersFinished / this.timeAll * 100).toFixed(2);
        $('#formProgressBar').css('width', percent + '%').text(percent + '% (' + this.workersFinished + '/' + this.timeAll + ')');

        $('#formCalcTimeCompleted').text(this.workersFinished);
        $('#formCalcTimeAll').text(this.timeAll);
        $('#formCalcProfit').text(Math.round(this.profit * 100000) / 100000);

        $('#formCalcWorkers').text(this.workersCurrent);

        let $this = this;
        let htmlProfit = "";
        $.each(this.featuresNames, function (index, name) {
            const profit = Math.round($this.featuresprofits[index] * 10000) / 10000;
            htmlProfit += "<b>" + name + "</b>:" + $this.featuresprofits[index] + "<br />";
        });
        $('#featuresProfit').html(htmlProfit);
    }
}

const tester = new Tester('Jon Snow');


$('#btn-start').on('click', () => tester.run());
$('#formTesterPause').on('click', () => tester.changePause());