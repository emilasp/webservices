'use strict';

import Vue from 'vue';
import TraderDashboardApp from "./TraderDashboardApp.vue";
import Vuelidate from "vuelidate";
import VModal from 'vue-js-modal'

Vue.use(VModal, { componentName: "modal" });
Vue.use(Vuelidate);

Vue.config.productionTip = false;

require('./css/app.scss');

new Vue(TraderDashboardApp);
