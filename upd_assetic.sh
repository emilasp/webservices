#!/bin/sh

./bin/console assets:install --symlink
yarn encore dev

./bin/console fos:js-routing:dump --format=json --target=assets/js/fos_js_routes.json
#php -d memory_limit=512M ./bin/console assets:dump --watch --force
