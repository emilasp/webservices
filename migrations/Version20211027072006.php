<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211027072006 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE trading_strategy (id INT AUTO_INCREMENT NOT NULL, image_id INT DEFAULT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, market VARCHAR(255) DEFAULT NULL, timeframe VARCHAR(255) DEFAULT NULL, instrument VARCHAR(255) DEFAULT NULL, trend_type SMALLINT UNSIGNED NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, enter_rules LONGTEXT DEFAULT NULL, exit_rules LONGTEXT DEFAULT NULL, risk_management LONGTEXT DEFAULT NULL, profit_percent NUMERIC(5, 2) NOT NULL, is_robot_test TINYINT(1) NOT NULL, risk SMALLINT UNSIGNED NOT NULL, difficultly SMALLINT UNSIGNED NOT NULL, rating SMALLINT UNSIGNED NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_CE43E1EC3DA5256D (image_id), INDEX IDX_CE43E1ECDE12AB56 (created_by), INDEX IDX_CE43E1EC16FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trading_trade (id INT AUTO_INCREMENT NOT NULL, strategy_id INT NOT NULL, trade_base_id INT DEFAULT NULL, image_id INT DEFAULT NULL, image_result_id INT DEFAULT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, type SMALLINT UNSIGNED NOT NULL, status SMALLINT NOT NULL, market VARCHAR(20) NOT NULL, timeframe VARCHAR(10) NOT NULL, instrument VARCHAR(100) NOT NULL, side SMALLINT UNSIGNED NOT NULL, price NUMERIC(12, 5) NOT NULL, volume NUMERIC(6, 3) NOT NULL, balance NUMERIC(12, 2) NOT NULL, take_profit NUMERIC(12, 5) DEFAULT NULL, stop_loss NUMERIC(12, 5) DEFAULT NULL, profit_potential_percent NUMERIC(5, 2) DEFAULT NULL, profit_percent NUMERIC(5, 2) DEFAULT NULL, profit_result NUMERIC(10, 5) DEFAULT NULL, profit_result_highest NUMERIC(10, 5) DEFAULT NULL, is_result_knock TINYINT(1) NOT NULL, risk_balance_percent NUMERIC(5, 2) DEFAULT NULL, risk_calc_type SMALLINT NOT NULL, comment_enter LONGTEXT DEFAULT NULL, comment_exit LONGTEXT DEFAULT NULL, comment_result LONGTEXT DEFAULT NULL, comment_result_improve LONGTEXT DEFAULT NULL, psyhology_rate SMALLINT UNSIGNED NOT NULL, enter_reasons LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', trend_side SMALLINT NOT NULL, trend_type SMALLINT NOT NULL, trend_phase SMALLINT NOT NULL, trend_nxt_tf_type SMALLINT NOT NULL, trend_nxt2tf_type SMALLINT NOT NULL, trend_strenght SMALLINT DEFAULT NULL, trend_pattern_count SMALLINT DEFAULT NULL, atr_value NUMERIC(6, 2) DEFAULT NULL, is_filter_atr TINYINT(1) NOT NULL, is_filter_volume TINYINT(1) NOT NULL, is_filter_cftc_type TINYINT(1) NOT NULL, is_analized TINYINT(1) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_CA4E5B4DD5CAD932 (strategy_id), INDEX IDX_CA4E5B4D9AE787B7 (trade_base_id), UNIQUE INDEX UNIQ_CA4E5B4D3DA5256D (image_id), UNIQUE INDEX UNIQ_CA4E5B4DDF256D17 (image_result_id), INDEX IDX_CA4E5B4DDE12AB56 (created_by), INDEX IDX_CA4E5B4D16FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE trading_strategy ADD CONSTRAINT FK_CE43E1EC3DA5256D FOREIGN KEY (image_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE trading_strategy ADD CONSTRAINT FK_CE43E1ECDE12AB56 FOREIGN KEY (created_by) REFERENCES users (id)');
        $this->addSql('ALTER TABLE trading_strategy ADD CONSTRAINT FK_CE43E1EC16FE72E1 FOREIGN KEY (updated_by) REFERENCES users (id)');
        $this->addSql('ALTER TABLE trading_trade ADD CONSTRAINT FK_CA4E5B4DD5CAD932 FOREIGN KEY (strategy_id) REFERENCES trading_strategy (id)');
        $this->addSql('ALTER TABLE trading_trade ADD CONSTRAINT FK_CA4E5B4D9AE787B7 FOREIGN KEY (trade_base_id) REFERENCES trading_trade (id)');
        $this->addSql('ALTER TABLE trading_trade ADD CONSTRAINT FK_CA4E5B4D3DA5256D FOREIGN KEY (image_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE trading_trade ADD CONSTRAINT FK_CA4E5B4DDF256D17 FOREIGN KEY (image_result_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE trading_trade ADD CONSTRAINT FK_CA4E5B4DDE12AB56 FOREIGN KEY (created_by) REFERENCES users (id)');
        $this->addSql('ALTER TABLE trading_trade ADD CONSTRAINT FK_CA4E5B4D16FE72E1 FOREIGN KEY (updated_by) REFERENCES users (id)');
        $this->addSql('ALTER TABLE trade_quant_feature CHANGE config config VARCHAR(1000) NOT NULL');
        $this->addSql('ALTER TABLE trade_quant_feature_analize_10 CHANGE config config VARCHAR(1000) NOT NULL');
        $this->addSql('ALTER TABLE trade_quant_feature_analize_3 CHANGE config config VARCHAR(1000) NOT NULL');
        $this->addSql('ALTER TABLE trade_quant_feature_analize_30 CHANGE config config VARCHAR(1000) NOT NULL');
        $this->addSql('ALTER TABLE trade_quant_feature_analize_5 CHANGE config config VARCHAR(1000) NOT NULL');
        $this->addSql('ALTER TABLE trade_quant_feature_analize_90 CHANGE config config VARCHAR(1000) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE trading_trade DROP FOREIGN KEY FK_CA4E5B4DD5CAD932');
        $this->addSql('ALTER TABLE trading_trade DROP FOREIGN KEY FK_CA4E5B4D9AE787B7');
        $this->addSql('DROP TABLE trading_strategy');
        $this->addSql('DROP TABLE trading_trade');
        $this->addSql('ALTER TABLE trade_quant_feature CHANGE config config VARCHAR(1000) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE trade_quant_feature_analize_10 CHANGE config config VARCHAR(1000) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE trade_quant_feature_analize_3 CHANGE config config VARCHAR(1000) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE trade_quant_feature_analize_30 CHANGE config config VARCHAR(1000) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE trade_quant_feature_analize_5 CHANGE config config VARCHAR(1000) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE trade_quant_feature_analize_90 CHANGE config config VARCHAR(1000) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
