#!/bin/sh

 ./bin/console doctrine:cache:clear-metadata
 ./bin/console doctrine:schema:update  --force
 ./bin/console make:entity --regenerate