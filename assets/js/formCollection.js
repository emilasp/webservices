//require('./app');
require('../../public/js/jquery.collection.js');
require('../../node_modules/jquery-ui-sortable/jquery-ui.min');

var isDragging = false;

$('.my-selector').collection({
    position_field_selector: '.my-position',
    allow_duplicate: true,
    drag_drop_options: {
        stop: function (ui, event, elements, element) {
            if (isDragging) {
                console.log('<div class="alert alert-warning">Position unchanged!</div>');
                isDragging = false;
            }
        }
    },
    drag_drop_start: function (ui, event, elements, element) {
        console.log('<div class="alert alert-danger">Dragging....</div>');
        isDragging = true;
    },
    drag_drop_update: function (ui, event, elements, element) {
        console.log('<div class="alert alert-success">Position updated!</div>');
        isDragging = false;
    }
});

