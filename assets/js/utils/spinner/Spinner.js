require('./spinner.scss');

export default class Spinner {
    create(el, size = 'md', static_pos)
    {
        let spinner = el.children('.spinner');
        if (spinner.length && !spinner.hasClass('spinner-remove')) return null;
        !spinner.length && (spinner = $('<div class="spinner-' + size + ' spinner' + (static_pos ? '' : ' spinner-absolute') + '"/>').appendTo(el));
        this.animateSpinner(spinner, 'add');
    }

    remove(el, callback)
    {
        let spinner = el.children('.spinner');
        spinner.length && this.animateSpinner(spinner, 'remove', callback);
    }

    animateSpinner(el, animation, callback)
    {
        if (el.data('animating')) {
            el.removeClass(el.data('animating')).data('animating', null);
            el.data('animationTimeout') && clearTimeout(el.data('animationTimeout'));
        }
        el.addClass('spinner-' + animation).data('animating', 'spinner-' + animation);
        el.data('animationTimeout', setTimeout(function() { animation === 'remove' && el.remove(); callback && callback(); }, parseFloat(el.css('animation-duration')) * 1000));
    }
}
