export default class ImageHelper {
    initType()
    {
        $(document).on('click', '.image-input-thumb', function () {
            $(this).closest('.image-type-container').find('input[type="file"]').click();
        });
        $(document).on('change', '.custom-file input[type="file"]', function () {
            if (this.files && this.files[0]) {
                let imgTag = $(this).closest('.image-type-container').find('img');
                let reader = new FileReader();
                reader.onload = function (e) {
                    imgTag.attr('src', e.target.result);
                };
                reader.readAsDataURL(this.files[0]);
            }
        });
    }
}



