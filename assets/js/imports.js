/** ROUTING **/
const routes = require('../js/fos_js_routes.json');
const Routing = require('../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js');

Routing.setRoutingData(routes);
/** ROUTING END **/

/** jQuery **/
import $ from 'jquery';
import jQuery from 'jquery';
window.$ =  window.jQuery = jQuery;
/** jQuery END **/


/** Imports **/
import bootstrap from 'bootstrap';

require('./form');

require('./app');
