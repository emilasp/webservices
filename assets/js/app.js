import _ from 'lodash';
import jBox from 'jbox';
import Spinner from './utils/spinner/Spinner';
import ImageHelper from './utils/image/imageHelper';

require('../scss/app.scss');

/**
 * Core Application
 */
class Application {

    /**
     * Constructor
     */
    constructor() {
        this.initTooltip();
        this.initAutoNotices();
        this.initSpinner();
        this.initImage();

        this._ = _;
    }

    /**
     * Инициализируем Spinner
     */
    initSpinner(){
        this.spinner = new Spinner();

        $('.buttonLoaded').on('click', (event) =>  {
            var button = $(event.target);
            button.addClass('disabled');
            this.spinner.create(button);
        });
    }

    /**
     * Инициализируем Spinner
     */
    initImage(){
        this.image = new ImageHelper();
        this.image.initType();
    }


    /**
     * Инициализируем всплывающие подсказки
     */
    initTooltip(){
        new jBox('Tooltip', {
            attach: '.tooltip',
            getTitle: 'data-title',
            getContent: 'data-content'
        });
    }


    /**
     * Инициализируем всплывающие подсказки
     */
    initGallery(){
        new jBox('Image');
    }


    /**
     * Инициализируем всплывающие подсказки
     */
    progress(type = 'Line'){

    }


    /**
     * Инициализируем всплывающие подсказки из предустановленного Html
     */
    initAutoNotices(){
        let notices = $('.app-notice');
        let timeout = 0;
        let app = this;
        notices.each((index, el) => {
            setTimeout(function() {
                let notice = $(el);
                app.notice(notice.html(), notice.data('color'));
            }, timeout);
            timeout += 500;
        });
    }


    /*getConfirm() {
        return new jBox('Confirm', {
            confirmButton: 'Да',
            cancelButton: 'нет'
        });
    }*/


    /**
     *  Выводим уведомление
     **/
    notice(message, type = 'notice', autoclose = '5000') {
        let colorTypes = {notice: 'blue', warning: 'yellow', danger: 'red', success: 'green', black: 'black'};
        let color = colorTypes.hasOwnProperty(type) ? colorTypes[type] : colorTypes['notice'];

        new jBox('Notice', {
            content: message,
            color: color,
            autoClose: autoclose,
            showCountdown: true
        });
    };

}

window.App = global.App = new Application();
