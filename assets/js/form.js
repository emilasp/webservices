require('./formSelect2');


import date from 'bootstrap4-datetimepicker/build/js/bootstrap-datetimepicker.min';

$(function () {
    $('.datetimepicker').datetimepicker({
        locale: 'ru',
        sideBySide: true,
        format: 'YYYY-MM-DD HH:mm',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    });
});
