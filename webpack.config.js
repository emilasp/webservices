var Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already
// configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

const NODE_ENV = 'development'//process.env.NODE_ENV;

Encore
// directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */
    // Add style entry

    .addStyleEntry('css/template', './assets/scss/template.scss')


    .addEntry('js/fosjsrouting', './public/bundles/fosjsrouting/js/router.js')
    .addEntry('js/imports', './assets/js/imports.js')
    .addEntry('js/form/collection', './assets/js/formCollection.js')

    // Social JS
    .addEntry('social/comments/vue', './bundles/SocialBundle/Assets/js/vue/apps/comments/comments.js')
    // Course JS
    .addEntry('course/lesson/vue', './bundles/CoursesBundle/Assets/js/vue/apps/lesson/lesson.js')
    .addEntry('js/course/dashboard/vue', './bundles/CoursesBundle/Assets/js/vue/apps/dashboard/dashboard.js')
    .addEntry('js/course/lesson/periodic/vue', './bundles/CoursesBundle/Assets/js/vue/elements/periodicType/periodicType.js')
    // Eav
    .addEntry('js/eav/attributes/vue', './bundles/EavBundle/Assets/js/vue/apps/attributes/index.js')
    .addEntry('js/eav/attributesEntity/vue', './bundles/EavBundle/Assets/js/vue/apps/AttributesEntity/index.js')
    //Affirmations
    .addEntry('js/affirmations/affirmationAdd/vue', './bundles/AffirmationsBundle/Assets/js/vue/apps/AffirmationAdd/index.js')
    .addEntry('js/affirmations/CatalogFilters/vue', './bundles/AffirmationsBundle/Assets/js/vue/apps/CatalogFilters/index.js')

    .addEntry('js/trade/tester/chart', './bundles/TradeBundle/Assets/js/tester/index.js')
    .addEntry('js/trade/trader/trade/vue', './bundles/TradeBundle/Assets/js/vue/apps/trader/traderTrade.js')
    .addEntry('js/trade/trader/dashboard/vue', './bundles/TradeBundle/Assets/js/vue/apps/trader/dashboard.js')

    /*.addEntry('js/form/select2', './assets/js/formSelect2.js')*/
    //.addEntry('page1', './assets/js/page1.js')
    //.addEntry('page2', './assets/js/page2.js')

    // When enabled, Webpack "splits" your files
    // into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables @babel/preset-env polyfills
    .configureBabel(() => {
    }, {
        useBuiltIns: 'usage',
        corejs: 3
    })

    // enables Sass/SCSS support
    //.enablePostCssLoader()
    .enableSassLoader()
    .enableSassLoader(function (sassOptions) {
    }, {
        resolveUrlLoader: false
    })

    .enableVueLoader()

    /*.configureBabel(function(babelConfig) {
     babelConfig.presets.push('es2017');
     })*/

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes()

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()

// uncomment if you use API Platform Admin (composer req api-admin)
//.enableReactPreset()
//.addEntry('admin', './assets/js/admin.js')

    .configureDefinePlugin(options => {
        const env = require('dotenv').config({path: __dirname + '/.env.js.' + NODE_ENV});

        if (env.error) {
            throw env.error;
        }

        options['process.env'].PARAMS = JSON.stringify(env.parsed);
    })
;


const dotenv = require('dotenv').config({path: __dirname + '/.env.js.' + process.env.NODE_ENV});

module.exports = Encore.getWebpackConfig();;
