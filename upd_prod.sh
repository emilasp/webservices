#!/bin/sh
red=`tput setaf 1`
yellow=`tput setaf 3`
green=`tput setaf 2`
reset=`tput sgr0`

echo "${yellow}Начинаем обновление проекта..${yellow}"

echo "${green}Обновляемся с репозитория${reset}"

git reset --hard
git pull

echo "${green}Очищаем кеш Doctrine${reset}"

./bin/console doctrine:cache:clear-metadata

echo "${green}Обновляем базу до текущих entity${reset}"
./bin/console doctrine:schema:update  --force

echo "${green}Обновляем composer зависимости${reset}"

composer install

echo "${green}Применяем миграции${reset}"

bin/console  doctrine:migrations:migrate --allow-no-migration --dry-run

echo "${green}Обновляем npm зависимости${reset}"

npm install

echo "${green}Обновляем Ассеты ${reset}"

./bin/console assets:install --symlink
yarn encore dev

echo "${green}Выгружаем js-routing${reset}"

./bin/console fos:js-routing:dump --format=json --target=assets/js/fos_js_routes.json
#php -d memory_limit=512M ./bin/console assets:dump --watch --force

echo "${green}Перезапускаем демоны${reset}"

supervisorctl restart webservices: