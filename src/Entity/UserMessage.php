<?php

namespace App\Entity;

use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\BlameableEntityTrait;
use App\Core\Traits\TimestampableEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use FileBundle\Entity\File;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserMessageRepository")
 */
class UserMessage implements IEntityCore
{
    use TimestampableEntityTrait;
    use BlameableEntityTrait;

    public const TYPE_ERROR   = 0;
    public const TYPE_OFFER   = 1;
    public const TYPE_UPGRADE = 2;
    public const TYPE_THANKS  = 3;
    public const TYPE_OTHER   = 7;

    public const TYPES = [
        self::TYPE_THANKS  => 'благодарность',
        self::TYPE_OFFER   => 'сотрудничество',
        self::TYPE_UPGRADE => 'улучшение',
        self::TYPE_ERROR   => 'ошибка',
        self::TYPE_OTHER   => 'другое',
    ];

    public const STATUS_NEW     = 0;
    public const STATUS_READ    = 1;
    public const STATUS_IN_JOB  = 2;
    public const STATUS_DELETED = 3;

    public const STATUSES = [
        self::STATUS_NEW     => 'новое',
        self::STATUS_READ    => 'прочитно',
        self::STATUS_IN_JOB  => 'в работе',
        self::STATUS_DELETED => 'удалено',
    ];


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @ORM\Column(type="json")
     */
    private $env;


    /**
     * @ORM\OneToOne(targetEntity="FileBundle\Entity\File", cascade={"persist"})
     */
    private $image;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(?File $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getEnv(): ?array
    {
        return $this->env;
    }

    public function setEnv(array $env): self
    {
        $this->env = $env;

        return $this;
    }
}
