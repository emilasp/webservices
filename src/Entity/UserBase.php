<?php

namespace App\Entity;

use App\Core\Helpers\StringHelper;
use App\Core\Interfaces\IEntityCore;
use App\Core\Traits\TimestampableEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass
 * @UniqueEntity(fields={"email"}, message="У вас уже есть аккаунт")
 * @ORM\Table(name="users")
 */
abstract class UserBase implements UserInterface, IEntityCore
{
    use TimestampableEntityTrait;

    public const ROLE_GUEST            = 'ROLE_GUEST';
    public const ROLE_USER             = 'ROLE_USER';
    public const ROLE_ADMIN            = 'ROLE_ADMIN';
    public const ROLE_SUPER_ADMIN      = 'ROLE_SUPER_ADMIN';

    public const ROLES = [
        self::ROLE_ADMIN => 'Админ',
        self::ROLE_USER  => 'Пользователь',
    ];


    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=70, nullable=true, options={"default":null})
     */
    private $password = null;

    /**
     * @var string
     *
     */
    private $plainPassword;


    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     */
    private $roles = [];


    /**
     * @var string
     * @ORM\Column(type="string", length=80, nullable=true, options={"default":null})
     */
    private $confirmationCode;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true, options={"default": null})
     */
    private $bannedAt = null;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true, options={"default": null})
     */
    private $lastLoginAt = null;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true, options={"default": null})
     */
    private $lastActivityAt = null;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, options={"default":null})
     */
    private $greeting;

    /**
     * @var string
     * @ORM\Column(type="string", length=30, nullable=true, options={"default":null})
     */
    private $name;


    /**
     * @var string
     * @ORM\Column(type="string", length=30, nullable=true, options={"default":null})
     */
    private $lastname;
    /**
     * @var string
     * @ORM\Column(type="string", length=11, nullable=true, options={"default":null})
     */
    private $phone;


    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->enabled = false;
    }


    /**
     * @return array
     */
    public function getRoles(): array
    {
        $roles   = $this->roles;
        $roles[] = self::ROLE_USER;

        return array_unique($roles);
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->email;
    }

    /**
     *
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }


    public function setRoles($roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getConfirmationCode(): ?string
    {
        return $this->confirmationCode;
    }

    public function setConfirmationCode(?string $confirmationCode): self
    {
        $this->confirmationCode = $confirmationCode;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }


    /**
     * @return string
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     */
    public function setPlainPassword(string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return \DateTime
     */
    public function getBannedAt(): ?\DateTime
    {
        return $this->bannedAt;
    }

    /**
     * @param \DateTime $bannedAt
     */
    public function setBannedAt(\DateTime $bannedAt = null): void
    {
        $this->bannedAt = $bannedAt;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname(string $lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = StringHelper::getPurifiedPhone($phone);
    }

    public function getLastLoginAt(): ?\DateTimeInterface
    {
        return $this->lastLoginAt;
    }

    public function setLastLoginAt(?\DateTimeInterface $lastLoginAt): self
    {
        $this->lastLoginAt = $lastLoginAt;

        return $this;
    }


    /**
     * @return string
     */
    public function getNameFull(): ?string
    {
        return $this->name . ' ' . $this->lastname;
    }

    public function getLastActivityAt(): ?\DateTimeInterface
    {
        return $this->lastActivityAt;
    }

    public function setLastActivityAt(?\DateTimeInterface $lastActivityAt): self
    {
        $this->lastActivityAt = $lastActivityAt;

        return $this;
    }

    public function getGreeting(): ?string
    {
        return $this->greeting;
    }

    public function setGreeting(?string $greeting): self
    {
        $this->greeting = $greeting;

        return $this;
    }
}
