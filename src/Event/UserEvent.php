<?php

declare(strict_types=1);

namespace App\Event;

use CoursesBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserEvent extends Event
{
    public const NAME_REGISTER       = 'user.register.email';
    public const NAME_REGISTER_FINAL = 'user.register.final';
    public const NAME_RECOVER        = 'user.recover';

    /**
     * @var User
     */
    private $registeredUser;

    /**
     * @param User $registeredUser
     */
    public function __construct(User $registeredUser)
    {
        $this->registeredUser = $registeredUser;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->registeredUser;
    }
}