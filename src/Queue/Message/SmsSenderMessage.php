<?php


namespace App\Queue\Message;

use App\Core\Abstracts\AQueueMessage;

/**
 * Отправляем SMS сообщение через очередь
 *
 * Class SmsSenderMessage
 *
 * @package App\Queue\Message
 */
class SmsSenderMessage extends AQueueMessage
{
    /** @var array */
    public $phones;
    /** @var string */
    public $from;
    /** @var string */
    public $message;

    /**
     * EmailSenderMessage constructor.
     *
     * @param array  $phones
     * @param string $from
     * @param string $message
     */
    public function __construct(array $phones, string $message, string $from = null)
    {
        $this->phones  = $phones;
        $this->message = $message;
        $this->from    = $from;
    }
}