<?php


namespace App\Queue\Message;

use App\Core\Abstracts\AQueueMessage;

/**
 * Отправляем сообщение через очередь
 *
 * Class EmailSenderMessage
 *
 * @package App\Queue\Message
 */
class EmailSenderMessage  extends AQueueMessage
{
    /** @var string */
    public $subject;
    /** @var array */
    public $to;
    /** @var string */
    public $from;
    /** @var string */
    public $template;
    /** @var array */
    public $params;

    /**
     * EmailSenderMessage constructor.
     *
     * @param string $subject
     * @param array  $to
     * @param string $from
     * @param string $template
     * @param array  $params
     */
    public function __construct(string $subject, array $to, string $from, string $template, array $params)
    {
        $this->subject  = $subject;
        $this->to       = $to;
        $this->from     = $from;
        $this->template = $template;
        $this->params   = $params;
    }
}