<?php

namespace App\Queue\MessageHandler;

use App\Queue\Message\SmsSenderMessage;
use App\Service\ExternalApi\SmsRuSenderService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Отрпавляем SMS сообщение
 *
 * Class SmsSenderMessageHandler
 *
 * @package App\Queue\MessageHandler
 */
class SmsSenderMessageHandler implements MessageHandlerInterface
{
    /** @var SmsRuSenderService */
    public $smsRuSender;

    /**
     * SmsSenderMessageHandler constructor.
     *
     * @param SmsRuSenderService $smsRuSender
     */
    public function __construct(SmsRuSenderService $smsRuSender)
    {
        $this->smsRuSender = $smsRuSender;
    }

    /**
     * @param SmsSenderMessage $message
     *
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function __invoke(SmsSenderMessage $message)
    {
        $this->sendSms($message);

        echo 'Send message: ' . $message->phones[0] . " - " . $message->message . "\n";
    }

    /**
     * Отправляем SMS пользователю
     *
     * @param SmsSenderMessage $message
     *
     * @return bool
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function sendSms(SmsSenderMessage $message): bool
    {
        return $this->smsRuSender->sendSms($message->phones, $message->message, $message->from);
    }
}
