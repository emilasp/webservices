<?php

namespace App\Queue\MessageHandler;

use App\Queue\Message\EmailSenderMessage;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Twig\Environment;

/**
 * Отрпавляем сообщение
 *
 * Class EmailSenderMessageHandler
 *
 * @package App\Queue\MessageHandler
 */
class EmailSenderMessageHandler implements MessageHandlerInterface
{
    /** @var Environment */
    public $twig;
    /** @var ContainerInterface */
    public $container;

    /**
     * EmailSenderMessageHandler constructor.
     *
     * @param Environment  $twig
     * @param ContainerInterface $container
     */
    public function __construct(Environment $twig, ContainerInterface $container)
    {
        $this->twig      = $twig;
        $this->container = $container;
    }

    /**
     * @param EmailSenderMessage $message
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function __invoke(EmailSenderMessage $message)
    {
        $this->sendEmail($message);

        echo 'Send message: ' . $message->subject . "\n";
    }

    /**
     * Отправляем письмо пользователю
     *
     * @param EmailSenderMessage $message
     *
     * @return int
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendEmail(EmailSenderMessage $message): int
    {
        $messageBody = $this->twig->render($message->template, $message->params);

        $emailMessage = new Swift_Message();
        $emailMessage
            ->setSubject($message->subject)
            ->setFrom($message->from)
            ->setTo($message->to)
            ->setBody($messageBody, 'text/html');


        $mailer = $this->container->get('swiftmailer.mailer.default');
        return $mailer->send($emailMessage);
    }
}
