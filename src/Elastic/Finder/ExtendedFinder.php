<?php
namespace App\Elastic\Finder;

use FOS\ElasticaBundle\Finder\TransformedFinder as BaseFinder;

class ExtendedFinder extends BaseFinder
{
    public function resultSetQuery($query)
    {
        // $query = new \Elastica_Query_QueryString($query);
        return $this->search($query);
    }

    public function findHybrid($query, $limit = null, $options = [])
    {
        $resultSet = $this->search($query, $limit);
        return array($resultSet, $this->transformer->hybridTransform($resultSet));
    }
}