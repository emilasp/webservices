<?php

namespace App\Controller;

use App\Core\Abstracts\AController;
use CoursesBundle\Entity\User;
use App\Form\UserType;
use CoursesBundle\Repository\UserRepository;
use App\Service\EntityService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * UserController *
 * @Route("/user")
 * @Security("is_granted('ROLE_USER')")
 */
class UserController extends AController
{
    /**
     * Список
     *
     * @Route("/", name="users_index", methods={"GET"})
     */
    public function indexAction(UserRepository $userRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $this->addBreadcrumbs(['Главная' => 'index', 'User', "Список"]);

        $queryBuilder = $userRepository->createQueryBuilder('base');
        $pagination = $paginator->paginate($queryBuilder, $request->query->getInt('page', 1), 10);

        return $this->render('user/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }


    /**
    * @Route("/show/{id}", name="user_show", methods={"GET"})
    */
    public function showAction(User $user): Response
    {
        $this->addBreadcrumbs([
            'Главная' => 'index',
            'User' => 'user_index',
            'Просмотр: ' . $user->getId()
        ]);

        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/create", name="user_new", methods={"GET","POST"})
     */
    public function newAction(Request $request, EntityManagerInterface $em): Response
    {
        $this->addBreadcrumbs([
            'Главная' => 'index',
            'User' => 'users_index',
            'Создание'
        ]);

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function editAction(Request $request, User $user, EntityService $es, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder): Response
    {
        $this->addBreadcrumbs([
            'Главная' => 'index',
            'User' => 'users_index',
            'Редактирование: ' . $user->getId() => ['user_show', ['id' => $user->getId()]]
        ]);

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($plainPassword = $user->getPlainPassword()) {
                $user->setPassword($encoder->encodePassword($user, $plainPassword));
            }

            $es->attachUploadedImage($request, $user, 'user', 'avatar');

            $em->flush();

            return $this->redirectToRoute('users_index', [
                'id' => $user->getId(),
            ]);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, User $user, EntityManagerInterface $em): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}
