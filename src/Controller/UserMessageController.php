<?php

namespace App\Controller;

use App\Core\Abstracts\AController;
use CoursesBundle\Entity\User;
use App\Entity\UserMessage;
use App\Form\UserMessageType;
use App\Service\EntityService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * UserMessageController
 * @Route("/user")
 */
class UserMessageController extends AController
{
    /**
     * @Route("/message", name="user_message")
     * @Route("/message/{type}", name="user_message_type")
     */
    public function showAction(Request $request, EntityService $es, int $type = UserMessage::TYPE_UPGRADE)
    {
        $message = new UserMessage();
        $message->setType($type);
        $message->setStatus(UserMessage::STATUS_NEW);


        $form = $this->createForm(UserMessageType::class, $message);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $message = $form->getData();
            $message->setEnv($_SERVER);

            $this->getEm()->persist($message);
            $this->getEm()->flush($message);

            $es->attachUploadedImage($request, $message, 'userMessage', 'image');

            $this->getEm()->flush();

            $this->addFlash('success', 'Сообщение успешно отправлено');

            return $this->redirectToRoute('index');
        }

        return $this->render('user/send.admin.message.html.twig', [
            'form'       => $form->createView(),
            'message'    => $message,
        ]);
    }

}
