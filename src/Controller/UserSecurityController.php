<?php

declare(strict_types=1);

namespace App\Controller;

use App\Core\Abstracts\AController;
use CoursesBundle\Entity\User;
use App\Event\UserEvent;
use App\Form\UserRegisterAdditionalType;
use App\Form\UserRegisterType;
use CoursesBundle\Repository\UserRepository;
use App\Service\CodeGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Регистрация нового пользователя
 *
 * Class UserRegisterController
 *
 * @package App\Controller
 */
class UserSecurityController extends AController
{
    /**
     * Авторизация пользователя
     *
     * @Route("/login", name="login")
     */
    public function loginAction(AuthenticationUtils $authenticationUtils)
    {
        $lastUsername = $authenticationUtils->getLastUsername();
        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error
        ]);
    }

    /**
     * Регистрируем почту пользователя
     *
     * @Route("/register", name="register")
     */
    public function registerAction(
        Request $request,
        EventDispatcherInterface $eventDispatcher,
        CodeGenerator $codeGenerator
    ) {
        $user = new User();
        $form = $this->createForm(UserRegisterType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user->setConfirmationCode($codeGenerator->getConfirmationCode());

            $this->getEm()->persist($user);
            $this->getEm()->flush();

            $eventDispatcher->dispatch(UserEvent::NAME_REGISTER, new UserEvent($user));

            $this->addFlash('success', 'Проверьте Вашу почту :)');

            return $this->render('security/register.email.sended.html.twig', [
                'form' => $form->createView(),
                'user' => $user,
            ]);
        }

        return $this->render('security/register.email.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * После подтверждения почты заполняем обязательные данные и авторизуем пользователя
     *
     * @Route("/confirm/{code}", name="email_confirmation")
     */
    public function confirmEmailAction(
        Request $request,
        UserRepository $userRepository,
        EventDispatcherInterface $eventDispatcher,
        UserPasswordEncoderInterface $encoder,
        string $code
    ) {
        /** @var User $user */
        if (!$code || !$user = $userRepository->findOneBy(['confirmationCode' => $code])) {
            return new Response('404');
        }

        $form = $this->createForm(UserRegisterAdditionalType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $user->setPassword($encoder->encodePassword($user, $user->getPlainPassword()));
            $user->setEnabled(true);
            $user->setConfirmationCode('');

            $this->getEm()->flush();

            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->container->get('security.token_storage')->setToken($token);
            $this->container->get('session')->set('_security_main', serialize($token));

            $eventDispatcher->dispatch(UserEvent::NAME_REGISTER_FINAL, new UserEvent($user));

            $this->addFlash('success', 'Поздравляем! Вы успешно зарегистрировались в нашем университете!');

            return $this->redirectToRoute('index');
        }

        $this->addFlash('success', 'Остался последний шаг - задайте Имя и пароль <i class="em em-smile"></i>');

        return $this->render('security/register.additional.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }


    /**
     * Восстановление доступа
     * @Route("/recover", name="user_recover")
     */
    public function recoverAction(
        Request $request,
        EventDispatcherInterface $eventDispatcher,
        CodeGenerator $codeGenerator
    ) {
        $user = new User();
        $form = $this->createForm(UserRegisterType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $user = $form->getData();

            /** @var User $user */
            $userIsset = $this->getEm()->getRepository(User::class)->findOneBy(['email' => $user->getEmail()]);

            if ($userIsset) {
                $userIsset->setConfirmationCode($codeGenerator->getConfirmationCode());

                $this->getEm()->flush();

                $eventDispatcher->dispatch(UserEvent::NAME_RECOVER, new UserEvent($userIsset));

                $this->addFlash('success', "Письмо для восстанвления доступа выслано на вашу почту: <b>{$user->getEmail()}</b>");

                return $this->redirectToRoute('index');
            } else {
                $this->addFlash('danger', "Пользователь <b>{$user->getEmail()}</b> не найден");
            }
        }

        return $this->render('security/recover.email.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * После перехода по ссылке на почте - авторизуем и редиректим на профиль
     *
     * @Route("/recovery/{code}", name="email_recovery")
     */
    public function confirmEmailRecoveryAction(UserRepository $userRepository, string $code)
    {
        /** @var User $user */
        if (!$code || !$user = $userRepository->findOneBy(['confirmationCode' => $code])) {
            return new Response('404');
        }

        $user->setEnabled(true);
        $user->setConfirmationCode('');

        $this->getEm()->flush();

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->container->get('security.token_storage')->setToken($token);
        $this->container->get('session')->set('_security_main', serialize($token));

        $this->addFlash('success', 'Поздравляем с успешным входом! :)');

        return $this->redirectToRoute('user_profile');
    }


    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
    }
}