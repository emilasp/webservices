<?php

declare(strict_types=1);

namespace App\Controller;

use App\Core\Abstracts\AController;
use CoursesBundle\Entity\User;
use App\Form\UserProfileType;;

use App\Service\EntityService;
use FileBundle\Service\FileService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Профиль пользователя
 *
 * Class UserProfileController
 *
 * @Route("/user")
 *
 * @package App\Controller
 */
class UserProfileController extends AController
{
    /**
     * Профиль пользователя
     *
     * @Route("/profile", name="user_profile")
     */
    public function profile(Request $request, EntityService $es, UserPasswordEncoderInterface $encoder)
    {
        /** @var User $user */
        $user   = $this->getUser();

        $form = $this->createForm(UserProfileType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();

            if ($plainPassword = $user->getPlainPassword()) {
                $user->setPassword($encoder->encodePassword($user, $plainPassword));
            }

            $es->attachUploadedImage($request, $user, 'user_profile', 'avatar');

            $this->getEm()->flush();

            $this->addFlash('success', 'Профиль успешно обновлен');

            $form = $this->createForm(UserProfileType::class, $user);
        }

        return $this->render('security/profile.html.twig', [
            'form'   => $form->createView(),
            'user'   => $user,
        ]);
    }
}