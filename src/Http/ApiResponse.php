<?php

declare(strict_types=1);

namespace App\Http;

use Symfony\Component\HttpFoundation\JsonResponse;

class ApiResponse extends JsonResponse
{
    /**
     * ApiResponse constructor.
     *
     * @param bool  $isSuccess
     * @param string $message
     * @param mixed  $data
     * @param array  $errors
     * @param int    $status
     * @param array  $headers
     * @param bool   $json
     */
    public function __construct(bool $isSuccess, string $message, $data = null, array $errors = [], int $status = 200, array $headers = [], bool $json = false)
    {
        parent::__construct($this->format($isSuccess, $message, $data, $errors), $status, $headers, $json);
    }

    /**
     * Format the API response.
     *
     * @param bool $isSuccess
     * @param string $message
     * @param mixed  $data
     * @param array  $errors
     *
     * @return array
     */
    private function format(bool $isSuccess, string $message, $data = null, array $errors = [])
    {
        $this->encodingOptions = JSON_UNESCAPED_UNICODE;

        if ($data === null) {
            $data = new \ArrayObject();
        }

        $response = [
            'status'  => $isSuccess,
            'message' => $message,
            'data'    => $data,
        ];

        if ($errors) {
            $response['errors'] = $errors;
        }

        return $response;
    }
}