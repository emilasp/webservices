{% extends 'base.html.twig' %}

{% block title %} <?= $title ?> {% endblock %}
{% block h1 %} <?= $title ?> <small>  <?= $title ?></small> {% endblock %}


{% block body %}

    This friendly message is coming from:
    <ul>
        <li>Your controller at <code><?= $helper->getFileLink("$root_directory/$controller_path", "$controller_path"); ?></code></li>
        <li>Your template at <code><?= $helper->getFileLink("$root_directory/$relative_path", "$relative_path"); ?></code></li>
    </ul>

{% endblock %}

{% block sidebar %}
<div class="col-md-3 order-2" id="sticky-sidebar">
    <div class="sticky-top">
        SIDEBAR
    </div>
</div>
{% endblock %}