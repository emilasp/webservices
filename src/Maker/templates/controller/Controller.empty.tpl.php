<?= "<?php\n" ?>

namespace <?= $namespace; ?>;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Core\Abstracts\AController;
use Symfony\Component\HttpFoundation\Request;

/**
* Class <?= $class_name; ?>
*
* @Route("<?= $route_path ?>")
*
* @package <?= $namespace; ?>
*/
class <?= $class_name; ?>  extends AController<?= "\n" ?>
{
    /**
    * @Route("/index", name="<?= $route_name ?>")
    *
    * @param Request $request
    *
    * @Template(template="/<?= $name ?>/index.html.twig")
    * @return array
    */
    public function index()
    {
<?php if ($with_template) { ?>
        return[
            'controller_name' => '<?= $class_name ?>',
        ];
<?php } else { ?>
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => '<?= $relative_path; ?>',
        ]);
<?php } ?>
    }
}
