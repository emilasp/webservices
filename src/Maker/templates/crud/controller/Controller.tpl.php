<?= "<?php\n" ?>

namespace <?= $namespace ?>;

use App\Core\Abstracts\AController;
use <?= $entity_full_class_name ?>;
use <?= $form_full_class_name ?>;
<?php if (isset($repository_full_class_name)): ?>
use <?= $repository_full_class_name ?>;
<?php endif ?>
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * <?= $class_name ?>
 *
 * @Route("<?= $route_path ?>")
 * @Security("is_granted('ROLE_USER')")
 */
class <?= $class_name ?> extends AController
{
    /**
     * Список
     *
     * @Route("/", name="<?= $route_name ?>_index", methods={"GET"})
     */
<?php if (isset($repository_full_class_name)): ?>
    public function indexAction(<?= $repository_class_name ?> $<?= $repository_var ?>, Request $request, PaginatorInterface $paginator): Response
    {
        $this->addBreadcrumbs(['Главная' => 'index', '<?= $entity_class_name ?>', "Список"]);

        $queryBuilder = $<?= $repository_var ?>->createQueryBuilder('base');
        $pagination = $paginator->paginate($queryBuilder, $request->query->getInt('page', 1), 10);

        return $this->render('<?= $templates_path ?>/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }
<?php else: ?>
    public function indexAction(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator): Response
    {
        $this->addBreadcrumbs([
            'Главная' => 'index',
            '<?= $entity_class_name ?>' => '<?= $route_name ?>_index',
            'Список'
        ]);

        $queryBuilder = $em->getRepository(<?= $entity_class_name ?>::class)->createQueryBuilder('base');
        $pagination = $paginator->paginate($queryBuilder, $request->query->getInt('page', 1), 10);

        return $this->render('<?= $templates_path ?>/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }
<?php endif ?>


    /**
    * @Route("/show/{<?= $entity_identifier ?>}", name="<?= $route_name ?>_show", methods={"GET"})
    */
    public function showAction(<?= $entity_class_name ?> $<?= $entity_var_singular ?>): Response
    {
        $this->addBreadcrumbs([
            'Главная' => 'index',
            '<?= $entity_class_name ?>' => '<?= $route_name ?>_index',
            'Просмотр: ' . $<?= $entity_var_singular ?>->getId()
        ]);

        return $this->render('<?= $templates_path ?>/show.html.twig', [
            '<?= $entity_twig_var_singular ?>' => $<?= $entity_var_singular ?>,
        ]);
    }

    /**
     * @Route("/create", name="<?= $route_name ?>_new", methods={"GET","POST"})
     */
    public function newAction(Request $request, EntityManagerInterface $em): Response
    {
        $this->addBreadcrumbs([
            'Главная' => 'index',
            '<?= $entity_class_name ?>' => '<?= $route_name ?>_index',
            'Создание'
        ]);

        $<?= $entity_var_singular ?> = new <?= $entity_class_name ?>();
        $form = $this->createForm(<?= $form_class_name ?>::class, $<?= $entity_var_singular ?>);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($<?= $entity_var_singular ?>);
            $em->flush();

            return $this->redirectToRoute('<?= $route_name ?>_index');
        }

        return $this->render('<?= $templates_path ?>/new.html.twig', [
            '<?= $entity_twig_var_singular ?>' => $<?= $entity_var_singular ?>,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{<?= $entity_identifier ?>}/edit", name="<?= $route_name ?>_edit", methods={"GET","POST"})
     */
    public function editAction(Request $request, <?= $entity_class_name ?> $<?= $entity_var_singular ?>, EntityManagerInterface $em): Response
    {
        $this->addBreadcrumbs([
            'Главная' => 'index',
            '<?= $entity_class_name ?>' => '<?= $route_name ?>_index',
            'Редактирование: ' . $<?= $entity_var_singular ?>->getId() => ['<?= $route_name ?>_show', ['id' => $<?= $entity_var_singular ?>->getId()]]
        ]);

        $form = $this->createForm(<?= $form_class_name ?>::class, $<?= $entity_var_singular ?>);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            return $this->redirectToRoute('<?= $route_name ?>_index', [
                '<?= $entity_identifier ?>' => $<?= $entity_var_singular ?>->get<?= ucfirst($entity_identifier) ?>(),
            ]);
        }

        return $this->render('<?= $templates_path ?>/edit.html.twig', [
            '<?= $entity_twig_var_singular ?>' => $<?= $entity_var_singular ?>,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{<?= $entity_identifier ?>}", name="<?= $route_name ?>_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, <?= $entity_class_name ?> $<?= $entity_var_singular ?>, EntityManagerInterface $em): Response
    {
        if ($this->isCsrfTokenValid('delete'.$<?= $entity_var_singular ?>->get<?= ucfirst($entity_identifier) ?>(), $request->request->get('_token'))) {
            $em->remove($<?= $entity_var_singular ?>);
            $em->flush();
        }

        return $this->redirectToRoute('<?= $route_name ?>_index');
    }
}
