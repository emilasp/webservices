<?= $helper->getHeadPrintCode('New '.$entity_class_name) ?>

{% block title %} Create new <?= $entity_class_name ?> {% endblock %}
{% block h1 %} Create new <?= $entity_class_name ?> {% endblock %}

{% block body %}

    <div class="bg-white p-3">
        {{ include('<?= $route_name ?>/_form.html.twig') }}
    </div>

{% endblock %}
