{{ form_start(form) }}

    {{ form_widget(form) }}

    <div class="clearfix">
        <div class="float-left">
            <a href="index" class="btn btn-info"><i class="fa fa-list"></i> К списку</a>
        </div>
        <div class="float-right">
            <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Сохранить</button>
        </div>
    </div>

{{ form_end(form) }}
