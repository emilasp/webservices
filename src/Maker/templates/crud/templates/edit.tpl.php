<?= $helper->getHeadPrintCode('Edit '.$entity_class_name) ?>

{% block title %} Редактирование <?= $entity_class_name ?> {% endblock %}
{% block h1 %} Редактирование <?= $entity_class_name ?> <?= $entity_class_name ?> {% endblock %}

{% block body %}

    <div class="bg-white p-3">
        {{ include('<?= $route_name ?>/_form.html.twig', {'button_label': 'Update'}) }}
    </div>

{% endblock %}
