<?= $helper->getHeadPrintCode($entity_class_name.' index'); ?>


{% block title %} <?= $entity_class_name ?>: список {% endblock %}
{% block h1 %} <?= $entity_class_name ?>: список {% endblock %}

{% block body %}

<div class="clearfix mb-1">
    <div class="float-left"><b>Всего записей:</b> {{ pagination.getTotalItemCount }}</div>
    <a href="{{ path('<?= $route_name ?>_new') }}" class="btn btn-success float-right"><i class="fa fa-plus"></i> Создать</a>
</div>

<!--{{ knp_pagination_filter(pagination, {'base.name': 'Name'}) }}-->
<div class="overflow-auto">
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <?php foreach ($entity_fields as $field): ?>
                <th{% if pagination.isSorted('base.<?= $field['fieldName'] ?>') %} class="sorted"{% endif %}>
                {{ knp_pagination_sortable(pagination, '<?= ucfirst($field['fieldName']) ?>', 'base.<?= $field['fieldName'] ?>') }}
                </th>
            <?php endforeach; ?>

            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        {% for <?= $entity_twig_var_singular ?> in pagination %}
        <tr>
            <?php foreach ($entity_fields as $field): ?>
                <td>{{ <?= $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> }}</td>
            <?php endforeach; ?>
            <td>
                <a href="{{ path('<?= $route_name ?>_show', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}"><i class="fa fa-eye"></i> </a>
                <a href="{{ path('<?= $route_name ?>_edit', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}"><i class="fa fa-edit"></i> </a>
                <a href="{{ path('<?= $route_name ?>_delete', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}"><i class="fa fa-trash text-danger"></i> </a>
            </td>
        </tr>
        {% else %}
        <tr>
            <td colspan="<?= (count($entity_fields) + 1) ?>">no records found</td>
        </tr>
        {% endfor %}
        </tbody>
    </table>
</div>

<div class="navigation">
    {{ knp_pagination_render(pagination) }}
</div>
{% endblock %}

