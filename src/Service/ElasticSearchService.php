<?php

declare(strict_types=1);

namespace App\Service;

use App\Core\Helpers\StringHelper;
use App\Core\Interfaces\IEntityCore;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Elastica\Result;
use FileBundle\Service\FileService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class ElasticSearchService
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Получаем мета по поисковому запросу
     *
     * @param string $text
     * @param string $index
     * @param array  $types
     * @param array  $fields
     * @param float  $minScore
     *
     * @return array
     */
    public function getSearchMeta(string $text, string $index, array $types, array $fields, float $minScore = 0)
    {
        $elasticaClient = $this->container->get('fos_elastica.client');
        $index          = $elasticaClient->getIndex($index);

        $fieldQuery = new \Elastica\Query\Match();
        foreach ($fields as $field => $params) {
            $fieldQuery->setFieldQuery($field, $text);
            foreach ($params as $param => $paramValue) {
                $fieldQuery->setFieldParam($field, $param, $paramValue);
            }
        }


        $boolQuery = new \Elastica\Query\BoolQuery();
        $boolQuery->addShould($fieldQuery);

        $search = $index->createSearch();
        foreach ($types as $type) {
            $search->addType($type);
        }
        $resultSet = $search->search($boolQuery);

        $data = [];
        /** @var Result $result */
        foreach ($resultSet->getResults() as $result) {
            if ($result->getScore() > $minScore) {
                $data[] = [
                    'id'    => $result->getData()['id'],
                    'score' => $result->getScore(),
                    'hint'  => $result->getHit(),
                    'item'  => $result->getData(),
                ];
            }
        }

        return $data;
    }
}