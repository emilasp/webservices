<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\UserBase;
use App\Queue\Message\SmsSenderMessage;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class SmsRuSender
 *
 * @package App\Service
 */
class SmsSenderQueue
{
    /** @var MessageBusInterface */
    private $bus;

    /**
     * Mailer constructor.
     *
     * @param MessageBusInterface $bus
     */
    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }


    /**
     * Отправка сообщения SMS
     *
     * @param UserBase    $user
     * @param string      $message
     * @param string|null $from
     */
    public function sendSms(UserBase $user, string $message, string $from = null)
    {
        $phone = '9261028050';
        $user->setPhone($phone);
        if ($user->getPhone()) {
            $this->bus->dispatch(
                new SmsSenderMessage([$user->getPhone()], $message, $from)
            );
        }
    }
}