<?php

declare(strict_types=1);

namespace App\Service;

use App\Core\Helpers\StringHelper;
use App\Core\Interfaces\IEntityCore;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FileBundle\Service\FileService;
use Symfony\Component\HttpFoundation\Request;

class EntityService
{
    /** @var FileService  */
    private $fileService;
    /** @var EntityManagerInterface  */
    private $em;

    public function __construct(FileService $fileService, EntityManagerInterface $entityManager)
    {
        $this->fileService = $fileService;
        $this->em = $entityManager;
    }

    /**
     * Обновляем связи
     *
     * @param IEntityCore            $subject
     * @param string                 $attribute
     * @param string                 $attributeRelation
     * @param EntityManagerInterface $em
     */
    public function setRelationOneToMany(IEntityCore $subject, string $attribute, string $attributeRelation, ArrayCollection $originals)
    {
        $methodManyGet    = 'get' . ucfirst($attributeRelation);
        $methodManyRemove = 'remove' . substr(ucfirst($attributeRelation),0,-1);
        $methodInverseSet = 'set' . ucfirst($attribute);

        // удалить отошения
        foreach ($originals as $section) {
            if (false === $subject->{$methodManyGet}()->contains($section)) {
                $subject->{$methodManyRemove}($section);
                $this->em->remove($section);
            }
        }
        // Добавить отношения
        foreach ($subject->{$methodManyGet}() as $section) {
            if (!$section->getId()) {
                $section->{$methodInverseSet}($subject);
            }
        }
    }

    /**
     * Получаем оригинальную коллекцию
     *
     * @param IEntityCore $subject
     * @param string      $attributeRelation
     *
     * @return ArrayCollection
     */
    public function getOriginalRelationCollection(IEntityCore $subject, string $attributeRelation): ArrayCollection
    {
        $originals = new ArrayCollection();

        $methos = 'get' . ucfirst($attributeRelation);
        foreach ($subject->{$methos}() as $section) {
            $originals->add($section);
        }
        return $originals;
    }

    /**
     * Загружаем изображения для Entity
     *
     * @param Request     $request
     * @param IEntityCore $subject
     * @param string      $attributeUpload
     * @param string      $attribute
     */
    public function attachUploadedImage(Request $request, IEntityCore $subject, string $attributeUpload, string $attribute)
    {
        if ($files = $request->files->get(StringHelper::deCamelCase($attributeUpload))) {
            $methodSet = 'set' . ucfirst($attribute);

            if ($file = $files[$attribute]) {
                $image = $this->fileService->createFile($file, $subject);
                $subject->{$methodSet}($image);
            }
        }
    }

    /**
     * Загружаем изображения для свзяанных Entity
     *
     * @param Request     $request
     * @param IEntityCore $subject
     * @param string      $attributeUpload
     * @param string      $attributeRel
     * @param string      $attributeRelFile
     */
    public function attachUploadedRelationImages(Request $request, IEntityCore $subject, string $attributeUpload, string $attributeRel, string $attributeRelFile)
    {
        if ($files = $request->files->get(StringHelper::deCamelCase($attributeUpload))) {
            $methodRelSet = 'set' . ucfirst($attributeRelFile);
            $methodRelGet = 'get' . ucfirst($attributeRel);
            foreach ($subject->{$methodRelGet}() as $index => $section) {
                if ($file = $files[$attributeRel][$index][$attributeRelFile]) {
                    $image = $this->fileService->createFile($file, $section);
                    $section->$methodRelSet($image);
                }
            }
        }
    }
}