<?php

declare(strict_types=1);

namespace App\Service;

use CoursesBundle\Entity\User;
use App\Queue\Message\EmailSenderMessage;
use Symfony\Component\Messenger\MessageBusInterface;
use Twig\Environment;

/**
 * Отправка почты
 *
 * Class MailSenderQueue
 *
 * @package App\Service
 */
class MailSenderQueue
{
    public const FROM_ADDRESS = 'law@pravo-zakonno.ru';

    /** @var Environment */
    private $twig;
    /** @var MessageBusInterface */
    private $bus;

    /**
     * Mailer constructor.
     *
     * @param Environment         $twig
     * @param MessageBusInterface $bus
     */
    public function __construct(Environment $twig, MessageBusInterface $bus)
    {
        $this->twig = $twig;
        $this->bus  = $bus;
    }

    /**
     * Отправляем письмо пользователю
     *
     * @param string $subject
     * @param User   $user
     * @param string $template
     * @param array  $params
     */
    public function sendEmailToUser(string $subject, User $user, string $template, array $params)
    {
        $this->bus->dispatch(
            new EmailSenderMessage(
                $subject,
                [$user->getEmail()],
                self::FROM_ADDRESS,
                $template,
                $params
            )
        );
    }
}