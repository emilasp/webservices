<?php

declare(strict_types=1);

namespace App\Service\ExternalApi;

use App\Core\Abstracts\AExternalApiService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class SmsRuSender
 *
 * @package App\Service
 */
class SmsRuSenderService extends AExternalApiService
{
    protected const URL_BASE        = 'https://sms.ru';
    private const   ACTION_SEND_SMS = 'sms/send';

    /** @var MessageBusInterface */
    private $bus;
    /** @var ContainerInterface */
    private $container;

    /**
     * Mailer constructor.
     *
     * @param MessageBusInterface $bus
     */
    public function __construct(MessageBusInterface $bus, ContainerInterface $container)
    {
        parent::__construct();

        $this->bus       = $bus;
        $this->container = $container;
    }


    /**
     * Отправка сообщения SMS
     *
     * @param array       $phones
     * @param string      $message
     * @param string|null $from
     *
     * @return bool
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function sendSms(array $phones, string $message, string $from = null): bool
    {
        $apiId = $this->container->getParameter('smsru.api_id');

        $url = $this->getHttpMethodUrl(self::ACTION_SEND_SMS, [
            'api_id' => $apiId,
            'to'     => implode(',', $phones),
            'msg'    => $message,
            'from'   => $from,
            'json'   => 1,
        ]);

        $response   = $this->httpClient->request('GET', $url);
        $content    = $response->toArray();
        $statusCode = $content['status_code'] ?? null;

        return $statusCode === 100 ? true : false;
    }
}