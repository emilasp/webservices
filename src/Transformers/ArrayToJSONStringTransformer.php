<?php


namespace App\Transformers;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class ArrayToJSONStringTransformer
 *
 * @package App\Transformers
 */
class ArrayToJSONStringTransformer implements DataTransformerInterface
{
    /**
     * Transform an array to a JSON string
     */
    public function transform($array):string
    {
        return $array ? json_encode($array) : '';
    }

    /**
     * Transform a JSON string to an array
     */
    public function reverseTransform($string):array
    {
        return $string ? json_decode($string, true) : [];
    }
}