<?php

namespace App\Menu;

use CoursesBundle\Entity\User;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Builder
 *
 * @package App\Menu
 */
class Builder
{
    /** @var FactoryInterface */
    private $factory;
    /** @var array */
    private $menuConfigurate;

    /** @var RequestStack */
    private $requestStack;
    /** @var Security  */
    private $security;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, Security $security)
    {
        $this->factory         = $factory;
        $this->security        = $security;
        $this->menuConfigurate = $this->loadMenuConfig();
    }

    /**
     * Верхнее главное меню - вывод слева
     *
     * @param RequestStack $requestStack
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function secondMenu(RequestStack $requestStack)
    {
        return $this->renderMenu('secondMenu', $requestStack);
    }


    /**
     * Верхнее главное меню - вывод слева
     *
     * @param RequestStack $requestStack
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function mainMenuLeft(RequestStack $requestStack)
    {
        return $this->renderMenu('mainMenuLeft', $requestStack);
    }


    /**
     * Верхнее главное меню - вывод  справа
     *
     * @param RequestStack $requestStack
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function mainTopRight(RequestStack $requestStack)
    {
        return $this->renderMenu('mainTopRight', $requestStack);
    }

    /**
     * Формируем item
     *
     * @param ItemInterface $menuItem
     * @param array         $menuConf
     * @param bool          $isDropDown
     *
     * @return ItemInterface
     */
    private function renderItems(ItemInterface $menuItem, array $menuConf, bool $isDropDown = false):? ItemInterface
    {
        if ($isDropDown) {
            $menuItem
                ->setChildrenAttribute('class', 'dropdown-menu dropdown-menu-right rounded-0')
                ->setChildrenAttribute('role', 'menu');
        } else {
            $menuItem->setChildrenAttribute('class', $menuConf['class'] ?? 'navbar-nav mr-auto');
        }

        foreach ($menuConf['items'] as $item) {
            if (isset($item['roles']) && !$this->checkGranted($item['roles'])) {
                continue;
            }

            $params = array_merge(['extras' => ['safe_label' => true]], $item);

            $name = $this->getName($item);

            $child = $menuItem->addChild($name, $params);

            $classLink = 'nav-link';
            if (isset($item['route']) && $this->requestStack->getCurrentRequest()->get('_route') === $item['route']) {
                $classLink .= ' active';
            }
            if (isset($item['classItem'])) {
                $classLink .= ' ' . $item['classItem'];
            }

            $child->setLinkAttribute('class', $classLink)->setAttribute('class', 'nav-item' );

            if (isset($item['items'])) {
                $child->setAttribute('class', 'dropdown')
                    ->setLinkAttribute('data-toggle', 'dropdown')
                    ->setLinkAttribute('class', 'nav-link dropdown-toggle');

                $child = $this->renderItems($child, $item, true);
            }

            if ($isDropDown) {
                $child->setLinkAttribute('class', 'dropdown-item')->setAttribute('class', 'nav-item');
            }
        }

        return $menuItem;
    }

    /**
     * Формируем наименование item
     *
     * @param array $itemConf
     *
     * @return string
     */
    private function getName(array $itemConf): string
    {
        $name = '';

        if (!empty($itemConf['name_call'])) {
            if (method_exists($this, $itemConf['name_call'])) {
                return $this->{$itemConf['name_call']}();
            }
        }
        if (!empty($itemConf['raw'])) {
            $name .= $itemConf['raw'];
        }

        if (!empty($itemConf['icon'])) {
            $name .= "<i class=\"fa fa-{$itemConf['icon']}\"></i> ";
        }

        if (isset($itemConf['callback']) && method_exists($this, $itemConf['callback'])) {
            $name .= $this->{$itemConf['callback']}();
        }

        if (isset($itemConf['name'])) {
            $name .= $itemConf['name'];
        }

        return $name;
    }

    /**
     * Формируем меню из конфига
     *
     * @param string       $menuName
     * @param RequestStack $requestStack
     *
     * @return ItemInterface
     */
    private function renderMenu(string $menuName, RequestStack $requestStack):? ItemInterface
    {
        $this->requestStack = $requestStack;

        $menuConf = $this->menuConfigurate['menus'][$menuName];
        $rootItem = $this->factory->createItem('root');

        return $this->renderItems($rootItem, $menuConf);
    }

    /**
     * Загружаем конфиг
     *
     * @return mixed
     */
    private function loadMenuConfig()
    {
        return Yaml::parse(file_get_contents(__DIR__ . '/../../config/menus.yaml'));
    }


    /**
     * Проверяем есть ли права у текущего пользователя на пункт меню
     *
     * @param array $roles
     *
     * @return bool
     */
    private function checkGranted(array $roles): bool
    {
        if (!$this->security->getUser() && array_search('guest', $roles) !== false) {
            return true;
        }
        return $this->security->getToken() && $this->security->isGranted($roles);
    }

    /**
     * Получаем имя ghjabkz
     *
     * @return string
     */
    private function getProfileName(): string
    {
        /** @var User $user */
        $user = $this->security->getUser();

        $name = $user->getEmail();

        return $name;
    }
}