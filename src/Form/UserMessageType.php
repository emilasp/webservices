<?php

namespace App\Form;

use App\Entity\UserMessage;
use App\Form\base\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserMessageType
 *
 * @package App\Form
 */
class UserMessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();

        $builder
            //->add('email', EmailType::class, ['label' => 'Email', 'attr' => ['disabled' => 'disabled']])
            ->add('type', ChoiceType::class, ['label' => 'Тип обращения', 'choices' => array_flip(UserMessage::TYPES)])
            ->add('message', TextareaType::class, ['label' => 'Сообщение'])
            ->add('image', ImageType::class, [
                'label'    => 'Изображение',
                'mapped'   => false,
                'required' => false,
                'width'    => '150px',
                'file'     => $entity->getImage(),
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserMessage::class,
        ]);
    }
}
