<?php

namespace App\Form;

use CoursesBundle\Entity\User;
use App\Form\base\ImageType;
use IT\InputMaskBundle\Form\Type\TextMaskType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

/**
 * Class UserProfileType
 *
 * @package App\Form
 */
class UserProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();

        $builder
            //->add('email', EmailType::class, ['label' => 'Email', 'attr' => ['disabled' => 'disabled']])
            ->add('plainPassword', PasswordType::class, ['label' => 'Пароль', 'required' => false])
            ->add('name', TextType::class, ['label' => 'Имя'])
            ->add('lastname', TextType::class, ['label' => 'Фамилия'])
            ->add('phone', TextMaskType::class, array(
                'label' => 'Телефон',
                'mask'  => '+7(999)999-99-99'
            ))
            ->add('avatar', ImageType::class, [
                'label'       => 'Аватар',
                'mapped'      => false,
                'required'    => false,
                'width'       => '150px',
                'file'        => $entity->getAvatar(),
                'constraints' => [
                    new Image([
                        'maxSize' => '5M'
                    ])
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
