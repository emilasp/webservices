<?php

namespace App\Form;

use CoursesBundle\Entity\User;
use App\Form\base\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();

        $builder
            ->add('plainPassword', PasswordType::class, ['label' => 'Пароль', 'required' => false])
            ->add('name', TextType::class, ['label' => 'Имя'])
            ->add('lastname', TextType::class, ['label' => 'Фамилия'])
            ->add('phone', TextType::class, ['label' => 'Телефон'])
            ->add('avatar', ImageType::class, [
                'label'    => 'Аватар',
                'mapped'   => false,
                'required' => false,
                'width'    => '150px',
                'file'     => $entity->getAvatar(),
            ])
            ->add('email', EmailType::class, ['label' => 'Email', 'required' => false])
            ->add('confirmationCode')
            ->add('enabled', CheckboxType::class, ['label' => 'Активен', 'required' => false])
            ->add('roles', ChoiceType::class, [
                'choices'  => array_flip(User::ROLES),
                'multiple' => true,
                'expanded' => true,
                'label'    => 'Роли',
            ])
            ->add('bannedAt');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
