<?php

namespace App\Twig;

use App\Core\Abstracts\AWidget;
use App\Core\Interfaces\IEntityCore;
use FileBundle\Entity\File;
use FileBundle\Service\FileService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class FileExtension
 *
 * @package FileBundle\Twig
 */
class CoreExtension extends AbstractExtension
{
    private $containerAware;

    /**
     * FileExtension constructor.
     *
     * @param FileService $fileService
     */
    public function __construct(ContainerInterface $containerAware)
    {
        $this->containerAware = $containerAware;
    }

    /**
     * @return array
     */
    /*public function getFilters(): array
    {
        return [
            new TwigFilter('get_image_url', [$this, 'getImageUrl']),
        ];
    }*/

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('className', [$this, 'getEntityClass']),
        ];
    }

    /**
     * Поулчаем имя класса
     *
     * @param IEntityCore $entity
     *
     * @return string
     * @throws \ReflectionException
     */
    public function getEntityClass(IEntityCore $entity)
    {
        return (new \ReflectionClass($entity))->getName();
    }
}
