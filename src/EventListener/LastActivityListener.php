<?php

namespace App\EventListener;

use App\Entity\UserBase;
use CoursesBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Отслеживаем активность пользователя
 *
 * Class LastActivityListener
 *
 * @package App\EventListener
 */
class LastActivityListener implements EventSubscriberInterface
{
    public const CACHE_USER_ACTIVITIES = 'user.activity';
    public const CACHE_COUNT_SAVE      = 10;

    /** @var TokenStorageInterface */
    private $tokenStorage;
    /** @var CacheItemPoolInterface */
    private $cache;
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(TokenStorageInterface $tokenStorage, CacheItemPoolInterface $cache, EntityManagerInterface $em)
    {
        $this->tokenStorage = $tokenStorage;
        $this->cache        = $cache;
        $this->em           = $em;
    }

    public function onResponse(ResponseEvent $event)
    {
        $token = $this->tokenStorage->getToken();

        if ($token && $token->isAuthenticated()) {
            $user = $token->getUser();

            if ($user instanceof UserBase) {
                $activityCachePool = $this->cache->getItem(self::CACHE_USER_ACTIVITIES);
                $lastActivities    = $activityCachePool->get() ?: ['count' => 0, 'users' => []];

                if ($lastActivities['count'] >= self::CACHE_COUNT_SAVE) {
                    $this->saveUserActivities($lastActivities['users']);
                    $lastActivities = ['count' => 0, 'users' => []];
                } else {
                    $lastActivities['count']++;
                    $lastActivities['users'][$user->getId()] = time();
                }

                $activityCachePool->set($lastActivities);
                $this->cache->save($activityCachePool);
            }
        }
    }

    /**
     * Сохраняем все активности
     *
     * @param array $users
     *
     * @throws \Exception
     */
    private function saveUserActivities(array $users)
    {
        foreach ($users as $userId => $time) {
            if($user = $this->em->getRepository(User::class)->find($userId)) {
                $user->setLastActivityAt(\DateTime::createFromFormat('U', $time));
            }
        }
        $this->em->flush();
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => 'onResponse',
        ];
    }
}