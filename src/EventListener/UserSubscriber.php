<?php

namespace App\EventListener;

use App\Event\UserEvent;
use App\Service\MailSenderQueue;
use App\Service\SmsSenderQueue;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UserSubscriber
 *
 * @package App\EventListener
 */
class UserSubscriber implements EventSubscriberInterface
{
    /**
     * @var MailSenderQueue
     */
    private $mailer;
    /** @var SmsSenderQueue  */
    private $smsSender;

    /** @var ContainerInterface */
    private $container;

    /**
     * UserSubscriber constructor.
     *
     * @param MailSenderQueue    $mailer
     * @param SmsSenderQueue     $smsSender
     * @param ContainerInterface $container
     */
    public function __construct(MailSenderQueue $mailer, SmsSenderQueue $smsSender, ContainerInterface $container)
    {
        $this->mailer    = $mailer;
        $this->smsSender = $smsSender;
        $this->container = $container;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            UserEvent::NAME_REGISTER       => 'onUserRegister',
            UserEvent::NAME_REGISTER_FINAL => 'onUserRegisterFinal',
            UserEvent::NAME_RECOVER        => 'onUserRecover'
        ];
    }

    /**
     * @param UserEvent $registeredUserEvent
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function onUserRegister(UserEvent $userEvent)
    {
        $user = $userEvent->getUser();
        $this->mailer->sendEmailToUser(
            'Вы успешно прошли регистрацию!',
            $user,
            'security/email.confirmation.html.twig',
            [
                'host'             => $this->container->getParameter('http_domain'),
                'username'         => $user->getUsername(),
                'confirmationCode' => $user->getConfirmationCode(),
            ]
        );

        //$this->smsSender->sendSms($userEvent->getUser(), 'вы успешно зарегистрировались');
    }

    /**
     * @param $userEvent $userEvent
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function onUserRegisterFinal(UserEvent $userEvent)
    {
        $user = $userEvent->getUser();
        $this->mailer->sendEmailToUser(
            'Вы успешно зарегистрировались в университете успеха!',
            $user,
            'security/email.register.final.html.twig',
            [
                'host'     => $this->container->getParameter('http_domain'),
                'username' => $user->getUsername(),
                'siteName' => $this->container->getParameter('site_name')
            ]
        );
    }

    /**
     * @param $userEvent $userEvent
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function onUserRecover(UserEvent $userEvent)
    {
        $user = $userEvent->getUser();
        $this->mailer->sendEmailToUser(
            'Восстановление доступа',
            $user,
            'security/email.recover.html.twig',
            [
                'host'             => $this->container->getParameter('http_domain'),
                'username'         => $user->getUsername(),
                'confirmationCode' => $user->getConfirmationCode(),
            ]
        );
    }
}