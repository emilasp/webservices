<?php

namespace App\EventListener;


use App\Core\Interfaces\IEntityCore;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * Class EntityListener
 *
 * @package App\EventListener
 */
class EntityListener
{
    public function preUpdate(IEntityCore $subject, PreUpdateEventArgs $event)
    {
        /*if ($event->hasChangedField('name')) {
            $updatedName = $event->getNewValue('name'). ' the dog';
            $dog->setName($updatedName);
        }

        if ($event->hasChangedField('age')) {
            $updatedAge = $event->getNewValue('age') % 2;
            $dog->setAge($updatedAge);
        }*/

    }

    /**
     * Gets all the entities to flush
     *
     * @param OnFlushEventArgs $eventArgs Event args
     */
    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        /*$em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        //Insertions
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            # your code here for the inserted entities
        }

        //Updates
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            # your code here for the updated entities
        }

        //Deletions
        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            # your code here for the deleted entities
        }*/
    }
}