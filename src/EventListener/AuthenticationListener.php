<?php

namespace App\EventListener;

use CoursesBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class AuthenticationListener
 *
 * @package App\EventListener
 */
class AuthenticationListener
{
    /** @var ContainerInterface */
    private $container;

    /**
     * AuthenticationListener constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * onAuthenticationFailure
     *
     * @param AuthenticationFailureEvent $event
     *
     */
    public function onAuthenticationFailure(AuthenticationFailureEvent $event)
    {
    }

    /**
     * onAuthenticationSuccess
     *
     * @param InteractiveLoginEvent $event
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onAuthenticationSuccess(InteractiveLoginEvent $event)
    {
        /** @var User $user */
        $user = $event->getAuthenticationToken()->getUser();
        $user->setLastLoginAt(new \DateTime());

        $this->container->get('doctrine.orm.entity_manager')->flush($user);
    }
}