<?php

namespace App\Core\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Поведение для сущностей, хранящих временные метки создания и обновления
 */
trait TimestampableEntityTrait
{
    /**
     * @Gedmo\Mapping\Annotation\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true, options={"default": null})
     */
    private $createdAt;

    /**
     * @Gedmo\Mapping\Annotation\Timestampable(on="change", field={"name"})
     * @ORM\Column(type="datetime", nullable=true, options={"default": null})
     */
    private $updatedAt;

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
