<?php


namespace App\Core\Traits;

use App\Core\Interfaces\IEntityCore;

/**
 * Поведение для сущностей
 */
trait EntityTrait
{
    /** @var IEntityCore */
    public $entityPrev;


    /**
     * Проверяем что Entity новая
     */
    public function isNewRecord(): bool
    {
        return !(bool)$this->getId();
    }
}
