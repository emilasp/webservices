<?php

namespace App\Core\Traits;

use App\Core\Interfaces\IEntityCore;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Добавляем свойство к нужным классам
 */
trait JsonPropertiesTrait
{
    /**
     * При необходимости можно в нужном классе переопределить тип в string
     *
     * @ORM\Column(name="jsonProperties", type="json", nullable=false, options={"default"="{}"})
     *
     * @var string
     */
    protected $jsonProperties = [];

    /**
     * @param $properties
     */
    public function setJsonProperties(array $properties)
    {
        $this->jsonProperties = $properties;
    }

    /**
     * @return mixed
     */
    public function getJsonProperties(): array
    {
        //$this->jsonProperties = is_string($this->jsonProperties) ? json_decode($this->jsonProperties, true) : $this->jsonProperties;
        return $this->jsonProperties ?? [];
    }

    /**
     * @param string $property
     * @param mixed  $value
     */
    public function setJsonProperty(string $property, $value)
    {
        $properties = $this->getJsonProperties();
        PropertyAccess::createPropertyAccessor()->setValue($properties, $property, $value);

        $this->setJsonProperties($properties);
    }

    /**
     * @param string $property
     *
     * @return mixed
     */
    public function getJsonProperty(string $property)
    {
        return PropertyAccess::createPropertyAccessor()->getValue($this->getJsonProperties(), $property);
    }

    /**
     * @param string $property
     */
    public function removeJsonProperty(string $property)
    {
        $this->setJsonProperty($property, null);
    }

    public function hasJsonProperty(string $property) : bool
    {
        return (bool)$this->getJsonProperty($property);
    }
}
