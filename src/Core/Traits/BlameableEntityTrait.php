<?php


namespace App\Core\Traits;

use CoursesBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Поведение для сущностей, хранящих временные метки создания и обновления
 */
trait BlameableEntityTrait
{
    /**
     * @var User $createdBy
     *
     * @Gedmo\Mapping\Annotation\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;

    /**
     * @var User $updatedBy
     *
     * @Gedmo\Mapping\Annotation\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="CoursesBundle\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;

    //    /**
    //     * @var User $contentChangedBy
    //     *
    //     * @Gedmo\Mapping\Annotation\Blameable(on="change", fields={"title", "body"})
    //     * @ORM\ManyToOne(targetEntity="Path\To\Entity\User")
    //     * @ORM\JoinColumn(name="content_changed_by", referencedColumnName="id")
    //     */

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

}
