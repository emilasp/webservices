<?php


namespace App\Core\Traits;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * SEO Поведение для сущностей
 */
trait SeoEntityTrait
{
    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;


    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }
}
