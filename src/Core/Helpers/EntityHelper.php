<?php 

namespace App\Core\Helpers;

use Doctrine\ORM\Mapping\Entity;

/**
 * Методы для работы с сущностями и их коллекциями
 */
class EntityHelper
{
    /**
     * Возвращает массив id сущностей
     *
     * @return int[] 
     */
    public static function getEntityIds(iterable $entities): array
    {
        $results = [];
        foreach ($entities as $entity) {
            $results[] = $entity->getId();
        }

        return $results;
    }

    /**
     * Возвращает массив вида [id => объект] где индексом является
     * id сущности.
     */ 
    public static function getEntitiesHashed(iterable $entities): array 
    {
        $hashed = [];
        foreach ($entities as $entity) {
            $hashed[$entity->getId()] = $entity;
        }

        return $hashed;
    }

    /**
     * Возвращает массив вида [id => объект] где индексом является
     * id сущности.
     */
    public static function getItemsForSelect(iterable $entities, string $idColumn = 'id', string $nameColumn = 'name'): array
    {
        $getterId = 'get' . ucfirst($idColumn);
        $getterName = 'get' . ucfirst($nameColumn);
        $hashed = [];
        foreach ($entities as $entity) {
            $hashed[$entity->{$getterId}()] = $entity->{$getterName}();
        }
        return $hashed;
    }


    /**
     * Получаем значение по пути
     *
     * @param Entity $entity
     * @param string $path
     *
     * @return mixed
     */
    public static function getValue(Entity $entity, string $path)
    {
        $pathData = explode('.', $path);

        $value = $entity;
        foreach ($pathData as $field) {
            $methodName = 'get' . ucfirst($field);
            if ($value) {
                $value = $value->{$methodName}();
            }
        }
        return is_object($value) ? '' : $value;
    }

    /**
     * Возвращает массив вида [oid => true], где oid - идентификатор, которое
     * возвращает spl_object_hash().
     */ 
    public static function getEntityOidsSet(iterable $entities): array 
    {
        $results = [];
        foreach ($entities as $entity) {
            $results[spl_object_hash($entity)] = true;
        }

        return $results;
    }

    /**
     * Задает индексом значение объектов в массиве
     *
     * @param array $entities
     * @param string $field
     *
     * @return array
     */
    public static function setFieldAsKey(array $entities, string $field = 'id') : array
    {
        $method = 'get' . ucfirst($field);
        return (array) array_combine(array_map(function($entity) use ($method) {
            return $entity->{$method}();
        }, $entities), $entities);
    }

    /**
     * Для списка МАССИВОВ делает переданное поле ключами массива с группировкой по ключу
     *
     * @param array $entities ассоциативный массив
     * @param string $field ключ по которому будем группировать
     * @param string|null $columnAsValue ключ значения для которого будем выбирать в итоговый массив
     *
     * @return array
     */
    public static function setFieldAsKeyWithGroup(array $entities, string $field = 'id', string $columnAsValue = null) : array
    {
        $method = 'get' . ucfirst($field);
        $methodCAV = $columnAsValue ? 'get' . ucfirst($columnAsValue) : null;
        $result = [];
        array_walk($entities, function($entity) use (&$result, $field, $methodCAV, $method) {
            $result[$entity->{$method}()][] = $methodCAV ? $entity->{$methodCAV}() : $entity;
        });
        return $result;
    }


    /**
     * Возвращает список, в котором оставлены только сущности с 
     * указанными в массиве id. Ключи списка не сохраняются.
     */ 
    public static function filterById(iterable $entities, array $ids): array
    {
        $hash = array_fill_keys($ids, true);
        $result = [];
        foreach ($entities as $entity) {
            if (isset($hash[$entity->getId()])) {
                $result[] = $entity;
            }
        }

        return $result;
    }
}