<?php

namespace App\Core\Helpers;

use Doctrine\Common\Collections\Collection;

/**
 * Дополнительные инструменты для работы с массивами
 */
final class ArrayHelper
{
    /**
     * Sort the array by the specified list
     *
     * @param array $a
     * @param array $order
     *
     * @return array
     */
    public static function sort(array $a, array $order) : array
    {
        $ret = [];

        foreach ($order as $key) {
            if (array_key_exists($key, $a)) {
                $ret[] = $a[$key];
                unset($a[$key]);
            }
        }

        return array_merge($ret, array_values($a));
    }

    public static function list(array $assoc, bool $empty = false) : array
    {
        return array_values($empty ? $assoc : array_filter($assoc));
    }

    public static function merge(array &$target, $merge)
    {
        array_splice($target, count($target), 0, (array) $merge);
    }

    public static function combine(array $list) : array
    {
        return array_combine($list, $list);
    }

    public static function replaceKeys(array $src, array $map) : array
    {
        foreach ($src as $key => $value) {
            unset($src[$key]);
            $src[$map[$key]] = $value;
        }

        return $src;
    }

    public static function sum($collection, \Closure $handler) : int
    {
        $collection = $collection instanceof Collection
            ? $collection->map($handler)->toArray()
            : array_map($handler, (array) $collection);

        return ceil(array_sum($collection));
    }

    /**
     * Преобразует переданное значение в список
     *
     * @param mixed    $value
     * @param callable $map       [optional]
     * @param string   $delimiter [optional]
     *
     * @return array
     */
    public static function toList($value, $map = 'intval', string $delimiter = ',') : array
    {
        if (!is_array($value)) {
            $value = explode($delimiter, $value);
        }

        return array_filter(array_map($map, $value));
    }

    /**
     * Для списка МАССИВОВ делает переданное поле ключами массива
     *
     * @param array $entities
     * @param string $field
     *
     * @return array
     */
    public static function setFieldAsKey(array $entities, string $field = 'id') : array
    {
        return (array) array_combine(array_map(function(array $entity) use ($field) {
            return $entity[$field];
        }, $entities), $entities);
    }

    /**
     * Для списка МАССИВОВ делает переданное поле ключами массива с группировкой по ключу
     *
     * @param array $entities ассоциативный массив
     * @param string $field ключ по которому будем группировать
     * @param string|null $columnAsValue ключ значения для которого будем выбирать в итоговый массив
     *
     * @return array
     */
    public static function setFieldAsKeyWithGroup(array $entities, string $field = 'id', string $columnAsValue = null) : array
    {
        $result = [];
        array_walk($entities, function($item) use (&$result, $field, $columnAsValue) {
            $result[$item[$field]][] = $columnAsValue ? $item[$columnAsValue] : $item;
        });
        return $result;
    }


    /**
     * Разбирает строку и извлекает из нее массив целых чисел, разделенных
     * запятыми. Не-числа игнорируются.
     *
     * Пример: 
     * 
     * - "12,,abc,34"   => [12,34]
     * - "abcdef"       => []
     * 
     * @return int[]
     */
    public static function parseNumberList(string $str): array
    {
        $parts = explode(',', $str);
        $numbers = [];

        foreach ($parts as $part) {
            $part = trim($part);
            if ('' === $part || !ctype_digit($part)) {
                continue;
            }

            $numbers[] = intval($part);
        }

        return $numbers;
    }

    /**
     * Разбивает строку на массив подстрок по запятой. Лишние пробелы
     * с краев подстрок обрезаются, пустые значения пропускаются.
     *
     * Пример: "12,, abc, 34 " => ['12', 'abc', '34']
     *
     * @return string[]
     */
    public static function parseStringList(string $str): array
    {
        $parts = explode(',', $str);
        $parts = [];

        foreach ($parts as $part) {
            $part = trim($part);
            if ('' === $part) {
                continue;
            }

            $parts[] = $part;
        }

        return $parts;
    }

    /**
     * Сортируем массив объектов/массивов по массиву значений(для свойства или колонки через callable)
     *
     * Пример:
     * $array        = [{id:1}, {id:2}, {id:3}, {id:4}, {id:5}]
     * $sortValues   = [5, 3, 1, 2, 4]
     * $getItemValue = function ($item) {return $item->id;}
     *
     * Результат:      [{id:5}, {id:3}, {id:1}, {id:2}, {id:4}]
     *
     *
     * @param array    $array
     * @param array    $sortValues
     * @param callable $getItemValue
     *
     * @return array
     */
    public static function sortArrayByFieldValues(array $array, array $sortValues, callable $getItemValue): array
    {
        $resultSorted = array_map(function ($val) { return null;}, array_flip($sortValues));

        foreach ($array as $item) {
            $resultSorted[$getItemValue($item)] = $item;
        }

        return array_filter($resultSorted, function ($value) {return (bool)$value;});
    }
}
