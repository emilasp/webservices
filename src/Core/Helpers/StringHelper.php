<?php


namespace App\Core\Helpers;

/**
 * Class StringHelper
 *
 * @package App\Core\Helpers
 */
class StringHelper
{
    /**
     * Переаодим CamalCase в строку с разделителем
     *
     * @param string $string
     * @param string $separator
     *
     * @return string
     */
    public static function deCamelCase(string $string, string $separator = '_'): string
    {
        return strtolower(join(preg_split('/(?<=[a-z])(?=[A-Z])/x', $string), $separator));
    }

    /**
     * Приводим телефон к стандарту - 10 цифр
     *
     * @param string $phone
     * @param string $prefix
     *
     * @return string
     */
    public static function getPurifiedPhone(string $phone, string $prefix = ''): string
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);
        return $prefix . substr($phone, -10, 10);
    }

    /**
     * Форматируем в человеконятный вид размер
     *
     * @param $bytes
     *
     * @return string
     */
    public static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }
        return $bytes;
    }
}