<?php

namespace App\Core\Helpers;

use Doctrine\Common\Collections\Collection;

/**
 * Дополнительные инструменты для работы с массивами
 */
final class FileHelper
{

    /**
     * Выбираем данные из CSV файла
     *
     * @param string   $path
     * @param int|null $columnAsIndex
     *
     * @return array
     */
    public static function getCsvFileData(string $path, int $columnAsIndex = null, string $delimeter = ","): array
    {
        $data   = [];
        $handle = fopen($path, "r");
        while (($dataRow = fgetcsv($handle, 1000, $delimeter)) !== false) {
            if ($columnAsIndex !== null) {
                $data[$dataRow[$columnAsIndex]] = $dataRow;
            } else {
                $data[] = $dataRow;
            }
        }
        fclose($handle);

        return $data;
    }

    /**
     * Для чтения больших файлов
     *
     * @param $file
     * @return \Generator
     */
    public static function getRowsFromFileGenerator($file)
    {
        $handle = fopen($file, 'rb');
        while (!feof($handle)) {
            yield fgets($handle, 10000000);
        }
        fclose($handle);
    }

    /**
     * Записываем строки в конец файла
     *
     * @param string $fileName
     * @param array  $rows
     */
    public static function addRowsInFile(string $fileName, array $rows)
    {
        $fp = fopen($fileName, 'ax');
        foreach ($rows as $row) {
            fwrite($fp, $row . PHP_EOL);
        }
        fclose($fp);
    }

    function addRowsInFilePrepend(string $filename, string $string) {
        $context = stream_context_create();
        $origFile = fopen($filename, 'ar', 1, $context);

        $tmpFilename = tempnam(sys_get_temp_dir(), 'php_prepend_');
        file_put_contents($tmpFilename, $string);
        file_put_contents($tmpFilename, $origFile, FILE_APPEND);

        fclose($origFile);
        unlink($filename);
        rename($tmpFilename, $filename);
    }

    /**
     * Выбираем данные из файла
     *
     * @param string   $path
     * @param int|null $columnAsIndex
     *
     * @return array
     */
    public static function getRowsFromFile(string $path): array
    {
        $data   = [];
        $handle = fopen($path, "r");
        while (!feof($handle)) {
            $data[] = fgets($handle, 10000000);
        }
        fclose($handle);

        return $data;
    }

    /**
     * Save array to csv file
     *
     * @param string $path
     * @param array  $data
     *
     * @throws \Exception
     */
    public static function saveDataToCsv(string $path, array $data, $deliveter = "\t")
    {
        if ($data) {
            $csvFileReport = fopen($path, 'w');
            fputcsv($csvFileReport, array_keys($data[0]), $deliveter);
            foreach ($data as $row) {
                fputcsv($csvFileReport, array_values($row), $deliveter);
            }
            fclose($csvFileReport);
        }
    }
}
