<?php


namespace App\Core\Helpers;

/**
 * Class DateHelper
 *
 * @package App\Core\Helpers
 */
class DateHelper
{
    /**
     * Получаем массив дат по перодам мужду двумя датами
     *
     * @param \DateTime $from
     * @param \DateTime $to
     * @param string    $intervalSpec
     *
     * @return array|\DateTime[]
     * @throws \Exception
     */
    public static function getDatesFromRange(\DateTime $from, \DateTime $to, string $intervalSpec = 'P1D', bool $removeLastOver = true): array
    {
        $dates    = [];
        $interval = new \DateInterval($intervalSpec);
        $period   = new \DatePeriod($from, $interval, $to->add($interval));

        foreach ($period as $key => $value) {
            $dates[] = $value;
        }

        if ($dates && $removeLastOver) {
            unset($dates[count($dates)-1]);
        }

        return $dates;
    }
}