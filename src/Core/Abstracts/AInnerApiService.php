<?php


namespace App\Core\Abstracts;


use App\Core\Interfaces\IEntityCore;
use Doctrine\ORM\EntityManagerInterface;
use FileBundle\Service\FileService;

/**
 * Class AInnerApiService
 *
 * @package CoursesBundle\Service
 */
class AInnerApiService
{
    /** @var EntityManagerInterface */
    protected $em;
    /** @var FileService */
    protected $fileService;

    public function __construct(EntityManagerInterface $em, FileService $fileService) {
        $this->em                = $em;
        $this->fileService       = $fileService;
    }

    /**
     * Получаем изобраения для Entity
     *
     * @param IEntityCore $entity
     * @param string      $imageAttribute
     * @param array       $formats
     *
     * @return array
     */
    protected function getEntityImages(IEntityCore $entity, string $imageAttribute, array $formats): ?array
    {
        $relationName = 'get' . ucfirst($imageAttribute);
        if ($file = $entity->{$relationName}()) {
            return $this->fileService->getImageFormatsUrls($file, $formats);
        }
        return null;
    }
}