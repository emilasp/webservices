<?php

namespace App\Core\Abstracts;

use Symfony\Component\HttpClient\HttpClient;

/**
 * Базовый класс для сервисов, работающих со сторонними API
 *
 * Class AExternalApiService
 *
 * @package App\Core\Abstracts
 */
abstract class AExternalApiService
{
    /**
     * @var HttpClient
     */
    protected $httpClient;


    public function __construct()
    {
        $this->httpClient = HttpClient::create();
    }


    /**
     * Поулчаем полный url с параметрами
     *
     * @param string $action
     * @param array $params
     *
     * @return string
     */
    protected function getHttpMethodUrl(string $action, array $params = []): string
    {
        $url = static::URL_BASE . DIRECTORY_SEPARATOR . $action;

        if ($params) {
            $url .= '?' . http_build_query($params);
        }
        return $url;
    }
}
