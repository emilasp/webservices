<?php


namespace App\Core\Abstracts;


use App\Core\Helpers\StringHelper;
use Doctrine\ORM\EntityManagerInterface;
use FileBundle\Service\FileService;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Добавляет функционал экшенов для консольной команды
 *
 * Class ACommandActions
 *
 * @package App\Core\Abstracts
 */
abstract class ACommandActions extends Command
{
    use LockableTrait;

    public const COLOR_BLACK   = 'black';
    public const COLOR_RED     = 'red';
    public const COLOR_GREEN   = 'green';
    public const COLOR_YELLOW  = 'yellow';
    public const COLOR_BLUE    = 'blue';
    public const COLOR_MAGENTA = 'magenta';
    public const COLOR_CYAN    = 'cyan';
    public const COLOR_WHITE   = 'white';
    public const COLOR_DEFAULT = 'default';

    const ACTION_ARG_NAME = 'function';

    /** @var EntityManagerInterface */
    protected $em;
    /** @var KernelInterface */
    protected $kernel;
    /** @var Filesystem */
    protected $fileSystem;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;
    /** @var ContainerInterface */
    protected $container;

    /** @var OutputInterface */
    protected $output;
    /** @var InputInterface */
    protected $input;

    private $startTime;
    private $startMemory;

    /** @var string */
    private $logFilePath;


    /**
     * ACommandActions constructor.
     *
     * @param string|null            $name
     * @param EntityManagerInterface $em
     */
    public function __construct(
        string $name = null,
        EntityManagerInterface $em = null,
        EventDispatcherInterface $eventDispatcher = null,
        Filesystem $fileSystem = null,
        KernelInterface $kernel = null,
        ContainerInterface $container = null
    ) {
        parent::__construct($name);

        $this->em              = $em;
        $this->eventDispatcher = $eventDispatcher;
        $this->kernel          = $kernel;
        $this->fileSystem      = $fileSystem;
        $this->container       = $container;
    }

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $this->output = $output;
        $class        = explode('\\', get_class($this));

        $this->logFilePath = implode(DIRECTORY_SEPARATOR, [
            $this->kernel->getLogDir(),
            'cli',
            end($class),
            date('Y.m.d') . '.log',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $this->addArgument(
            static::ACTION_ARG_NAME,
            InputArgument::REQUIRED,
            "Функция (подкоманда), которую надо выполнить. Функция xxx вызывает метод xxxAction()"
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            //return 0;
        }

        $action = $input->getArgument(static::ACTION_ARG_NAME);
        if (!$action) {
            throw new \InvalidArgumentException(sprintf('You must specify "%s" parameter', static::ACTION_ARG_NAME));
        }

        if (!method_exists($this, $action .= 'Action')) {
            throw new \RuntimeException("Method $action not implemented");
        }

        $this->output = $output;
        $this->input  = $input;

        $this->onStart();
        $result = $this->$action(...func_get_args());
        $this->onEnd();

        return $result;
    }

    /**
     * @param string $text
     * @param string $color - black, red, green, yellow, blue, magenta, cyan, white
     */
    protected function display(string $text, string $color = 'white')
    {
        $this->output->writeln('<comment>[' . date("H:i:s") . "]</comment> <fg={$color}>" . $text . '</>');

        if ($this->logFilePath) {
            $date = date("d.m.y H:i:s");
            $this->fileSystem->appendToFile($this->logFilePath, "[{$date}]: {$text}" . PHP_EOL);
        }
    }

    /**
     * Cli start
     */
    private function onStart()
    {
        $this->startTime   = microtime(true);
        $this->startMemory = memory_get_usage();

        $this->display("-----Cli START-----");
    }

    /**
     * Cli end
     */
    private function onEnd()
    {
        $time   = round(microtime(true) - $this->startTime, 4);
        $memory = StringHelper::formatSizeUnits(memory_get_usage() - $this->startMemory);

        $this->display("-----SCRIPT FINISHED-----");
        $this->display("Time  : {$time} sec");
        $this->display("Memory: {$memory}");
    }
}