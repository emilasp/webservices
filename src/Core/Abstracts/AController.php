<?php


namespace App\Core\Abstracts;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Базовый класс контроллера
 *
 * Class AController
 *
 * @package App\Core\Abstracts
 */
abstract class AController extends AbstractController
{
    /** @var Breadcrumbs */
    protected $breadcrumbs;

    /** @var EntityManagerInterface */
    protected $em;

    /**
     * AFrontController constructor.
     *
     * @param Breadcrumbs $breadcrumbs
     */
    public function __construct(Breadcrumbs $breadcrumbs)
    {
        $this->breadcrumbs = $breadcrumbs;
    }


    /**
     * Добавляем хлебные крошки
     *
     * @param array $items
     */
    protected function addBreadcrumbs(array $items): void
    {
        foreach ($items as $name => $item) {
            if (is_numeric($name)) {
                $this->breadcrumbs->addItem($item);
            } else {
                if (is_array($item)) {
                    $this->breadcrumbs->addItem($name, $this->get("router")->generate($item[0], $item[1]));
                }else {
                    $this->breadcrumbs->addItem($name, $this->get("router")->generate($item));
                }

            }
        }
    }

    /**
     * Отдаем EntityManager
     *
     * @return EntityManagerInterface
     */
    protected function getEm(): EntityManagerInterface
    {
        if (!$this->em) {
            $this->em = $this->getDoctrine()->getManager();
        }
        return $this->em;
    }
}