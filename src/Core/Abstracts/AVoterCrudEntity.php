<?php


namespace App\Core\Abstracts;

use App\Core\Interfaces\IEntityCore;
use CoursesBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

/**
 * Базовый класс виджета
 *
 * Class AWidget
 *
 * @package App\Core\Abstracts
 */
abstract class AVoterCrudEntity extends Voter
{
    public const VIEW   = 'view';
    public const CREATE = 'create';
    public const EDIT   = 'edit';
    public const OWNER  = 'owner';

    protected const ATTRIBUTES = [
        self::VIEW,
        self::CREATE,
        self::EDIT,
        self::OWNER,
    ];

    /** @var Security */
    protected $security;

    protected $entityClass = IEntityCore::class;

    /**
     * CourseVoter constructor.
     *
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;

        $this->entityClass = static::getEntityClass();
    }

    abstract protected function getEntityClass(): string;


    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        return (!$subject && $attribute === self::CREATE) || $subject instanceof $this->entityClass && in_array($attribute, self::ATTRIBUTES);
    }

    /**
     * @param string         $attribute
     * @param IEntityCore    $subject
     * @param TokenInterface $t
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            return true;
        }

        /** @var User $user */
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::OWNER:
                return $this->isOwner($user, $subject);
            case self::VIEW:
                return $this->canView($subject, $user);
            case self::CREATE:
                return $this->canCreate($subject, $user);
            case self::EDIT:
                return $this->canEdit($subject, $user);
        }


        throw new \LogicException('Invalid attribute: ' . $attribute);
    }

    /**
     * @param IEntityCore $post
     * @param User   $user
     *
     * @return bool
     */
    protected function canView(IEntityCore $subject, User $user)
    {
        if ($this->isOwner($user, $subject)) {
            return true;
        }
        return $subject->getStatus() === $this->entityClass::STATUS_ACTIVE;
    }

    /**
     * @param IEntityCore $subject
     * @param User   $user
     *
     * @return bool
     */
    protected function canEdit(IEntityCore $subject, User $user)
    {
        return $this->isOwner($user, $subject);
    }

    /**
     * @param IEntityCore $subject
     * @param User   $user
     *
     * @return bool
     */
    protected function canCreate(IEntityCore $subject, User $user)
    {
        return $this->security->isGranted('ROLE_COURSE_ADMIN');
    }


    /**
     * @param User|null $user
     * @param IEntityCore    $course
     *
     * @return bool
     */
    protected function isOwner(?User $user, IEntityCore $subject)
    {
        if (!$user || !$subject->getCreatedBy()) {
            return false;
        }

        return $user->getId() === $subject->getCreatedBy()->getId();
    }
}