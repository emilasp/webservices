<?php


namespace App\Core\Abstracts;

/**
 * Базовый класс сообщения в очередь
 *
 * Class AQueueMessage
 *
 * @package App\Core\Abstracts
 */
class AQueueMessage implements \Serializable
{
    /**
     * @return false|string
     */
    public function serialize()
    {
        return json_encode(get_object_vars($this));
    }

    /**
     * @param string $serialized
     *
     * @return $this|void
     */
    public function unserialize($serialized)
    {
        $data = json_decode($serialized, true);

        foreach ($data as $propery => $value) {
            if (property_exists($this, $propery)) {
                $this->{$propery} = $value;
            }
        }

        return $this;
    }
}